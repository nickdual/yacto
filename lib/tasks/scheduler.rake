desc "This task is called by the Heroku scheduler add-on"
#task :geoname_data => :environment do
# Rake::Task['db:geoname_data'].invoke
#end
#task :api_handollar_data => :environment do
#   Rake::Task['db:api_handollar_data'].invoke
#end
task :geoname_timezone => :environment do
   Rake::Task['db:geoname_timezone'].invoke
end
#task :api_8coupons_data => :environment do
#  Rake::Task['db:api_8coupons_data'].invoke
#end
task :delete_expire_day_offers => :environment do
  Rake::Task['db:delete_expire_day_offers'].invoke
end
task :deactive_offers => :environment do
  Rake::Task['db:deactive_offers'].invoke
end
task :update_offer_city => :environment do
  Rake::Task['db:update_offer_city'].invoke
end
task :api_8coupons_data => :environment do
  Rake::Task['db:api_8coupons_data'].invoke
end
task :update_merchant_city => :environment do
    Rake::Task['db:update_merchant_city'].invoke
end
task :api_handollar_data => :environment do
    ActiveSupport::Deprecation.silenced = true
    Rake::Task['db:api_handollar_data'].invoke
end
task :api_vouchercodes_data_second => :environment do
   Rake::Task['db:api_vouchercodes_data_second'].invoke
end
task :api_gotime_data_1 => :environment do
  Rake::Task['db:api_gotime_data_1'].invoke
end
task :api_gotime_data_2 => :environment do
  Rake::Task['db:api_gotime_data_2'].invoke
end
task :api_gotime_data_3 => :environment do
  Rake::Task['db:api_gotime_data_3'].invoke
end
task :api_gotime_data_4 => :environment do
  Rake::Task['db:api_gotime_data_4'].invoke
end
task :delete_gotime => :environment do
  Rake::Task['db:delete_gotime'].invoke
end
task :update_closed_merchant => :environment do
  Rake::Task['db:update_closed_merchant'].invoke
end
task :add_new_factual_data => :environment do
  Rake::Task['db:add_new_factual_data'].invoke
end
task :delete_old_users => :environment do
  Rake::Task['db:delete_old_users'].invoke
end
task :delete_old_merchants => :environment do
  Rake::Task['db:delete_old_merchants'].invoke
end
task :api_sqoot_data => :environment do
  Rake::Task['db:api_sqoot_data'].invoke
end
task :update_merchant_type_offer => :environment do
  Rake::Task['db:update_merchant_type_offer'].invoke
end
task :api_vouchercodes_data_1 => :environment do
  Rake::Task['db:api_vouchercodes_data_1'].invoke
end
task :api_vouchercodes_data_2 => :environment do
  Rake::Task['db:api_vouchercodes_data_2'].invoke
end
task :api_vouchercodes_data_3 => :environment do
  Rake::Task['db:api_vouchercodes_data_3'].invoke
end
task :api_vouchercodes_data_4 => :environment do
  Rake::Task['db:api_vouchercodes_data_4'].invoke
end
