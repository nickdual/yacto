desc "This task is called by the Heroku cron add-on"
task :cron => :environment do
  if Time.now.hour == 6 # run at midnight
    Rake::Task['db:api_handollar_data'].invoke
  end
  if Time.now.hour == 8 # run at midnight
    Rake::Task['db:geoname_data'].invoke
  end
  if Time.now.hour % 6 == 4 # run every four hours
    puts "Updating feed..."
    Rake::Task['db:geoname_timezone'].invoke
    puts "done."
  end

  if Time.now.hour == 0 # run at midnight
    Rake::Task['db:api_8coupons_data'].invoke
  end

end