require 'geonames'
require 'geokit'
require 'nokogiri'
require 'open-uri'
require 'net/http'
require 'date'
require 'timeout'
namespace :db do
  task :delete_no_city_merchant => :environment do
    Merchant.where("city_id is null").destroy_all
  end
  task :delete_no_city_offer => :environment do
    Offer.where("city_id is null").destroy_all
  end
  task :delete_default_merchant_type_merchant => :environment do
    Merchant.where("merchant_type_id=1").destroy_all
  end
  task :delete_cities => :environment do
    count = 0
    conn = ActiveRecord::Base.connection
    sql = "Select distinct t1.id FROM cities t1, cities t2 WHERE t1.id > t2.id AND t1.name = t2.name and t1.country_iso_code_two_letters = t2.country_iso_code_two_letters"
    res = conn.execute sql
    res.each do |row|
      begin
        City.find(row["id"].to_i).delete
        count =count + Offer.where("city_id = ?", row["id"].to_i).count
        Offer.where("city_id = ?", row["id"].to_i).destroy_all
        Merchant.where("city_id = ?", row["id"].to_i).destroy_all
      rescue
      end
    end
    puts count
  end
  task :delete_gotime => :environment do
    merchants = Merchant.where("third_party_code = ?", 'gotime').limit(1000)
    merchants.destroy_all
  end
  task :delete_duplicate_merchants => :environment do
    conn = ActiveRecord::Base.connection
    sql = "Select t1.id as id_1 ,t2.id as id_2, t1.city_id as city_id_1 ,t2.city_id as city_id_2 FROM merchants t1, merchants t2 WHERE t1.id > t2.id and t1.phone = t2.phone and t1.name = t2.name"
    res = conn.execute sql
    number = 0
    res.each do |row|
      number = number + 1
      begin
        Merchant.find(row["id_2"].to_i).delete
        #country_iso_code_two_letters_1 = City.find(row["city_id_1"].to_i).country_iso_code_two_letters
        #country_iso_code_two_letters_2 = City.find(row["city_id_2"].to_i).country_iso_code_two_letters
        #if country_iso_code_two_letters_1 != 'US' and country_iso_code_two_letters_1 != 'GB'
         # Merchant.find(row["id_1"].to_i).delete
        #end
        #if country_iso_code_two_letters_2 != 'US' and country_iso_code_two_letters_2 != 'GB'
         # Merchant.find(row["id_2"].to_i).delete
        #end
      rescue
      end
    end
    puts number
  end
  task :delete_no_phone_merchants => :environment do

  end
  task :delete_expire_day_offers => :environment do
    cities = City.find(:all, :conditions => ["country_iso_code_two_letters in (?) and timezone is not null", ['US', 'GB']])
    cities.each do |city|
      begin
        tz = TZInfo::Timezone.get(city.timezone)
        now_time = tz.utc_to_local(Time.current)
        Offer.where("local_end_day < ? and city_id = ? and local_end_day is not null", now_time, city.id).destroy_all
      rescue
      end
    end
  end
  task :delete_merchants_offers => :environment do
    offers = Offer.where("merchant_id is not null")
    number = 0
    offers.each do |offer|
      if offer.merchant == nil
        #offer.delete
        puts number
        number = number + 1
      end
    end
  end
  task :delete_no_user_merchants => :environment do
    merchants = Merchant.where("mobile_number is null or mobile_number = ?", '')
    merchants.destroy_all
  end
  task :delete_old_users => :environment do
    users = User.where("created_at <= ?", Time.utc(2012,9,1)).limit(10000)
    users.destroy_all
  end
  task :delete_old_merchants => :environment do
    merchants = Merchant.where("created_at <= ?", Time.utc(2012,9,1)).limit(10000)
    merchants.destroy_all
  end
end