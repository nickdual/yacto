Yacto::Application.routes.draw do

  resources :error_messages

  get 'cities/search' => "cities#search"
  resources :cities

  resources :countries

  devise_for :users, :controllers => {:sessions => "sessions", :registrations => "registrations" ,:omniauth_callbacks => "omniauth_callbacks"} do
    get '/users/auth/:provider' => 'omniauth_callbacks#twitter'
    get '/users/sign_out' => "devise/sessions#destroy"
   
  end
  get "users/session_user"
  get "users/get_session"
  get "offers/dashboard"
  get "start_page/index"
  get "start_page/index3"
  get "start_page/offer"
  get "start_page/page03"
  get "start_page/offer03"
	# create links to static pages
	%w(about_us blog contact_us our_partners terms_and_conditions privacy business_owners how_it_works_merchants how_it_works_consumers).each	do |page|
		get page,  :controller => "static_pages",:action => page
	end

  get "home/ajax_twitter_on"
  get "home/ajax_twitter_off"
  get "home/ajax_gplus_on"
  get "home/ajax_gplus_off"
  get "home/ajax_facebook_on"
  get "home/ajax_facebook_off"
	get "home/about_us"
  get "offers/ajax_favorite"
  get "offers/ajax_unfavorite"
  get "merchants/ajax_favorite"
  get "merchants/ajax_unfavorite"
  get "offers/ajax_lonlat"
  get "offers/require_sign_in"
  post "home/lookup_address"
  post "offers/unique_email"
  post "offers/create_offer"
  post "offers/get_google_places"
  post "offers/get_business_detail"
  post "offers/post_social_network_offer"
  post "offers/post_persional_detail"
  get 'home/autocomplete'
  get 'home/autocomplete_address'
  get "offers/filter_to_offers"
  get 'offers/redirect_to_offers'
  get 'offers/get_more_product'
  get 'offers/filter'
  get "merchants/sms_request"
  get "merchant_types/top_lvl"
  get 'offers/list_detail'
  get "merchants/call"
  get "offers/nearby"
  get "offers/ajax_time"
  get "offers/ajax_distance"
  get "offers/ajax_offer_group"
  get "offers/set_location"
  get "merchants/survey"
  get "merchants/survey_city"
  get "merchants/survey_source"
  post "merchants/search_google_place"
  match "merchants/admin" => "merchants#admin"

  put 'users/generate_code/:id' => "users#generate_code"
  put 'offers/update_repeat_day/:id' => "offers#update_repeat_day"
  put 'offers/update_local_end_day/:id' => "offers#update_local_end_day"
  put 'merchants/update_repeat_merchant/:id' => "merchants#update_repeat_merchant"
  put 'merchants/update_active/:id' => "merchants#update_active"
  put 'offers/update_merchant_type/:id' => 'offers#update_merchant_type'

  put "users/account_setting/:id"  => "users#account_setting"

  match "users/show" => "users#show"
   # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  resources :offers, :offer_insertions, :merchant_types

  resource :location, :only => [:show]
  resources :favorites, :only => [:create, :index, :destroy]

  resource :dashboard, :only => [:show] do
    get "offers", :on => :member
  end

  resource :bb, :only => :show, :controller => "backbone"

  resources :merchants do
    collection do
      post  "search"
      post "sms_request"
     
    end
  end
  match ':merchant_id' => "users#index", :as => :merchant_details
  
  resources :users do
    member do
      get "toggle_offer", "change_recurrence"
      
    end
    collection do
      get "ajax_sub_types"
      post "new_employee"
      post "new_merchant_offer"
    end
  end


  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'home#index'
  get "home/index"
  resources :merchant_photos
  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
