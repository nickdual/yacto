require 'spec_helper'

describe Merchant do
  let (:merchant) { FactoryGirl.build_stubbed(:merchant) }
  describe "#validations" do
    subject { merchant }
    it { should be_valid }
    it { should_not allow_value("a"*141).for(:name) }
    it { should_not allow_value("a"*501).for(:mobile_description) }  
    it { should_not allow_value("a"*31).for(:third_party_code) }  
    it { should_not allow_value("a"*201).for(:why_visit) }  
    it { should_not allow_value("a"*201).for(:best_thing) }  
    it { should_not allow_value("a"*201).for(:unique) }  
    it { should_not allow_value("a"*201).for(:featured_review) }  
  end

  describe "self.merchant_type" do 
    def self.results_hash 
      {
        ['ARTS AND ENTERTAINMENT','ENTERTAINMENT'] => 'Fun',
        ['AUTOMOTIVE','EDUCATION', 'HOME SERVICES','PETS','PROFESSIONAL SERVICES','SERVICES'] => 'Services',
        ['BEAUTY & SPA', 'BEAUTY & SPAS', 'HEALTH & FITNESS'] => 'Look & Feel',
        ['FOOD & DRINK', 'RESTAURANTS', 'RESTAURANT', 'BAKERY'] => 'Eat',
        ['GROUPON'] => 'Default',
        ['NIGHTLIFE'] => 'Drink',
        ['SHOPPING'] => 'Shop',
        ['TRAVEL'] => 'Tourism'
      }
    end

    def self.define_specs
      results_hash.each do |input_values, return_value|
        input_values.each do |input|
          define_spec(input, return_value)
          define_spec(input.downcase, return_value)
          define_spec(input.capitalize, return_value)
        end
      end
    end

    def self.define_spec(input, result)
      it "should return #{result} after call with #{input}" do
        Merchant.merchant_type(input).should == result
      end
    end

    define_specs

    define_spec("Test", "Test")
    define_spec("A"*300, "A"*300)
    define_spec("a"*300, "a"*300)
  end
    # def self.merchant_type(merchant_type)
    # if  ['ARTS AND ENTERTAINMENT','ENTERTAINMENT'].include? merchant_type.strip.upcase
    #   return 'Fun'
    # end
    # if ['AUTOMOTIVE','EDUCATION', 'HOME SERVICES','PETS','PROFESSIONAL SERVICES','SERVICES'].include? merchant_type.strip.upcase
    #   return 'Services'
    # end
    # if ['BEAUTY & SPA', 'BEAUTY & SPAS', 'HEALTH & FITNESS'].include? merchant_type.strip.upcase
  #   if .include? merchant_type.strip.upcase
  #     return 
  #   end
  #   return merchant_type
  # end
end
