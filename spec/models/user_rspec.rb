require 'spec_helper'

describe User do
  before(:each) do
    @user = FactoryGirl.create(:user)
  end

  it "should be valid" do
    @user.should be_valid
  end

  it "should not be valid, first name length exceed maximum limit" do
    @user.first_name = "a"*21
    @user.should_not be_valid
  end

  it "should not be valid, last name length exceed maximum limit" do
    @user.last_name = "a"*21
    @user.should_not be_valid
  end

  it "should not be valid, username length exceed maximum limit" do
    @user.username = "a"*51
    @user.should_not be_valid
  end

  it "should not be valid, username not present" do
    @user.username = ""
    @user.should_not be_valid
  end

  it "should not be valid, mobile_emei length exceed maximum limit" do
    @user.mobile_emei = "a"*31
    @user.should_not be_valid
  end

  it "should not be valid, mobile_authorized length exceed maximum limit" do
    @user.mobile_authorized = "a"*11
    @user.should_not be_valid
  end

  it "should not be valid, mobile handset brand length exceed maximum limit" do
    @user.mobile_handset_brand = "a"*21
    @user.should_not be_valid
  end

  it "should not be valid, password length exceed maximum limit" do
    @user.password = "a"*21
    @user.should_not be_valid
  end

  it "should not be valid, authorization level length exceed maximum limit" do
    @user.authorization_level = "a"*21
    @user.should_not be_valid
  end

  it "should not be valid, email address length exceed maximum limit" do
    @user.email = "a"*51
    @user.should_not be_valid
  end

  it "should not be valid, mobile number not present" do
    @user.mobile_number = ""
    @user.should_not be_valid
  end


  it "should not be valid, username not unique" do
    @user = User.create!(valid_user_hash)
    @user_temp = User.new(valid_user_hash)
    @user_temp.should_not be_valid
  end

  def valid_user_hash
    {
      :first_name           => "test first_name",
      :last_name            => "test last name",
      :username             => "test username",
      :mobile_emei          => "test mobile emei",
      :mobile_authorized    => "mob auth",
      :mobile_handset_brand => "mob handset brand",
      :password             => "password",
      :authorization_level  => "auth level",
      :email                => "test@tester.com",
      :mobile_number        => "123456789"
    }
  end
end
