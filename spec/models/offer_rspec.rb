require 'spec_helper'

describe Offer do
  def offer(options={})
    @offer ||= FactoryGirl.build(:offer, options)
  end
  
  def merchant(options = {}) 
    @merchant ||= FactoryGirl.build(:merchant, options)
  end 
  
  describe "validation" do
    subject {offer}
    it { should be_valid }
    it { should_not allow_value(-21.2).for(:price) }  
    it { should_not allow_value("a"*141).for(:name) }  
    it { should_not allow_value("a"*201).for(:description) }  
    it { should_not allow_value("a"*31).for(:third_party_code) }  
    it { should_not allow_value(31.233).for(:discount) }  
    it { should allow_value(31).for(:discount) }  
    it { should_not allow_value("www.google.com").for(:link) }  
    it { should_not allow_value("www.google.com").for(:image) }  
    it { should_not allow_value("www.google.com").for(:image_thumbnail) }  
    it { should_not allow_value("www.google.com").for(:image_thumbnail_retina) }  
  end
  
  describe "#offer_yacto_id" do
    it "should generate differen yacto_id-s"     
  end
  
  describe "#period" do
    it "should return nil if offer.end_time is less than 1 minute different" do
      start_time = Time.parse("10:55")
      end_time = start_time + 10.minutes
      offer(:end_time =>  end_time)
      offer.period(start_time).should be_nil
    end

    it "should return number in string for argument less than 1 minute after end_time" do 
      start_time = Time.parse("09:59:55")
      end_time = start_time + 10.seconds
      offer(:end_time =>  end_time)
      offer.period(start_time).should == "1"
    end

    #different type of result ?
    it "should return amount of minutes between end_time and argument, for times from the same hour" do
      start_time = Time.parse("12:12:55")
      end_time = start_time + 43.minutes
      offer(:end_time =>  end_time)
      offer.period(start_time).should == 43
    end
  end

  describe "#set_merchant" do
    it "should set merchant_id" do
      merchant(:id => 42)
      offer.set_merchant(merchant)
      offer.merchant_id.should == 42
    end
    
    it "should set city_id" do
      merchant(:city_id => 124)
      offer.set_merchant(merchant)
      offer.city_id.should == 124
    end
    
    it "should set lat" do
      merchant(:lat => 59.69)
      offer.set_merchant(merchant)
      offer.lat.should == 59.69
    end
    
    it "should set lon" do
      merchant(:lon => -13.001)
      offer.set_merchant(merchant)
      offer.lon.should == -13.001
    end
    
    it "should set primary_address" do
      merchant(:primary_address => "221B Baker Street, London")
      offer.set_merchant(merchant)
      offer.primary_address.should == "221B Baker Street, London"
    end
  end
end

