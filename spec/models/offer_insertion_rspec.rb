require 'spec_helper'

describe OfferInsertion do
  before(:each) do
    @offer_insertion = OfferInsertion.new(valid_offer_insertion_hash)
  end

  it "should be valid" do
    @offer_insertion.should be_valid
  end

  it "should not be valid, name length exceed maximum limit" do
    @offer_insertion.name = "a"*141
    @offer_insertion.should_not be_valid
  end


  def valid_offer_insertion_hash
    {
      :name => "test name"
    }
  end
end


