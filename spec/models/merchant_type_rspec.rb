require 'spec_helper'

describe MerchantType do
  before(:each) do
    @merchant_type = MerchantType.new(valid_merchant_type_hash)
  end

  it "should be valid" do
    @merchant_type.should be_valid
  end

  it "should not be valid due to invalid merchant_type" do
    @merchant_type.merchant_type = 'not included in the list'
    @merchant_type.should_not be_valid
  end

  it "should not be valid, merchant_type length exceed maximum limit" do
    @merchant_type.merchant_type = "a"*21
    @merchant_type.should_not be_valid
  end

  it "should not be valid due to invalid sub type" do
    @merchant_type.sub_type = 'not included in the list'
    @merchant_type.should_not be_valid
  end

  it "should not be valid, sub type length exceed maximum limit" do
    @merchant_type.sub_type = "a"*21
    @merchant_type.should_not be_valid
  end

  it "should not be valid due to invalid language" do
    @merchant_type.language = 'not included in the list'
    @merchant_type.should_not be_valid
  end

  it "should not be valid, sub type length exceed maximum limit" do
    @merchant_type.language = "a"*31
    @merchant_type.should_not be_valid
  end

  def valid_merchant_type_hash
    {
      :merchant_type => "Eat",
      :sub_type => "Chinese",
      :language => "en"
    }
  end
end



