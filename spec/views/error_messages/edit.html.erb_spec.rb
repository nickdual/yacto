require 'spec_helper'

describe "error_messages/edit" do
  before(:each) do
    @error_message = assign(:error_message, stub_model(ErrorMessage,
      :message => "MyString"
    ))
  end

  it "renders the edit error_message form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => error_messages_path(@error_message), :method => "post" do
      assert_select "input#error_message_message", :name => "error_message[message]"
    end
  end
end
