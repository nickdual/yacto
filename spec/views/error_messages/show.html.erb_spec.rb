require 'spec_helper'

describe "error_messages/show" do
  before(:each) do
    @error_message = assign(:error_message, stub_model(ErrorMessage,
      :message => "Message"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Message/)
  end
end
