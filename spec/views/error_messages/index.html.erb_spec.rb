require 'spec_helper'

describe "error_messages/index" do
  before(:each) do
    assign(:error_messages, [
      stub_model(ErrorMessage,
        :message => "Message"
      ),
      stub_model(ErrorMessage,
        :message => "Message"
      )
    ])
  end

  it "renders a list of error_messages" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Message".to_s, :count => 2
  end
end
