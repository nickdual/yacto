require 'spec_helper'

describe "error_messages/new" do
  before(:each) do
    assign(:error_message, stub_model(ErrorMessage,
      :message => "MyString"
    ).as_new_record)
  end

  it "renders new error_message form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => error_messages_path, :method => "post" do
      assert_select "input#error_message_message", :name => "error_message[message]"
    end
  end
end
