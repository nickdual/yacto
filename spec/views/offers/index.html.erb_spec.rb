#require 'spec_helper'
#
#describe "offers/index" do
#  before(:each) do
#    assign(:offers, [
#      stub_model(Offer,
#        :name => "Name",
#        :price => 1.5,
#        :value => 1.5,
#        :description => "MyText",
#        :third_party_code => "Third Party Code",
#        :link => "Link",
#        :image => "Image",
#        :image_thumbnail => "Image Thumbnail",
#        :image_thumbnail_retina => "Image Thumbnail Retina",
#        :discount => 1.5,
#        :active => 1,
#        :primary_address => "Primary Address",
#        :detail_views => 2,
#        :third_party_id => "Third Party"
#      ),
#      stub_model(Offer,
#        :name => "Name",
#        :price => 1.5,
#        :value => 1.5,
#        :description => "MyText",
#        :third_party_code => "Third Party Code",
#        :link => "Link",
#        :image => "Image",
#        :image_thumbnail => "Image Thumbnail",
#        :image_thumbnail_retina => "Image Thumbnail Retina",
#        :discount => 1.5,
#        :active => 1,
#        :primary_address => "Primary Address",
#        :detail_views => 2,
#        :third_party_id => "Third Party"
#      )
#    ])
#  end
#
#  it "renders a list of offers" do
#    render
#    # Run the generator again with the --webrat flag if you want to use webrat matchers
#    assert_select "tr>td", :text => "Name".to_s, :count => 2
#    assert_select "tr>td", :text => 1.5.to_s, :count => 2
#    assert_select "tr>td", :text => 1.5.to_s, :count => 2
#    assert_select "tr>td", :text => "MyText".to_s, :count => 2
#    assert_select "tr>td", :text => "Third Party Code".to_s, :count => 2
#    assert_select "tr>td", :text => "Link".to_s, :count => 2
#    assert_select "tr>td", :text => "Image".to_s, :count => 2
#    assert_select "tr>td", :text => "Image Thumbnail".to_s, :count => 2
#    assert_select "tr>td", :text => "Image Thumbnail Retina".to_s, :count => 2
#    assert_select "tr>td", :text => 1.5.to_s, :count => 2
#    assert_select "tr>td", :text => 1.to_s, :count => 2
#    assert_select "tr>td", :text => "Primary Address".to_s, :count => 2
#    assert_select "tr>td", :text => 2.to_s, :count => 2
#    assert_select "tr>td", :text => "Third Party".to_s, :count => 2
#  end
#end
