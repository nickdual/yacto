require 'spec_helper'

describe "offers/new" do
  before(:each) do
    assign(:offer, stub_model(Offer,
      :name => "MyString",
      :price => 1.5,
      :value => 1.5,
      :description => "MyText",
      :link => "MyString",
      :city_id => 12,
      :discount => 1.5,
      :active => 1,
      :primary_address => "MyString",
      :detail_views => 1
    ).as_new_record)
  end

  it "renders new offer form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => offers_path, :method => "post" do
      assert_select "input#offer_name", :name => "offer[name]"
      assert_select "input#offer_price", :name => "offer[price]"
      assert_select "input#offer_value", :name => "offer[value]"
      assert_select "input#offer_discount", :name => "offer[discount]"
      assert_select "input#offer_city_id", :name => "offer[city_id]"
      assert_select "textarea#offer_description", :name => "offer[description]"
      assert_select "input#offer_link", :name => "offer[link]"
      assert_select "input#offer_active", :name => "offer[active]"
      assert_select "input#offer_primary_address", :name => "offer[primary_address]"
      assert_select "input#offer_detail_views", :name => "offer[detail_views]"
    end
  end
end