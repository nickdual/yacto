require 'spec_helper'

describe "offers/show" do
  include Devise::TestHelpers
  
  before(:each) do
    @offer = assign(:offer, stub_model(Offer,
      :name => "Name",
      :price => 1.5,
      :value => 1.5,
      :description => "MyText",
      :third_party_code => "Third Party Code",
      :link => "Link",
      :image => "Image",
      :image_thumbnail => "Image Thumbnail",
      :image_thumbnail_retina => "Image Thumbnail Retina",
      :discount => 1.5,
      :active => 1,
      :primary_address => "Primary Address",
      :detail_views => 2,
      :third_party_id => "Third Party"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/1.5/)
    rendered.should match(/1.5/)
    rendered.should match(/MyText/)
    rendered.should match(/Third Party Code/)
    rendered.should match(/Link/)
    rendered.should match(/Image/)
    rendered.should match(/Image Thumbnail/)
    rendered.should match(/Image Thumbnail Retina/)
    rendered.should match(/1.5/)
    rendered.should match(/1/)
    rendered.should match(/Primary Address/)
    rendered.should match(/2/)
    rendered.should match(/Third Party/)
  end
end
