require 'spec_helper'

describe "offer_insertions/show" do
  before(:each) do
    @offer_insertion = assign(:offer_insertion, stub_model(OfferInsertion,
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
  end
end
