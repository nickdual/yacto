require 'spec_helper'

describe "offer_insertions/index" do
  before(:each) do
    assign(:offer_insertions, [
      stub_model(OfferInsertion,
        :name => "Name"
      ),
      stub_model(OfferInsertion,
        :name => "Name"
      )
    ])
  end

  it "renders a list of offer_insertions" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
