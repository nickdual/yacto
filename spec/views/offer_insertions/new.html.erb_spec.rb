require 'spec_helper'

describe "offer_insertions/new" do
  before(:each) do
    assign(:offer_insertion, stub_model(OfferInsertion,
      :name => "MyString"
    ).as_new_record)
  end

  it "renders new offer_insertion form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => offer_insertions_path, :method => "post" do
      assert_select "input#offer_insertion_name", :name => "offer_insertion[name]"
    end
  end
end
