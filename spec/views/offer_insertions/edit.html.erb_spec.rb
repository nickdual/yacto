require 'spec_helper'

describe "offer_insertions/edit" do
  before(:each) do
    @offer_insertion = assign(:offer_insertion, stub_model(OfferInsertion,
      :name => "MyString"
    ))
  end

  it "renders the edit offer_insertion form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => offer_insertions_path(@offer_insertion), :method => "post" do
      assert_select "input#offer_insertion_name", :name => "offer_insertion[name]"
    end
  end
end
