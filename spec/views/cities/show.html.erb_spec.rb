require 'spec_helper'

describe "cities/show" do
  before(:each) do
    @city = assign(:city, stub_model(City,
      :name => "Name",
      :slug => "Slug",
      :state => "State",
      :longitude => 32.12,
      :latitude => 42.11
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/Slug/)
    rendered.should match(/State/)
    rendered.should match(/32.12/)
    rendered.should match(/42.11/)
  end
end
