require 'spec_helper'

describe "cities/edit" do
  before(:each) do
    @city = assign(:city, stub_model(City,
      :name => "MyString",
      :slug => "MyString",
      :state => "MyString",
      :lon => 1.5,
      :lat => 1.5
    ))
  end

  it "renders the edit city form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => cities_path(@city), :method => "post" do
      assert_select "input#city_name", :name => "city[name]"
      assert_select "input#city_slug", :name => "city[slug]"
      assert_select "input#city_state", :name => "city[state]"
      assert_select "input#city_lon", :name => "city[lon]"
      assert_select "input#city_lat", :name => "city[lat]"
    end
  end
end
