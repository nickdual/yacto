require 'spec_helper'

describe "cities/index" do
  before(:each) do
    assign(:cities, [
      stub_model(City,
        :name => "Name",
        :slug => "Slug",
        :state => "State",
      :longitude => 32.12,
      :latitude => 42.11
      ),
      stub_model(City,
        :name => "Name",
        :slug => "Slug",
        :state => "State",
      :longitude => 13.51,
      :latitude => 43.53
      )
    ])
  end

  it "renders a list of cities" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Slug".to_s, :count => 2
    assert_select "tr>td", :text => "State".to_s, :count => 2
    assert_select "tr>td", :text => 32.12.to_s, :count => 1
    assert_select "tr>td", :text => 42.11.to_s, :count => 1
    assert_select "tr>td", :text => 13.51.to_s, :count => 1
    assert_select "tr>td", :text => 43.53.to_s, :count => 1
  end
end
