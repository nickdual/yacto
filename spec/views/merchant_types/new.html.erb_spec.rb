require 'spec_helper'

describe "merchant_types/new" do
  before(:each) do
    assign(:merchant_type, stub_model(MerchantType,
      :merchant_type => "MyString",
      :sub_type => "MyString",
      :language => "MyString",
      :lookup_item_id => "MyString"
    ).as_new_record)
  end

  it "renders new merchant_type form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => merchant_types_path, :method => "post" do
      assert_select "input#merchant_type_merchant_type", :name => "merchant_type[merchant_type]"
      assert_select "input#merchant_type_sub_type", :name => "merchant_type[sub_type]"
      assert_select "input#merchant_type_language", :name => "merchant_type[language]"
      assert_select "input#merchant_type_lookup_item_id", :name => "merchant_type[lookup_item_id]"
    end
  end
end
