require 'spec_helper'

describe "merchant_types/index" do
  before(:each) do
    assign(:merchant_types, [
      stub_model(MerchantType,
        :merchant_type => "Merchant Type",
        :sub_type => "Sub Type",
        :language => "Language",
        :lookup_item_id => "Lookup Item"
      ),
      stub_model(MerchantType,
        :merchant_type => "Merchant Type",
        :sub_type => "Sub Type",
        :language => "Language",
        :lookup_item_id => "Lookup Item"
      )
    ])
  end

  it "renders a list of merchant_types" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Merchant Type".to_s, :count => 2
    assert_select "tr>td", :text => "Sub Type".to_s, :count => 2
    assert_select "tr>td", :text => "Language".to_s, :count => 2
    assert_select "tr>td", :text => "Lookup Item".to_s, :count => 2
  end
end
