require 'spec_helper'

describe "merchant_types/edit" do
  before(:each) do
    @merchant_type = assign(:merchant_type, stub_model(MerchantType,
      :merchant_type => "MyString",
      :sub_type => "MyString",
      :language => "MyString",
      :lookup_item_id => "MyString"
    ))
  end

  it "renders the edit merchant_type form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => merchant_types_path(@merchant_type), :method => "post" do
      assert_select "input#merchant_type_merchant_type", :name => "merchant_type[merchant_type]"
      assert_select "input#merchant_type_sub_type", :name => "merchant_type[sub_type]"
      assert_select "input#merchant_type_language", :name => "merchant_type[language]"
      assert_select "input#merchant_type_lookup_item_id", :name => "merchant_type[lookup_item_id]"
    end
  end
end
