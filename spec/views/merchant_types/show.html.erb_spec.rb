require 'spec_helper'

describe "merchant_types/show" do
  before(:each) do
    @merchant_type = assign(:merchant_type, stub_model(MerchantType,
      :merchant_type => "Merchant Type",
      :sub_type => "Sub Type",
      :language => "Language",
      :lookup_item_id => "Lookup Item"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Merchant Type/)
    rendered.should match(/Sub Type/)
    rendered.should match(/Language/)
    rendered.should match(/Lookup Item/)
  end
end
