require 'spec_helper'

describe "users/new" do
  before(:each) do
    assign(:user, stub_model(User,
      :first_name => "MyString",
      :last_name => "MyString",
      :merchant_id => 40,
      :username => "MyString",
      :mobile_emei => "MyString",
      :mobile_number => "MyString",
      :mobile_authorized => "MyString",
      :mobile_handset_brand => "MyString",
      :password => "MyString",
      :authorization_level => "MyString",
      :email_address => "MyString"
    ).as_new_record)
  end

  it "renders new user form" do
    pending "Old spec" do
      render

      # Run the generator again with the --webrat flag if you want to use webrat matchers
      assert_select "form", :action => users_path, :method => "post" do
        assert_select "input#user_first_name", :name => "user[first_name]"
        assert_select "input#user_last_name", :name => "user[last_name]"
        assert_select "input#user_username", :name => "user[username]"
        assert_select "input#user_mobile_emei", :name => "user[mobile_emei]"
        assert_select "input#user_mobile_number", :name => "user[mobile_number]"
        assert_select "input#user_mobile_authorized", :name => "user[mobile_authorized]"
        assert_select "input#user_mobile_handset_brand", :name => "user[mobile_handset_brand]"
        assert_select "input#user_password", :name => "user[password]"
        assert_select "input#user_authorization_level", :name => "user[authorization_level]"
        assert_select "input#user_email_address", :name => "user[email_address]"
      end
    end
  end
end
