require 'spec_helper'

describe "users/edit" do
  before(:each) do
    @user = assign(:user, stub_model(User,
      :first_name => "MyString",
      :last_name => "MyString",
      :username => "MyString",
      # :mobile_emei => "MyString",
      :mobile_number => "MyString",
      :mobile_authorized => "MyString",
      :mobile_handset_brand => "MyString",
      :password => "MyString",
      :authorization_level => "MyString",
      # :email_address => "MyString"
    ))
  end

  it "renders the edit user form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => users_path(@user), :method => "post" do
      assert_select "input#user_first_name", :name => "user[first_name]"
      assert_select "input#user_last_name", :name => "user[last_name]"
      assert_select "input#user_username", :name => "user[username]"
      # assert_select "input#user_mobile_emei", :name => "user[mobile_emei]"
      assert_select "input#user_mobile_number", :name => "user[mobile_number]"
      assert_select "input#user_mobile_authorized", :name => "user[mobile_authorized]"
      assert_select "input#user_mobile_handset_brand", :name => "user[mobile_handset_brand]"
      assert_select "input#user_password", :name => "user[password]"
      assert_select "input#user_authorization_level", :name => "user[authorization_level]"
      # assert_select "input#user_email_address", :name => "user[email_address]"
    end
  end
end
