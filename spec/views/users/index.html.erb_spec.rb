require 'spec_helper'

describe "users/index" do
  before(:each) do
    users = [
      stub_model(User,
        :name => "Username",
        :mobile_number => "Mobile Number",
        :email => "Email Address"
      ),
      stub_model(User,
        :name => "Username",
        :mobile_number => "Mobile Number",
        :email => "Email Address"
      )
    ]
    #assign(:users, users)
    assign(:merchant, FactoryGirl.build_stubbed(:merchant_with_type,{
        :users => users, :offers => []
        }))
    assign(:user, FactoryGirl.build_stubbed(:user))
  end

  it "renders a list of users" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Key Business Details/)
  end
end
