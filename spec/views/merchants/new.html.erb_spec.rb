require 'spec_helper'

describe "merchants/new" do
  before(:each) do
    #@merchant = assign(:merchant, FactoryGirl.build_stubbed(:merchant_with_type,
    #  :mobile_description => "MyText",
    #  :primary_address => "MyString"
    #))
    #@merchant = Merchant.new
    assign(:merchant, Merchant.new)
    assign(:map_center,[12.345,-54.321])
  end

  it "renders new merchant form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => merchants_path, :method => "post" do
      assert_select "input#merchant_name", :name => "merchant[name]"
      assert_select "textarea#merchant_mobile_description", :name => "merchant[mobile_description]"
      assert_select "input#merchant_primary_address", :name => "merchant[primary_address]"
      assert_select "input#merchant_postal_code", :name => "merchant[postal_code]"
      assert_select "select#merchant_merchant_type_id", :name => "merchant[merchant_type]"
      assert_select "select#merchant_sub_type", :name => "merchant[sub_type]"
    end
  end
end
