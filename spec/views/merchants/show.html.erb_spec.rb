require 'spec_helper'

describe "merchants/show" do
  before(:each) do
    assign(:merchant, FactoryGirl.build_stubbed(:merchant,
      :name => "Nameeeee",
      :mobile_description => "MyTextttt",
      :primary_address => "Primary Address111",
      :user_rating => 3,
      :merchant_type => FactoryGirl.build_stubbed(:merchant_type, :merchant_type => "Merchant Type 123")
      ) )
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Nameeeee/)
    rendered.should match(/MyTextttt/)
    rendered.should match(/Primary Address111/)
    rendered.should match(/3/)
    rendered.should match(/Merchant Type 123/)
  end
end