require 'spec_helper'

describe "merchants/edit" do
  before(:each) do
    @merchant = assign(:merchant, FactoryGirl.build_stubbed(:merchant_with_type,
      :mobile_description => "MyText",
      :primary_address => "MyString"
    ))
  end

  it "renders the edit merchant form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Editing Merchant/)
  end
end
