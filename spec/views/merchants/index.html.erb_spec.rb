require 'spec_helper'

describe "merchants/index" do
  include Devise::TestHelpers

  before(:each) do
    merchants = [
      stub_model(Merchant,
        :name => "Nameeee",
        :administrative_contact => "AD Contact",
        :phone => "512 123 222",
        :administrative_email => "ad@email.ds.dg",
        :primary_address => "Primary Addressss"
      ),
      stub_model(Merchant,
        :name => "Nameeee",
        :administrative_contact => "AD Contact",
        :phone => "555 312 231",
        :administrative_email => "ad@email.ds.dg",
        :primary_address => "Primary Addressss"
      )
    ]
    @country = FactoryGirl.create(:country)

    merchants.stub!(:total_pages).and_return(1)
    assign(:merchants, merchants)
  end

  it "renders a list of merchants" do
    render
    assert_select "tr>td:nth-child(2)", :text => "Nameeee".to_s, :count => 2
    assert_select "tr>td:nth-child(3)", :text => "AD Contact", :count => 2
    assert_select "tr>td:nth-child(4)", :text => "512 123 222", :count => 1
    assert_select "tr>td:nth-child(4)", :text => "555 312 231", :count => 1
    assert_select "tr>td:nth-child(5)", :text => "ad@email.ds.dg", :count => 2
    assert_select "tr>td:nth-child(6)", :text => "Primary Addressss", :count => 2
  end
end