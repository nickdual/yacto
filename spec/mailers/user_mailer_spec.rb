require "spec_helper"

describe UserMailer do
  describe "confirm_merchant_type" do
    def mail(message)
      UserMailer.confirm_merchant_type(message)
    end

    it "renders the headers" do
      mail = mail("test")
      mail.subject.should eq("Confirm merchant type")
      mail.to.should eq(["abohlig@gmail.com"])
      mail.from.should eq(["abohlig@gmail.com"])
    end

    it "renders the body" do
      mail("test").body.encoded.should match("Hi")
    end
  end

end
