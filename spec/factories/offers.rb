FactoryGirl.define do
  factory :offer do
    name "test name"
    description "test description"
    start_time '09:00'
    end_time '21:00'
    primary_address 'New York, NY, USA'
    lon -81.519421
    lat 41.079671
  end
  factory :offer2, :class => 'Offer' do
    name "offer test"
    description "test test test test"
    start_time '09:00'
    end_time '21:00'
    primary_address '222 S Main St'
    lon -81.520757
    lat 41.079284

  end

end