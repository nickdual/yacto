FactoryGirl.define do
  factory :merchant do
    sub_type "Eat"
    merchant_type_name "Drink"
    name "Dutch Cafe"
    website_url 'test.com.vn'
    primary_address "21 May St"
    mobile_number "123456789"
  end

  factory :merchant2, :class => "Merchant" do
    sub_type "test"
    merchant_type_name "Fun"
    name "Smile girl"
    website_url 'testfun.com.vn'
    primary_address "374 S Main St"
    mobile_number "1234234219"
  end

  factory :merchant_with_type, :parent => :merchant do
    association :merchant_type
  end
end