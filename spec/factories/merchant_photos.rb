FactoryGirl.define do
  factory :merchant_photo do
    photo_file_name 'home.png'
    photo_file_size '8983'
    photo_content_type 'image/png'
    url_medium 'http://s3.amazonaws.com/streetmavens.test/medium/99/golf1.jpg?1345691610'
    url_thumb 'http://s3.amazonaws.com/streetmavens.test/thumb/99/golf1.jpg?1345691610'


  end
  factory :merchant_photo2, class: MerchantPhoto do
    photo_file_name 'home.png'
    photo_file_size '8983'
    photo_content_type 'image/png'
    url_medium 'http://s3.amazonaws.com/streetmavens.test/medium/99/golf1.jpg?1345691610'
    url_thumb 'http://s3.amazonaws.com/streetmavens.test/thumb/99/golf1.jpg?1345691610'


  end
end