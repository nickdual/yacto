FactoryGirl.define do
  factory :country do
    iso_code_two_letter "US"
    iso_code_three_letter "USA"
    iso_number "840"
    name "United States"
    geonames_id '6252001'
  end
end
