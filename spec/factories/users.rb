FactoryGirl.define do
  factory :user do
    mobile_number "555321412"
    first_name "Ted"
    last_name "Johnson"
    password "password"
    password_confirmation "password"
    email "abc748420@gmail.com"
    username "Username"
    mobile_authorized "true"
  end

  factory :user2, :class => "User" do
    mobile_number "6545674we"
    first_name "Peter"
    last_name "Jack"
    password "password"
    password_confirmation "password"
    email "abc123@gmail.com"
    username "usertest"
    mobile_authorized "true"
  end
  factory :invalid_message,:class => User do
    email nil
    user nil
    mobile_number nil
    first_name nil
    last_name nil
  end

end
