FactoryGirl.define do
  factory :city do
    name "test name"
    latitude 12.345
    longitude -54.321
    association :country, factory: :country

  end
end
