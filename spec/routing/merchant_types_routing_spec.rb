require "spec_helper"

describe MerchantTypesController do
  describe "routing" do

    it "routes to #index" do
      get("/merchant_types").should route_to("merchant_types#index")
    end

    it "routes to #new" do
      get("/merchant_types/new").should route_to("merchant_types#new")
    end

    it "routes to #show" do
      get("/merchant_types/1").should route_to("merchant_types#show", :id => "1")
    end

    it "routes to #edit" do
      get("/merchant_types/1/edit").should route_to("merchant_types#edit", :id => "1")
    end

    it "routes to #create" do
      post("/merchant_types").should route_to("merchant_types#create")
    end

    it "routes to #update" do
      put("/merchant_types/1").should route_to("merchant_types#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/merchant_types/1").should route_to("merchant_types#destroy", :id => "1")
    end

  end
end
