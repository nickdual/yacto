require "spec_helper"

describe OfferInsertionsController do
  describe "routing" do

    it "routes to #index" do
      get("/offer_insertions").should route_to("offer_insertions#index")
    end

    it "routes to #new" do
      get("/offer_insertions/new").should route_to("offer_insertions#new")
    end

    it "routes to #show" do
      get("/offer_insertions/1").should route_to("offer_insertions#show", :id => "1")
    end

    it "routes to #edit" do
      get("/offer_insertions/1/edit").should route_to("offer_insertions#edit", :id => "1")
    end

    it "routes to #create" do
      post("/offer_insertions").should route_to("offer_insertions#create")
    end

    it "routes to #update" do
      put("/offer_insertions/1").should route_to("offer_insertions#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/offer_insertions/1").should route_to("offer_insertions#destroy", :id => "1")
    end

  end
end
