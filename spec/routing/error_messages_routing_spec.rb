require "spec_helper"

describe ErrorMessagesController do
  describe "routing" do

    it "routes to #index" do
      get("/error_messages").should route_to("error_messages#index")
    end

    it "routes to #new" do
      get("/error_messages/new").should route_to("error_messages#new")
    end

    it "routes to #show" do
      get("/error_messages/1").should route_to("error_messages#show", :id => "1")
    end

    it "routes to #edit" do
      get("/error_messages/1/edit").should route_to("error_messages#edit", :id => "1")
    end

    it "routes to #create" do
      post("/error_messages").should route_to("error_messages#create")
    end

    it "routes to #update" do
      put("/error_messages/1").should route_to("error_messages#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/error_messages/1").should route_to("error_messages#destroy", :id => "1")
    end

  end
end
