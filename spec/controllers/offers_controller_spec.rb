require 'spec_helper'
#
## This spec was generated by rspec-rails when you ran the scaffold generator.
## It demonstrates how one might use RSpec to specify the controller code that
## was generated by Rails when you ran the scaffold generator.
##
## It assumes that the implementation code is generated by the rails scaffold
## generator.  If you are using any extension libraries to generate different
## controller code, this generated spec may or may not pass.
##
## It only uses APIs available in rails and/or rspec-rails.  There are a number
## of tools you can use to make these specs even more expressive, but we're
## sticking to rails and rspec-rails APIs to keep things simple and stable.
##
## Compared to earlier versions of this generator, there is very limited use of
## stubs and message expectations in this spec.  Stubs are only used when there
## is no simpler way to get a handle on the object needed for the example.
## Message expectations are only used when there is no simpler way to specify
## that an instance is receiving a specific message.
#
describe OffersController do
#
#  # This should return the minimal set of attributes required to create a valid
#  # Offer. As you add validations to Offer, be sure to
#  # update the return value of this method accordingly.
  def valid_attributes
    {
        :name => "test offer",
        :description => "test test",
        :start_time => '06:00',
        :end_time => '23:00'
        #:primary_address => '778 N Main St'
    }
  end
  def invalid_attributes
    {
        :name => "",
        :description => "",
        :start_time => '06:00',
        :end_time => '23:00'
    }
  end


  describe "GET edit" do
    it "assigns the requested offer as @offer" do
      @offer = FactoryGirl.create(:offer)
      get :edit, {:id => @offer.to_param}
      response.should render_template('edit')
    end
  end

#
  describe "PUT update" do        # ==> ok
    describe "with valid params" do

      before(:each) do
        @offer = FactoryGirl.create(:offer)
        #raise @offer.to_yaml
      end
      it "updates the requested offer" do
        Offer.any_instance.should_receive(:update_attributes).with({'these' => 'params'})
        put :update, {:id => @offer.to_param, :offer => {'these' => 'params'}}
      end

      it "update offer as @offer" do
        put :update, {:id => @offer.to_param, :offer => valid_attributes}
        assigns(:offer).should eq(@offer)
      response.should redirect_to(@offer)
      end
      it "update offer as @offer json" do
        put :update, :id => @offer.to_param, :offer => valid_attributes,:format => :json
        assigns(:offer).should eq(@offer)
        response.should be_ok
        response.body.should contain("test offer")
      end
      it "stop offer" do
        put :update, :id => @offer.to_param,:offer => {:active => 2} ,:format => :json
        response.should be_ok
        response.body.should contain(@offer.name)
      end
      it "active offer json" do
        put :update, :id => @offer.to_param,:offer => {:active => 1} ,:format => :json
        response.should be_ok
        response.body.should contain(@offer.name)
      end

    end

    describe "with invalid params" do
        before(:each) do
          @offer = FactoryGirl.create(:offer)
          #raise @offer.to_yaml
        end
      it "assigns the offer as @offer" do
        put :update, {:id => @offer.id, :offer => invalid_attributes, :format => :json}
        response.body.should have_content "{\"name\":[\"The name can't be empty\"]}"
      end

      it "re-renders the 'edit' template" do
        put :update, {:id => @offer.to_param, :offer => invalid_attributes}
        response.should render_template("edit")
      end
    end
  end

  #describe "DELETE destroy" do
  #   before(:each) do
  #        @offer = FactoryGirl.create(:offer)
  #       #raise @offer.to_yaml
  #   end
  #  it "destroys the requested offer" do
  #    delete :destroy, {:id => @offer.to_param,:format => :json}
  #    response.status.should eq(204)
  #  end
  #
  #  it "redirects to the offers list" do
  #
  #    delete :destroy, {:id => @offer.to_param}
  #    response.should redirect_to(offers_url)
  #  end
  #end
#
end
