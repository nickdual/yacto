require 'spec_helper'
describe DashboardsController do
  before(:each) do
    @country = FactoryGirl.create(:country)
    @city = FactoryGirl.create(:city,:country_id => @country.id)
    @merchant_type = FactoryGirl.create(:merchant_type)
    @merchant = FactoryGirl.create(:merchant,:city_id => @city.id,:merchant_type_id=>@merchant_type.id)
    @offer_all = FactoryGirl.create(:offer,:merchant_id => @merchant.id)
  end
  it "filter all " do
    get :offers,{"address"=>"Akron", "distance"=>"0.25", "time"=>"0"},format: "json"
    assigns(:offers).should eq([@offer_all])
  end
end