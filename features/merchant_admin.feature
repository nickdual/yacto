@javascript

Feature: Merchant_admin
  Scenario: I am in Merchant_admin page and i press back button and search merchant
    Given data exits
    Then I go to merchants
    And I fill in "merchant_name" with "Dutch Cafe"
    When I click "Search"


  Scenario: go to page new merchant
    Given data exits
    Given I am on merchants
    When I click "New Merchant"


  Scenario: Go to page offer statistic
    Given data exits
    Given I am on merchants
    When I click "Offer Statistics"


  Scenario: Successful create new user
  Admin want to creat a new user
    Given data exits
    And I am in merchant admin page
    When I press "Add user"
    Then I should see an form
    When I fill in "user_first_name" with "update me"
    And I fill in "user_last_name" with "update me"
    And I fill in "user_email" with "abc123@gmail.com"
    And I fill in "user_mobile_number" with "123454234567"
    And I press button "Save"
##



  Scenario: Successful resend code
    Given data exits
    And I am in merchant admin page
    When I click "resend_code"



  Scenario: Sign in and active offer
    Given data exits
    And I am on home page
    When I press "LogIn"
    And I fill in "signin_email" with "abc748420@gmail.com"
    And I fill in "user_password" with "password"
    And I click "submit_signin"
    Then I redirect to merchant admin page
    And I click "active_offer"



  Scenario: Successful create new offer
  Admin or (who is allowed) want to creat a new offer
    Given data exits
    And I am in merchant admin page
    When I press "New offer"
    And I fill in "offer_name" with "test"
    And I fill in "offer_description" with "test test test"
    And I fill in "offer_start_time" with "09:00"
    And I fill in "offer_end_time" with "21:00"
    And I press button "Save"

  Scenario: Unsuccessful create new offer
    Given data exits
    And I am in merchant admin page
    When I press "New offer"
    And I fill in "offer_name" with ""
    And I fill in "offer_description" with ""
    And I fill in "offer_start_time" with "09:00"
    And I fill in "offer_end_time" with "21:00"
    And I press button "Save"
    Then I should see a error message

  Scenario: Successful copy offer
  Admin or (who is allowed) want to copy offer
    Given data exits
    And I am in merchant admin page
    When I click "copy_offer"




  Scenario: I click to button view my page and go to merchant page
    Given data exits
    And I am in merchant admin page
    When I click "View my page"


  Scenario: Check checkbox to repeat the day offer
    Given data exits
    And I am in merchant admin page
    When I check "monday_1"
    And I check "tuesday_1"
    And I check "wednesday_1"
    And I check "thursday_1"
    And I check "friday_1"
    And I check "saturday_1"
    And I check "sunday_1"
    And I uncheck "monday_1"
    And I uncheck "tuesday_1"
    And I uncheck "wednesday_1"
    And I uncheck "thursday_1"
    And I uncheck "friday_1"
    And I uncheck "saturday_1"
    And I uncheck "sunday_1"


  Scenario: delete user
  I am admin and the user which I want to destroy is not mysefl
    Given data exits
    And I am on home page
    When I press "LogIn"
    And I fill in "signin_email" with "abc748420@gmail.com"
    And I fill in "user_password" with "password"
    And I click "submit_signin"
    Then I redirect to merchant admin page
    When I click "destroy_user"
