class CreateBillingInformations < ActiveRecord::Migration
  def self.up
    create_table :billing_informations do |t|
      t.string :name
      t.string :billing_type
      t.string :number
      t.string :security_code

      t.timestamps
    end
  end

  def self.down
    drop_table :billing_informations
  end
end
