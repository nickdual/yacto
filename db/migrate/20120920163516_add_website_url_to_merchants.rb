class AddWebsiteUrlToMerchants < ActiveRecord::Migration
  def change
    add_column :merchants, :website_url, :string

  end
end
