class AddLanguageToThirdPartyMerchantTypes < ActiveRecord::Migration
  def change
    add_column :third_party_merchant_types, :language, :string

  end
end
