class AddTwitterNameFacebookPageIdToMerchants < ActiveRecord::Migration
  def change
    add_column :merchants, :twitter_name, :string

    add_column :merchants, :facebook_page_id, :string

  end
end
