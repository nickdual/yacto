class RemoveActiveFromMerchants < ActiveRecord::Migration
  def up
    remove_column :merchants, :active
      end

  def down
    add_column :merchants, :active, :integer
  end
end
