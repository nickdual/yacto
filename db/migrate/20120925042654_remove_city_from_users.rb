class RemoveCityFromUsers < ActiveRecord::Migration
  def up
    remove_column :users, :city
      end

  def down
    add_column :users, :city, :string
  end
end
