class AddThirdPartyMerchantTypeIdToMerchants < ActiveRecord::Migration
  def change
    add_column :merchants, :third_party_merchant_type_id, :integer

  end
end
