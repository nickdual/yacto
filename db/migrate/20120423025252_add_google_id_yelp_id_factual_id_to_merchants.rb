class AddGoogleIdYelpIdFactualIdToMerchants < ActiveRecord::Migration
  def change
    add_column :merchants, :google_id, :string

    add_column :merchants, :yelp_id, :string

    add_column :merchants, :factual_id, :string

  end
end
