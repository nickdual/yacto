class CreateUsers < ActiveRecord::Migration
  def self.up
    create_table :users do |t|
      t.references :merchant

      t.string :first_name
      t.string :last_name
      t.string :mobile_emei
      t.string :mobile_number
      t.string :username
      t.string :mobile_authorized
      t.string :mobile_handset_brand
      t.string :password
      t.string :authorization_level
      t.string :fb_auth_data
      t.string :twitter_auth_data
      t.string :twitter_name
      t.boolean :user_admin
      t.integer :role_id
      t.string :facebook_name
      t.string :google_name
      t.string :twitter_secret
      t.string :twitter_token
      t.string :facebook_token
      t.string :google_token
      t.string :timezone
      t.string :timeformat
      t.timestamps
      
    end
  end

  def self.down
    drop_table :users
  end
end
