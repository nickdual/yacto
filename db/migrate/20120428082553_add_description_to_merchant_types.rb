class AddDescriptionToMerchantTypes < ActiveRecord::Migration
  def change
    add_column :merchant_types, :description, :string
  end
end
