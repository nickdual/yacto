class AddYahooToUser < ActiveRecord::Migration
  def change
    add_column :users, :yahoo_name, :string
    add_column :users, :yahoo_token, :string,:limit => 10000
  end
end
