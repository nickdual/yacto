class CreateOfferInsertions < ActiveRecord::Migration
  def self.up
    create_table :offer_insertions do |t|
      t.string :name

      t.timestamps
    end
  end

  def self.down
    drop_table :offer_insertions
  end
end
