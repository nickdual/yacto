class ChangeOfferLinkTypeToText < ActiveRecord::Migration
  def change

     change_column :offers, :link, :text
  end
end
