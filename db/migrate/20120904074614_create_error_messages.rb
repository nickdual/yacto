class CreateErrorMessages < ActiveRecord::Migration
  def change
    create_table :error_messages do |t|
      t.string :message
      t.string :error
      t.timestamps
    end
  end
end
