class AddExpireDayToOffers < ActiveRecord::Migration
  def change
    add_column :offers, :expire_day, :datetime

  end
end
