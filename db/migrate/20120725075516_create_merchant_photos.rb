class CreateMerchantPhotos < ActiveRecord::Migration
  def change
    create_table :merchant_photos do |t|
      t.integer :merchant_id
      t.string :url_medium
      t.string :url_thumb
      t.string :url_original
      t.attachment :photo
    end
  end
end
