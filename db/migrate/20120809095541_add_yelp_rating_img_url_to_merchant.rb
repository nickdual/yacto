class AddYelpRatingImgUrlToMerchant < ActiveRecord::Migration
  def change
    add_column :merchants, :yelp_rating_img_url, :string
  end
end