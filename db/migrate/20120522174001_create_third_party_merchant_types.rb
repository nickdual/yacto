class CreateThirdPartyMerchantTypes < ActiveRecord::Migration
  def change
    create_table :third_party_merchant_types do |t|
      t.string :name

      t.timestamps
    end
  end
end
