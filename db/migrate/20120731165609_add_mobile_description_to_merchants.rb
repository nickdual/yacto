class AddMobileDescriptionToMerchants < ActiveRecord::Migration
  def change
    add_column :merchants, :flickr_id, :string
    add_column :merchants, :instagram_id, :string
  end
end
