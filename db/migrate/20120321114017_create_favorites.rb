class CreateFavorites < ActiveRecord::Migration
  def self.up
    create_table :favorites do |t|
      t.references :merchant
      t.references :offer
      t.references :user
      t.string :name
      t.timestamps
    end
  end

  def self.down
    drop_table :favorites
  end
end
