class AddDaysToOffers < ActiveRecord::Migration
  def change
    add_column :offers, :mon, :boolean ,:default => false
    add_column :offers, :tue, :boolean,:default => false
    add_column :offers, :wed, :boolean,:default => false
    add_column :offers, :thu, :boolean,:default => false
    add_column :offers, :fri, :boolean,:default => false
    add_column :offers, :sat, :boolean,:default => false
    add_column :offers, :sun, :boolean,:default => false
  end
end
