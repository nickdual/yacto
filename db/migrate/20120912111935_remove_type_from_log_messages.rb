class RemoveTypeFromLogMessages < ActiveRecord::Migration
	def up
		rename_column :log_messages, :type, :third_party_provider
	end
	def down
		rename_column :log_messages, :third_party_provider, :type
	end
end