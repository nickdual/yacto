class AddTimeZoneToMerchants < ActiveRecord::Migration
  def change
    add_column :merchants, :time_zone, :string

  end
end
