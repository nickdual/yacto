class CreateOffers < ActiveRecord::Migration
  def self.up
    create_table :offers do |t|
      t.references :merchant
      t.references :city
      t.string :offer_id
      t.string :name
      t.integer :active, :default => 1
      t.string :description
      t.string :primary_address
      t.time :start_time
      t.integer :detail_views
      t.string :third_party_code
      t.string :third_party_id
      t.float :price
      t.float :value
      t.float :discount
      t.string :link
      t.string :image
      t.string :image_thumbnail
      t.string :image_thumbnail_retina
      t.string :offer_duration
      t.string :recurrence
      t.float  :lon 
      t.float  :lat
      t.time :end_time
      t.string :currency
      t.string :timezone
      t.string :source
      t.integer :offer_guid
      t.string  :city_name
      t.integer :user_id
      t.integer :merchant_id
      t.timestamps
    end
  end

  def self.down
    drop_table :offers
  end
end
