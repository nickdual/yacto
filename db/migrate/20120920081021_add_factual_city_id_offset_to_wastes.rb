class AddFactualCityIdOffsetToWastes < ActiveRecord::Migration
  def change
    add_column :wastes, :factual_city_id_offset, :integer

  end
end
