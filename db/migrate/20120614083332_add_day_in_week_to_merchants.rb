class AddDayInWeekToMerchants < ActiveRecord::Migration
  def change
    add_column :merchants, :mon, :boolean

    add_column :merchants, :mon_start_time, :time

    add_column :merchants, :mon_end_time, :time

    add_column :merchants, :tue, :boolean

    add_column :merchants, :tue_start_time, :time

    add_column :merchants, :tue_end_time, :time

    add_column :merchants, :wed, :boolean

    add_column :merchants, :wed_start_time, :time

    add_column :merchants, :wed_end_time, :time

    add_column :merchants, :thu, :boolean

    add_column :merchants, :thu_start_time, :time

    add_column :merchants, :thu_end_time, :time

    add_column :merchants, :fri, :boolean

    add_column :merchants, :fri_start_time, :time

    add_column :merchants, :fri_end_time, :time

    add_column :merchants, :sat, :boolean

    add_column :merchants, :sat_start_time, :time

    add_column :merchants, :sat_end_time, :time

    add_column :merchants, :sun, :boolean

    add_column :merchants, :sun_start_time, :time

    add_column :merchants, :sun_end_time, :time

  end
end
