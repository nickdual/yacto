class AddLocalUtcStartEndDayToOffers < ActiveRecord::Migration
  def change
    add_column :offers, :local_start_day, :datetime

    add_column :offers, :utc_start_day, :datetime

    add_column :offers, :local_end_day, :datetime

    add_column :offers, :utc_end_day, :datetime

  end
end
