class AddNewFieldToMerchants < ActiveRecord::Migration
  def change
    add_column :merchants, :why_visit, :string

    add_column :merchants, :best_thing, :string

    add_column :merchants, :unique, :string

    add_column :merchants, :featured_review, :string

  end
end
