class CreateMerchants < ActiveRecord::Migration
  def self.up
    create_table :merchants do |t|
      t.references :city
      t.references :merchant_type
      t.string :merchant_id 
      t.string :name
      t.text :mobile_description
      t.text :desktop_description
      t.string :primary_address
      t.integer :user_rating
      t.string :merchant_type_name
      t.string :sub_type
      t.string :latest_offer_id
      t.string :third_party_code
      t.string :third_party_id
      t.text :address_description
      t.string :special_instructions
      t.string :another_address
      t.string :state_province_region
      t.string :postal_code
      t.string :country
      t.string :mobile_number
      t.float  :lon 
      t.float  :lat
      t.string :phone
      t.string :city_name
      t.integer :open_hours
      t.integer :merchant_type_id
      t.time :start_open_time
      t.time :end_open_time
      t.timestamps
    end
  end

  def self.down
    drop_table :merchants
  end
end
