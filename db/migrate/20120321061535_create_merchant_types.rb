class CreateMerchantTypes < ActiveRecord::Migration
  def self.up
    create_table :merchant_types do |t|
      t.string :language
      t.string :lookup_item_id
      t.string :merchant_type
      t.string :sub_type
      t.string :color
      t.string :image
      t.string :marker_image
      t.timestamps
    end
  end

  def self.down
    drop_table :merchant_types
  end
end
