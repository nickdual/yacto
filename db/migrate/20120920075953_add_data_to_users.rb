class AddDataToUsers < ActiveRecord::Migration
  def change
    add_column :users, :gender, :boolean
    add_column :users, :address_line1, :string
    add_column :users, :address_line2, :string
    add_column :users, :city, :string
    add_column :users, :postal_code, :string
    add_column :users, :state_province_region, :string
    add_column :users, :country, :string

  end
end
