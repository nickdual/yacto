class AddVouchercodesMerchantIdToMerchants < ActiveRecord::Migration
  def change
    add_column :merchants, :vouchercodes_merchant_id, :integer
    add_column :merchants, :vouchercodes_place_id, :integer
    
  end
end
