class AddLanguageTwilioNumberToCountries < ActiveRecord::Migration
  def change
    add_column :countries, :language, :string

    add_column :countries, :twilio_number, :string

  end
end
