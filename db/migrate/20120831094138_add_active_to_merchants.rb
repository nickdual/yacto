class AddActiveToMerchants < ActiveRecord::Migration
  def change
    add_column :merchants, :active, :integer, :default => 1

  end
end
