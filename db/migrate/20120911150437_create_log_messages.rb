class CreateLogMessages < ActiveRecord::Migration
  def change
    create_table :log_messages do |t|
      t.string :process
      t.string :type
      t.string :event
      t.text :description

      t.timestamps
    end
  end
end
