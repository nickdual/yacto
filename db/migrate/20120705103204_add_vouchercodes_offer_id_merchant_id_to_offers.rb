class AddVouchercodesOfferIdMerchantIdToOffers < ActiveRecord::Migration
  def change
    add_column :offers, :vouchercodes_offer_id, :integer

    add_column :offers, :vouchercodes_merchant_id, :integer

  end
end
