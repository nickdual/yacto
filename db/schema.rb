# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20121002025608) do

  create_table "billing_informations", :force => true do |t|
    t.string   "name"
    t.string   "billing_type"
    t.string   "number"
    t.string   "security_code"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "cities", :force => true do |t|
    t.integer "country_id",                                                  :null => false
    t.integer "geonames_id"
    t.string  "name",                                                        :null => false
    t.string  "slug"
    t.string  "state"
    t.decimal "latitude",                     :precision => 14, :scale => 8, :null => false
    t.decimal "longitude",                    :precision => 14, :scale => 8, :null => false
    t.string  "country_iso_code_two_letters"
    t.integer "geonames_timezone_id"
    t.string  "timezone"
    t.boolean "status"
  end

  add_index "cities", ["geonames_id"], :name => "index_cities_on_geonames_id", :unique => true
  add_index "cities", ["name"], :name => "index_cities_on_name"

  create_table "consumers", :force => true do |t|
    t.string   "consumer_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "countries", :force => true do |t|
    t.string  "iso_code_two_letter",   :null => false
    t.string  "iso_code_three_letter", :null => false
    t.integer "iso_number",            :null => false
    t.string  "name",                  :null => false
    t.string  "capital"
    t.string  "continent"
    t.integer "geonames_id"
    t.string  "language"
    t.string  "twilio_number"
  end

  add_index "countries", ["geonames_id"], :name => "index_countries_on_geonames_id"
  add_index "countries", ["iso_code_two_letter"], :name => "index_countries_on_iso_code_two_letter", :unique => true

  create_table "error_messages", :force => true do |t|
    t.string   "message"
    t.string   "error"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "favorites", :force => true do |t|
    t.integer  "merchant_id"
    t.integer  "offer_id"
    t.integer  "user_id"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "log_messages", :force => true do |t|
    t.string   "process"
    t.string   "third_party_provider"
    t.string   "event"
    t.text     "description"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "merchant_photos", :force => true do |t|
    t.integer  "merchant_id"
    t.string   "url_medium"
    t.string   "url_thumb"
    t.string   "url_original"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
  end

  create_table "merchant_types", :force => true do |t|
    t.string   "language"
    t.string   "lookup_item_id"
    t.string   "merchant_type"
    t.string   "sub_type"
    t.string   "color"
    t.string   "image"
    t.string   "marker_image"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.string   "description"
    t.string   "map_icon"
  end

  create_table "merchants", :force => true do |t|
    t.integer  "city_id"
    t.integer  "merchant_type_id"
    t.string   "merchant_id"
    t.string   "name"
    t.text     "mobile_description"
    t.text     "desktop_description"
    t.string   "primary_address"
    t.integer  "user_rating"
    t.string   "merchant_type_name"
    t.string   "sub_type"
    t.string   "latest_offer_id"
    t.string   "third_party_code"
    t.string   "third_party_id"
    t.text     "address_description"
    t.string   "special_instructions"
    t.string   "another_address"
    t.string   "state_province_region"
    t.string   "postal_code"
    t.string   "country"
    t.string   "mobile_number"
    t.float    "lon"
    t.float    "lat"
    t.string   "phone"
    t.string   "city_name"
    t.integer  "open_hours"
    t.time     "start_open_time"
    t.time     "end_open_time"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
    t.string   "google_id"
    t.string   "yelp_id"
    t.string   "factual_id"
    t.string   "twitter_name"
    t.string   "facebook_page_id"
    t.integer  "third_party_merchant_type_id"
    t.string   "time_zone"
    t.boolean  "mon"
    t.time     "mon_start_time"
    t.time     "mon_end_time"
    t.boolean  "tue"
    t.time     "tue_start_time"
    t.time     "tue_end_time"
    t.boolean  "wed"
    t.time     "wed_start_time"
    t.time     "wed_end_time"
    t.boolean  "thu"
    t.time     "thu_start_time"
    t.time     "thu_end_time"
    t.boolean  "fri"
    t.time     "fri_start_time"
    t.time     "fri_end_time"
    t.boolean  "sat"
    t.time     "sat_start_time"
    t.time     "sat_end_time"
    t.boolean  "sun"
    t.time     "sun_start_time"
    t.time     "sun_end_time"
    t.string   "why_visit"
    t.string   "best_thing"
    t.string   "unique"
    t.string   "featured_review"
    t.integer  "vouchercodes_merchant_id"
    t.integer  "vouchercodes_place_id"
    t.string   "twitter_id"
    t.string   "flickr_id"
    t.string   "instagram_id"
    t.boolean  "payment"
    t.string   "yelp_rating_img_url"
    t.string   "website_url"
  end

  create_table "offer_insertions", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "offers", :force => true do |t|
    t.integer  "merchant_id"
    t.integer  "city_id"
    t.string   "offer_id"
    t.string   "name"
    t.integer  "active",                   :default => 1
    t.string   "description"
    t.string   "primary_address"
    t.time     "start_time"
    t.integer  "detail_views"
    t.string   "third_party_code"
    t.string   "third_party_id"
    t.float    "price"
    t.float    "value"
    t.float    "discount"
    t.text     "link"
    t.string   "image"
    t.string   "image_thumbnail"
    t.string   "image_thumbnail_retina"
    t.string   "offer_duration"
    t.string   "recurrence"
    t.float    "lon"
    t.float    "lat"
    t.time     "end_time"
    t.string   "currency"
    t.string   "timezone"
    t.string   "source"
    t.integer  "offer_guid"
    t.string   "city_name"
    t.integer  "user_id"
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
    t.integer  "day_in_week"
    t.datetime "local_start_day"
    t.datetime "utc_start_day"
    t.datetime "local_end_day"
    t.datetime "utc_end_day"
    t.integer  "vouchercodes_offer_id"
    t.integer  "vouchercodes_merchant_id"
    t.boolean  "mon",                      :default => false
    t.boolean  "tue",                      :default => false
    t.boolean  "wed",                      :default => false
    t.boolean  "thu",                      :default => false
    t.boolean  "fri",                      :default => false
    t.boolean  "sat",                      :default => false
    t.boolean  "sun",                      :default => false
    t.datetime "expire_day"
    t.integer  "merchant_type_id"
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "third_party_merchant_types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.integer  "merchant_type_id"
    t.string   "language"
  end

  create_table "users", :force => true do |t|
    t.integer  "merchant_id"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "mobile_emei"
    t.string   "mobile_number"
    t.string   "username"
    t.string   "mobile_authorized"
    t.string   "mobile_handset_brand"
    t.string   "password"
    t.string   "authorization_level"
    t.string   "fb_auth_data"
    t.string   "twitter_auth_data"
    t.string   "twitter_name"
    t.boolean  "user_admin"
    t.integer  "role_id"
    t.string   "facebook_name"
    t.string   "google_name"
    t.string   "twitter_secret"
    t.string   "twitter_token"
    t.string   "facebook_token"
    t.string   "google_token"
    t.string   "timezone"
    t.string   "timeformat"
    t.datetime "created_at",                                              :null => false
    t.datetime "updated_at",                                              :null => false
    t.string   "email",                                   :default => "", :null => false
    t.string   "encrypted_password",                      :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                           :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "yahoo_name"
    t.string   "yahoo_token",            :limit => 10000
    t.string   "code"
    t.boolean  "gender"
    t.string   "address_line1"
    t.string   "address_line2"
    t.string   "postal_code"
    t.string   "state_province_region"
    t.string   "country"
    t.string   "city_name"
    t.integer  "city_id"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "wastes", :force => true do |t|
    t.integer  "page_handollar"
    t.integer  "number_city"
    t.integer  "page_8coupon"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.integer  "eight_coupon_number"
    t.integer  "offset_vouchercodes"
    t.datetime "day_restart"
    t.integer  "factual_city_id_offset"
    t.integer  "page_sqoot"
  end

end
