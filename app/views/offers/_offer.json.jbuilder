json.extract! offer, :id, :lon, :lat, :description, :link, :link_present?
json.distance number_with_precision(offer.distance_from(@res, :units=>:miles),:precision => 1)  #number_with_precision(offer.distance)
json.offer_time offer_time(offer)
json.urgent_offer offer.period(@now_time)
json.buy_via_link_label new_buy_via_label(offer)
json.sms_body URI.escape(offer_sms_body(offer))
json.email_link URI.escape(offer_email_link(offer))
json.twitter_str URI.escape(offer_twitter_str(offer))
json.partial! "offers/merchant", :merchant => offer.merchant