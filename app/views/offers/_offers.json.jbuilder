json.array!(offers) do |json, offer|
  json.partial! "offers/offer", :offer => offer
end