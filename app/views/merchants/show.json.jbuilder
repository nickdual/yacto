json.extract! @merchant, :id, :lon, :lat, :desktop_description, :why_visit, :best_thing, :featured_review, :name, :unique, :primary_address, :yelp_rating_img_url, :twitter_name, :website_url
json.phone @merchant.phone_format || ""
json.open_time merchant_open_time(@merchant)
json.open @merchant.start_open_time.present?
json.merchant_social_string URI.escape(merchant_social_string(@merchant))

json.favorite @favorite.present?
json.merchant_type @merchant.merchant_type, :sub_type, :merchant_type, :marker_image

json.offers_mon do |json|
  json.partial! "merchants/simple_offers", :offers => @offers_mon
end

json.offers_tue do |json|
  json.partial! "merchants/simple_offers", :offers => @offers_tue
end

json.offers_wed do |json|
  json.partial! "merchants/simple_offers", :offers => @offers_wed
end

json.offers_thu do |json|
  json.partial! "merchants/simple_offers", :offers => @offers_thu
end

json.offers_fri do |json|
  json.partial! "merchants/simple_offers", :offers => @offers_fri
end

json.offers_sat do |json|
  json.partial! "merchants/simple_offers", :offers => @offers_sat
end

json.offers_sun do |json|
  json.partial! "merchants/simple_offers", :offers => @offers_sun
end

json.merchant_photos @merchant.merchant_photos, :url_medium
json.city @merchant.city, :name