json.array!(offers) do |json, offer|
  json.partial! "merchants/simple_offer", :offer => offer
end