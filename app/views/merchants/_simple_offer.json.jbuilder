json.extract! offer, :id, :description, :link#, :lon, :lat, :link_present?, :distance
json.offer_time offer_time(offer)
json.sms_body URI.escape(offer_sms_body(offer))
json.email_link URI.escape(offer_email_link(offer))
json.twitter_str URI.escape(offer_twitter_str(offer))
json.buy_via_link_label new_buy_via_label(offer)
#TODO this can be optimized on js side (not sent with every offer, merchant is the same), but let's not do it prematurely
json.partial! "dashboards/merchant", :merchant => offer.merchant