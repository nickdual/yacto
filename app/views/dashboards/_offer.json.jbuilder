json.extract! offer, :id, :lon, :lat, :description, :distance, :link, :link_present?
json.offer_time offer_time(offer)
json.buy_via_link_label buy_via_link_label(offer)
json.sms_body URI.escape(offer_sms_body(offer))
json.email_link URI.escape(offer_email_link(offer))
json.twitter_str URI.escape(offer_twitter_str(offer))
json.partial! "dashboards/merchant", :merchant => offer.merchant