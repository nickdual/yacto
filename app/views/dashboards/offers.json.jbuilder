json.array!(@offers) do |json, offer|
  json.partial! "dashboards/offer", :offer => offer
end