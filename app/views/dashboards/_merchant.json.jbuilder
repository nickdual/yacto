json.merchant do |json|
  json.extract! merchant, :primary_address, :name, :merchant_id, :id
  json.phone merchant.phone_format || ""
  json.open_time merchant_open_time(merchant)
  json.open merchant.start_open_time.present?
  json.url merchant_path(merchant)
  json.merchant_type merchant.merchant_type, :marker_image, :merchant_type
end