var I18n = I18n || {};
I18n.translations = {
    "en":
    {
        "date":
        {
            "formats":
            {
                "default":"%Y-%m-%d","short":"%b %d",
                "long":"%B %d, %Y"
            },
            "day_names":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],
            "abbr_day_names":["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],
            "month_names":[null,"January","February","March","April","May","June","July","August","September","October","November","December"],
            "abbr_month_names":[null,"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
            "order":["year","month","day"]},
        "time":{
            "formats":{
                "default":"%a, %d %b %Y %H:%M:%S %z",
                "short":"%d %b %H:%M",
                "long":"%B %d, %Y %H:%M"
            },
            "am":"am",
            "pm":"pm"},
        "support":{
            "array":{
                "words_connector":", ",
                "two_words_connector":" and ",
                "last_word_connector":", and "
            }
        },
        "errors":{
            "format":"%{attribute} %{message}",
            "messages":
            {
                "inclusion":"is not included in the list",
                "exclusion":"is reserved",
                "invalid":"is invalid",
                "confirmation":"doesn't match confirmation",
                "accepted":"must be accepted",
                "empty":"can't be empty",
                "blank":"can't be blank",
                "too_long":"is too long (maximum is %{count} characters)",
                "too_short":"is too short (minimum is %{count} characters)",
                "wrong_length":"is the wrong length (should be %{count} characters)",
                "not_a_number":"is not a number",
                "not_an_integer":"must be an integer",
                "greater_than":"must be greater than %{count}",
                "greater_than_or_equal_to":"must be greater than or equal to %{count}",
                "equal_to":"must be equal to %{count}",
                "less_than":"must be less than %{count}",
                "less_than_or_equal_to":"must be less than or equal to %{count}",
                "odd":"must be odd","even":"must be even",
                "expired":"has expired, please request a new one",
                "not_found":"not found",
                "already_confirmed":"was already confirmed, please try signing in",
                "not_locked":"was not locked",
                "not_saved":{
                    "one":"1 error prohibited this %{resource} from being saved:",
                    "other":"%{count} errors prohibited this %{resource} from being saved:"
                }
            }
        }
        ,"activerecord":
        {
        "errors":{
            "messages":{
                "taken":"has already been taken",
                "record_invalid":"Validation failed: %{errors}"
            }
        }
        },
        "number":{
            "format":{
                "separator":".",
                "delimiter":",",
                "precision":3,
                "significant":false,
                "strip_insignificant_zeros":false
            },
            "currency":{
                "format":{
                    "format":"%u%n",
                    "unit":"$",
                    "separator":".",
                    "delimiter":",",
                    "precision":2,
                    "significant":false,
                    "strip_insignificant_zeros":false
                }
            },
            "percentage":{
                "format":{
                    "delimiter":""
                }
            },
            "precision":{
                "format":{
                    "delimiter":""
                }
            },
            "human":{
                "format":{
                    "delimiter":"",
                    "precision":3,"significant":true,"strip_insignificant_zeros":true
                },
                "storage_units":{
                    "format":"%n %u","units":{
                        "byte":{
                            "one":"Byte","other":"Bytes"
                        },
                        "kb":"KB","mb":"MB","gb":"GB","tb":"TB"
                    }},
                "decimal_units":{"format":"%n %u",
                    "units":{
                    "unit":"","thousand":"Thousand",
                        "million":"Million","billion":"Billion",
                        "trillion":"Trillion","quadrillion":"Quadrillion"
                    }}}},
        "datetime":{"distance_in_words":{"half_a_minute":"half a minute",
            "less_than_x_seconds":{"one":"less than 1 second",
                "other":"less than %{count} seconds"},
            "x_seconds":{
                "one":"1 second","other":"%{count} seconds"},
            "less_than_x_minutes":{"one":"less than a minute","other":"less than %{count} minutes"},
            "x_minutes":{"one":"1 minute","other":"%{count} minutes"},
            "about_x_hours":{"one":"about 1 hour","other":"about %{count} hours"},
            "x_days":{"one":"1 day","other":"%{count} days"},"about_x_months":{
                "one":"about 1 month","other":"about %{count} months"},
            "x_months":{"one":"1 month","other":"%{count} months"},
            "about_x_years":{"one":"about 1 year","other":"about %{count} years"},
            "over_x_years":{"one":"over 1 year","other":"over %{count} years"},
            "almost_x_years":{"one":"almost 1 year","other":"almost %{count} years"}},
            "prompts":{"year":"Year","month":"Month","day":"Day","hour":"Hour","minute":"Minute",
                "second":"Seconds"}},"helpers":{"select":{"prompt":"Please select"},
        "submit":{"create":"Create %{model}","update":"Update %{model}","submit":"Save %{model}"},
        "button":{"create":"Create %{model}","update":"Update %{model}","submit":"Save %{model}"}},
        "will_paginate":{"previous_label":"&#8592; Previous","next_label":"Next &#8594;","page_gap":"&hellip;",
            "page_entries_info":{"single_page":{"zero":"No %{model} found","one":"Displaying 1 %{model}",
                "other":"Displaying all %{count} %{model}"},"single_page_html":{"zero":"No %{model} found",
                "one":"Displaying <b>1</b> %{model}",
                "other":"Displaying <b>all&nbsp;%{count}</b> %{model}"},
                "multi_page":"Displaying %{model} %{from} - %{to} of %{count} in total",
                "multi_page_html":"Displaying %{model} <b>%{from}&nbsp;-&nbsp;%{to}</b> of <b>%{count}</b> in total"}},
        "devise":{
            "failure":{
            "already_authenticated":"You are already signed in.",
            "unauthenticated":"You need to sign in or sign up before continuing.",
            "unconfirmed":"You have to confirm your account before continuing.",
            "locked":"Your account is locked.",
            "invalid":"Invalid email or password.",
            "invalid_token":"Invalid authentication token.",
            "timeout":"Your session expired, please sign in again to continue.",
            "inactive":"Your account was not activated yet."},
            "sessions":{
                "signed_in":"Signed in successfully.",
                "signed_out":"Signed out successfully."},
            "passwords":{
                "send_instructions":"You will receive an email with instructions about how to reset your password in a few minutes.",
                "updated":"Your password was changed successfully. You are now signed in.",
                "updated_not_active":"Your password was changed successfully.",
                "send_paranoid_instructions":"If your e-mail exists on our database, you will receive a password recovery link on your e-mail",
                "no_token":"You can't access this page without coming from a password reset email. If you do come from a password reset email, please make sure you used the full URL provided."},"confirmations":{"send_instructions":"You will receive an email with instructions about how to confirm your account in a few minutes.","send_paranoid_instructions":"If your e-mail exists on our database, you will receive an email with instructions about how to confirm your account in a few minutes.","confirmed":"Your account was successfully confirmed. You are now signed in.","new":"Resend confirmation instructions"},"registrations":{"signed_up":"Welcome! You have signed up successfully.","signed_up_but_unconfirmed":"A message with a confirmation link has been sent to your email address. Please open the link to activate your account.","signed_up_but_inactive":"You have signed up successfully. However, we could not sign you in because your account is not yet activated.","signed_up_but_locked":"You have signed up successfully. However, we could not sign you in because your account is locked.","updated":"You updated your account successfully.","update_needs_confirmation":"You updated your account successfully, but we need to verify your new email address. Please check your email and click on the confirm link to finalize confirming your new email address.","destroyed":"Bye! Your account was successfully cancelled. We hope to see you again soon."},"unlocks":{"send_instructions":"You will receive an email with instructions about how to unlock your account in a few minutes.","unlocked":"Your account has been unlocked successfully. Please sign in to continue.","send_paranoid_instructions":"If your account exists, you will receive an email with instructions about how to unlock it in a few minutes."},"omniauth_callbacks":{"success":"Successfully authorized from %{kind} account.","failure":"Could not authorize you from %{kind} because \"%{reason}\"."},"mailer":{"confirmation_instructions":{"subject":"Confirmation instructions"},"reset_password_instructions":{"subject":"Reset password instructions"},"unlock_instructions":{"subject":"Unlock Instructions"},"confirm":"Confirm my account","confirmmessage":"You can confirm your account email through the link below:","welcome":"Welcome","hello":"Hello","passwordchangerequest":"Someone has requested a link to change your password, and you can do this through the link below.","passwordchange":"Change my password","ignoreemail":"If you didn`t request this, please ignore this email.","passwordremains":"Your password won`t change until you access the link above and create a new one.","accountlocked":"Your account has been locked due to an excessive amount of unsuccessful sign in attempts.","accountunlockmessage":"Click the link below to unlock your account:","accountunlock":"Unlock my account"},"instructions":"Didn`t receive confirmation instructions?","unlockinstructions":"Didn`t receive unlock instructions?","password":{"new":"New password","confirmnew":"Confirm new password","change":"Change my password","headingtwo":"Change your password","forgot":"Forgot your password?","reset":"Send me reset password instructions"}},"set_location":{"submit":"Submit","location":"Location"},"home":{"index":{"my_info":"My info","sign_out":"Sign out","sign_in":"Sign in","cancel":"Cancel","create_an_account":"New User? Create an Account","sign_in_with":"Sign in with","post_to":"Post to","off":"Off","on":"On","please_sign_in":"To save a favorite, please sign in"}},
        "users":{
            "create":"User was successfully created.",
            "update":"User was successfully updated.",
            "confirmationsent":"You have signed up successfully. If enabled, a confirmation was sent to your e-mail.",
            "sign_in":{
                "login":"Username or Email",
                "password":"Password"
            },
            "account_setting":{
                "account_setting":"Account Settings",
                "email":"Email" ,
                "password":"Password",
                "confirm_password":"Confirm Password ",
                "current_password":"Current Password",
                "gender":"Gender",
                "male":"Male",
                "undefine":"--",
                "female":"Female",
                "mobile_number":"Mobile Number",
                "option_info":"Optional Information" ,
                "address_info":"Address Information",
                "line1":"Line 1",
                "line2":"Line 2",
                "city":"City",
                "postal_code":"Postal Code",
                "state":"State"   ,
                "country":"Country",
                "social_network":"Social Media Information",
                "connect_face":"Connect To Facebook" ,
                "connect_twitter":"Connect To Twitter" ,
                "connect_google":"Connect To Google +" ,
                "connect_yahoo":"Connect To Yahoo" ,
                "connected_face":"Connected To Facebook" ,
                "connected_twitter":"Connected To Twitter" ,
                "connected_google":"Connected To Google +" ,
                "connected_yahoo":"Connected To Yahoo" ,
                "discard":"Discard",
                "save":"Save",
                "post_on":"Post on",
                "error_current_pass":"Current password can't be blank!"  ,
                "error_match_pass":"Password is not match!" ,
                "error_length_pass":"Password is too short! (Minimum is 6 characters)"
            },
            "sign_up":{
                "user_name":"Username",
                "email":"Email Adress",
                "password":"Password",
                "password_confirmation":"Password confirmation",
                "first_name":"First Name",
                "last_name":"Last Name",
                "mobile_emei":"Mobile emei",
                "mobile_number":"Mobile Number",
                "mobile_authorized":"Mobile authorized",
                "mobile_handset_brand":"Mobile handset brand",
                "authorization_level":"Authorization level",
                "sign_up":"Sign up",
                "sign_in":"Sign up",
                "forgot_your_password":"Forgot your password?",
                "receive_confirmation instructions":"Didn't receive confirmation instructions?"},
            "detailheading":"Yacto User Detail Page",
            "userdetail":"User Detail",
            "authorization":"Authorization",
            "name":"Name",
            "mobilenumber":"Mobile No.",
            "email":"Email",
            "privileges":"Privileges",
            "norole":"No role is defined"},
        "offers":{"create":"Offer was successfully created.",
            "update":"Offer was successfully updated.",
            "no_offers":"No offers are available that meet your criteria.  Change your filter criteria to see offers."},
        "merchant_type":{
            "title":"Merchant Type",
            "sub_type":"Sub Type",
            "create":"Merchant type was successfully created.",
            "update":"Merchant type was successfully updated."},
        "filter":{"title":"Filter",
            "start_time":"Start Time",
            "end_time":"End Time",
            "distance":"Distance",
            "merchant_type":"Merchant Type",
            "distance_01mi":"0.1 MI",
            "distance_05mi":"0.5 MI",
            "distance_1mi":"1 MI","distance_5mi":"5 MI",
            "distance_10mi":"10 MI","time_now":"Now",
            "time_2hr":"2 HR","time_3hrs":"3 HRS",
            "time_4hrs":"4 HRS","time_later":"LATER",
            "all_offers":"ALL Offers","all":"All"},
        "merchant":{
            "city":"City","type":"Merchant type:",
            "language":"Language:","lookup":"Lookup item:",
            "postal_code":"Post Code","third_party_code":"Third party code",
            "third_party_id":"Third party id","user_rating":"User rating",
            "primary_address":"Primary address","start_open_time":"Start Open Time",
            "end_open_time":"End Open Time","desc":"Description","name":"Name",
            "phone":"Phone","email":"Email","contact":"Contact","id":"ID",
            "link":"link","create":"Merchant was successfully created.",
            "update":"Merchant was successfully updated.","merchant_type":"Merchant type",
            "google_place":"No data in google place","not_available":"Not Available",
            "accountdetail":{"heading":"Merchant account details",
                "detailinfo":"Merchant Detail Information","addressinfo":"Address Information",
                "merchantusers":"Approved Merchant Users","offers":"Offers"},
            "smsrequest":{
                "fourcommaformat":"We could not understand your message. Please post in the Yacto format: Time, Your business name, Ad, Address (with no commas), Phone number","timeformat":"time is not properly format. format is 1700-1800","helpinfo":"language specific help information","newoffer":"new offer has been created","disablelatestoffer":"latest offer has been deactivated","offerabsent":"offer does not exist","disableoffer":"all offers have been deactivated","enableoffer":"all offers have been activated","merchantexist":"sms is not properly formated, merchant has already been created therefore write sms with one comma in order to create new offer","offerrecurred":"latest offer has been recurred to %{parameters}","invalidcommand":"unrecognized command: plz enter sms with: HELP, STOP ALL, STOP, Daily etc","merchantandoffercreated":"Welcome to Yacto!  View your account and offers at yacto.com/%{merchant_id}.  Log in with username: %{username} and password %{password}.","verify_address_error":"Address could not be validated, please provide street and post code","success_merchant_type":"Merchant Type is created successful","fail_merchant_type":"Merchant Type is not created","require_merchant_type":"Please respond with your business type.%{merchant_types}.Or edit at yacto.com/%{merchant_id}.","twilio_number":"Your country doesn't have the sms service","exist_phone_number":"Please add Phone number"}},"map":{"must_verify":"You must successfully verify the company address before continuing","please_continue":"Drag the marker to the desired location on the map.  You can zoom in or out with the controls on the left side of the map. Click Finish! to continue.","error_location":"Address could not be validated, please provide street and post code.","address_verified":"Address verified. If this is not the location you expected, please refine your address and try again. Don't forget to save your changes!","verify_address":"Verify Address","manually_set_location":"Manually Set Location"},"offerinsertion":{"name":"Name"},
        "form":{
            "edit":"Edit","update":"Update","back":"Back",
            "new":"New","show":"Show","save":"Save",
            "copy":"Copy","search":"Search",
            "destroy":"Destroy","cancelaccount":"Cancel my account",
            "unhappy":"Unhappy?","confirmdelete":"Are you sure?",
            "passwordhelp":"we need your current password to confirm your changes",
            "passwordchangehelp":"leave blank if you don`t want to change it",
            "signup":"Sign up","signin":"Sign in","signout":"Sign out",
            "myinfo":"My info","unlockinstructions":"Resend unlock instructions",
            "signinwithname":"Sign in with %{name}","posttoname":"Post to %{name}",
            "errorswithclass":"prohibited this %{class} from being saved:",
            "editingclass":"Editing %{class}","newclass":"New %{class}","listingclass":"Listing %{class}",
            "listclasses":"List %{classes}","fav":"Favorite","resendcode":"Resend Code","cancel":"Cancel"},
        "offer":{"name":"Name","price":"Price","value":"Value","city":"City","description":"Description",
            "third_party_code":"Third party code","link":"Link","image":"Image","image_thumbnail":"Image thumbnail",
            "image_thumbnail_retina":"Image thumbnail retina","discount":"Discount","active":"Active",
            "expire_day": "Expire_Day",
            "primary_address":"Primary address","start_time":"Start time",
            "end_time":"End Time",
            "detail_views":"Detail views","third_party_id":"Third party id",
            "duration":"Duration","repeat":"Repeat","start":"Start","stop":"Stop","daily":"Daily",
            "weekly":"Weekly","other":"Other","minute":"min","all_day":"All day"},
        "message":{
            "i_like":"I like ",
            "check_it_out":", check it out ! www.yacto.com/"}},

    "vn":
    {
        "home":{
            "index":{
                "my_info":"Tai khoan","sign_out":"Thoat","sign_in":"Dang Nhap","cancel":"Huy","create_an_account":"Them user? Tao tai khoan",
                "sign_in_with":"Dang nhap voi","post_to":"Gui kem"}},"users":{"sign_up":{"user_name":"Ten Dang Nhap","email":"Dia Chi Email",
        "password":"Mat Khau","password_confirmation":"Nhap lai Mat Khau","first_name":"Ho","last_name":"Ten","mobile_emei":"Dien Thoai emei",
        "mobile_number":"Di Dong","mobile_authorized":"Uy quyen so di dong","mobile_handset_brand":"Thi\u1ebft b\u1ecb c\u1ea7m tay th\u01b0\u01a1ng hi\u1ec7u di \u0111\u1ed9ng","authorization_level":"\u1ee7y quy\u1ec1n c\u1ea5p","sign_up":"Dang Ky","sign_in":"Dang Nhap","forgot_your_password":"Quen mat khau?","receive_confirmation instructions":"Kh\u00f4ng nh\u1eadn \u0111\u01b0\u1ee3c h\u01b0\u1edbng d\u1eabn x\u00e1c nh\u1eadn?"}},"form":{"edit":"Edit","update":"Update","back":"Back","new":"New","show":"Show","destroy":"Destroy","cancelaccount":"Cancel my account","unhappy":"Unhappy?","confirmdelete":"Are you sure?","passwordhelp":"we need your current password to confirm your changes","passwordchangehelp":"leave blank if you don`t want to change it","signup":"Sign up","signin":"Dang Nhap","signout":"Sign out","myinfo":"My info","unlockinstructions":"Resend unlock instructions","signinwithname":"Sign in with %{name}","posttoname":"Post to %{name}","errorswithclass":"prohibited this %{class} from being saved:","editingclass":"Editing %{class}","newclass":"New %{class}","listingclass":"Listing %{class}","listclasses":"List %{classes}","fav":"Favorite"}}};