class window.OfferInfoBox
#merchant.name = "Tony's Pizza"
#merchant.primary_address = "123 Street 12"
#merchant.url = merchant_path(@offer.merchant)

  show: (merchant, latlng, map) ->
  
    boxText = document.getElementById("info")
    boxText.innerHTML = @merchantInfoBoxHTML(merchant)
    x = parseInt($(boxText).height()+ 10)
    x = x *(-1)
    myOptions = {
      id: "infoBox"
      content: boxText
      disableAutoPan: true
      maxWidth: 0
      pixelOffset: new google.maps.Size(-135,x)
      position: latlng
      zIndex: 100
      boxStyle: {
#*background: "url('/assets/markers/bg_info.png') no-repeat"
        opacity: 0.75
        width: "260"
        height: "47px"
        border: "none"
      }
      closeBoxMargin: ""
      closeBoxURL: "#"
      infoBoxClearance: new google.maps.Size(1, 1)
      isHidden: false
      pane: "floatPane"
      enableEventPropagation: false
    }

    infowindow = new InfoBox(myOptions);
    infowindow.open(map);
    
   merchantInfoBoxHTML: (merchant) ->
     '<div id="infowindow" class="downarrowdiv"><div class="format_text_popup_name">' + merchant.name + '</div><div class="format_text_popup_address">' + merchant.primary_address + '</div><div class="div_right" style="z-index: 9999;"><a href="/bb#merchants/' + merchant.id + '"><img src="/assets/markers/icon_next.png" style="z-index:9999;" /></a></div></div>'
  