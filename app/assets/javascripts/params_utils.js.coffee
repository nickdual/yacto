window.Param =
  byName: (name) ->
    params = window.location.search.substr(1)?.split("&") || []
    theParam = (param for param in params when param.split("=")[0] == name )
    if theParam.length == 0 || theParam[0].split("=").length == 0 || theParam[0].split("=")[1] == undefined
      return ""
    else
      theParam[0].split("=")[1]
