class UrlHelper
  
  paramsOrDefault: (name, opts) ->
    "#{name}=#{opts[name] || Param.byName(name)}"

  createParams: (opts) ->
    "#{@paramsOrDefault("lon", opts)}&#{@paramsOrDefault("lat", opts)}&#{@paramsOrDefault("distance", opts)}&#{@paramsOrDefault("time", opts)}&#{@paramsOrDefault("merchant_type_id", opts)}"

  nearbyPath: (options = {}) ->
    path = "/offers/nearby?#{@createParams(options)}" 
    return "#{path}&offer_id=#{options.offer_id}"  if options.offer_id?

window.UrlHelper = new UrlHelper()
