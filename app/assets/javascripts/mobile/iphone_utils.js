IPhoneUtils = {
  init:function () {
    IPhoneUtils.detectIphone();
  },

  detectIphone:function () {
    var ua = navigator.userAgent,
      iphone = ~ua.indexOf('iPhone') || ~ua.indexOf('iPod');

    if (iphone) {
      $('html').addClass('iphone_ua');
    }
  },

  removeSafariBar:function () {
    window.addEventListener("load", function () {
      // Set a timeout...
      setTimeout(function () {
        // Hide the address bar!
        window.scrollTo(0, 1);
      }, 100);
    });
  }
}

$(IPhoneUtils.init);
IPhoneUtils.removeSafariBar();