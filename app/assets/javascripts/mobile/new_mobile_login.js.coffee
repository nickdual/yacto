#= require ../lib/bootstrap-modal
#= require ../lib/iphone-style-checkboxes
#= require ./templates/new_mobile_login
#= require jquery.cookie

class NewMobileLogin
  template: JST["mobile/templates/new_mobile_login"]

  constructor: ->
    Login.addCallback(@afterLogin)
    ForgotPass.addCallback(@afterForgotPass)

  show: (@afterLoginCallback) ->
    @renderIfNotPresent()
    @initSwitches()
    $("#log_in").modal()

  initSwitches: ->
    $('.post_on_facebook').iphoneStyle(
      checkedLabel:'<img src="/assets/ios-style-checkboxes/post_facebook.png">'
      uncheckedLabel:'<img class="image_off" src="/assets/ios-style-checkboxes/post_facebook.png">'
      onChange: @cookieSwitcher("facebook_on")
    )
    $('.post_on_twitter').iphoneStyle(
      checkedLabel:'<img src="/assets/ios-style-checkboxes/post_twitter.png">'
      uncheckedLabel:'<img class="image_off" src="/assets/ios-style-checkboxes/post_twitter.png">'
      onChange: @cookieSwitcher("twitter_on")
    )
    $('.post_on_gplus').iphoneStyle(
      checkedLabel:'<img src="/assets/ios-style-checkboxes/post_gplus.png">'
      uncheckedLabel:'<img class="image_off" src="/assets/ios-style-checkboxes/post_gplus.png">'
      onChange: @cookieSwitcher("gplus_on")
    )

  cookieSwitcher: (cookieName) ->
    (elem, value) ->
      if value
        $.cookie(cookieName, true)
      else
        $.cookie(cookieName, null)

  afterLogin: =>
    $("#log_in").modal("hide")
    @afterLoginCallback?()

  afterForgotPass: ->
    $("#log_in").modal("hide")


  renderIfNotPresent: ->
    if $("#log_in").length == 0
      $("body").append(@template(
        authenticityToken: @authenticityToken()
        isFacebookOn: @isFacebookOn()    
        isTwitterOn: @isTwitterOn()    
        isGooglePlusOn: @isGooglePlusOn()    
        ))
      @addHandlers()
      
  addHandlers: ->
    $("#log_in .forgot_password, #log_in .back_to_login").click(@toggleLoginAndForget)
    $("#log_in .log_in_form").on("ajax:error", @loginError)
    
  loginError: (event, xhr, status, error) ->
    if xhr.status == 401
      alert("Login/Password invalid")

  toggleLoginAndForget: (e) ->
    e.preventDefault()
    $("#log_in").toggleClass("forgotten_pass")

  authenticityToken: ->
    $("meta[name=csrf-token]").attr("content")

  isFacebookOn: ->
    $.cookie("facebook_on") == 'true'

  isTwitterOn: ->
    $.cookie("twitter_on") == 'true'

  isGooglePlusOn: ->
    $.cookie("gplus_on") == 'true'


window.NewMobileLogin = new NewMobileLogin()