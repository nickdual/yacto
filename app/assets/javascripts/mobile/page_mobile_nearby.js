//= require ../lib/bootstrap-transition
//= require ../lib/bootstrap-carousel-with-touch
//= require ./favorites

var MobileNearby = (function ($) {
  return {
    template: JST["mobile/templates/nearby_offer"],
    offers: [],
    PAGE_SIZE: 15,
    offset: 0,
    MAX_OFFERS: 300,

    showCarousel:function () {
        var $container = $('.nearby_offer_wrapper');
        $container.slideDown();

        //init the offer carousel
        $container.carousel({
          interval: 5000000
        });
    },

    isLastOffer: function(){
      var currentOfferNr = $(".list_item.active").data("nr");
      if (currentOfferNr === MobileNearby.offers.length - 1){
        return false;
      }
      return true;
    },

    fetchMoreOffers: function(){
      //I do miss the backbone models right now :)
      $.ajax({
        type: 'GET',
        url: '/offers/nearby.json',
        data: MobileNearby.createQueryParams(),
        success: MobileNearby.addOffers
      });
    },

    //TODO this is a utility, with the 3 basic touch handlers, but for now I'll leave it here
    blockTapOn: function($container, selector, finalTouchHandler){
      $container.delegate(selector, "touchstart", MobileNearby.onStartTouch);
      $container.delegate(selector, "touchmove", MobileNearby.onTouchMove);
      $container.delegate(selector, "touchend", finalTouchHandler || MobileNearby.onEndTouch);
    },

    onStartTouch: function(e){
      MobileNearby.startX = e.originalEvent.touches ? e.originalEvent.touches[0].pageX : e.pageX;
      MobileNearby.startY = e.originalEvent.touches ? e.originalEvent.touches[0].pageY : e.pageY;
      MobileNearby.endX = undefined;
      MobileNearby.endY = undefined;
    },

    onTouchMove: function(e){
      MobileNearby.endX = e.originalEvent.touches ? e.originalEvent.touches[0].pageX : e.pageX;
      MobileNearby.endY = e.originalEvent.touches ? e.originalEvent.touches[0].pageY : e.pageY;
    },

    onEndTouch: function(e){
      if (MobileNearby.endX === undefined && MobileNearby.endY === undefined){ //no move
        MobileNearby.stopPropagationHandler(e);
      }
    },

    stopPropagationHandler: function(e){
      e.stopPropagation();
      e.stopImmediatePropagation();
      return false;
    },

    onFavorite: function(e){
      e.preventDefault();
      e.stopPropagation();
      e.stopImmediatePropagation();

      var favOfferNr = $(this).parents(".list_item").data("nr");
      //this is a global state, but passing it soooo deep, and all the callbacks - I'd thing is worse
      MobileNearby.favOfferNr = favOfferNr; 
      Favorites.favoriteObject = MobileNearby.offers[favOfferNr];
      Favorites.favoriteObjectType = "offer";
      Favorites.onFavorite();
      return false;
    },

    handleFavorite:function () {
      Favorites.favoriteAddedCallback = MobileNearby.favoriteAddedCallback;
      Favorites.favoriteRemovedCallback = MobileNearby.favoriteRemovedCallback;
      Favorites.favoritesFetchedCallback = MobileNearby.favoritesFetchedCallback;
    },

    favoriteAddedCallback: function(data){
      $(".list_item[data-nr=" + MobileNearby.favOfferNr + "] .fav").addClass('starred');
      Favorites.currentFavObj().favorite = {id: data.id};
    },

    favoriteRemovedCallback: function(){
      $(".list_item[data-nr=" + MobileNearby.favOfferNr + "] .fav").removeClass('starred');
      Favorites.currentFavObj().favorite = undefined;
    },

    favoritesFetchedCallback: function(data){
      MobileNearby.favorites = data;
      MobileNearby.addFavoritesToOffers(MobileNearby.offers);
      if (typeof MobileNearby.favOfferNr === "number"){
        MobileNearby.offersLoader(MobileNearby.favOfferNr)();
        Favorites.addOrRemoveFav();
      } else if (typeof MobileNearby.refreshFavoriteOnLoad === "number"){
        MobileNearby.offersLoader(MobileNearby.refreshFavoriteOnLoad)();
      }
    },

    // this has O(n^2) - so we'd better watch it :(
    addFavoritesToOffers: function(offers){
      if (MobileNearby.favorites === undefined) { return }
      var i = 0, offerNr, favorites = MobileNearby.favorites, favorite;
      for (i = 0; i < favorites.length; i++){
        favorite = favorites[i];
        offerNr = MobileNearby.offerNrById(favorite.offer_id, offers);
        if (offerNr !== undefined){
          offers[offerNr].favorite = {id: favorite.id};
        }
      }
    },

    // TODO - extract common social actions actions
    googlePlusClick: function(e){
      e.preventDefault();
      var href = $(this).attr("href");
      window.open(href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
      return false;
    },

    handleActions: function() {
      var $container = $('.nearby_offer_wrapper');
      //Touch events gets handled before click, so we have to block them in a special way (also we don't want to block swipe)
      MobileNearby.blockTapOn($container, ".offer-name-link");
      MobileNearby.blockTapOn($container, ".offer-description-link");
      MobileNearby.blockTapOn($container, ".offer-small-details a");
      MobileNearby.blockTapOn($container, ".tweet");
      MobileNearby.blockTapOn($container, ".email");
      MobileNearby.blockTapOn($container, ".phone");
      MobileNearby.blockTapOn($container, ".redeem_offer");
      MobileNearby.blockTapOn($container, ".fav", MobileNearby.onFavorite);
      MobileNearby.blockTapOn($container, "a.gplus", MobileNearby.googlePlusClick);

      $container.delegate("a.next_offer", "click", MobileNearby.isLastOffer);
      $container.on("slide", MobileNearby.onSlide);
    },

    initMap: function(lat, lon) {
      if (Param.byName("distance")){
        var radius = parseFloat(Param.byName("distance"), 10);
      }
      else {
        var radius = 1;
      }
      var z = Math.round(14 - Math.log(radius)/Math.LN2);

      var latlng = new google.maps.LatLng(lat, lon);
      var image = '/assets/markers/pin/image.png';
      var myOptions = {
        zoom: z,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false
      }

      var map = new google.maps.Map(document.getElementById("map_show"), myOptions);
      var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        icon: image
      });

      var populationOptions = {
        map: map,
        center:  latlng,
        fillOpacity: 0,
        strokeOpacity:0,
        radius: radius * 1609.344
      };

      var circle = new google.maps.Circle(populationOptions);

      this.map = map;
      return map;
    },

    addOffers: function(offers){
      var i = 0, 
        oldOffers = MobileNearby.offers;
      startLength = oldOffers.length;
      for (i = 0; i < offers.length; i++){
        oldOffers.push(offers[i]);
      }
      MobileNearby.updateOffset();
      LocalStorage.saveOffers(oldOffers);

      for (i = startLength; i < oldOffers.length; i++){
        MobileNearby.addMapMarker(i);
      };

      MobileNearby.addFavoritesToOffers(offers);

      //When Offers are added (on page load initialy), we wait a sec, and load some more. Thou this may be performance killer
      if (offers.length > 0 && oldOffers.length < MobileNearby.MAX_OFFERS){
        setTimeout(MobileNearby.fetchMoreOffers, 1000);
      }
    },

    updateOffset: function(){
      //had it not been for broken data I could just use length here :(
      var brokenOffers = (this.PAGE_SIZE - (this.offers.length % this.PAGE_SIZE)) % this.PAGE_SIZE;
      this.offset = this.offers.length + brokenOffers;
    },

    addMerchantsPins: function(merchants){
      var map = this.map;
      for(var i = 0; i < merchants.length ;i++) {
        this.addMapMarker(merchants[i]);
      }
    },

    addMapMarker: function (i){
      var map = this.map;
      var offer = this.offers[i];
      var merchant = offer.merchant;
      var new_point = new google.maps.LatLng(offer.lat, offer.lon );
      var image = '/assets/markers/' +  (merchant.merchant_type.marker_image ? merchant.merchant_type.marker_image : 'default.png');
      var marker_point = new google.maps.Marker({
        position: new_point,
        map: map,
        icon: image,
        url: '/merchants/' + merchant.id
      });
      google.maps.event.addListener(marker_point, 'click', this.offersLoader(i));
    },

    //passing parameters to event handler, so I keep it in closure scope. It adds 3 elements (one before, and after active - so that carousel would work)
    offersLoader: function(i){
      var that = this;
      return function(){
        var $carousel = MobileNearby.$nearby_offer_inner;
        var offers = that.offers;
        $carousel.html("");  
        if (i > 0){
          $carousel.append(that.template({offer:offers[i-1], offerNr: i-1}));
        }
        $carousel.append(that.template({offer:offers[i], active: true, offerNr: i}));
        if (i < offers.length - 1){
          $carousel.append(that.template({offer:offers[i+1], offerNr: i+1}));
        }
        MobileNearby.parseFBXML($carousel[0]);
        MobileNearby.showCarousel();
        MobileNearby.showInfobox(offers[i]);
        MobileNearby.centerMap(offers[i]);
      }
    },

    parseFBXML: function(elem){
      if (typeof window.FB !== "undefined"){
        // FB.XFBML.parse(elem, function(){});
      }
    },

    onSlide: function(){
      // setTimeout(function(){
        var newOfferNr = $(".list_item.next, .list_item.prev").data("nr");
        var newOffer = MobileNearby.offers[newOfferNr];

        MobileNearby.addOfferToDOM();
        MobileNearby.showInfobox(newOffer);
        MobileNearby.centerMap(newOffer);
      // }, 0);
    },

    centerMap: function(offer){
      point = new google.maps.LatLng( offer.lat, offer.lon );
      this.map.setCenter(point);
      this.map.panBy(0, $(document).height()/10);
    },

    addOfferToDOM: function(){
      var currentOfferNr = $(".list_item.active").data("nr");
      MobileNearby.addOfferBeforeIfNeeded(currentOfferNr);
      MobileNearby.addOfferAfterIfNeeded(currentOfferNr);
    },

    addOfferBeforeIfNeeded: function(currentOfferNr){
      var smallestOfferNr = $(".nearby_offer_wrapper .list_item:first").data("nr");
      if (currentOfferNr > 1 && currentOfferNr - 2 < smallestOfferNr){
        // console.log("Moving to prev with current at: "+currentOfferNr);
        MobileNearby.$nearby_offer_inner.prepend(MobileNearby.template({
          offer: MobileNearby.offers[currentOfferNr-2],
          offerNr: currentOfferNr-2
        }));
        var $newOffer = $('.nearby_offer_inner .list_item:first');
        MobileNearby.parseFBXML($newOffer[0]);
      }    
    },

    addOfferAfterIfNeeded: function(currentOfferNr){
      var largestOfferNr = $(".nearby_offer_wrapper .list_item:last").data("nr");
      var offers = MobileNearby.offers;
      if (currentOfferNr < offers.length-2 && currentOfferNr + 2 > largestOfferNr ){
        // console.log("Moving to next with current at: "+currentOfferNr);
        MobileNearby.$nearby_offer_inner.append(MobileNearby.template({
          offer: offers[currentOfferNr+2],
          offerNr: currentOfferNr+2
        }));
        var $newOffer = $('.nearby_offer_inner .list_item:last');
        MobileNearby.parseFBXML($newOffer[0]);
      }
    },


    showInfobox: function(offer){
      var centerPoint = new google.maps.LatLng(offer.lat, offer.lon);
      new OfferInfoBox().show(offer.merchant, centerPoint, this.map);
      setTimeout(this.cleanupInfoboxes, 0);
    },

    cleanupInfoboxes: function(){
      if ($(".infoBox").length > 1){
        $(".infoBox:first").remove();
      }
    },

    createQueryParams: function(){
      var result = [];
      result.push("lat=");
      result.push(Param.byName("lat"));
      result.push("&lon=");
      result.push(Param.byName("lon"));
      result.push("&merchant_type_id=");
      result.push(Param.byName("merchant_type_id"));
      result.push("&favorite=");
      result.push(Param.byName("favorite"));
      result.push("&time=");
      result.push(Param.byName("time"));
      result.push("&distance=");
      result.push(Param.byName("distance"));
      result.push("&offset=");
      result.push(this.offset);
      return result.join("");
    },

    loadOffers: function(){
      this.addOffers(JSON.parse(LocalStorage.loadOffers()));
    },

    initMain:function () {
      $(document).ready(function () {
        if ( $(".nearby_page").length > 0 ){
          MobileNearby.$nearby_offer_inner = $('.nearby_offer_inner');
          MobileNearby.handleFavorite();
          MobileNearby.handleActions();
        };
      });
    }, 

    selectActiveOffer: function(){
      var offerId = Param.byName("offer_id");
      if (offerId === "") { return };
      offerId = parseInt(offerId, 10);
      var offerNr = MobileNearby.offerNrById(offerId);

      if (offerNr !== undefined){
        MobileNearby.offersLoader(offerNr)();
        MobileNearby.refreshFavoriteOnLoad = offerNr;
      }
    },

    offerNrById: function(id, offers){
      offers = offers || this.offers;
      var i;
      for (i = 0; i < offers.length ; i++){
        if (offers[i].id === id){
          return i;
        }
      }
    }
  };
// Pass in jQuery.
})(jQuery);

MobileNearby.initMain();