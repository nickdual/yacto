//It's a patch so that swipe "to the left" won't cycle from the first to the last. It should just stop, not go further.
(function($){
  $(function(){
    var Carousel = $.fn.carousel.Constructor;
    var oldSlide = Carousel.prototype.slide;
    Carousel.prototype.slide = function(type){
      if ( type ==="prev" && $(".list_item.active").index() === 0 ){
        return this;
      }
      return oldSlide.apply(this, arguments);
    } 
    
  });
})(jQuery);