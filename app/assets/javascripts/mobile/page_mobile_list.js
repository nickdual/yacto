//= require ../lib/jquery.hammer
//= require ../lib/jquery.specialevent.hammer

var MobileList = (function ($) {
  return {
    actionsTemplate: JST["mobile/templates/mobile_list_actions"],
    itemTemplate: JST["mobile/templates/mobile_list_item"],
    offers:[],
    PAGE_SIZE:15,
    offset:0,
    SWIPPING_THREASHOLD:10,  // how far should we scroll before deciding it's not a swipe
    SCROLL_LOAD_THRESHOLD: 20, // how far from the bottom should we start to load new offers

    handleActions:function () {
      var list_item = $('.list_item:not(.with_handlers)');

      list_item.bind("dragend", this.showActions);
      list_item.bind("swipe", this.showActions);
      list_item.bind("dragstart", this.hideActions);
      list_item.bind("tap", this.hideActions);

      list_item.addClass("with_handlers");
    },

    hideActions:function (e) {
      MobileList.windowTopAtStart = $(window).scrollTop();
      var $target = $(e.target);
      if ($target.find(".list_actions").length === 0) {
        $('.list_actions').remove();
      }
    },

    showActions:function (event) {
      event.preventDefault();
      if ((event.direction === 'right' || event.direction === 'left')) {
        var windowTopAtEnd = $(window).scrollTop();
        if (Math.abs(MobileList.windowTopAtStart - windowTopAtEnd) > MobileList.SWIPPING_THREASHOLD) {
          return;
        }
        var actionsTemplate = MobileList.actionsTemplate;
        var current = $(this);
        var offerNr = current.parents(".list_item_wrapper").index();
        if (current.find('.list_actions').length === 0) {
          $('.list_actions').remove();

          current.append(actionsTemplate({offer:MobileList.offers[offerNr]}));
          current.find("a.gplus").click(MobileList.googlePlusClick);


          if (typeof window.FB !== "undefined") {
            FB.XFBML.parse(current[0], function () {
            });
          }
        } else {
          current.find('.list_actions').remove();
        }
      } else if (event.direction === 'up' || event.direction === 'down') {
        return false;
      }
    },
    initiScroll:function () {
      this.myScroll = new iScroll('offer_list',{
        onScrollMove: MobileList.iScrollMoved, 
        onScrollEnd: MobileList.iScrollMoved
      });

      document.addEventListener('touchmove', function (e) {
        e.preventDefault();
      }, false);

    },

    iScrollMoved: function(){
      if (this.y < (this.maxScrollY + MobileList.SCROLL_LOAD_THRESHOLD )){
        MobileList.fetchMoreOffers();
      }
    },

    googlePlusClick:function (e) {
      e.preventDefault();
      var href = $(this).attr("href");
      window.open(href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
      return false;
    },

    addOffers:function (offers) {
      var i, oldOffers = MobileList.offers;
      for (i = 0; i < offers.length; i++) {
        oldOffers.push(offers[i]);
      }
      this.updateOffset();
      this.addFavoritesToOffers(offers);

      LocalStorage.saveOffers(oldOffers);

      if (offers.length === 0) {
        MobileList.isLastPage = true;
      }
    },

    updateOffset: function(){
      //had it not been for broken data I could just use length here :(
      var brokenOffers = (this.PAGE_SIZE - (this.offers.length % this.PAGE_SIZE)) % this.PAGE_SIZE;
      this.offset = this.offers.length + brokenOffers;
    },

    handleFavorite:function () {
      $(".list_items_wrapper").on('click', '.list_item .fav', function (e) {
        e.preventDefault();
        e.stopPropagation();

        var favOfferNr = $(this).parents(".list_item").index();
        MobileList.favOfferNr = favOfferNr;

        Favorites.favoriteObject = MobileList.offers[favOfferNr];
        Favorites.favoriteObjectType = "offer";
        Favorites.onFavorite();
      });

      Favorites.favoriteAddedCallback = MobileList.favoriteAddedCallback;
      Favorites.favoriteRemovedCallback = MobileList.favoriteRemovedCallback;
      Favorites.favoritesFetchedCallback = MobileList.favoritesFetchedCallback;
    },

    favoriteAddedCallback:function (data) {
      $(".list_item:nth-child(" + (MobileList.favOfferNr + 1) + ") .fav").addClass('starred');
      Favorites.currentFavObj().favorite = {id:data.id};
    },

    favoriteRemovedCallback:function () {
      $(".list_item:nth-child(" + (MobileList.favOfferNr + 1) + ") .fav").removeClass('starred');
      Favorites.currentFavObj().favorite = undefined;
    },

    favoritesFetchedCallback:function (data) {
      MobileList.favorites = data;
      MobileList.addFavoritesToOffers(MobileList.offers);
    },

    addFavoritesToOffers:function (offers) {
      if (MobileList.favorites === undefined) {
        return
      }
      var i = 0, offerNr, favorites = MobileList.favorites, favorite;
      for (i = 0; i < favorites.length; i++) {
        favorite = favorites[i];
        offerNr = MobileList.offerNrById(favorite.offer_id, offers);
        if (offerNr !== undefined) {
          $(".list_item:nth-child(" + offerNr + ") .fav").addClass('starred');
          MobileList.offers[offerNr].favorite = {id:favorite.id};
        }
      }
    },

    //refactor maybe - also in MobileNearby
    offerNrById:function (id, offers) {
      offers = offers || this.offers;
      var i;
      for (i = 0; i < offers.length; i++) {
        if (offers[i].id === id) {
          return i;
        }
      }
    },

    preloadActions:function () {
      //waiting a bit - giving normal page load a head start - 300ms
      if (MobileList.offers.length > 0) {
        setTimeout(function () {
          var current = $(".list_item[data-nr=-1]");
          current.append(MobileList.actionsTemplate({offer:MobileList.offers[0]}));
        }, 300);
      }
    },

    offersFetched:function () {
      MobileList.fetching = false;
      if (!MobileList.isLastPage) {
        MobileList.myScroll.refresh();
      }
    },

    fetchMoreOffers:function () {
      if (MobileList.haveOffersStored()){
        MobileList.showMoreOffers();
      } else{
        if (!MobileList.fetching){
          MobileList.fetching = true;
          $.ajax({
            data:MobileList.createQueryParams(),
            url:"/offers.js",
            success:MobileList.offersFetched,
            error: function(){ MobileList.fetching = false}
          })
        }
      }
    },

    haveOffersStored: function(){
      return $("#offer_list .list_item_wrapper").length < MobileList.offers.length
    },

    showMoreOffers: function(){
      var i, template = MobileList.itemTemplate,
        offers = MobileList.offers, 
        $container = $("#offer_list .list_items_wrapper"),
        numberOffersShown = $("#offer_list .list_item_wrapper").length;

      for (i = 0; i < MobileList.PAGE_SIZE; i++){
        if (offers.length <= i+numberOffersShown) { break; };
        $container.append(template({offer: offers[i+numberOffersShown]}));
      }
      MobileList.myScroll.refresh();
      MobileList.handleActions();
    },

    createQueryParams:function () {
      var result = [];
      result.push("lat=");
      result.push(Param.byName("lat"));
      result.push("&lon=");
      result.push(Param.byName("lon"));
      result.push("&merchant_type_id=");
      result.push(Param.byName("merchant_type_id"));
      result.push("&favorite=");
      result.push(Param.byName("favorite"));
      result.push("&time=");
      result.push(Param.byName("time"));
      result.push("&distance=");
      result.push(Param.byName("distance"));
      result.push("&offset=");
      result.push(this.offset);
      return result.join("");
    },

  loadOffers: function(){
    this.addOffers(JSON.parse(LocalStorage.loadOffers()));
  },

    initMain:function () {
      $(document).ready(function () {
        if ($("body.merchant_list_body").length > 0) {
          MobileList.handleActions();
          MobileList.handleFavorite();
          MobileList.initiScroll();
        }
      });
    }
  };
// Pass in jQuery.
})(jQuery);


MobileList.initMain();