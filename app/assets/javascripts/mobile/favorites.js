// Usage:
// set favoriteObject (with id field)
// set favoriteObjectType (string - "merchant" or "offer" - can by anything - for now these two supported)
// overwrite favoriteAddedCallback (favorite object passed as parameter)
// overwrite favoriteRemovedCallback (favorite object passed as parameter)
// overwrite favoritesFetchedCallback (favorites array passed as parameter)

Favorites = {
  currentFavObj: function(){
    return Favorites.favoriteObject;
  },
  currentFavObjType: function(){
    return Favorites.favoriteObjectType;
  },

  onFavorite: function(){
    if (Login.isLoggedIn()){
      Favorites.addOrRemoveFav();
    } else {
      Favorites.loginAndAddToFav();
    }
  },

  addOrRemoveFav: function(){
    if (Favorites.currentFavObj().favorite){
      Favorites.removeFromFavorites();
    } else {
      Favorites.addToFavorites();
    }
  },

  addToFavorites: function(){
    var data = {}, param = "favorite["+Favorites.currentFavObjType()+"_id]";
    data[param] = Favorites.currentFavObj().id;
    $.ajax({
      type: 'POST',
      url: '/favorites',
      dataType: 'json',
      data: data,
      success:  Favorites.favoriteAddedCallback
    });
  },
  
  favoriteAddedCallback: function(data){
  },

  removeFromFavorites: function(){
    $.ajax({
      type: 'POST',
      url: '/favorites/' + Favorites.currentFavObj().favorite.id,
      dataType: 'json',
      data: {"_method": "delete"},
      success: Favorites.favoriteRemovedCallback
    });
  },

  favoriteRemovedCallback: function(data){
  },

  loginAndAddToFav: function(){
    NewMobileLogin.show(Favorites.afterLoginCallback);
  },

  afterLoginCallback: function(){
    $.ajax({
      url: '/favorites',
      dataType: 'json',
      data: {offers: true},
      success:  Favorites.favoritesFetchedCallback,
    });
  },

  favoritesFetchedCallback: function(data){
  }
}