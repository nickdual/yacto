LocalStorage = {
  saveOffers: function(offers){
    var key = JSON.stringify(this.offersKey());
    localStorage.setItem(key, JSON.stringify(offers));
    $.cookie("offers_key", key, {expires: 1, path: '/'});
  },

  loadOffers: function(){
    return localStorage.getItem(JSON.stringify(this.offersKey()));
  },

  offersKey: function(){
    return [Param.byName("lat"), Param.byName("lon"), Param.byName("merchant_type_id")];
  }
}