class Login 
  callbacks: []
  addCallback: (callback) ->
    @callbacks.push(callback)

  onLogin: ->
    setTimeout(callback, 0) for callback in @callbacks

  isLoggedIn: ->
    $("meta[name=logged-in]").attr("content") == "true"

window.Login = new Login()

# use itself - we need to change the meta flag, so might as well use myself
window.Login.addCallback ->
  $("meta[name=logged-in]").attr("content", "true")