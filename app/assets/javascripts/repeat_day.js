$(function(){
  
  var mon = '',tue = '',wed = '' ,thu = '',fri= '',sat= '',sun= '',active = '';
  //sat value to field mon --- sun in offer


  $('.sunday').live('click', function(){
    if ($(this).is(':checked')) {
      sun = 'sun_check';
    }else{
      sun = 'sun_uncheck' ;
    }
    var sun_id = $(this).data('id');
    $.ajax({
      type: "PUT",
      url: "/offers/update_repeat_day/"+sun_id,
      data: {
        'offer[value]': sun
      },
      dataType: "json",
      success: function(msg){
        if(sun == 'sun_check'){
          $('input#sun_'+sun_id).attr('value','true');
        }
        else {
          $('input#sun_'+sun_id).attr('value','false');
        }

      },
      error: function(e){

      }
    });
      
  });

  $('.monday').live('click', function(){
    if ($(this).is(':checked')) {
      mon = 'mon_check';
    }else{
      mon = 'mon_uncheck' ;
    }
    var mon_id = $(this).data('id');
    $.ajax({
      type: "PUT",
      url: "/offers/update_repeat_day/"+mon_id,
      data: {
        'offer[value]': mon
      },
      dataType: "json",
      success: function(msg){
        if(mon== 'mon_check'){
            $('input#mon_'+mon_id).attr('value','true');
        }
        else {
            $('input#mon_'+mon_id).attr('value','false');
        }
        
      },
      error: function(e){

      }
    });
      
  })
  $('.tuesday').live('click', function(){
    if ($(this).is(':checked')) {
      tue = 'tue_check';
    }else{
      tue = 'tue_uncheck';
    }
    var tue_id = $(this).data('id');
    $.ajax({
      type: "PUT",
      url: "/offers/update_repeat_day/"+tue_id,
      data: {
        'offer[value]': tue
      },
      dataType: "json",
      success: function(msg){
        if(tue == 'tue_check'){
          $('input#tue_'+tue_id).attr('value','true');
        }
        else {
          $('input#tue_'+tue_id).attr('value','false');
        }

      },
      error: function(e){

      }
    });
      
  })
  $('.wednesday').live('click', function(){
    if ($(this).is(':checked')) {
      wed = 'wed_check';
    }else{
      wed = 'wed_uncheck';
    }
    var wed_id = $(this).data('id');
    $.ajax({
      type: "PUT",
      url: "/offers/update_repeat_day/"+wed_id,
      data: {
        'offer[value]': wed
      },
      dataType: "json",
      success: function(msg){
        if(wed== 'wed_check'){
          $('input#wed_'+wed_id).attr('value','true');
        }
        else {
           $('input#wed_'+wed_id).attr('value','false');
        }
      },
      error: function(e){

      }
    });
      
  })
  $('.thursday').live('click', function(){
    if ($(this).is(':checked')) {
      thu = 'thu_check';
    }else{
      thu = 'thu_uncheck';
    }
    var thu_id = $(this).data('id');
    $.ajax({
      type: "PUT",
      url: "/offers/update_repeat_day/"+thu_id,
      data: {
        'offer[value]': thu
      },
      dataType: "json",
      success: function(msg){
        if(thu == 'thu_check'){
          $('input#thu_'+thu_id).attr('value','true');
        }
        else {
          $('input#thu_'+thu_id).attr('value','false');
        }
      },
      error: function(e){

      }
    });
      
  })
  $('.friday').live('click', function(){
    if ($(this).is(':checked')) {
      fri = 'fri_check';
    }else{
      fri = 'fri_uncheck';
    }
    var fri_id = $(this).data('id');
    $.ajax({
      type: "PUT",
      url: "/offers/update_repeat_day/"+fri_id,
      data: {
        'offer[value]': fri
      },
      dataType: "json",
      success: function(msg){
        if(fri == 'fri_check'){
          $('input#fri_'+fri_id).attr('value','true');
        }
        else {
          $('input#fri_'+fri_id).attr('value','false');
        }
      },
      error: function(e){

      }
    });
      
  })
  $('.saturday').live('click', function(){
    if ($(this).is(':checked')) {
      sat = 'sat_check';
    }else{
      sat = 'sat_uncheck';
    }
    var sat_id = $(this).data('id');
    $.ajax({
      type: "PUT",
      url: "/offers/update_repeat_day/"+sat_id,
      data: {
        'offer[value]': sat
      },
      dataType: "json",
      success: function(msg){
        if(sat == 'sat_check'){
          $('input#sat_'+sat_id).attr('value','true');
        }
        else {
          $('input#sat_'+sat_id).attr('value','false');
        }
      },
      error: function(e){

      }
    });
      
  })



  // set value to local_end_day in offer
  $(".select_time").live('mouseover', function (e) {
    var offer_id = $(this).data('id');

    $("input#datepicker_"+offer_id).datepicker({
      dateFormat: "yy-mm-dd",
      onSelect: function(dateText, inst) {
        $.ajax({
          type: "PUT",
          url: "/offers/update_local_end_day/"+ offer_id,

          data: {
            "offer[value]" :dateText

          },
          success: function(msg){
            var local_end_day =  $("input#datepicker_"+offer_id).val();
            $('input.local_end_day').attr('value',local_end_day);
          },
          error: function(e){

          }

        });
      }
    })
  })

  //sat value to field mon --- sun in merchant
  //$("#merchant_open_hour").live('mouseover', function (e) {
  var merchant_id = $("#merchant_open_hour").data('id');
  //e.preventDefault();
  $('#sun_close_'+merchant_id).change(function(){
    if ($(this).is(':checked')) {
      sun = 'sun_check'
    }
    else{
      sun = 'sun_uncheck'
    }

    $.ajax({
      type: "PUT",
      url: "/merchants/update_repeat_merchant/"+merchant_id,
      data: {
        'merchant[value]': sun
      },
      dataType: "json",
      success: function(msg){

      },
      error: function(e){

      }
    });
          
  })
  $('#mon_close_'+merchant_id).change(function(){
    if ($(this).is(':checked')) {
      mon = 'mon_check'
    }else{
      mon = 'mon_uncheck'
    }
    $.ajax({
      type: "PUT",
      url: "/merchants/update_repeat_merchant/"+merchant_id,
      data: {
        'merchant[value]': mon
      },
      dataType: "json",
      success: function(msg){

      },
      error: function(e){

      }
    });
      
  })
  $('#tue_close_'+merchant_id).change(function(){
    if ($(this).is(':checked')) {
      tue = 'tue_check'
    } else {
      tue = 'tue_uncheck'
    }
    $.ajax({
      type: "PUT",
      url: "/merchants/update_repeat_merchant/"+merchant_id,
      data: {
        'merchant[value]': tue
      },
      dataType: "json",
      success: function(msg){

      },
      error: function(e){

      }
    });
      
  })
  $('#wed_close_'+merchant_id).change(function(){
    if ($(this).is(':checked')) {
      wed = 'wed_check'
    }else{
      wed = 'wed_uncheck'
    }
    $.ajax({
      type: "PUT",
      url: "/merchants/update_repeat_merchant/"+merchant_id,
      data: {
        'merchant[value]': wed
      },
      dataType: "json",
      success: function(msg){

      },
      error: function(e){

      }
    });
      
  })
  $('#thu_close_'+merchant_id).change(function(){
    if ($(this).is(':checked')) {
      thu = 'thu_check'
    }else{
      thu = 'thu_uncheck'
    }
    $.ajax({
      type: "PUT",
      url: "/merchants/update_repeat_merchant/"+merchant_id,
      data: {
        'merchant[value]': thu
      },
      dataType: "json",
      success: function(msg){

      },
      error: function(e){

      }
    });
      
  })
  $('#fri_close_'+merchant_id).change(function(){
    if ($(this).is(':checked')) {
      fri = 'fri_check'
    }else{
      fri = 'fri_uncheck'
    }
    $.ajax({
      type: "PUT",
      url: "/merchants/update_repeat_merchant/"+merchant_id,
      data: {
        'merchant[value]': fri
      },
      dataType: "json",
      success: function(msg){

      },
      error: function(e){

      }
    });
      
  })
  $('#sat_close_'+merchant_id).change(function(){
    if ($(this).is(':checked')) {
      sat = 'sat_check'
    }else{
      sat = 'sat_uncheck'
    }
    $.ajax({
      type: "PUT",
      url: "/merchants/update_repeat_merchant/"+merchant_id,
      data: {
        'merchant[value]': sat
      },
      dataType: "json",
      success: function(msg){

      },
      error: function(e){

      }
    });
      
  })

  var id_offer = '';
  $(".offer_merchant_type").live("click", function(e){
     id_offer = $(this).data('id');

  })


    $(".merchant_admin .offer_merchant_type select.chzn-select").live('change',function() {

        $.ajax({
            type: "PUT",
            url: "/offers/update_merchant_type/"+ id_offer,

            data: {
                "offer[value]" :$(this).val()
            },
            success: function(msg){
                var offer_merchant_type =  $("#offer_merchant_type_"+ id_offer).val();

                $('input.merchant_type').attr('value',offer_merchant_type);
            },
            error: function(e){
                alert("error")
            }

        });
    });

});

