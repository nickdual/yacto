//= require backbone/mobile_app
//= require lib/chosen
//= require jquery.cookie
//= require lib/bootstrap-modal
//= require lib/bootstrap-tab
//= require lib/bootstrap-carousel
//= require lib/iphone-style-checkboxes.js
//= require lib/jquery.validate

//= require ./map_directions
//= require ./facebook_utils
//= require ./params_utils
//= require mobile/iphone_utils
//= require ./login
//= require ./forgot_pass

// potentially useful libraries - add on demand
// require jquery_ujs
// require modernizr.min.js
// require i18n
// require i18n/translations
