
window.FacebookUtils = {
  init: function() {
    var appId;
    appId = $("meta[name=fb-app-id]").attr("content");
    if (typeof FB !== "undefined" && FB !== null) {
      return FB.init({
        appId: appId,
        status: true,
        cookie: true
      });
    }
  },
  postToFeed: function(merchant_name, merchant_id, offer_description) {
    var obj;
    obj = {
      method: 'feed',
      link: "https://blooming-sword.herokuapp.com/" + merchant_id,
      name: 'Yacto',
      description: "Check out this great limited-time special at " + merchant_name + " : www.yacto.com/" + merchant_id + " " + offer_description
    };
    if (typeof FB !== "undefined" && FB !== null) {
      return FB.ui(obj, function() {});
    } else {
      return setTimeout(function() {
        return alert("Problems loading Facebook - check you browser settings for extensions such as AdBlock or WidgetBlock");
      }, 0);
    }
  }
};

$(function() {
  return setTimeout(FacebookUtils.init, 0);
});
