window.FacebookUtils = {
  init: ->
    appId = $("meta[name=fb-app-id]").attr("content")
    if FB?
      FB.init({appId: appId, status: true, cookie: true})

  postToFeed: (merchant_name, merchant_id, offer_description) -> 

    # calling the API ...
    obj = {
      method: 'feed'
      link: "https://blooming-sword.herokuapp.com/#{merchant_id}"
      name: 'Yacto'
      description: "Check out this great limited-time special at #{merchant_name} : www.yacto.com/#{merchant_id} #{offer_description}"
    };
      
    if FB?
      FB.ui(obj, ->)
    else
      setTimeout( 
        -> 
          alert("Problems loading Facebook - check you browser settings for extensions such as AdBlock or WidgetBlock") 
        0)
}
$( -> setTimeout(FacebookUtils.init, 0) )