/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//= require jquery
//= require jquery_ujs

//= require jquery.ui.core
//= require jquery.ui.widget
//= require jquery.ui.position
//= require jquery.ui.autocomplete

//= require jquery.mobile-1.1.0.min
//= require klass.min
//= require jquery.mobile.simpledialog2
//= require jquery.mobile-checkbox-switch
//= require modernizr.min.js
//= require demo.js
//= require jquery.ui.map.js
//= require jquery.ui.map.extensions.js
//= require i18n
//= require i18n/translations
//= require ./offer_infobox
//= require_tree ./templates
//= require ./mobile_login
//= require ./map_directions
//= require ./login
//= require ./forgot_pass
