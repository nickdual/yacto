# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
jQuery ->
  $('input[value="Sent"]','form').mousedown ->
    message = $("#Body").val()
    request = $.post "/merchants/sms_request", From : $("#From").val(), Body: message
    request.success (data) ->
      alert(data.description)
  .click (e) ->
    e.preventDefault()

  $('#search_merchants').live "click", ->
    request = $.ajax url : "/merchants/search", method : "get", data :
      merchant_id     : $('#merchant_merchant_id').val(),
      name            : $('#merchant_name').val(),
      contact         : $('#merchant_administrative_contact').val(),
      mobile_number   : $('#merchant_mobile_number').val(),
      email           : $('#merchant_administrative_email').val(),
      primary_address : $('#merchant_primary_address').val()
    request.success (data) ->
      childNumber = 0
      for child in $("#merchant_list").children().children()
        if childNumber > 1
          $(child).hide()
        childNumber = childNumber + 1
      $("#merchant_list").append(data.merchant_rows_html)