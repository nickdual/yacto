class Yak.OffersRouter extends Backbone.Router
  routes:
    "" : "index"

  initialize: (@options) ->

  index: ->
    $("body").attr("class", "dashboard") 
    # $("#main").html(@view.render().el)
    $("#main").html(@getIndexView(@getIndexCollection(@options)).el)


  getIndexView: (offers) -> 
    if @view?
      _.defer( => @view.reatachEvents() )
      return @view 
    else
      @view = new Yak.OffersIndexView(collection: offers)
      @view.render()

  getIndexCollection: (options) ->
    return @offers if @offers?
    @offers = new Yak.Offers({}, options)
    @offers.fetch()
    @offers