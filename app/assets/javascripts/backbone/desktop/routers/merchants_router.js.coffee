class Yak.MerchantsRouter extends Backbone.Router
  routes:
    "merchants/:id" : "show"
    
  initialize: (@options) ->

  show: (id) ->
    $("body").attr("class", "merchant_page") 
#    $('head').append('<meta name="description" content="description 1" />')
#    $('head').append('<meta name="keywords" content="Fab, design, home, interiors, interior design, modern design, design deals" />')
#    $('head').append('<meta property="og:title" content="Henry Ferrera: Canvas Slip On Blue Denim, at 67% off!"/>')
#    $('head').append('<meta property="og:type" content="fab_graph_dev:product"/>')
#    $('head').append('<meta property="og:category" content="Womens Shoes &amp; Accessories"/>')
#    $('head').append('<meta property="og:image" content="http://s3.amazonaws.com/static.fab.com/product/196709-360x360-1345229919-primary.png"/>')
#    $('head').append('<meta property="og:site_name" content="Fab.com"/>')
#    $('head').append('<meta property="og:url" content="http://fab.com/sale/9531/product/196709?fref=fb-like"/>')
#    $('head').append('<meta property="og:description" content="I found this great design find on Fab.com. Visit Fab.com now and sign up for exclusive member-only offers. Fab is the marketplace for discovering everyday design. Smile, you&#39;re designed to!"/>')
    model = new Yak.Merchant()
    model.id = id
    model.fetch()
    @view = new Yak.MerchantView(model: model, options: @options)
    $("#main").html(@view.render().el)
    

