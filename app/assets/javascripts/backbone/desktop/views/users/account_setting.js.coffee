class Yak.Account_Setting extends Backbone.View
  template: JST["backbone/desktop/templates/users/account_setting"]

  events:
   # "click .subsection_title" : "collapseSubsection"
    "click .info_save": "account_setting"

  render: ->
    $(@el).html(@template())
    _.defer(@initSwitches)
    _.defer(@initDropDowns)

    @


  initSwitches: ->
    $('.post_on_twitter').iphoneStyle(
      checkedLabel:'<img src="../assets/ios-style-checkboxes/post_twitter.png">',
      uncheckedLabel:'<img class="image_off" src="../assets/ios-style-checkboxes/post_twitter.png">'
      onChange: (elem, value) ->
        if value
          $.ajax
            type: "GET"
            url: "/home/ajax_twitter_on"
            success:  ->
              $.cookie('twitter_on', true)
            error: ->
              console.log("There is an error : can't set data")
        else
          $.ajax
            type: "GET"
            url: "/home/ajax_twitter_off"
            success:  ->
              $.cookie('twitter_on', false)
            error: ->
              console.log("There is an error : can't set data")
    )

    $('.post_on_facebook').iphoneStyle(
      checkedLabel:'<img src="../assets/ios-style-checkboxes/post_facebook.png">',
      uncheckedLabel:'<img class="image_off" src="../assets/ios-style-checkboxes/post_facebook.png">'
      onChange: (elem, value) ->
        if value
          $.ajax
            type: "GET"
            url: "/home/ajax_facebook_on"
            success:  ->
              $.cookie('facebook_on', true)
            error: ->
              console.log("There is an error : can't set data")
        else
          $.ajax
            type: "GET"
            url: "/home/ajax_facebook_off"
            success:  ->
              $.cookie('facebook_on',false)
            error: ->
              console.log("There is an error : can't set data")

    )



  initDropDowns: ->
    $('.chzn-select').chosen({disable_search_threshold:10});


  account_setting: (e) ->
    id = $("#user_id").val()
    gender = ''
    if($("#gender_dd").val() == 'Female')
     gender = 1
    else if($("#gender_dd").val() == 'Male')
        gender = 0
      else gender = ''

    pass =  $("#user_password").val()
    length =  $("#user_password").val().length
    pass_confirm =  $("#user_confirm_password").val()
    pass_current =  $("#user_current_password").val()
    address_one = $("#address_line_one").val()

    if(pass == pass_confirm && pass =='' )
      $("#loading_update_merchant_info").show();
      document.getElementById('password_not_match').style.display='none';
      document.getElementById('current_password_blank').style.display='none';
      document.getElementById('length_pass').style.display='none';

      $.ajax
        type: "PUT"
        url: "/users/account_setting/" + id
        beforeSend: (xhr) ->
          xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        data:
          "user[email]" : $("#user_email").val()
          "user[mobile_number]" : $("#user_mobile_number").val()
          "user[gender]" : gender
          "user[address_line1]" : $("#address_line_one").val()
          "user[address_line2]" : $("#address_line_two").val()
          "user[city_name]" : $("#city_field").val()
          "user[postal_code]" : $("#postal_code_field").val()
          "user[state_province_region]" : $("#state_field").val()
          "user[country]" : $("#country_field").val()

        dataType: "json"
        success: (msg) ->
          $("#loading_update_merchant_info").hide();
          alert('Update information successful!')
          location.reload()
        error: (e) ->
          str = "Your address is wrong. Please make sure it's right!"
          alert str
          $("#loading_update_merchant_info").hide();

#    else if(pass == pass_confirm && pass =='' && address_one == '')
#      document.getElementById('address_one').style.display='block';



    else if(pass == pass_confirm && pass !='' && pass_current != '' && length >= 6 )
      $("#loading_update_merchant_info").show();
      document.getElementById('password_not_match').style.display='none';
      document.getElementById('current_password_blank').style.display='none';
      document.getElementById('length_pass').style.display='none';

      $.ajax
        type: "PUT"
        url: "/users"
        beforeSend: (xhr) ->
          xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        data:
          "user[email]" : $("#user_email").val()
          "user[password]" : $("#user_password").val()
          "user[password_confirmation]":$("#user_confirm_password").val()
          "user[current_password]": $("#user_current_password").val()


        dataType: "json"
        success: (msg) ->
          $("#user_password").val('')
          $("#user_confirm_password").val('')
          $("#user_current_password").val('')
          $.ajax
            type: "PUT"
            url: "/users/account_setting/" + id
            beforeSend: (xhr) ->
              xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
            data:
              "user[email]" : $("#user_email").val()
              "user[mobile_number]" : $("#user_mobile_number").val()
              "user[gender]" : gender
              "user[address_line1]" : $("#address_line_one").val()
              "user[address_line2]" : $("#address_line_two").val()
              "user[city_name]" : $("#city_field").val()
              "user[postal_code]" : $("#postal_code_field").val()
              "user[state_province_region]" : $("#state_field").val()
              "user[country]" : $("#country_field").val()

            dataType: "json"
            success: (msg) ->
              $("#loading_update_merchant_info").hide();
              alert('Update information successful!')
              location.reload()
            error: (e) ->
              str = "Your address is wrong. Please make sure it's right!"
              alert str
              $("#loading_update_merchant_info").hide();
          $("#loading_update_merchant_info").hide();

        error: (e) ->
          str = "Current password "
          data = ""
          temp = ""
          data = JSON.parse(e.responseText);
          for key of data
            temp =   data[key]
            console.log(temp)
            for key of temp
              str += temp[key]
          alert str
          $("#loading_update_merchant_info").hide();




    else if(pass == pass_confirm && pass !='' && pass_current != '' && length < 6 )
      document.getElementById('password_not_match').style.display='none';
      document.getElementById('length_pass').style.display='block';
      document.getElementById('current_password_blank').style.display='none';


    else if(pass == pass_confirm && pass !='' && pass_current == '' )
      document.getElementById('password_not_match').style.display='none';
      document.getElementById('length_pass').style.display='none';
      document.getElementById('current_password_blank').style.display='block';

    else
      document.getElementById('password_not_match').style.display='block';
      document.getElementById('length_pass').style.display='none';
      document.getElementById('current_password_blank').style.display='none';


    e.preventDefault()



