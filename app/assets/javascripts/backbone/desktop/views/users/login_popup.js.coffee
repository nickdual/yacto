class Yak.LoginPopup extends Backbone.View
  template: JST["backbone/desktop/templates/users/login_popup"]
  events:
    "click a.forgot_password": "forgotPassword"
    "click .back_to_login" : "backLogin"
    "click .reset_password" : "resetPassword"
    "click #signinButton" : "signinButton"
    "click #signupButton" : "signupButton"
    "click .submit_signin " : "submitSignin"
    "click .submit_signup" : "submitSignup"
  render: ->
    $(@el).html(@template())
    @
  show:  ->
    #$("#login_popup").html(@template())

    $('.post_on_twitter').iphoneStyle(
      checkedLabel:'<img src="../assets/ios-style-checkboxes/post_twitter.png">',
      uncheckedLabel:'<img class="image_off" src="../assets/ios-style-checkboxes/post_twitter.png">'
      onChange: (elem, value) ->
        if value
          $.ajax
            type: "GET"
            url: "home/ajax_twitter_on"
            success:  ->
              $.cookie('twitter_on', true)
            error: ->
              console.log("There is an error : can't set data")
        else
          $.ajax
            type: "GET"
            url: "home/ajax_twitter_off"
            success:  ->
              $.cookie('twitter_on', null)
            error: ->
              console.log("There is an error : can't set data")

    )
    $('.post_on_facebook').iphoneStyle(
      checkedLabel:'<img src="../assets/ios-style-checkboxes/post_facebook.png">',
      uncheckedLabel:'<img class="image_off" src="../assets/ios-style-checkboxes/post_facebook.png">'
      onChange: (elem, value) ->
        if value
          $.ajax
            type: "GET"
            url: "home/ajax_facebook_on"
            success:  ->
              $.cookie('facebook_on', true)
            error: ->
              console.log("There is an error : can't set data")
        else
          $.ajax
            type: "GET"
            url: "home/ajax_facebook_off"
            success:  ->
              $.cookie('facebook_on',null)
            error: ->
              console.log("There is an error : can't set data")
    )
    $('.post_on_gplus').iphoneStyle(
      checkedLabel:'<img src="../assets/ios-style-checkboxes/post_gplus.png">',
      uncheckedLabel:'<img class="image_off" src="../assets/ios-style-checkboxes/post_gplus.png">'
      onChange: (elem, value) ->
        if value
          $.ajax
            type: "GET"
            url: "home/ajax_gplus_on"
            success:  ->
              $.cookie('gplus_on', true)
            error: ->
              console.log("There is an error : can't set data")
        else
          $.ajax
            type: "GET"
            url: "home/ajax_gplus_off"
            success:  ->
              $.cookie('gplus_on', null)
            error: ->
              console.log("There is an error : can't set data")
    )

    response = undefined
    $.validator.addMethod "unique_email", ( value = ->
      $.ajax
        type: "POST"
        url: "/offers/unique_email"
        data: "email=" + $("#signup_email").val()
        dataType: "text"
        success: (msg) ->
          response = if (msg is "true") then true else false
      response
    ), "Email is already taken"

    $("form.signup_form").validate
      rules:
        signup_email:
          required: true
          email: true
          unique_email: true
        signup_password:
          required: true
          minlength: 6

    $("form.signin_form").validate
      rules:
        signin_email:
          required: true
          email: true
        signin_password:
          required: true
          minlength: 6
    $("form.forgot_form").validate
      rules:
        forgot_email:
          required: true
          email: true
  forgotPassword: (e) ->
    e.preventDefault()
    $('.signin_div').hide()
    $('.forgot_div').fadeIn()
  backLogin: (e) ->
    e.preventDefault()
    $('.forgot_div').hide()
    $('.signin_div').fadeIn()
  resetPassword: (e) ->
    e.preventDefault()
    $("#loading_reset_password").show()
    if $(".signin_field.forgot_email").valid()
      $.ajax
        type: "POST"
        url: "/users/password"
        beforeSend: (xhr) ->
          xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        data: "user[login]=" + $(".signin_field.forgot_email").val()
        dataType: "json"
        success: (msg) ->
          $("#loading_reset_password").hide()
          alert "You will receive an email with instructions about how to reset your password in a few minutes."
          $('#signinModal').modal('hide')
        error: (e) ->
          $("#loading_reset_password").hide()
          alert "The email is not existed"
    else
      alert "We cannot locate an account with that email address, please check the email entered"
  signinButton: (e) ->
    e.preventDefault()
    $('#signinModal').modal('hide')
    $('#signup_Modal').modal()
  signupButton: (e) ->
    e.preventDefault()
    $('#signup_Modal').modal('hide')
    $('#signinModal').modal()
  submitSignin: (e) ->
    if $("#signin_email").valid() and $("#user_password").valid()
      $.ajax
        type: "POST"
        url: "/users/sign_in"
        beforeSend: (xhr) ->
          xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        data: "user[login]=" + $(".signin_form .signin_email").val() + "&user[password]=" + $(".signin_form .signin_password").val()
        dataType: "json"
        success: (msg) ->
          $.cookie('user_id', msg.id)
          $('#signinModal').modal('hide')
          $("li.main-navigation-link.last").html('<a class="header-profile" href="/users/edit">Profile</a><a rel="nofollow" data-method="delete" class="header-sign-out" href="/users/sign_out">/ SignOut</a>')

#          alert "Sign in OK"
          $("#login_error").html("")

        error: (e) ->
          str = ""
          data = ""
          data = JSON.parse(e.responseText)
          for key of data
            str +=   data[key]

          $("#login_error").html(str)

    else

      #$("#login_error").html("username or password is incorrect, please try it again!")

    e.preventDefault()
  submitSignup: (e) ->
    date = new Date()


    if $("#signup_email").valid() and $("#signup_password").valid()
      $.ajax
        type: "POST"
        url: "/users"
        beforeSend: (xhr) ->
          xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        data: "user[email]=" + $(".signup_form .signup_email").val() + "&user[password]=" + $(".signup_form .signup_password").val() + "&user[first_name]=Update me" + "&user[last_name]=Update me" + "&user[username]=" + $(".signup_form .signup_email").val()
        dataType: "json"
        success: (msg) ->
          $("#signup_Modal").modal('hide')
          $("#myModal_sign_up").modal('show')
          $("li.main-navigation-link.last").html('<a class="header-profile" href="/users/edit">Profile</a><a rel="nofollow" data-method="delete" class="header-sign-out" href="/users/sign_out">/ SignOut</a>')
        error:(e) ->
          $("#signup_error").html("sign up unsuccessfuly. Please try again later.")
    else
     # $("#signup_error").html("Email or password is invaild, please try it again!")

    e.preventDefault()



