class Yak.MerchantView extends Backbone.View

  template: JST["backbone/desktop/templates/merchants/show"]
  render: ->
    $(@el).html(@template())
    @renderWidgets()
    @

  renderWidgets: ->
    @renderHeroWidget()
    @renderOffersWidget()
    @renderAboutWidget()
    @renderMapWidget()
    @renderBusinessWidget()
    @renderPhotoStackWidget()
    @render_header()
#later when logic is added and these methods still look very much alike we can refactor it into a loop iterating over hash {selector => className}
  renderAboutWidget: ->
    view = new Yak.MerchantAboutWidgetView(model: @model)
    @$("#about_widget").html(view.el)

  renderOffersWidget: ->
    view = new Yak.MerchantOffersWidgetView(model: @model)
    @$("#offers_widget").html(view.el)

  renderPhotoStackWidget: ->
    view = new Yak.MerchantPhotoStackWidgetView(model: @model)
    @$("#photostack_widget").html(view.el)

  renderBusinessWidget: ->
    view = new Yak.MerchantBusinessWidgetView(model: @model, options: @options.options)
    @$("#business_widget").html(view.el)

  renderMapWidget: ->
    view = new Yak.MerchantMapWidgetView(model: @model)

    @$("#map_widget").html(view.el)

  renderHeroWidget: ->
    view = new Yak.MerchantHeroWidgetView(model: @model)
    @$("#hero_widget").html(view.el)

  render_header: ->

    view = new Yak.MerchantHeader(model: @model)

    #$('head').append(JST["backbone/desktop/templates/merchants/header"](merchant: @model.toJSON()))
