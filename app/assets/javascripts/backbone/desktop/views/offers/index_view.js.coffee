class Yak.OffersIndexView extends Backbone.View
  template: JST["backbone/desktop/templates/offers/index"]

  events:
    "click .offer-type-filter": "filterOffers"
    "change .chzn-select": "filtersChanged"
    "blur .where-filter-wrapper input" : "filtersChanged"
    "click .post-an-offer": "postOffer"

  initialize: ->
    @collection.on("reset", @renderOffers, @)
    @collection.on("reset", @updateMap, @)
    @collection.on("reset", @attachInifiniteScroll, @)

  render: ->
    $(@el).html(@template(city: @collection.city))
    _.defer(@initializeMap)
    _.defer(@upgradeTemplate)
    _.defer(@renderPostOfferForm)
    @

  reatachEvents: ->
    @delegateEvents()
    _.each(@offersViews, (view, id) -> view.delegateEvents() )

    #TODO - maybe refactor map section into separate view (it getting big and most is just google.maps logic)
  initializeMap: =>
    latlng = new google.maps.LatLng( @collection.lat, @collection.lon )
    myOptions = {
      zoom: @collection.zoom()
      center: latlng
      mapTypeId: google.maps.MapTypeId.ROADMAP
      scrollwheel: false
    }
    @map = new google.maps.Map(document.getElementById("map_show"), myOptions)
    @markers = []
    @geolocateCenter()
    @geocoder = new google.maps.Geocoder()


  updateMap: ->
    @collection.each(@addMarker, @)
    @resetCenter()

  removeMarkers: () =>
    _.each(@markers, @removeMarker)
    @markers = []

  removeMarker: (marker) =>
    marker.setMap(null)

  addMarker: (offer) ->
    unless offer.added
      point = new google.maps.LatLng( offer.get("lat"), offer.get("lon") )
      icon = "/assets/markers/#{offer.get("merchant").get("merchant_type").get("marker_image")}"
      marker = new google.maps.Marker(
        position: point
        map: @map
        icon: icon
      )
      offer.added = true
      @markers.push(marker)

      google.maps.event.addListener(marker, 'click', () =>
        @offersViews[offer.get("id")]?.expandOffer(null, true)
      )


  resetCenter: ->
    return if @collection.length == 0
    @setCenter(@collection.averageLat(), @collection.averageLon())
    @map.setZoom(@collection.zoom())

  renderOffers: ->
    @$("#offers-list").empty()
    @offersViews = {}
    @collection.each(@renderOffer, @)

  appendOffers: ->
    @collection.each(@appendOffer, @)

  appendOffer: (offer) ->
    @renderOffer(offer, false)

  renderOffer: (offer, reset = true) ->
    if !offer.rendered || reset
      view = new Yak.OfferView(model: offer, map: @map)
      @$("#offers-list").append(view.render().el)
      offer.rendered = true
      @offersViews[offer.get("id")] = view
    
  upgradeTemplate: =>
    @$('.chzn-select').chosen(disable_search_threshold:10)
    @$('.offer-type-filter').tooltip()


  filterOffers: (e) ->
    e.preventDefault()
    @$('.offer-type-filter.active').removeClass('active')
    $(e.target).addClass('active')
    @filtersChanged(e)
    
  renderPostOfferForm: ->
    if $('div#signupModal').html() == ""
      $('div#signupModal').html(new Yak.OffersOfferView().render().el)

  postOffer: (e) ->
    e.preventDefault()
    form = new Yak.OffersOfferView()
    if $('div#signupModal').html() == ""
      $('div#signupModal').html(form.render().el)
    else
      $("div#signupModal").modal()
    
     
  filtersChanged: (e) ->
    @collection.city = @$(".where-filter-wrapper input").val()
    @collection.time = @$(".when-filter-wrapper select").val()
    @collection.distance = @$(".distance-filter-wrapper select").val()
    @collection.type = @$(".offer-type-filter.active").data("type")
    @collection.firstPage()
    @collection.fetch()
    @removeMarkers()

  attachInifiniteScroll: =>
    $('#lastPostLoader').hide()
    $(window).unbind('scroll', @scrollHandler)
    $(window).scroll(@scrollHandler) unless @collection.isLastPage()
    
  scrollHandler: (e) =>
    h_document = $(document).height()
    wall = h_document - 100
    h_window = $(window).height()
    if  $(window).scrollTop() >= ( wall -  h_window)
      $(window).unbind('scroll', @scrollHandler)
      $('#lastPostLoader').show()
      @collection.nextPage()
      @collection.fetch(add:true, success: @offersAdded)

  offersAdded: =>
    @appendOffers()
    @attachInifiniteScroll()
    @updateMap()

  geolocateCenter: ->
    if navigator.geolocation?
      navigator.geolocation.getCurrentPosition(@geolocationSuccess)

  geolocationSuccess: (position) =>
    @setCenter(position.coords.latitude, position.coords.longitude)
    @map.setZoom(Yak.Offers.zoom(position.coords.accuracy/1609.3)) if position.coords.accuracy?
    @reverseGeocode(position.coords.latitude, position.coords.longitude)


  setCenter: (lat, lon) ->
    point = new google.maps.LatLng(lat, lon)
    @map.setCenter(point)

  reverseGeocode: (lat, lon) ->
    latlng = new google.maps.LatLng(lat, lon)
    @geocoder.geocode({"latLng": latlng}, @reverseGeocodeSuccess)

  reverseGeocodeSuccess: (results, status) =>
    if status == google.maps.GeocoderStatus.OK && results[0]?
      @$(".where-filter-wrapper input").val(results[0].formatted_address)
      @filtersChanged()
