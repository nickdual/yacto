class Yak.MerchantSubView extends Backbone.View
  el: "#merchant_merchant_type_id"
  template: JST['backbone/desktop/templates/offers/merchant_sub']
  initialize: ->

  render: ->
    model = new Yak.MerchantSub()
    model.fetch()
    $(@el).html(@template(merchant_type: model.toJSON()))