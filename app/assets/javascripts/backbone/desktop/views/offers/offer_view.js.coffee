class Yak.OffersOfferView extends Backbone.View
  tagName: 'div'
  template: JST['backbone/desktop/templates/offers/offer']
  events:
    "click .createOffer-submit" : "createOffer"
    "click .businessDetails-submit" : "businessDetails"
    "click .socialMedia-submit": "socialMedia"
    "click .personalDetails-submit": "personalDetails"
    "click .merchant-detail-submit" : "merchantDetailSubmit"
    "click #create-facebbook": "createFacebbook"
    "click #create-twitter": "createTwitter"
    "click #create-gplaces":"createGplaces"
    "click #create-yelp": "createYelp"
    "click #create-yahoo": "createYahoo"
  set_value = (value, object) ->
    object.val value  unless value is "undefined"

  postOffer:() ->
    signupCarousel = $("#signupCarousel")
    signupModal = $("#signupModal")
    modalInput = $(".modalInput")
    required_email = $("#createOffer_email")
    required_textOffer = $("#offerText")
    required_businessDetails = $("#businessName_address")
    required_mobile = $("#personalInfo_mobile")
    #open modal window
    #$("#signupModal").modal()
    #@postOfferValidation()
    required_array = [0, 0, 0, 0, 0]
    ok = 0
    progress_bar = $(".progress")
    response = undefined
    $.validator.addMethod "unique_email", ( value = ->
      $.ajax
        type: "POST"
        url: "offers/unique_email"
        data: "email=" + value
        dataType: "text"
        success: (msg) ->
          response = if (msg is "true") then true else false
      response
    ), "Email is already taken"
    @$(".createOffer-form").live "focus",->
      $(this).validate
        onfocusout: (element) ->
          $(element).valid()
          ok = 0
          if $("#createOffer_email").valid()
            required_array[0] = 1
          else
            required_array[0] = 0
          if $("#offerText").valid()
            required_array[1] = 1
          else
            required_array[1] = 0
          i = 0
          len = required_array.length
          while i < len
            ok += required_array[i]
            i++
          $("span.ok").text ok

          $(".progress").animate width: 66 * ok
        rules:
          createOffer_email:
            required: true
            email: true
            unique_email: true
          offerText:
            minlength: 5
            maxlength: 140
            required: true

    $(".businessDetails-form ").live "focus", ->
      $(this).validate
        onfocusout: (element) ->
          $(element).valid()
          ok = 0
          if $("#businessName_address").valid()
            required_array[2] = 1
          else
            required_array[2] = 0
          if ($("#mon-fri-open").val() isnt "" and $("#mon-fri-close").val() isnt "") or ($("#sat-open").val() isnt "" and $("#sat-close").val() isnt "") or ($("#sun-open").val() isnt "" and $("#sun-close").val() isnt "")
            required_array[3] = 1
          else
            required_array[3] = 0
          i = 0
          len = required_array.length

          while i < len
            ok += required_array[i]
            i++
          $("span.ok").text ok
          $(".progress").animate width: 66 * ok
        rules:
          businessName_address:
            required: true

          businessHours:
            required: true
    $(".personalInfo-form ").live "focus", ->
      $(this).validate
        onfocusout: (element) ->
          $(element).valid()
          ok = 0
          if $("#personalInfo_mobile").valid()
            required_array[4] = 1
          else
            required_array[4] = 0
          i = 0
          len = required_array.length

          while i < len
            ok += required_array[i]
            i++
          $("span.ok").text ok
          progress_bar.animate width: 66 * ok
        rules:
          personalInfo_mobile:
            required: true
            digits: true
    #@postOfferValidation
    $(".modalInput").live("focus", ->
      $(this).parents(".field-wrapper").addClass "focused"
      $(".field-hint").removeClass "visible"
      $(this).siblings(".field-hint").addClass "visible"
      $("#signupCarousel").addClass "overflow_visible"
    ).live("blur", ->
      $(this).parents(".field-wrapper").removeClass "focused"
    ).live "change", ->
      #check of its valid and add checkmark
      if $(this).valid()
        $(this).parents(".field-wrapper").addClass "valid"
        $(this).parents(".field-wrapper").removeClass "required"
      else
        $("#signupCarousel").addClass "overflow_visible"
        $(this).parents(".field-wrapper").removeClass "valid"
        $(this).parents(".field-wrapper").addClass "required"

    $("#signupCarousel").carousel(interval: false).on("slide", ->
      $(this).removeClass "overflow_visible"
    ).on "slid", ->
      $(this).addClass('overflow_visible')


    #check for validation on second step - Your Business Details



  #check for validation on third step - Personal details

    #time-picker init
    $(".time-picker").timepicker timeFormat: "H:i"
    $('.time-picker').timepicker({
    timeFormat:'H:i'
    });

    match_radio = $(".match input[type=radio]")
    match_item = $(".match")

    #matches list
    $(".matches-list").live "change", ->
      match_item.removeClass "selected"
      match_radio.filter(":checked").parent(match_item).addClass "selected"
    #Toggling 'overflow: hidden' for popup container in order to see the categories properly
    $("#business_category_chzn a.chzn-single").live("focus", ->
      $("#signupCarousel").addClass "overflow_hidden"
    ).live "blur", ->
      $("#signupCarousel").removeClass "overflow_hidden"

    $(".bootstrap-checkbox").live "click", (e) ->
      e.preventDefault()


    #submit offer - ##just close the modal for now
    $(".postOffer-submit").live "click", ->
      $("#signupCarousel").modal "hide"

    #changing business hours for other days when setting Mon-Fri hours (triggers only once - first time)
    sat_open = $("#sat-open")
    sun_open = $("#sun-open")
    sat_close = $("#sat-close")
    sun_close = $("#sun-close")
    $("#mon-fri-open").one "change", ->
      value = $(this).val()
      sat_open.val(value).removeClass "error"
      sun_open.val(value).removeClass "error"

    $("#mon-fri-close").one "change", ->
      value = $(this).val()
      sat_close.val(value).removeClass "error"
      sun_close.val(value).removeClass "error"
    #pre-populate email field in the "Personal details" step
    $("#createOffer_email").live "change", ->
      value = $(this).val()
      $("input#personalInfo-email").val value
      $("input#personalInfo-email").attr "disabled", "disabled"
      $("input#personalInfo-email").attr "readonly", "readonly"

    places = undefined

    #show-up search-matches after user clicks on Search button in the second step
    $("a.search-matches-btn").live "click", ->

      $('.time-picker').timepicker({
      timeFormat:'H:i'
      });
      $.ajax
        type: "POST"
        url: "offers/get_google_places"
        data: "content=" + $("#businessName_address").val()
        dataType: "json"
        success: (msg) ->
          #If email exists, set response to true
          places = msg
          count = 0
          $(".matches-list").html ""
          for merchant of places
            description = places[merchant.toString()]["description"]
            description = description.substr(0, 32) + "..."  if description.length > 35
            name = places[merchant.toString()]["name"]
            count = count + 1
            $(".matches-list").append "<li class=\"match\"> <input id=\"match-" + merchant.toString() + "\" type=\"radio\" name=\"matches\" value=\"\" business_name=\"" + name + "\" business_street=\"" + places[merchant.toString()]["formatted_address"] + "\" business_city=\"" + places[merchant.toString()]["city_name"] + "\" business_postal_code=\"" + places[merchant.toString()]["postal_code"] + "\" merchant_type_id=\"" + places[merchant.toString()]["merchant_type_id"] + "\" business_city_id=\"" + places[merchant.toString()]["city_id"] + "\" state_province_region=\"" + places[merchant.toString()]["state_province_region"] +  "\" country_id=\"" + places[merchant.toString()]["country_id"] + "\" lat=\"" + places[merchant.toString()]["lat"] + "\" lng=\"" + places[merchant.toString()]["lng"] + "\"/>     <label class=\"match-label\" for=\"match-" + merchant.toString() + "\">" + description + "</label>         </li>"
            $("#match-" + merchant.toString()).click ->
              $("#business_name").val $(this).attr("business_name")
              $("#country_id").val $(this).attr("country_id")
              $("#state_province_region").val $(this).attr("state_province_region")
              $("#business_street").val $(this).attr("business_street")
              $("#business_city_id").val $(this).attr("business_city_id")  unless $(this).attr("business_city_id") is "undefined"
              #@set_value $(this).attr("business_city_id"), $("#business_city_id")

              $("#business_city").val $(this).attr("business_city") unless $(this).attr("business_city") is "undefined"
              #@set_value $(this).attr("business_city"), $("#business_city")
              $("#business_postal_code").val $(this).attr("business_postal_code") unless $(this).attr("business_postal_code") is "undefined"
              #@set_value $(this).attr("business_postal_code"), $("#business_postal_code")
              $("#business_lat").val $(this).attr("lat") unless $(this).attr("lat") is "undefined"
              #@set_value $(this).attr("lat"), $("#business_lat")
              $("#business_lng").val $(this).attr("lng") unless $(this).attr("lng") is "undefined"
              #@set_value $(this).attr("lng"), $("#business_lng")
              $("#business_city").attr "disabled", "disabled"
              $("#business_street").attr "disabled", "disabled"
              $("#business_postal_code").attr "disabled", "disabled"
              $("#merchant_merchant_type_id").val $(this).attr("merchant_type_id").toString()
              $(".map-wrapper").slideDown 1
              if $(this).attr("lat") != "undefined" and $(this).attr("lng") != "undefined"
                #alert(parseFloat($(this).attr("lat")))
                #alert(parseFloat($(this).attr("lng")))
                myOptions =
                  zoom: 16
                  center: new google.maps.LatLng(parseFloat($(this).attr("lat")), parseFloat($(this).attr("lng")))
                  #new google.maps.LatLng(parseFloat($(this).attr("lat")), parseFloat($(this).attr("lng")))
                  mapTypeId: google.maps.MapTypeId.ROADMAP
                  scrollwheel: false

                map = new google.maps.Map(document.getElementById("map_show_business"), myOptions)
                #new google.maps.Map($("#map_show"), myOptions)
                image = "/assets/markers/pin/image.png"
                verifyMarker = new google.maps.Marker(
                  map: map
                  position: new google.maps.LatLng(parseFloat($(this).attr("lat")), parseFloat($(this).attr("lng")))
                )

          $(".matches-list").append "<li class=\"match\"> <input id=\"match-" + count.toString() + "\" type=\"radio\" name=\"matches\" value=\"\"/>     <label class=\"match-label\" for=\"match-" + count.toString() + "\">Other. Let me fill in the details</label>         </li>"
          $("#match-" + count.toString()).on "click", ->
            $("#business_name").val ""
            $("#business_street").val ""
            $("#business_city").val ""
            $("#business_postal_code").val ""
            $("#business_lat").val ""
            $("#business_lng").val ""
            $(".map-wrapper").slideUp 1
            $("#business_city").removeAttr "disabled"
            $("#business_street").removeAttr "disabled"
            $("#business_postal_code").removeAttr "disabled"

          false

      $("div.hidden-matches").slideDown "300"
      $("ul.matches-list").on "change", ->
        $("div.detailed-data").slideDown "300"  unless $("div.detailed-data").is(":visible")

  initialize: () ->
    #signupModal
    this.signupCarousel = $("#signupCarousel")
    this.signupModal = $("#signupModal")
    this.modalInput = $(".modalInput")
    this.required_email = $("#createOffer_email")
    this.required_textOffer = $("#offerText")
    this.required_businessDetails = $("#businessName_address")
    this.required_mobile = $("#personalInfo_mobile")
    this.facebook = 0
    this.twitter = 0
    this.google_place = 0
    this.yelp = 0
    this.yahoo = 0
    @postOffer()



  render: ->
    @$el.html @template(this)
    #view = new Yak.MerchantSubView()
    #view.render()

    $.ajax
      type: "GET"
      url: "/merchant_types/top_lvl"
      dataType: "json"
      success: (data) ->
        $("#merchant_merchant_type_id").html(JST['backbone/desktop/templates/offers/merchant_sub'](merchant_type: data))
    this

  createOffer: ->
    if $("#createOffer_email").valid() and $("#offerText").valid()
      #if true
      $.ajax
        type: "POST"
        url: "offers/create_offer"
        data: "email=" + $("#createOffer_email").val() + "&offer_text=" + escape($("#offerText").val())
        dataType: "text"
        success: (msg) ->
          $.cookie('user_id', msg)
          console.log(msg)
          #If email exists, set response to true
          #next = (if (msg is "true") then true else false)
      $("#personalInfo-email").val($("#createOffer_email").val())
      $("#personalInfo-email").attr("readonly","readonly")
      $("#signupCarousel").carousel "next"
      $("#signupCarousel").carousel "pause"

    else
      $("#signupCarousel").addClass "overflow_visible"
      $("#signupModal").effect "shake",
        times: 1
        , 70

  businessDetails: (e)->
    if $("#businessName_address").valid() and (($("#mon-fri-open").val() isnt "" and $("#mon-fri-close").val() isnt "") or ($("#sat-open").val() isnt "" and $("#sat-close").val() isnt "") or ($("#sun-open").val() isnt "" and $("#sun-close").val() isnt ""))

      data =
        name: $("#business_name").val()
        primary_address: $("#business_street").val()
        city_id: $("#business_city_id").val()
        postal_code: $("#business_postal_code").val()
        lat: $("#business_lat").val()
        lng: $("#business_lng").val()
        country_id: $("#country_id").val()
        state_province_region: $("#state_province_region").val()
        merchant_type_id: $("#merchant_merchant_type_id").val()
        mon_fri_open: $("#mon-fri-open").val()
        mon_fri_close: $("#mon-fri-close").val()
        sat_open: $("#sat-open").val()
        sat_close: $("#sat-close").val()
        sun_open: $("#sun-open").val()
        sun_close: $("#sun-close").val()

      $.ajax
        type: "POST"
        url: "offers/get_business_detail"
        data: data
        dataType: "text"
        success: (msg) ->

          #If email exists, set response to true
          next = (if (msg is "true") then true else false)
      $("#signupCarousel").carousel "next"
      $("#signupCarousel").carousel "pause"
    else
      $("#signupCarousel").addClass "overflow_visible"
      $("#signupModal").effect "shake",
        times: 1
        , 70
      $(".time-picker").addClass "error"
    e.preventDefault()

  socialMedia: (e) ->
    #check for validation on third step - Social media Links
    facebook_id = $("#socialMedia-facebook").val()
    twitter_id = $("#socialMedia-twitter").val()
    google_place_id = $("#socialMedia-places").val()
    yelp_id = $("#socialMedia-yelp").val()
    $.ajax
      type: "POST"
      url: "offers/post_social_network_offer"
      data: "facebook_id=" + escape(facebook_id) + "&twitter_id=" + escape(twitter_id) + "&google_place_id=" + escape(google_place_id) + "&yelp_id=" + escape(yelp_id) + "&facebook=" + @facebook + "&google=" + @google_place + "&twitter=" + @twitter + "&yelp=" + @yelp + "&yahoo=" + @yahoo
      dataType: "text"
      success: (msg) ->

        #If email exists, set response to true
        next = (if (msg is "true") then true else false)

    $("#signupCarousel").carousel "next"
    $("#signupCarousel").carousel "pause"
    e.preventDefault()

  personalDetails: (e) ->
    $("#model_post_on_offer_complete").modal('show')
    if $("#personalInfo_mobile").valid()

      $.ajax
        type: "POST"
        url: "offers/post_persional_detail"
        data: "first_name=" + escape($("#personalInfo-fname").val()) + "&last_name=" + escape($("#personalInfo-lname").val()) + "&email=" + escape($("#personalInfo-email").val()) + "&password=" + escape($("#personalInfo-pass").val()) + "&mobile=" + escape($("#personalInfo_mobile").val())
        dataType: "text"
        success: (msg) ->

          #If email exists, set response to true
          next = (if (msg is "true") then true else false)

      #$("#signupModal").modal "hide"
      $("#signupCarousel").carousel "next"
      $("#signupCarousel").carousel "pause"
    else
      $("#signupCarousel").addClass "overflow_visible"
      $("#signupModal").effect "shake",
        times: 1
        , 70
      e.preventDefault()
  merchantDetailSubmit: (e) ->
    location.href = '/users/' + $.cookie('user_id')
    $("#signupModal").modal "hide"
  createFacebbook: (e) ->
    if $(this).attr("class") is "btn bootstrap-checkbox active"
      @facebook = 0
    else
      @facebook = 1
    e.preventDefault()
  createTwitter: (e) ->
    if $(this).attr("class") is "btn bootstrap-checkbox active"
      @twitter = 0
    else
      @twitter = 1
    e.preventDefault()
  createGplaces:(e) ->
    if $(this).attr("class") is "btn bootstrap-checkbox active"
      @google_place = 0
    else
      @google_place = 1
    e.preventDefault()
  createYelp: (e) ->
    if $(this).attr("class") is "btn bootstrap-checkbox active"
      @yelp = 0
    else
      @yelp = 1
    e.preventDefault()
  createYahoo: (e) ->
    if $(this).attr("class") is "btn bootstrap-checkbox active"
      @yahoo = 0
    else
      @yahoo = 1
    e.preventDefault()