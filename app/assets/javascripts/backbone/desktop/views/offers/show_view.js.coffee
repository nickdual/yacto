class Yak.OfferView extends Backbone.View
  template: JST["backbone/desktop/templates/offers/show"]
  className: "offer-item-wrapper"
  
  events:
    "click a.buy_via_link" : "normal"
    "click a.js-iphone" : "normal"
    "click a.js-email" : "normal"
    "click a.js-tweet" : "normal"
    "click a.view-more-from-merchat" : "normal"
    "click a.offer-address" : "normal"
    "click a.js-like" : "fbLike"
    "click a.js-gplus" : "googlePlus"
    "click a.favorite-offer.logged-in" : "favOffer"
    "click a.favorite-offer:not(.logged-in)" : "loggin"
    "click .offer-item" : "expandOffer"


  render: ->
    
    $(@el).html(@template(offer: @model.toJSON(), loggedIn: @model.collection.options.loggedIn ))
    @$(".offer-address").tooltip() 
    @

  expandOffer: (e, forceOpen = false) =>
    e?.preventDefault?()
    FB.XFBML.parse(@el,-> console.log("FB:like")) if (typeof FB isnt "undefined")
    if forceOpen
      @$(".offer-item").addClass('expanded')
      @$el.next().find('.offer-item').addClass('after-expanded')
    else
      @$(".offer-item").toggleClass('expanded')
      @$el.next().find('.offer-item').toggleClass('after-expanded')
    
    @handleExpansionOnMap()

  handleExpansionOnMap: ->
    if @$(".offer-item.expanded").length > 0
      centerPoint = @centerMap() 
      merchantJSON = @model.get("merchant").toJSON()
      merchantJSON.type = merchantJSON.merchant_type.merchant_type
      merchantJSON.url = "#merchants/#{merchantJSON.id}"
      new OfferInfoBox().show(merchantJSON , centerPoint, @options.map)
      _.defer(@cleanupInfoboxes)

  cleanupInfoboxes: ->
    if $(".infoBox").length > 1
      $(".infoBox:first").remove()

  centerMap: ->
    point = new google.maps.LatLng( @model.get("lat"), @model.get("lon") )
    @options.map.setCenter(point)
    @options.map.setZoom(@model.collection.zoom())
    point


  favOffer: (e) =>
    e.stopPropagation()
    $(e.target).toggleClass('starred')
    if @model.get("favorite").get("marked")
      successCallback = @model.collection.removeFavorite
    else
      successCallback = @model.collection.addFavorite
    #it might not seam like it but it saves the favorite
    @model.get("favorite").fetch(
      success: =>
        successCallback(@model)
    ) #TODO - make it more restful - separate resource and USE POST 

  loggin: (e) ->
    e.stopPropagation()
    pop = new Yak.LoginPopup(@model.collection.preserveState)
    pop.show()
#    LoginPopup.show(@model.collection.preserveState)
    
  normal: (e) ->
    e.stopPropagation()

  fbLike: (e) ->
#    e.stopPropagation()
#    FacebookUtils.postToFeed(@model.get("merchant").get("name"), @model.get("merchant").get("merchant_id"), @model.get("description"))

  googlePlus: (e) ->
    e.stopPropagation()
    href = @$("a.js-gplus").attr("href")
    window.open(href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600')
    return false


