class Yak.MobileFiltersRouter extends Backbone.Router
  routes:
    "filters" : "filters"
    "" : "filters"

  initialize: (@options) ->

  filters: ->
    @updateHeader()
    view = new Yak.MobileOffersFiltersView(collection: @options.get("merchantTypes") )
    $("#main").html(view.render().el)

  updateHeader: ->
    $(".mobile_header a").removeClass("active")
    $(".mobile_header a.start").addClass("active")