class Yak.MobileMerchantRouter extends Backbone.Router
  routes:
    "merchants/:id" : "show"

  initialize: (@options) ->

  show: (id) ->
    $("body").attr("class", "mobile_merchant") 
    model = new Yak.Merchant()
    model.id = id
    model.fetch()
    @updateHeader()
    view = new Yak.MobileMerchantView(model: model, options: @options)
    $("#main").html(view.render().el)

  updateHeader: ->
    $(".mobile_header a").removeClass("active")
    $(".mobile_header a.start").addClass("active")