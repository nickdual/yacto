class Yak.MobileOffersFiltersView extends Backbone.View
  template: JST["backbone/mobile/templates/offers/filters"]
  headerTemplate: JST["backbone/mobile/templates/shared/mobile_header"]

  className: "full_height"
    
  events:
    "click .footer_dropdown a": "normalClick"
    "click li.more": "footerMore"
    "click .filter_icon" : "submitFilters"
    "click .log_in" : "login"
    "click .sign_up" : "signup"
    "click .set_location" : "setLocation"
    "click .set_location_button" : "setLocationButton"
    "click .log_in_button" : "loginButton"
    "click .sign_up_button" : "signupButton"
    "click a.forgot_password": "forgotPassword"
    "click .back_to_login": "backLogin"
    "click .reset_password": "resetPassword"
    "click .signin_signupButton": "btn_signup"



  initialize: ->
    if Param.byName("lat") == "" || Param.byName("lon") == ""
      _.defer(@geolocateUser)
    console.log($.cookie("facebook_on"))

  distanceOrDefault:  ->
    Param.byName("distance") || "1.0"
  

  render: ->
    $(@el).html(@template(distance: @distanceOrDefault()))
    @$("#mobile_header").html(@headerTemplate(nearbyPath: @nearbyPath(),offersPath: @offersPath(),favoritesPath: @favoritesPath()))
    _.defer(@initDropDowns)
    _.defer(@initSwitches)
    _.defer(@checkLogin)
    response = undefined
    $.validator.addMethod "unique_email", ( value = ->
      $.ajax
        type: "POST"
        url: "/offers/unique_email"
        data: "email=" + $("#sign_up_email").val()
        dataType: "text"
        success: (msg) ->
          response = if (msg is "true") then true else false
      response
    ), "Email is already taken"
    @$("form.sign_up_form").validate
      rules:
        sign_up_email:
          required: true
          email: true
          unique_email: true
        sign_up_password:
          required: true
          minlength: 6

    @$("form.forgot_pass_form").validate
      rules:
        forgot_pass_field_email:
          required: true
          email: true
    @$("form.log_in_form").validate
      rules:
        yacto_email:
          required: true
          email: true
        yacto_password:
          required: true
          minlength: 6
    @

  initDropDowns: ->
    $('.chzn-select').chosen({disable_search_threshold:10})
  initSwitches: ->
    $('.post_on_twitter').iphoneStyle(
      checkedLabel:'<img src="../assets/ios-style-checkboxes/post_twitter.png">',
      uncheckedLabel:'<img class="image_off" src="../assets/ios-style-checkboxes/post_twitter.png">'
      onChange: (elem, value) ->
        if value
          $.ajax
            type: "GET"
            url: "home/ajax_twitter_on"
            success:  ->
              $.cookie('twitter_on', true)
            error: ->
              console.log("There is an error : can't set data")
        else
          $.ajax
            type: "GET"
            url: "home/ajax_twitter_off"
            success:  ->
              $.cookie('twitter_on', null)
            error: ->
              console.log("There is an error : can't set data")
    )

    $('.post_on_facebook').iphoneStyle(
      checkedLabel:'<img src="../assets/ios-style-checkboxes/post_facebook.png">',
      uncheckedLabel:'<img class="image_off" src="../assets/ios-style-checkboxes/post_facebook.png">'
      onChange: (elem, value) ->
        if value
          $.ajax
            type: "GET"
            url: "home/ajax_facebook_on"
            success:  ->
              $.cookie('facebook_on', true)
              console.log($.cookie)
            error: ->
              console.log("There is an error : can't set data")
        else
          $.ajax
            type: "GET"
            url: "home/ajax_facebook_off"
            success:  ->
              $.cookie('facebook_on',null)
            error: ->
              console.log("There is an error : can't set data")

    )

    $('.post_on_gplus').iphoneStyle(
      checkedLabel:'<img src="../assets/ios-style-checkboxes/post_gplus.png">',
      uncheckedLabel:'<img class="image_off" src="../assets/ios-style-checkboxes/post_gplus.png">'
      onChange: (elem, value) ->
        if value
          $.ajax
            type: "GET"
            url: "home/ajax_gplus_on"
            success:  ->
              $.cookie('gplus_on', true)
            error: ->
              console.log("There is an error : can't set data")
        else
          $.ajax
            type: "GET"
            url: "home/ajax_gplus_off"
            success:  ->
              $.cookie('gplus_on', null)
            error: ->
              console.log("There is an error : can't set data")
    )
  checkLogin: ->
    session = undefined
    session = new Yak.Session()
    da = undefined
    $.getJSON("/users/get_session", (data)->
      auth_hash = {"id":data.id,"access_token":data.access_token}
      session.save(auth_hash)
      $.cookie('user_id', auth_hash.id)

      )
    if $.cookie('user_id')

      $("a.log_in").parent().hide()
      $("a.sign_up").parent().hide()
      $("a.profile").parent().show()
    else

      $("a.log_in").parent().show()
      $("a.sign_up").parent().show()
      $("a.profile").parent().hide()

  normalClick: (e) ->
    e.stopPropagation()

  footerMore: (e) ->
    e.preventDefault()
    $target = $(e.currentTarget)

    $target.toggleClass('active')
    $target.find('.footer_dropdown').fadeToggle(250)

  time: ->
    @$(".when-filter").val()
    
  distance:  ->
    @$(".distance-filter").val()

  lat: ->
    @_lat || Param.byName("lat")

  lon: ->
    @_lon || Param.byName("lon")


  submitFilters: (e) ->
    e.preventDefault()
    $target = $(e.currentTarget)
    filter = $target.data("filter")
    merchant_type_id = @collection.typeByMerchantType(filter)?.get("id") || ""
    document.location = "/offers?distance=#{@distance()}&time=#{@time()}&merchant_type_id=#{merchant_type_id}&lat=#{@lat()}&lon=#{@lon()}"

  geolocateUser: =>
    if navigator.geolocation?
      navigator.geolocation.getCurrentPosition(@geolocationSuccess)

  geolocationSuccess: (position) =>
    @_lat = position.coords.latitude
    @_lon = position.coords.longitude
  login: (e) ->

    $("#log_in").modal('show')
    $("li.more").click()
    e.preventDefault()
  signup: (e) ->
    $("#log_in").modal('hide')
    $("#sign_up").modal('show')
    $("li.more").click()
    e.preventDefault()
  btn_signup: (e) ->
    $("#log_in").modal('hide')
    $("#sign_up").modal('show')
    e.preventDefault()
  setLocation: (e) ->
    $("#set_location").modal('show')
    $("li.more").click()
    e.preventDefault()
  setLocationButton: (e) ->
    $.ajax
      type: "GET"
      url: "/offers/set_location"
      data: "address=" +  $('.set_location_form .modal_input').val()
      success: (data) ->
        $("#set_location").modal('hide')

      error: (msg) ->
        alert 'We can not find your location, please enter it.'


    e.preventDefault()
  loginButton: (e) ->
    if $(".log_in_form .yacto_email").valid() and $(".log_in_form .yacto_password").val()
      $.ajax
        type: "POST"
        url: "/users/sign_in"
        beforeSend: (xhr) ->
          xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        data: "user[login]=" + $(".log_in_form .yacto_email").val() + "&user[password]=" + $(".log_in_form .yacto_password").val()
        dataType: "json"
        success: (msg) ->
          $.cookie('user_id', msg.id)
          $("#log_in").modal('hide')
          $("a.log_in").parent().hide()
          $("a.sign_up").parent().hide()
          $("a.profile").parent().show()
        error: (e) ->
          str = ""
          date = ""
          data = JSON.parse(e.responseText);
          for key of data
            str +=   data[key]
          alert str



    e.preventDefault()

  signupButton: (e) ->
    if $("#sign_up_email").valid() and $("#sign_up_password").valid()
      now = new Date();
      year = now.getYear()
      mon = now.getMonth()
      day = now.getDate()
      hour = now.getHours();
      minutes = now.getMinutes()
      seconds = now.getSeconds()
      $.ajax
        type: "POST"
        url: "/users"
        beforeSend: (xhr) ->
          xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        data: "user[email]=" + $("#sign_up_email").val() + "&user[password]=" + $("#sign_up_password").val() + "&user[first_name]=Update me" + "&user[last_name]=Update me" + "&user[username]=" + $("#sign_up_email").val() + "&user[mobile_number]=" + year + mon + day + hour + minutes + seconds
        dataType: "json"
        success: (msg) ->
          #$("#sign_up").modal('hide')
          alert "Thanks for signing up for Yacto, you can now connect additional social accounts, turn on or off auto-posting of your favorites to social networks, or click the x to close the screen and get back to business "
    else
      alert "error"
    e.preventDefault()
  postonFacebook: (e) ->

    e.preventDefault()

  forgotPassword: (e) ->
    e.preventDefault()
    @$('.login_div').hide()
    @$('.forgot_div').fadeIn()

  backLogin: (e) ->
    e.preventDefault()
    @$('.forgot_div').hide()
    @$('.login_div').fadeIn()
  resetPassword: (e) ->
    e.preventDefault()

    if $(".forgot_pass_form .yacto_email").valid()
      $("#loading_reset_password").show()
      $.ajax
        type: "POST"
        url: "/users/password"
        beforeSend: (xhr) ->
          xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        data: "user[login]=" + $(".forgot_pass_form .yacto_email").val()
        dataType: "json"
        success: (msg) ->
          $("#loading_reset_password").hide()
          alert "You will receive an email with instructions about how to reset your password in a few minutes."
          $("#log_in").modal('hide')
        error: (e) ->
          $("#loading_reset_password").hide()
          alert "Email not found"

    else
      alert "Please input email"
  createParams: ->
    "lon=#{@lon()}&lat=#{@lat()}&distance=#{@distance()}&time=#{@time()}"

  nearbyPath: ->
    "/offers/nearby?#{@createParams()}" 
  offersPath: ->
    "/offers?#{@createParams()}"
  favoritesPath: ->
    "/offers?#{@createParams()}&favorite=true"