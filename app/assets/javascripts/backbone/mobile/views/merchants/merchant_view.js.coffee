class Yak.MobileMerchantView extends Backbone.View
  template: JST["backbone/mobile/templates/merchants/show"]
  headerTemplate: JST["backbone/mobile/templates/shared/mobile_header"]
  
  events:
    "click a.scroll_bottom":"centerScreenToMap"

  render: ->
    $(@el).html(@template())
    @$("#mobile_header").html(@headerTemplate())
    @renderWidgets()
    @

  renderWidgets: ->
    @renderAboutWidget()
    mapView = @renderMapWidget()
    @renderDetailsWidget(mapView)
    @renderPhotosWidget()
    
  renderAboutWidget: ->
    view = new Yak.MerchantAboutWidgetView(model: @model)
    @$("#about_widget").html(view.el)

  renderOffersWidget: ->
    view = new Yak.MerchantOffersWidgetView(model: @model)
    @$("#offers_widget").html(view.el)

  renderPhotosWidget: ->
    view = new Yak.MobileMerchantPhotosWidgetView(model: @model)
    @$("#photos_widget").html(view.el)

  renderDetailsWidget: (mapView) ->
    view = new Yak.MobileMerchantDetailsWidgetView(model: @model, options: @options.options, mapView: mapView)
    @$("#details_widget").html(view.el)

  renderMapWidget: ->
    view = new Yak.MerchantMapWidgetView(model: @model,draggable: false)
    @$("#map_widget").html(view.el)
    view
  centerScreenToMap: (e) ->
    e.preventDefault()
    window.scrollTo(0, document.body.scrollHeight);

