class Yak.MobileMerchantPhotosWidgetView extends Backbone.View
  template: JST["backbone/mobile/templates/merchants/photos_widget"]
  events: 
    "click .show_gallery" : "showGallery"

  initialize: ->
    @model.on("change", @render, @)
    
  render: ->
    if @model.get("merchant_photos").length > 0
      $(@el).html(@template())
      _.defer(@renderPhotos) #loading photos after page loads
    @


  renderPhotos: =>
    @first = true
    @model.get("merchant_photos").each(@renderPhoto)

  renderPhoto: (photo) =>
    view = new Yak.MobileMerchantPhotoView(model: photo, first: @first)
    @first = false
    @$(".carousel-inner").append(view.render().el)

  showGallery: (e) ->
    e.preventDefault()
    $(e.currentTarget).hide()
    @$('.merchant_gallery_wrapper').fadeIn().children('.merchant_gallery').carousel()
