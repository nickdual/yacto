class Yak.MobileMerchantPhotoView extends Backbone.View
  template: JST["backbone/mobile/templates/merchants/photo"]

  className: "item"

  initialize: ->
    if @options.first
      @$el.addClass("active")

  render: ->
    $(@el).html(@template(photo: @model.toJSON()))
    @
