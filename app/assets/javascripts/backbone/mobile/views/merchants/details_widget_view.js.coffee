class Yak.MobileMerchantDetailsWidgetView extends Backbone.View
  template: JST["backbone/mobile/templates/merchants/details_widget"]
  events: 
    "click a.js-gplus" : "googlePlus"
    "click a.fav": "addToFav"
    "click a.get_directions": "getDirection"

  initialize: ->
    @model.on("change", @render, @)
    @options.options.on("change:loggedIn", @finishFavorite)
    
  render: ->
    $(@el).html(@template(merchant: @model.toJSON()))
    FB.XFBML.parse(@el,-> console.log("FB:like")) if (typeof FB isnt "undefined")
    @renderOffersWidget()
    @

  renderOffersWidget: ->
    view = new Yak.MerchantOffersWidgetView(model: @model)
    @$("#offers_widget").html(view.render().el)

  finishFavorite: =>
    @model.get("favorite").fetch(
      success: =>
        @$(".add_to_fav").toggleClass("favorited")
    )

  
  googlePlus: (e) ->
    e.preventDefault()
    e.stopPropagation()
    href = @$("a.js-gplus").attr("href")
    window.open(href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600')
    return false

  addToFav: (e) ->
    e.preventDefault()
    e.stopPropagation()
    if @options.options.get("loggedIn")
      @finishFavorite()
    else
      alert("TODO - define login")
      # LoginPopup.show(
      #   -> 
      #  true)
      
  getDirection: (e) ->
    @options.mapView.findLocation(e);