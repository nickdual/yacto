class Yak.MerchantAboutWidgetView extends Backbone.View
  template: JST["backbone/common/templates/merchants/about_widget"]

  events:
    "click .nav-tabs a": "showTab"

  initialize: ->
    @model.on("change", @render, @)

  render: ->
    $(@el).html(@template(merchant: @model.toJSON()))
    _.defer(@initTabs)
    @

  initTabs: ->
    tab_content = $('div.merchant_selling_points p')
    # check for empty tab and hide it
    tab_content.each(
      (index) -> 
        if !$.trim($(this).html()).length
          current = $(this)
          current.closest('.tab-pane').remove()
          $('#selling_point_tabs li').eq(index).hide()
    )
    $("#selling_point_tabs li:hidden").remove()
    
    if $('div.selling_points_content div.tab-pane').length == 0
      $('div.selling_points_content').remove()
      
    $('ul#selling_point_tabs a:first').tab('show').closest('li:visible').addClass('active') # Select first tab;
    $('div.selling_points_content div.tab-pane:first').addClass('fade in active')

  showTab: (e) ->
    e.preventDefault()
    $(e.target).tab('show')