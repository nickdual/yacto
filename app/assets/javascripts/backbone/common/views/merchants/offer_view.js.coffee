class Yak.MerchantOfferView extends Backbone.View
  template: JST["backbone/common/templates/merchants/offer"]

  events:
    "click a.js-like" : "fbLike"
    "click a.js-gplus" : "googlePlus"
    "click a.share": "empty"
    "click a.redeem_offer" : "showRedeemPopUp"

  render: ->
    $(@el).html(@template(offer: @model.toJSON()))
    @

  fbLike: (e) ->
    e.stopPropagation()
    FacebookUtils.postToFeed(@model.get("merchant").get("name"), @model.get("merchant").get("merchant_id"), @model.get("description"))

  googlePlus: (e) ->
    e.stopPropagation()
    href = @$("a.js-gplus").attr("href")
    window.open(href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600')
    return false

  empty: (e) ->
    e.preventDefault()

  showRedeemPopUp: (e) ->
    element = @$("a.redeem_offer").text()
    if element.indexOf("Use") != -1
      e.preventDefault()
      view = new Yak.MerchantRedeemPopupView()
      @$("#redeem-offer-wrapper").html(view.el)

      