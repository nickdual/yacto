class Yak.MerchantBusinessWidgetView extends Backbone.View
  template: JST["backbone/common/templates/merchants/business_widget"]
  events: 
    "click a.js-like" : "fbLike"
    "click a.js-gplus" : "googlePlus"
    "click a.add_to_fav": "addToFav"

  initialize: ->
    @model.on("change", @render, @)
    @options.options.on("change:loggedIn", @finishFavorite)
    
  render: ->
    $(@el).html(@template(merchant: @model.toJSON()))
    $(".offer-addres").tooltip()
    @

  finishFavorite: =>
    @model.get("favorite").fetch(
      success: =>
        @$(".add_to_fav").toggleClass("favorited")
    )

  fbLike: (e) ->
    e.preventDefault()
    e.stopPropagation()
    FacebookUtils.postToFeed(@model.get("name"), @model.get("merchant_id"), @model.get("unique") || "")

  googlePlus: (e) ->
    e.preventDefault()
    e.stopPropagation()
    href = @$("a.js-gplus").attr("href")
    window.open(href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600')
    return false

  addToFav: (e) ->
    e.preventDefault()
    e.stopPropagation()
    if @options.options.get("loggedIn")
      @finishFavorite()
    else
      popup = new Yak.LoginPopup()
      popup.show(
        -> 
       true)
      