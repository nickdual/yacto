class Yak.MerchantRedeemPopupView extends Backbone.View
  template: JST["backbone/common/templates/merchants/redeem_popup"]
  className: "modal redeem-offer-popup"
  id: "redeem-offer-popup"
  events:
  	"click .modal-body":"hideModal"
  initialize: ->
  	@render()

  render: ->
  	$(@el).html(@template())
  	@
  hideModal: ->
  	$(@el).hide()