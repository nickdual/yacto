class Yak.MerchantHeroWidgetView extends Backbone.View
  template: JST["backbone/common/templates/merchants/hero_widget"]

  initialize: ->
    @model.on("change", @render, @)

  render: ->
    $(@el).html(@template(merchant: @model.toJSON()))
    @