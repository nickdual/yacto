class Yak.MerchantOffersWidgetView extends Backbone.View
  template: JST["backbone/common/templates/merchants/offers_widget"]

  events:
    "click .nav-tabs a": "showTab"

  offersByDay:
      "#mon": "offers_mon"
      "#tue": "offers_tue"
      "#wed": "offers_wed"
      "#thu": "offers_thu"
      "#fri": "offers_fri"
      "#sat": "offers_sat"
      "#sun": "offers_sun"

  initialize: ->
    @model.on("change", @render, @)
    
  render: ->
    $(@el).html(@template(activeDay: @currentDay()))
    _(@offersByDay).each(@renderOffers)
    @
    
  currentDay: ->
    new Date().getDay()

  renderOffers: (merchantField, elemSelector) =>
    offers = @model.get(merchantField)
    if offers.length > 0
      @$(elemSelector).html("")
      
    offers.each( (offer) =>
      view = new Yak.MerchantOfferView(model: offer)
      @$(elemSelector).append(view.render().el)
    )


  showTab: (e) ->
    e.preventDefault()
    $(e.target).tab('show')