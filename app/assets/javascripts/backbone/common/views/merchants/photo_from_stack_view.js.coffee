class Yak.MerchantPhotoFromStackView extends Backbone.View
  template: JST["backbone/common/templates/merchants/photo_from_stack"]

  render: ->
    $(@el).html(@template(photo: @model.toJSON()))
    @