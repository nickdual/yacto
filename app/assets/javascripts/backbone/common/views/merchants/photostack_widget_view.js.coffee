class Yak.MerchantPhotoStackWidgetView extends Backbone.View
  template: JST["backbone/common/templates/merchants/photostack_widget"]

  initialize: ->
    @model.on("change", @render, @)

  render: ->
    $(@el).html(@template(showNavigation: @model.get("merchant_photos").length > 1))
    @model.get("merchant_photos").each(@renderPhoto)
    _.defer(@initView)
    @

  renderPhoto: (photo) ->
    view = new Yak.MerchantPhotoFromStackView(model: photo)
    @$(".merchant_photo_stack").append(view.render().el)

  initView: =>
    @initFancyBox()
    @initPhotoStack()

  initFancyBox: ->
    $(".fancybox").fancybox({
      padding: 0,
      openEffect : 'elastic',
      openSpeed  : 300,
      closeEffect : 'elastic',
      closeSpeed  : 300,
      nextClick : true
    })

  initPhotoStack: ->
    PhotoStack.init("#photostack_widget div.merchant_photo_stack")