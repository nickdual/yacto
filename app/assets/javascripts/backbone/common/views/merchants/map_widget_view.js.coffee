class Yak.MerchantMapWidgetView extends Backbone.View
  template: JST["backbone/common/templates/merchants/map_widget"]

  @draggable: true 

  events:
    "click button.get_directions_button": "findLocation"
    "click a.get_directions": "findLocation"

  initialize: ->
    @model.on("change", @render, @)
    @model.on("change", @initializeMap, @)
    @location = new Yak.Location()
    @location.on("change", @showDirections, @)
    # to disable draggable functionality of the map, pass in draggable : false 

    @draggable = @options.draggable if @options.draggable != undefined 
    
  render: ->
    $(@el).html(@template())
    @

  initializeMap: ->
    merchantLocation = new google.maps.LatLng( @model.get("lat"), @model.get("lon") )
    myOptions = {
      zoom: 16 #the same as in mobile merchant#show
      center: merchantLocation
      mapTypeId: google.maps.MapTypeId.ROADMAP
      scrollwheel: false
      draggable: @draggable
    }
    @map = new google.maps.Map(document.getElementById("map_show"), myOptions)
    @showMarker(merchantLocation)
    @prepoulateUserDirections()

  showMarker: (point) ->
    if @model.get('merchant_type') 
      image_filename = @model.get('merchant_type').get("marker_image")
    else
      image_filename = 'look.png'

    image = "/assets/markers/#{image_filename}"
    verifyMarker = new google.maps.Marker({
      map: @map,
      position: point,
      icon: image
    });

  prepoulateUserDirections: ->
    if navigator.geolocation?
      navigator.geolocation.getCurrentPosition(@geolocationSuccess)
  
  geolocationSuccess: (position) =>
    @location.set("lat",position.coords.latitude)
    @location.set("lon",position.coords.longitude)
  
  findLocation: (e) ->
    e.preventDefault()
    #TODO we may consider binding in the future, but just for one field
    @location.address = @$(".location_input").val()
    @location.fetch()

  showDirections: ->
    #setting it again, if the mapService is initialized (2nd directions query on one page)
    @mapService().startLat = @location.get("lat")
    @mapService().startLon = @location.get("lon")
    @mapService().showDirections()

  mapService: ->
    @_mapService ||= new MapDirections(@map, @location.get("lat"), @location.get("lon"), @model.get("lat"), @model.get("lon"))