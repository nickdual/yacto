class Yak.Merchant extends Backbone.Model
  url: ->
    "/merchants/#{@id}"

  initialize: ->
    @initMerchantType()
    @initFavorite()

  initMerchantType: ->

    @attributes.merchant_type = new Yak.MerchantType( @get("merchant_type") ) if @attributes.merchant_type?

  initFavorite: ->
    @attributes.favorite = new Yak.MerchantFavorite(@attributes.favorite)
    @attributes.favorite.merchant_id = @get("id")

    
  parse: (resp, xhr) ->
    #console.log(resp)
    resp.merchant_type = new Yak.MerchantType(resp.merchant_type) 
    resp.merchant_photos = new Yak.MerchantPhotos(resp.merchant_photos)
    @parseOffers(resp)
    @parseFavorite(resp)
    resp

  parseOffers: (resp) ->
    resp.offers_mon = new Yak.Offers(resp.offers_mon)
    resp.offers_tue = new Yak.Offers(resp.offers_tue)
    resp.offers_wed = new Yak.Offers(resp.offers_wed)
    resp.offers_thu = new Yak.Offers(resp.offers_thu)
    resp.offers_fri = new Yak.Offers(resp.offers_fri)
    resp.offers_sat = new Yak.Offers(resp.offers_sat)
    resp.offers_sun = new Yak.Offers(resp.offers_sun)

  parseFavorite: (resp) ->
    resp.favorite = new Yak.MerchantFavorite(marked: resp.favorite)
    resp.favorite.merchant_id = resp.id


  toJSON: ->
    json = super()
    json.merchant_type = @get("merchant_type")?.toJSON()
    json.any_photos = json.merchant_photos?.length > 0
    json.hero_photo = json.merchant_photos?.at(0)?.get("url_medium")
    json.favorite = json.favorite?.get("marked")
#    json.desktop_decription = @get("desktop_decription")
    json
