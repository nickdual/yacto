#= require ./merchant_type
class Yak.MerchantTypes extends Backbone.Collection
  model: Yak.MerchantType

  typeByMerchantType: (merchantType) ->
    _.find(
      @models
      (model) ->
        model.get("merchant_type") == merchantType
    )