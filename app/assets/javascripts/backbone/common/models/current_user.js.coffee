class Yak.CurrentUser extends Backbone.Model
  url: -> "users/session_user"
  defaults:
    access_token: null
    user_id: null
  authenticated: ->
    Boolean(@get("access_token"))
