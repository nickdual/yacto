#= require ./offer
class Yak.Offers extends Backbone.Collection
  model: Yak.Offer

  url: ->
    url = "/dashboard/offers.json?address=#{@city}&distance=#{@distance}&time=#{@time}"
    url += "&merchant_type=#{@type}" if @type?.length > 0
    url += "&offset=#{@offset}" if @offset > 0
    url

  initialize: (models, @options)->
    @initialState(options)
    @clearCookies()
    @favoriteOffers = @arrayToObject(@options?.favoriteOffers)
    @on("reset", @favoritesSelected, @)
    @on("add", @markIfFavorite)

  initialState: (options) ->
    @city = $.cookie("city_name") || options?.currentCity.name
    @distance = $.cookie("distance") || "0.25"
    @time = $.cookie("time") || "0"
    @lat = $.cookie("lat") || options?.currentCity.latitude
    @lon = $.cookie("lon") || options?.currentCity.longitude
    
    @offset = 0


  clearCookies: ->
    $.cookie("city_name", null)
    $.cookie("distance", null)
    $.cookie("time", null)
    $.cookie("lat", null)
    $.cookie("lon", null)

  arrayToObject: (array) ->
    _.reduce(
      array
      (memo, val) -> 
        memo[val] = true
        memo
      {}
      )

  favoritesSelected: ->
    _.map( 
      @models
      @markIfFavorite
    )

  markIfFavorite: (offer) =>
    if (offer.get("favorite").get("marked") == undefined)
      favorite = @favoriteOffers[offer.get("id")]
      offer.get("favorite").set("marked", favorite)

  zoom: ->
    Yak.Offers.zoom(@distance)

  averageLon: ->
    lonSum = _.reduce(
      @pluck("lon")
      (memo, lon) -> memo + lon
      0
    )
    lonSum/@length

  averageLat: ->
    latSum = _.reduce(
      @pluck("lat")
      (memo, lat) -> memo + lat
      0
    )
    latSum/@length

  isLastPage: ->
    (@length % 20) > 0 || @length == 0

  nextPage: ->
    @offset += 20

  firstPage: ->
    @offset = 0

  addFavorite: (offer) =>
    @favoriteOffers[offer.get("id")] = true

  removeFavorite: (offer) =>
    @favoriteOffers[offer.get("id")] = undefined
    offer.get("favorite").unset("marked")


  preserveState: =>
    $.cookie("city_name", @city)
    $.cookie("distance", @distance)
    $.cookie("time", @time)
    $.cookie("lat", @lat)
    $.cookie("lon", @lon)
    true

  @zoom: (distance) ->
    Math.round(14 - Math.log(parseFloat(distance, 10) )/Math.LN2)