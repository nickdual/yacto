class Yak.MerchantType extends Backbone.Model

  toJSON: ->
    json = super()
    json.className = @get("marker_image").split(".")[0]
    json.bothTypes = @bothTypes()
    json

  bothTypes: ->
    @get("merchant_type") && @get("sub_type")