class Yak.Offer extends Backbone.Model

  initialize: ->
    @initializeMerchant()
    @initializeFavorite()

  initializeMerchant: ->
    @attributes.merchant = new Yak.Merchant( @get("merchant") )

  initializeFavorite: ->
    @attributes.favorite = new Yak.Favorite()
    @attributes.favorite.offer_id = @get("id")

  toJSON: ->
    json = super()
    json.distance = @distance()
    json.merchant = @get("merchant").toJSON()
    json.favorite = json.favorite.get("marked")
    json
  
  distance: ->
    Math.round(@get("distance")*100)/100

