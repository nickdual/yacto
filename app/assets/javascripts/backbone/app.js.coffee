#= require underscore
#= require backbone

#= require_self
#= require ./common
#= require_tree ./desktop/models
#= require_tree ./desktop/templates
#= require_tree ./desktop/views
#= require_tree ./desktop/routers
window.Yak = {}

class Yak.App
  constructor: (options) ->
    @options = new Yak.Options(options)
    new Yak.OffersRouter(options)
    new Yak.MerchantsRouter(@options)


  start: ->
    Backbone.history.start()
    #hidding flash for now because it breaks offers list (TODO need better way to show)
    $("#flash_notice,#flash_error,#flash_alert").hide()  

