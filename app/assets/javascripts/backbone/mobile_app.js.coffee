#= require underscore
#= require backbone

#= require_self
#= require ./common
#= require_tree ./mobile/models
#= require_tree ./mobile/templates
#= require_tree ./mobile/views
#= require_tree ./mobile/routers
window.Yak = {}

class Yak.MobileApp
  constructor: (options) ->
    @options = new Yak.Options(options)
    new Yak.MobileFiltersRouter(@options)
    new Yak.MobileMerchantRouter(@options)

  start: ->
    Backbone.history.start()
    #hidding flash for now because it breaks offers list (TODO need better way to show)
    # $("#flash_notice,#flash_error,#flash_alert").hide()  

