jQuery ->
  ########################## activating or deactivating offer
  $("#toggle_offer").live "click", (e) ->
    e.preventDefault()
    $this = $(this)
    request = $.get "/users/" + $this.attr("data-id") + "/toggle_offer"
    request.success (data) ->
      if data.success
        $this.html(data.state)
        $this.attr('disabled', 'disabled')
        $this.parent().next().html(data.value)

  ########################## changing recurrence for offer
 # $("#change_recurrence").live "click", (e) ->
  #  e.preventDefault()
   # $this = $(this)
    #request = $.get "/users/" + $this.attr("data-id") + "/change_recurrence?" + "type=" + $this.html().trim()
    #request.success (data) ->
      #if data.success
       # $this.parent().children(":first").html(data.state)


  ########################## for creating ajax based merchant update
  $('input#Save[type="button"]').live "click", (e) ->
    e.preventDefault()
    $this = $(this)

  ########################## updating select fields for merchant sub type ######
  ## redundent code also included in merchants.js.coffee #######################
  merchantType = $("#merchant_sub_type")
  subType = $("#merchant_merchant_type_id")
  
  merchantType.live "change", (e) ->
    request = $.get "/users/ajax_sub_types?" + "type=" + escape(merchantType.val().trim())
    request.success (data) ->
      if data.success
        visibleFirstChild = ''
        onlyExecuteOnce = true
        for child in subType.children()
          $child =  $(child)
          $type_id = $("#merchant_merchant_type_id_chzn_o_" +$child.val())
          $child.css "display", "block"
          $type_id.css "display", "block"
          if data.shown_ids.indexOf(parseInt($child.val())) == -1
            $child.css "display", "none"
            $type_id.css "display", "none"
          if $child.css("display") == 'block' && onlyExecuteOnce
            visibleFirstChild = $child
            onlyExecuteOnce = false
        subType.val(visibleFirstChild.val())

  ########################## updating select fields for merchant sub type ######
  ## redundent code also included in merchants.js.coffee #######################
  if merchantType.val() != undefined
    request = $.get "/users/ajax_sub_types?" + "type=" + escape(merchantType.val().trim())
    request.success (data) ->
      if data.success
        for child in subType.children()
          $child = $(child)
          $child.css "display", "block"
          if data.shown_ids.indexOf(parseInt($child.val())) == -1
            $child.css "display", "none"


  ## posting user form
  merchantUserForm = $('#merchant_user')
  newUserLink = $('#new_merchant_user')
  cancelNewUserLink = $('#cancel_new_user')
  merchantUserForm.hide()
  newUserLink.live "click", (e) ->
    e.preventDefault()
    merchantUserForm.slideDown()
  cancelNewUserLink.live "click", (e) ->
    e.preventDefault()
    merchantUserForm.slideUp()
  ## posting offer form
  merchant_offer_form = $('#merchant_offer')
  new_offer_link = $('#new_merchant_offer')
  cancel_new_offer_link = $('#cancel_new_offer')
  merchant_offer_form.hide()
  new_offer_link.live "click", (e) ->
    e.preventDefault()
    merchant_offer_form.slideDown()
  cancel_new_offer_link.live "click", (e) ->
    e.preventDefault()
    merchant_offer_form.slideUp()    
  