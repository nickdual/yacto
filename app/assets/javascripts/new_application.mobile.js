// This is a new main js file for new mobile pages. Lots of javascripts from old will go away, so I decided to use a new 
// one (which when all are migrated will replace the old "application.mobile.js")


//= require jquery
//= require jquery_ujs
//= require ./login
//= require ./forgot_pass
//= require_directory ./mobile/templates
//= require_directory ./mobile
//= require lib/iscroll


//= require ./params_utils
//= require mobile/iphone_utils
//= require infobox
//= require ./offer_infobox