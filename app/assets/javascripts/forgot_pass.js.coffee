class ForgotPass 
  callbacks: []
  addCallback: (callback) ->
    @callbacks.push(callback)

  onForgotPass: ->
    setTimeout(callback, 0) for callback in @callbacks

window.ForgotPass = new ForgotPass()
