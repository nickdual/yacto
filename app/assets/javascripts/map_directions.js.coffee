class window.MapDirections 
  constructor: (@map, @startLat, @startLon, @endLat, @endLon) ->
    @directionsDisplay = new google.maps.DirectionsRenderer()
    @directionsService = new google.maps.DirectionsService()
    @directionsDisplay.setMap(@map)

  showDirections: ->
    start = new google.maps.LatLng(@startLat, @startLon)
    end = new google.maps.LatLng(@endLat, @endLon)
    request = {
      origin: start
      destination: end
      travelMode:  google.maps.DirectionsTravelMode.DRIVING
    }

    @directionsService.route(request, @directionServiceCallback)

  directionServiceCallback: (response, status) =>
    if status == google.maps.DirectionsStatus.OK
      @directionsDisplay.setDirections(response)

