Hint = {
  initialize: function(){
    Hint.addObservers();
  },
  addObservers: function(){
    $(document).delegate('.close-hint', 'click', function (e) {
      var $hint = $(this).closest('.hint');
      $hint.animate({"opacity": 0}, "slow", function(){
        $hint.slideUp(300);
        //check if we are closing the filter hint if so move the filter-bar to the top
        if ($hint.hasClass('hint-filter')) {

//          var filterBar = $('.offers-filters');
//          var  fixedSidebar = $('.fixed-sidebar-wrapper');
          var  offerList = $('.offers-list');

//          fixedSidebar.animate({
//            top:'60px'
//          });
//          filterBar.stop().animate({
//            top:'60px'
//          });
          offerList.stop().animate({
            paddingTop:'80px'
          }, 400);
        }
      });



      e.preventDefault();
  });
  }
}

$(Hint.initialize)