
var Dashboard = (function ($) {
  return {
    initDropDowns:function () {
      $('.chzn-select').chosen({
        disable_search_threshold:10
      });
    },
    filterOffers:function () {
      $('.offer-type-filter').on('click',
        function (e) {
          $('.offer-type-filter').removeClass('active');
          $(this).addClass('active');

          e.preventDefault();
        }).tooltip();
    },
    set_value:function(value, object) {
      if(value != "undefined") {
        object.val(value);
      }
    },
    offerItem:function () {
      var offer_item = $('.offer-item');

      offer_item.on('hover', function () {
        $(this).toggleClass('hover')
      });

      offer_item.on('click', function (e) {
        $(this).find('.details').trigger('click');


        e.preventDefault();
        e.stopPropagation();
      })
    },
    set_social_network:function(object) {
      $("#create-" + object).on('click', function (e) {
        if($(this).attr("class") == "btn bootstrap-checkbox active") {
          return 1;
        }else{
          return 0;
        }
      })
    },
    expandOffer:function () {
      $('.details').on('click', function (e) {
        $(this).toggleClass('expand collapse')
        .closest('.offer-item').toggleClass('expanded')
        .next('.offer-item').toggleClass('after-expanded')

        e.preventDefault();
        e.stopPropagation();
      });
    },
    favOffer:function () {
      $('i.star').on('click', function (e) {
        $(this).toggleClass('starred');

        e.preventDefault();
        e.stopPropagation();
      })
    },
    likeOffer:function () {
      $('.js-like').on('click', function (e) {
        console.log('fb like');
        $("a.connect_widget_like_button").click();
        e.preventDefault();
        e.stopPropagation();
      })
    },
    tweetOffer:function () {
      $('.js-tweet').on('click', function (e) {
        console.log('tweet!');

        e.preventDefault();
        e.stopPropagation();
      })
    },
    emailOffer:function () {
      $('.js-email').on('click', function (e) {
        console.log('email action!');

        e.preventDefault();
        e.stopPropagation();
      })
    },
    gplusOffer:function () {
      $('.js-gplus').on('click', function (e) {
        console.log('gplus!?');

        e.preventDefault();
        e.stopPropagation();
      })
    },
    textToPhone:function () {
      $('.js-iphone').on('click', function (e) {
        console.log('Text to phone action');

        e.preventDefault();
        e.stopPropagation();
      })
    },
    moreOffers:function () {
      $('.view-more-from-merchat').on('click', function (e) {
        console.log('more offers');

        e.preventDefault();
        e.stopPropagation();
      });
    },
    getDirections:function () {
      $('.offer-address').on('click',
        function (e) {
          console.log('Get directions ->');

          e.preventDefault();
          e.stopPropagation();
        }).tooltip();
    },
    offerActions:function () {
      //Expand-collapse action
      Dashboard.expandOffer();

      //Fav-unfav action
      Dashboard.favOffer();

      //Facebook-like action
      Dashboard.likeOffer();

      //Tweet action
      Dashboard.tweetOffer();

      //Email action
      Dashboard.emailOffer();

      //Gplus action
      Dashboard.gplusOffer();

      //More offers from merchant action
      Dashboard.moreOffers();

      //textToPhone action
      Dashboard.textToPhone();

      Dashboard.getDirections();
    },
    closeHint:function () {
      $('.close-hint').on('click', function (e) {
        $(this).closest('.hint').fadeOut(500);

        //check if we are closing the filter hint if so move the filter-bar to the top
        if ($(this).closest('.hint').hasClass('hint-filter')) {
          console.log('tt');

          var filterBar = $('.offers-filters'),
          fixedSidebar = $('.fixed-sidebar-wrapper'),
          offerList = $('.offers-list');

          fixedSidebar.animate({
            top:'60px'
          });
          filterBar.stop().animate({
            top:'60px'
          });
          offerList.stop().animate({
            paddingTop:'80px'
          });
        }

        e.preventDefault();
      });
    },
    postOffer:function () {
      //signupModal
      var signupModal = $('#signupModal');
      //open modal window
      $('.post-an-offer').on('click', function (e) {
        console.log("post-an-offer");
        form = new Yak.OffersOfferView()
        if (signupModal.html() == ""){
          signupModal.html(form.render().el)
           signupModal.modal()
        }else{
          signupModal.modal()
        } 

        e.preventDefault();
      });
      
      var signupCarousel = $('#signupCarousel'),
      modalInput = $('.modalInput');
      Dashboard.postOfferValidation();


      //display/hide field-hint on focus ++ changing opacity of the required-indicator
      modalInput.on('focus',
        function () {
          $(this).parents('.field-wrapper').addClass('focused');
          $('.field-hint').removeClass('visible')
          $(this).siblings('.field-hint').addClass('visible');
          signupCarousel.addClass('overflow_visible');
        }).on('blur',
        function () {
          $(this).parents('.field-wrapper').removeClass('focused');
                   
        }).on('change', function () {
        //check of its valid and add checkmark
        if ($(this).valid()) {
          $(this).parents('.field-wrapper').addClass('valid');
          $(this).parents('.field-wrapper').removeClass('required');
        } else {
          signupCarousel.addClass('overflow_visible');
          $(this).parents('.field-wrapper').removeClass('valid');
          $(this).parents('.field-wrapper').addClass('required');
        }
      });


      signupCarousel.carousel({
        interval:false
      }).on('slide',
        function () {
          signupCarousel.removeClass('overflow_visible');
        }).on('slid', function () {
        //signupCarousel.addClass('overflow_visible');
        });

      //required fields declaration
      var required_email = $('#createOffer_email'),
      required_textOffer = $('#offerText'),
      required_businessDetails = $('#businessName_address'),
      required_mobile = $('#personalInfo_mobile');

      //check for validation on first step - Your offer
      $('.createOffer-submit').on('click', function (e) {

              
        if (required_email.valid() && required_textOffer.valid()) {
          $.ajax({
            type: "POST",
            url: 'offers/create_offer',
            data: "email="+required_email.val() +"&offer_text=" + escape(required_textOffer.val()),
            dataType:"text",
            success: function(msg)
            {
              //If email exists, set response to true
              var next = ( msg == 'true' ) ? true : false;
                
            }
          })

                    
          signupCarousel.carousel('next');
              

        } else {
          signupCarousel.addClass('overflow_visible');
          signupModal.effect('shake', {
            times:1
          }, 70);
        }
        e.preventDefault();

      });


      //check for validation on second step - Your Business Details
      $('.businessDetails-submit ').on('click', function (e) {


        if (required_businessDetails.valid() && (
          ($('#mon-fri-open').val() !== "" && $('#mon-fri-close').val() !== "")
          || ($('#sat-open').val() !== "" && $('#sat-close').val() !== "")
          || ($('#sun-open').val() !== "" && $('#sun-close').val() !== "")
          )) {
          var data = {
            name:$('#business_name').val(),
            primary_address:$('#business_street').val(),
            city_id:$('#business_city_id').val(),
            postal_code:$('#business_postal_code').val(),
            lat:$("#business_lat").val(),
            lng:$("#business_lng").val(),
            mon_fri_open:$("#mon-fri-open").val(),
            mon_fri_close:$("#mon-fri-close").val(),
            sat_open:$("#sat-open").val(),
            sat_close:$("#sat-close").val(),
            sun_open:$("#sun-open").val(),
            sun_close:$("#sun-close").val()
          }
          $.ajax({
            type: "POST",
            url: 'offers/get_business_detail',
            data: data,
            dataType:"text",
            success: function(msg)
            {
              //If email exists, set response to true
              var next = ( msg == 'true' ) ? true : false;

            }
          })
          signupCarousel.carousel('next');
        } else {
          signupCarousel.addClass('overflow_visible');
          signupModal.effect('shake', {
            times:1
          }, 70);
          $('.time-picker').addClass('error');
        }
        e.preventDefault();
      });

      //check for validation on third step - Social media Links
      var facebook = 0;
      var twitter = 0;
      var google_place = 0;
      var yelp = 0;
      var yahoo = 0;
      $("#create-facebbook").on('click', function (e) {
        if($(this).attr("class") == "btn bootstrap-checkbox active") {
          facebook = 0;
        }else{
          facebook= 1;
        }
      });
      $("#create-twitter").on('click', function (e) {
        if($(this).attr("class") == "btn bootstrap-checkbox active") {
          twitter = 0;
        }else{
          twitter= 1;
        }
      });
      $("#create-gplaces").on('click', function (e) {
        if($(this).attr("class") == "btn bootstrap-checkbox active") {
          google_place = 0;
        }else{
          google_place = 1;
        }
      });
      $("#create-yelp").on('click', function (e) {
        if($(this).attr("class") == "btn bootstrap-checkbox active") {
          yelp = 0;
        }else{
          yelp= 1;
        }
      });
      $("#create-yahoo").on('click', function (e) {
        if($(this).attr("class") == "btn bootstrap-checkbox active") {
          yahoo = 0;
        }else{
          yahoo = 1;
        }
      });

      $('.socialMedia-submit').on('click', function (e) {
        var  facebook_id = $("#socialMedia-facebook").val(),
        twitter_id =  $("#socialMedia-twitter").val(),
        google_place_id = $("#socialMedia-places").val(),
        yelp_id = $("#socialMedia-yelp").val();
        $.ajax({
          type: "POST",
          url: 'offers/post_social_network_offer',
          data: "facebook_id="+escape(facebook_id) +"&twitter_id=" + escape(twitter_id) + "&google_place_id=" + escape(google_place_id) + "&yelp_id=" + escape(yelp_id) + "&facebook=" + facebook + "&google=" + google_place + "&twitter=" + twitter + "&yelp=" + yelp + "&yahoo=" + yahoo,
          dataType:"text",
          success: function(msg)
          {
            //If email exists, set response to true
            var next = ( msg == 'true' ) ? true : false;

          }
        })
        signupCarousel.carousel('next');
        e.preventDefault();
      });

      //check for validation on third step - Personal details
      $('.personalDetails-submit  ').on('click', function (e) {
        if (required_mobile.valid()) {
          $.ajax({
            type: "POST",
            url: 'offers/post_persional_detail',
            data: "first_name="+escape($("#personalInfo-fname").val()) +"&last_name=" + escape($("#personalInfo-lname").val()) + "&email=" + escape($("#personalInfo-email").val()) + "&password=" + escape($("#personalInfo-pass").val()) + "&mobile=" +  escape($("#personalInfo_mobile").val()),
            dataType:"text",
            success: function(msg)
            {
              //If email exists, set response to true
              var next = ( msg == 'true' ) ? true : false;

            }
          })
          signupModal.modal('hide');
        } else {
          signupCarousel.addClass('overflow_visible');
          signupModal.effect('shake', {
            times:1
          }, 70);
        }

        e.preventDefault();
      });


      //time-picker init
      $('.time-picker').timepicker({
        timeFormat:'H:i'
      });

      var match_radio = $('.match input[type=radio]'),
      match_item = $('.match');

      //matches list
      $('.matches-list').on('change', function () {
        match_item.removeClass('selected');
        match_radio.filter(':checked').parent(match_item).addClass('selected');
      });

      //Toggling 'overflow: hidden' for popup container in order to see the categories properly
      $('#business_category_chzn a.chzn-single').on('focus',
        function () {
          signupCarousel.addClass('overflow_hidden');
        }).on('blur', function () {
        signupCarousel.removeClass('overflow_hidden');
      });

      $('.bootstrap-checkbox').on('click', function (e) {
        e.preventDefault();
      });

      //submit offer - ##just close the modal for now
      $('.postOffer-submit').on('click', function () {
        $('#signupCarousel').modal('hide')
      });

      //changing business hours for other days when setting Mon-Fri hours (triggers only once - first time)
      var sat_open = $('#sat-open'),
      sun_open = $('#sun-open'),
      sat_close = $('#sat-close'),
      sun_close = $('#sun-close');
      $('#mon-fri-open').one('change', function () {
        var value = $(this).val();

        sat_open.val(value).removeClass('error');
        sun_open.val(value).removeClass('error');
      });
      $('#mon-fri-close').one('change', function () {
        var value = $(this).val();

        sat_close.val(value).removeClass('error');
        sun_close.val(value).removeClass('error');
      });


      //pre-populate email field in the "Personal details" step
      required_email.on('change', function () {
        var value = $(this).val();

        $('input#personalInfo-email').val(value);
        $('input#personalInfo-email').attr("disabled", "disabled");
        $('input#personalInfo-email').attr('readonly', 'readonly');

      });
      var places;
      //show-up search-matches after user clicks on Search button in the second step
      $('a.search-matches-btn').on('click', function () {
        $.ajax({
          type: "POST",
          url: 'offers/get_google_places',
          data: "content=" + $('#businessName_address').val(),
          dataType:"json",
          success: function(msg)
          {
            //If email exists, set response to true
 
            places = msg;
            var count = 0
            $('.matches-list').html("");
            for(var merchant in places) {
              var description = places[merchant.toString()]["description"] ;
              if(description.length > 35) {
                description = description.substr(0,32) + '...';
              }
              var name = places[merchant.toString()]["name"];
           
              count = count + 1;
              $('.matches-list').append('<li class="match"> <input id="match-' + merchant.toString() + '" type="radio" name="matches" value="" business_name="' + name + '" business_street="' + places[merchant.toString()]["formatted_address"] + '" business_city="' +  places[merchant.toString()]["city_name"] + '" business_postal_code="' + places[merchant.toString()]["postal_code"] + '" merchant_type_id="' + places[merchant.toString()]["merchant_type_id"] + 
                '" business_city_id="' + places[merchant.toString()]["city_id"] + '" lat="' + places[merchant.toString()]["lat"] + '" lng="' + places[merchant.toString()]["lng"] + '"/>     <label class="match-label" for="match-' + merchant.toString() + '">' + description + '</label>         </li>');
              $('#match-' + merchant.toString()).click(function () {
                $('#business_name').val($(this).attr("business_name"));
                $('#business_street').val($(this).attr("business_street"));
                Dashboard.set_value($(this).attr("business_city_id"), $('#business_city_id'));
                Dashboard.set_value($(this).attr("business_city"), $('#business_city'));
                Dashboard.set_value($(this).attr("business_postal_code"), $('#business_postal_code'));
                Dashboard.set_value($(this).attr("lat"), $('#business_lat'));
                Dashboard.set_value($(this).attr("lng"), $('#business_lng'));
                $("#business_city").attr("disabled", "disabled");
                $('#business_street').attr("disabled", "disabled");
                $('#business_postal_code').attr("disabled", "disabled");
                $("#merchant_merchant_type_id").val($(this).attr("merchant_type_id").toString());
                $('.map-wrapper').slideDown(1);
                if($(this).attr("lat") != "undefined" && $(this).attr("lng") != "undefined") {

                  var myOptions = {
                    zoom: 16,
                    center: new google.maps.LatLng(parseFloat($(this).attr("lat")), parseFloat($(this).attr("lng"))),
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    scrollwheel: false
                  };

                  var map = new google.maps.Map(document.getElementById("map_show"), myOptions);

                  var image = '/assets/markers/pin/image.png';
                  verifyMarker = new google.maps.Marker({
                    map: map,
                    position: new google.maps.LatLng(parseFloat($(this).attr("lat")), parseFloat($(this).attr("lng")))

                  });
                }
              });

            }
            $('.matches-list').append('<li class="match"> <input id="match-' + count.toString() + '" type="radio" name="matches" value=""/>     <label class="match-label" for="match-' + count.toString() + '">Other. Let me fill in the details</label>         </li>');
            $('#match-' + count.toString()).on('click', function () {
              $('#business_name').val('');
              $('#business_street').val('');
              $('#business_city').val('');
              $('#business_postal_code').val('');
              $("#business_lat").val("");
              $("#business_lng").val("");
              $('.map-wrapper').slideUp(1);
              $("#business_city").removeAttr('disabled');
              $('#business_street').removeAttr('disabled');
              $('#business_postal_code').removeAttr('disabled');
            });
            return false;
          }
        })
        $('div.hidden-matches').slideDown('300');
      });
      $('ul.matches-list').on('change', function () {
        if (!$('div.detailed-data').is(':visible')) {
          $('div.detailed-data').slideDown('300');
        }
            
      });


    },
    postOfferValidation:function () {
      var required_array = [0, 0, 0, 0, 0],
      ok = 0,
      progress_bar = $('.progress');
      var response;
      $.validator.addMethod("unique_email", function(value) {
        $.ajax({
          type: "POST",
          url: 'offers/unique_email',
          data: "email="+value,
          dataType:"text",
          success: function(msg)
          {
            //If email exists, set response to true
            response = ( msg == 'true' ) ? true : false;
          }
        })
        return response;
      }, "Email is already taken");
      $('.createOffer-form').validate({
        onfocusout:function (element) {
            
          $(element).valid();
          ok = 0;
            
          if ($('#createOffer_email').valid()) {
            required_array[0] = 1;
          } else {
            required_array[0] = 0;
          }
            
          if ($('#offerText').valid()) {
            required_array[1] = 1;
          } else {
            required_array[1] = 0;
          }
            
          for (var i = 0, len = required_array.length; i < len; i++) {
            ok += required_array[i];
          }
            
          $('span.ok').text(ok);
          console.log(ok);
          progress_bar.animate({
            width:66 * ok
          });
        },
        rules:{
          createOffer_email:{
            required:true,
            email:true,
            unique_email:true
          },
          offerText:{
            minlength:5,
            maxlength:140,
            required:true
          }
        }
      });
      $('.businessDetails-form ').validate({
        onfocusout:function (element) {
          $(element).valid();

          ok = 0;


          if ($('#businessName_address').valid()) {
            required_array[2] = 1;
          } else {
            required_array[2] = 0;
          }

          if (($('#mon-fri-open').val() !== "" && $('#mon-fri-close').val() !== "")
            || ($('#sat-open').val() !== "" && $('#sat-close').val() !== "")
            || ($('#sun-open').val() !== "" && $('#sun-close').val() !== "")) {
            required_array[3] = 1;
          } else {
            required_array[3] = 0;
          }


          for (var i = 0, len = required_array.length; i < len; i++) {
            ok += required_array[i];
          }

          $('span.ok').text(ok);
          progress_bar.animate({
            width:66 * ok
          });
        },
        rules:{
          businessName_address:{
            required:true
          },
          businessHours:{
            required:true
          }
        }
      });

      $('.personalInfo-form ').validate({
        onfocusout:function (element) {
          $(element).valid();

          ok = 0;


          if ($('#personalInfo_mobile').valid()) {
            required_array[4] = 1;
          } else {
            required_array[4] = 0;
          }

          for (var i = 0, len = required_array.length; i < len; i++) {
            ok += required_array[i];
          }

          $('span.ok').text(ok);
          progress_bar.animate({
            width:66 * ok
          });
        },
        rules:{
          personalInfo_mobile:{
            required:true,
            digits: true
          }
        }
      });


    },
        
    
    initMain:function () {
      $(document).ready(function () {
        Dashboard.initDropDowns();
        Dashboard.filterOffers();
        Dashboard.offerItem();
        Dashboard.offerActions();
        Dashboard.closeHint();
        Dashboard.postOffer();

      })
    }
  };
// Pass in jQuery.
})(jQuery);


Dashboard.initMain();