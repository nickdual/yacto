var Merchant_admin = (function ($) {
    return {
        renameMerchant:function () {
            $('.merchant_name_action').on('click', function () {
                var clicked_el = $(this),
                merchant_name = $('.merchant_name');

                if (clicked_el.hasClass('merchant_name_edit')) {
                    merchant_name.attr('contenteditable', 'true').focus();
                } else {
                    merchant_name.attr('contenteditable', 'false');
                }

                clicked_el.toggleClass('merchant_name_edit merchant_name_accept');
            });
        },
        collapseSubsection:function () {
            var subesection_title = $('.subsection_title ');

            subesection_title.on('click', function (e) {

                $(this).toggleClass('collapsed')
                .closest('h3')
                .siblings('.subsection_content ').slideToggle();

                e.preventDefault();
            });

            $('a.new_offer').on('click', function (e) {
                $('div.add_offer_section').fadeIn()
                $('div.addOffer_subsection').show()
                $('div.addOffer_subsection .subsection_content').show()
                $(this).addClass('overlay');
                //.find(subesection_title).trigger('click');
              
                e.preventDefault();
            });

            $('a.add_user').on('click', function (e) {
                $('div.add_user_section').fadeIn()
                $('div.addUser_subsection').show()
                $('div.addUser_subsection .subsection_content').show()
                $(this).addClass('overlay');
                //.find(subesection_title).trigger('click');
                //$("#edit_user_merchant").hide();
                //$("#add_user_merchant").show();
                
                e.preventDefault();
            });

        },
        initDropDowns:function () {
            $('.chzn-select').chosen({
                disable_search_threshold:10
            });
        },
        initTooltips:function () {
            $('.' +
                'offer_action ').tooltip();
            $('.user_action ').tooltip();
            $('.comingSoon_tooltip').tooltip();
        },
        initTimePicker:function () {
            //time-picker init
            $('.time_picker').timepicker({
                timeFormat:'H:i'
            });
        },
        closeHint:function () {
            $('.close-hint').on('click', function (e) {
                $(this).closest('.hint').fadeOut(500);

                e.preventDefault();
            })
        },
        sortableGallery:function () {
            $('.uploaded_photos').sortable();
            $('.uploaded_photos').disableSelection();
        },
        initMain:function () {
            $(document).ready(function () {
                Merchant_admin.renameMerchant();
                Merchant_admin.collapseSubsection();
                Merchant_admin.initDropDowns();
                Merchant_admin.initTooltips();
                Merchant_admin.initTimePicker();
                Merchant_admin.closeHint();
                Merchant_admin.sortableGallery();
            })
        }
    };
// Pass in jQuery.
})(jQuery);


Merchant_admin.initMain();