// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
// LIBRARIES:
//= require jquery
//= require jquery_ujs

//= require lib/jquery-ui-1.8.24.custom.min


//= require jquery.cookie
//= require i18n
//= require i18n/translations

//= require lib/handlebars-1.0.0.beta.6
//= require lib/bootstrap-transition
//= require lib/jquery.easing.1.3


//= require lib/chosen
//= require lib/iphone-style-checkboxes
//= require lib/bootstrap-modal
//= require lib/bootstrap-tooltip
//= require lib/bootstrap-tab
//= require lib/bootstrap-dropdown
//= require lib/bootstrap-button
//= require lib/bootstrap-carousel
//= require lib/jquery.timepicker.min
//= require lib/jquery.validate
//= require lib/jquery.fancybox.pack
//= require lib/jquery.royalslider.min
//= require lib/jquery.jeditable

// OUR SCRIPTS:
// require lib/page_dashboard
//= require lib/page_merchant_admin
//= require backbone/app
//= require ./hint
//= require ./offer_infobox
//= require ./facebook_utils
//= require ./params_utils
//= require_directory ./templates

//= require ./users
//= require ./merchants
//= require get_city
//= require ./photo_stack
//= require ./map_directions
//= require ./login
//= require ./forgot_pass
//= require repeat_day
// require backbone/desktop/views/users/put_popup

