var PhotoStack = {
  init: function(mainElemSelector){
    PhotoStack.layoutImages(mainElemSelector);
    PhotoStack.observeClicks(mainElemSelector);
  },

  layoutImages: function(mainElemSelector){
    var $imgs = $(mainElemSelector+' img');
    $imgs.each(function(i, image){
      var r = Math.floor(Math.random() * 10) * -1,
        rotation = 'rotate(' + r + 'deg)';

      $(image).css({
        '-moz-transform': rotation,
        '-webkit-transform': rotation,
        '-o-transform': rotation,
        'transform': rotation
      });
    })
  },

  observeClicks: function(mainElemSelector){
    $(mainElemSelector+" .next_photo").on("click", PhotoStack.nextPhoto);
    $(mainElemSelector+" .prev_photo").on("click", PhotoStack.prevPhoto);
  },

  nextPhoto: function(e){
    e.preventDefault();
    var $mainElem = $(e.target).parents('div.merchant_photo_stack');
    var $current = $mainElem.find('img:last');
    var r = Math.floor(Math.random() * 10) * -1;

    var currentPositions = {
      marginLeft:$current.css('margin-left'),
      marginTop:$current.css('margin-top')
    };

    var $new_current = $current.prev();


    $current.animate({
        'marginLeft':'250px',
        'marginTop':'-350px'
      }, 250, function () {
        $(this).insertBefore($mainElem.find('img:first'))
            .css({
                '-moz-transform':'rotate(' + r + 'deg)',
                '-webkit-transform':'rotate(' + r + 'deg)',
                '-o-transform':'rotate(' + r + 'deg)',
                'transform':'rotate(' + r + 'deg)'
            })
            .animate({
                'marginLeft':currentPositions.marginLeft,
                'marginTop':currentPositions.marginTop
            }, 250, function () {
              $new_current.css({
                  '-moz-transform':'rotate(0deg)',
                  '-webkit-transform':'rotate(0deg)',
                  '-o-transform':'rotate(0deg)',
                  'transform':'rotate(0deg)'
              });
            });
    });
  },

  prevPhoto: function(e){
    e.preventDefault();

    var $mainElem = $(e.target).parents('div.merchant_photo_stack');
    var $current = $mainElem.find('img:first');
    var r = Math.floor(Math.random() * 10);
    var rotation = 'rotate(' + r + 'deg)';

    var currentPositions = {
      marginLeft:$current.css('margin-left'),
      marginTop:$current.css('margin-top')
    };

    var $new_current = $current.next();

    $current.animate({
        'marginLeft':'250px',
        'marginTop':'-350px'
    }, 250, function () {
        $(this).insertAfter($mainElem.find('img:last'))
            .css({
                '-moz-transform': rotation,
                '-webkit-transform':rotation,
                '-o-transform':rotation,
                'transform':rotation
            })
            .animate({
                'marginLeft':currentPositions.marginLeft,
                'marginTop':currentPositions.marginTop
            }, 250, function () {
                $new_current.css({
                    '-moz-transform':rotation,
                    '-webkit-transform':rotation,
                    '-o-transform':rotation,
                    'transform':rotation
                });
            });
    });

  }

}
