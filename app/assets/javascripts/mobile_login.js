window.MobileLogin = {
  template: JST["templates/mobile_login"],

  init: function(){
    MobileLogin.addObservers();
  },

  addObservers: function(){
    $(document).delegate('.require-login', 'click', MobileLogin.showLoginPopup);
  },

  showLoginPopup: function(e) {
    e.preventDefault()
    // var sign_in = '<%= t("form.signin") %>';
    // var sign_up = '<%= t("form.signup") %>';
    var sign_in_with = I18n.t('home.index.sign_in_with');
    var post_to = I18n.t('home.index.post_to');
    var on = I18n.t('home.index.on');
    var off = I18n.t('home.index.off');
    
    $('<div>').simpledialog2({
      mode: 'blank',
      headerText: 'Yacto',
      headerClose: true,
      blankContent : 
        MobileLogin.template({
          on: on,
          off: off,
          post_to: post_to,
          sign_in_with: sign_in_with,
          isFacebookOn: MobileLogin.isFacebookOn,
          isTwitterOn: MobileLogin.isTwitterOn,
          authenticityToken: MobileLogin.authenticityToken()
        })
    })
  },

  authenticityToken: function(){
    return $("meta[name=csrf-token]").attr("content");
  },

  isFacebookOn: function(){
    var facebook_on = document.cookie["facebook_on"];
    return facebook_on == 'true';
  },

  isTwitterOn: function(){
    var twitter_on = document.cookie["twitter_on"];
    return twitter_on == 'true';
  }
}

$(function(){
  MobileLogin.init();
})