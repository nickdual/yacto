class RegistrationsController < Devise::RegistrationsController
  def create
    @user = User.new(params[:user])
    begin
      @client = Twilio::REST::Client.new ACCOUNT_SID, AUTH_TOKEN
      @client.account.sms.messages.create(:from => TWILIO_NUM, :to => params[:user][:mobile_number],
        :body => "please login yacto.com with username=" + params[:user][:username] + ",password=" + params[:user][:password])

    rescue
      puts 'International SMS delivery not available'
    end
    if params[:user][:email] == ''
      @user.skip_confirmation!      
      @user.email = params[:user][:username] + '@' + params[:user][:username] + '.com'
    end
    @user.role_id = Role.find(:first, :conditions => ["name = ?", 'Consumer']).id
    if  @user.save
      puts '1'
      flash[:notice] = t("users.confirmationsent")
      sign_in(@user)
      respond_to do |format|
        format.html {redirect_to root_url}
        format.json { render json: @user }
        format.mobile { redirect_to root_url}

      end

    else
      puts '2'
      respond_to do |format|
        format.html {render :action => :new}
        format.json { render json: @user.errors }
        format.mobile { render :action => :new }

      end

    end  
  end

end
