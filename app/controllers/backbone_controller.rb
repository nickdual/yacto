class BackboneController < ApplicationController
  layout :choose_layout
  helper_method :city, :favorites, :merchant_types

  def show
    set_current_city
    puts session
    puts 'fffffffffffff'
    if current_user
      @favorites = current_user.favorites.pluck("favorites.offer_id")
    end
  end

  def merchant_types
    @merchant_types ||= MerchantType.top_lvl
  end

  def city
    @city ||= locate_ip(request.remote_ip.to_s)
  end

  def favorites
    return @favorites ||= current_user.favorites.pluck("favorites.offer_id") if current_user
    @favorites ||= []
  end

  def choose_layout
    return "backbone" if request.format == :mobile
    layout_by_role
  end

end