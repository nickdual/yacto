require 'geonames'
class ApplicationController < ActionController::Base
  include Geokit::Geocoders
  helper_method :body_class, :layout_by_role

  has_mobile_fu
  before_filter :set_locale
  before_filter :prepare_for_mobile
  protect_from_forgery
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url #, :alert => exception.message
	end
	layout :layout_by_role

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  def current_city
    @current_city = City.find_by_name("New York")
  end

  def current_country
    # return current_city.country
  end

  def current_location(lat,lon, cookies)
    if lat
      location = GoogleGeocoder.reverse_geocode([lat,lon])

    else 
      if cookies[:lon]
        location = GoogleGeocoder.reverse_geocode([cookies[:lat],cookies[:lon]])
      else
        if Rails.env == 'production'
          location = MultiGeocoder.geocode(request.remote_ip)
        else
          location = MultiGeocoder.geocode("San Francisco, California, US")
        end
      end
      
    end
    return location
  end

  def locate_ip(ip)
    IpGroupCity.locate_ip(ip)
  end

  #TODO does it really need to be in application_controller ?
  def set_current_city
    city = locate_ip(request.remote_ip.to_s)
    session[:city_id] = city.id
    session[:timezone] = city.timezone
  end

  #TODO does it really need to be in application_controller ?
  def set_country_and_timezone(locations)
    locations.all.each do |location| 
      city = find_city(location.hash)
      if city
        session[:timezone] = city.timezone
        session[:city_id] = city.id
        #          session[:twilio_number] = city.country.twilio_number if city.country
        break
      end
    end

    #TODO this looks like it's own method
    if session[:timezone].blank?
      sleep(24/24.0)
      timezone = Geonames::WebService.timezone locations.lat,locations.lng
      session[:timezone] = timezone.timezone_id
      #      country = Geonames::WebService.country_code location.lat,location.lng
      #      if country[0] and country[0].country_code
      #        if Country.where(:iso_code_two_letter => country[0].country_code).first
      #          session[:twilio_number] =  Country.where(:iso_code_two_letter => country[0].country_code).first.twilio_number
      #        end
      #      end
    end
  end

  private

  def find_city(options)
    city = City.scoped
    city = city.cityname(options[:city]) if options[:city]
    city = city.country_code(options[:country_code]) if options[:country_code]
    city.first
  end

  helper_method :mobile_device?

  def prepare_for_mobile
    request.format = :mobile if  in_mobile_view?
    request.format = :html if in_tablet_view?
  end

  #  def after_sign_out_path_for(resource_or_scope)
  #    request.referrer
  #  end

  def after_sign_in_path_for(resource)
    if request.format == :mobile
      offers_url(:lat => cookies[:lat], :lon => cookies[:lon], :time => 0,:distance => 1.0)
    else
      bb_url #backbone root - dashboard
    end
    #    return request.env['omniauth.origin'] || stored_location_for(resource) || root_path
  end

  def log_error(e)
    logger.error(e)
    logger.error(e.backtrace.join("\n"))
  end

  def self.body_class(body_class)
    before_filter lambda{ @body_class = body_class }
  end

  
  def body_class
    @body_class || "merchant_admin"
	end


	# load role specific navigation elements
	def layout_by_role
  	if current_user and request.format != :mobile
			case current_user.role_id
				when 1
					'yacto_staff'
				when 2
					'yacto_staff'
				when 3
					'merchant'
				when 4
					'merchant'
				when 5
					'consumer'
				when 6
			    'yacto_staff'
				else
					'default'
			end
		elsif request.format == :mobile
			'application'
		else
			'default'
		end

	end

  def cleanup_offers(offers)
    offers.reject do |offer|
      if offer.merchant.nil?
        puts "OFFER WITH NON-EXISTING MERCHANT - id: #{offer.id} <====================================="
        true
      else
        if offer.merchant.merchant_type.nil?
          puts "MERCHANT WITHOUT MERCHANT_TYPE - id: #{offer.merchant.id} <====================================="
          true
        else
          false
        end
      end
    end
  end
end
