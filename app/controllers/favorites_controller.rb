class FavoritesController < ApplicationController
  def create
    favorite = Favorite.new(params[:favorite])
    favorite.user = current_user
    favorite.twitter_on = cookies[:twitter_on]
    favorite.facebook_on = cookies[:facebook_on]
    if favorite.save
      render json: favorite, status: :created
    else
      render json: favorite.errors, status: :unprocessable_entity
    end
  end

  def destroy
    current_user.favorites.find(params[:id]).destroy
    respond_to do |format|
      format.json { render text: "", status: :ok }
    end
  end

  def index
    respond_to do |format|
      format.json {
        favorites = current_user.favorites
        favorites = favorites.for_offers if params[:offers].present?
        render json: favorites
      }
      format.mobile {
        prepare_params
        @favorites = merge_results(favorite_offers_by_distance, favorite_merchants_by_distance)
        @active_tab = :favorites 
        render mobile: @ofavorites, :layout => "new_application"
      } 
    end
  end

  def prepare_params
    session[:current_location] = current_location(params[:lat].to_f, params[:lon].to_f, cookies) if session[:current_location] == nil
    set_country_and_timezone(session[:current_location]) if session[:timezone] == nil
  #   set_current_city if session[:city_id] == nil
    @res = session[:current_location]

    session[:timezone] = "Asia/Ho_Chi_Minh"
    tz = TZInfo::Timezone.get(session[:timezone])
    @now_time = tz.utc_to_local(Time.current)
  end

  def favorite_offers_by_distance
    @offers = current_user.favorite_offers
    @offers = @offers.geo_scope(:origin => @res, :within => distance_or_default)
    @offers = @offers.where(query_string, conditions) if query_string != ''
    if params[:merchant_type_id].present? #is it OK that we ignore query_string here ?
      @offers = @offers.joins(:merchant)
      @offers = @offers.where("merchants.merchant_type_id = ?", params[:merchant_type_id])
    end            
    # @offers = @offers.joins({:merchant => {:offers => :favorites}})
    # @offers = @offers.where({:favorites => {:user_id => current_user.id}})

    @offers = @offers.active.limit(15).order('distance asc').with_merchant
    @offers = @offers.includes(:merchant => [:merchant_type])

    @offers = cleanup_offers(@offers)
  end

  def favorite_merchants_by_distance
    @merchants = current_user.favorite_merchants
    @merchants = @merchants.geo_scope(:origin => @res, :within => distance_or_default)
    # @merchants = @merchants.where(query_string, conditions) if query_string != ''
    if params[:merchant_type_id].present? #is it OK that we ignore query_string here ?
      # @offers = @offers.joins(:merchant)
      @merchants = @merchants.where("merchants.merchant_type_id = ?", params[:merchant_type_id])
    end            
    # @offers = @offers.joins({:merchant => {:offers => :favorites}})
    # @offers = @offers.where({:favorites => {:user_id => current_user.id}})

    @merchants = @merchants.limit(15).order('distance asc').where("merchant_type_id IS NOT NULL")
    @merchants = @merchants.includes(:merchant_type)

    # @offers = cleanup_offers(@offers)
  end

  def merge_results(offers, merchants)
    (offers+merchants).sort { |a,b| a.distance <=> b.distance }
  end

  # def favorite_offers
  #   # cookies[:offset] = 0
  #   session[:current_location] = current_location(params[:lat].to_f, params[:lon].to_f, cookies) if session[:current_location] == nil
  #   set_country_and_timezone(session[:current_location]) if session[:timezone] == nil
  #   set_current_city if session[:city_id] == nil
  #   @res = session[:current_location]
  #   #params[:merchant_type_id] = nil  if params[:favorite] == 'true'
  #   session[:timezone] = "Asia/Ho_Chi_Minh"
  #   tz = TZInfo::Timezone.get(session[:timezone])
  #   @now_time = tz.utc_to_local(Time.current)

  #   @offers = Offer.geo_scope(:origin => @res, :within => distance_or_default)
  #   @offers = @offers.where(query_string, conditions) if query_string != ''
  #   if params[:merchant_type_id].present? #is it OK that we ignore query_string here ?
  #     @offers = @offers.joins(:merchant)
  #     @offers = @offers.where("merchants.merchant_type_id = ?", params[:merchant_type_id])
  #   end            
  #   @offers = @offers.joins({:merchant => {:offers => :favorites}})
  #   @offers = @offers.where({:favorites => {:user_id => current_user.id}})

  #   @offers = @offers.active.limit(15).order('distance asc').with_merchant
  #   @offers = @offers.includes(:merchant => [:merchant_type])

  #   if @offers and @offers.last
  #     @offer_last_id = @offers.last.id
  #   end

  #   @offers = cleanup_offers(@offers)
  # end

  def query_string
    params[:time] ? Offer.compare_time_with_labels : ""
  end

  def conditions
    conditions = {}
    if params[:time]
      if params[:time] == '0'
        next_hours = Time.current.end_of_day
      else
        next_hours = @now_time + params[:time].to_i.hour
      end
      conditions[:start_time] = @now_time.strftime("%H-%M-%S")
      conditions[:end_time] = next_hours.strftime("%H-%M-%S")
    end
    conditions
  end

  def distance_or_default
    params[:distance] ? params[:distance].to_f : 1
  end
end
