class UsersController < ApplicationController
  load_and_authorize_resource

  # GET /users
  # GET /users.json
  def index

    @merchant = Merchant.where(:merchant_id => params[:merchant_id]).first
    @user = @merchant.users.find(:all,:joins => :role,:conditions => {:roles => {:name => ["Merchant admin","Super User"]}} ).first

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @users }
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id]) unless params[:id].blank?
    @merchant = @user.merchant
    @user_merchant = @user
    
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/new
  # GET /users/new.json
  def new
    @user = User.new
    #    unless params[:merchant_id].blank?
    #      merchant = Merchant.find params[:merchant_id]
    #      @user.merchant_id = merchant.id
    #      @user.role_id = Role.first.id
    #    end
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
  end



  # POST /users
  # POST /users.json
  def create
    @user = User.new(params[:user])
    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: t("users.create") }
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { render action: "new" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    params[:user].delete(:password) if params[:user][:password].blank?
    @user = User.find(params[:id])
    
    respond_to do |format|
      if @user.update_attributes(params[:user])
        @user["role_name"] = @user.role.name
        if @user.role.name == "Merchant admin"
          @users = User.where("merchant_id = ? AND id != ?",params[:user][:merchant_id],@user.id)
          @role_id = Role.where(:name => "Merchant employee").first.id
          @users.each do |user|
            user.role_id = @role_id
            user.user_admin = false
            user.save
          end
        end
        format.html { redirect_to @user, notice: t("users.update") }
        format.json { render json: @user,status: :created, location: @user }
      else
        puts 'aa'
        puts @user.errors.full_messages
        puts 'aa'
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def account_setting
    address = ''
    address = params[:user][:address_line1] if  params[:user][:address_line1].present?
    address += ' , ' + params[:city_name] if params[:city_name].present?
    address += ' , ' + params[:user][:state_province_region] if params[:user][:state_province_region].present?
    address += ' ' + params[:user][:postal_code] if params[:user][:postal_code].present?
    address += ' , '  + params[:user][:country] if params[:user][:country].present?


    loc = GoogleGeocoder.geocode(address)
    params[:user][:address_line1] = loc.street_address

    @user = User.find(params[:id])

    if (loc.street_address != '')
      params[:user][:address_line1] = loc.street_address
      params[:user][:postal_code] = loc.zip
      params[:user][:state_province_region] = loc.state

      loc.all.each { |e|
        if e.hash[:city]

          city = City.where(:name => e.hash[:city].strip,:country_iso_code_two_letters => loc.country_code).first
          if city
            params[:user][:city_name] = city.name
            params[:user][:city_id] = city.id
            #          session[:twilio_number] = city.country.twilio_number if city.country
            break
          end
        end
        }

      params[:user][:country] = City.find(params[:user][:city_id].to_i).country.name if params[:user][:city_id]
    end


    @user.update_attributes(params[:user])

    render :text => 'true'
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user = User.find(params[:id])
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end
 
  def sign_out
    
  end

  def toggle_offer
    state = ""
    offer = Offer.find params["id"]
    if offer.active == 0
      offer.active = 1
      state = "Stop"
    else
      offer.active = 0
      state = "Start"
    end
    offer.save!
    return render :json => {:success => true, :state => state, :value => offer.active}
  end
  
  def change_recurrence
    offer = Offer.find params["id"]
    offer.recurrence = params["type"]

    offer.save!
    return render :json => {:success => true , :state => offer.recurrence}
  end

  def ajax_sub_types
    ids = MerchantType.where(:merchant_type => params["type"]).map(&:id)
    return render :json => {:success => true , :shown_ids => ids}
  end

  def new_employee
    chars = 'abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ23456789'
    password = ''
    puts 'dddd'
    6.times { |i| password << chars[rand(chars.length)] }
    puts password
    params[:user][:password]  = password
    params[:user][:username] =  "usman+#{(Time.now.to_f)}@devsinc.com"
    @merchant = Merchant.find params[:user][:merchant_id]
    @user = User.new params[:user]
    #    @user_merchant = params[:user_merchant_id]
    @user.username = params[:user][:email]

    @user.role_id = (params[:user][:role_id].nil?) ? Role.where(:name => "Merchant employee").first.id : params[:user][:role_id]
    
    respond_to do |format|
      if @user.save
        @user["role_name"] = @user.role.name
        if @user.role.name == "Merchant admin"
          @users = User.where("merchant_id = ? AND id != ?",params[:user][:merchant_id],@user.id)
          @role_id = Role.where(:name => "Merchant employee").first.id
          @users.each do |user|
            user.role_id = @role_id
            user.user_admin = false
            user.save
          end
        end
        format.html { redirect_to merchant_details_path @merchant.merchant_id}
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { render action: "show" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
  def new_merchant_offer
    @merchant = Merchant.find params[:offer][:merchant_id]
    #    @user = User.find(params[:offer][:user_id])
    @offer = Offer.new(params[:offer] )

    #puts(params[:offer][:merchant_type_id])
    @offer.primary_address = @merchant.primary_address
    @offer.lat = @merchant.lat
    @offer.lon = @merchant.lon
    @offer.merchant = @merchant
    @offer.city_id = @merchant.city_id
    #puts(@offer.merchant_type_id)
    respond_to do |format|
      if @offer.save
        @offer.update_attributes(:merchant_type_id => params[:offer][:merchant_type_id])if params[:offer][:merchant_type_id].present?
        #puts(@offer.merchant_type_id)
        if params[:offer][:user_id]
          user = User.find(params[:offer][:user_id].to_i)
          if user
            user.offer_description( @offer.description)
          end
        end
        @offer["format_local_end_day"] = @offer.local_end_day.strftime("%Y-%m-%d") if @offer.local_end_day
        @offer["format_start_time"] = @offer.start_time.strftime("%H:%M") if  @offer.start_time
        @offer["format_end_time"] = @offer.end_time.strftime("%H:%M") if  @offer.end_time
        format.html { redirect_to url_for(@offer.user) }
        format.json { render json: @offer, status: :created, location: @offer }
      else
        format.html { render "show" }
        format.json { render json: @offer.errors, status: :unprocessable_entity }
      end
    end
  end
  def session_user
    @user = current_user
    if @user.blank?
      render :json => nil
    else
      render :json => {:email => @user.email, :id => @user.id, :firs_name => @user.first_name, :last_name => @user.last_name}
    end

  end
  def get_session
    #puts current_user.id
    @user = current_user
    if @user.blank?
      render :json => {:id => nil, :access_token => nil}
    else
      render :json => {:id => @user.id, :access_token => "xxxxxxxx"}
    end

  end

  def generate_code()
    @user = User.find(params[:id])
    chars = 'abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ23456789'
    code = ''
    4.times { |i| code << chars[rand(chars.length)] }
    city = @user.merchant.city
    twilio_number = city.country.twilio_number if city.country
    if params[:user][:value]== '1'
      Merchant.send_twilio(@user.mobile_number,twilio_number ? twilio_number : TWILIO_DEFAULT_PHONE, I18n.t("merchant.smsrequest.code.confirm", :code => @user.code ))
      @user.update_attributes(:code => code)
    end

    render :text => 'true'
  end

end
