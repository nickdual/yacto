class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  #  before_filter :oauth_twitter_require, :only => :twitter
  #  def self.consumer
  #    # The readkey and readsecret below are the values you get during registration
  #    OAuth::Consumer.new(ENV['TWITTER_CONSUMER_KEY'], ENV['TWITTER_CONSUMER_SECRET'],
  #      { :site=>"http://twitter.com" })
  #  end
  #  def oauth_twitter_require
  #    @request_token = OmniauthCallbacksController.consumer.get_request_token
  #    session[:request_twitter_token] = @request_token.token
  #    session[:request_twitter_token_secret] = @request_token.secret
  #    puts'session'
  #    puts session
  #    puts 'session'
  #  end
  
  def twitter
    # You need to implement the method below in your model
    @user = User.find_for_twitter_oauth(request.env["omniauth.auth"], current_user)
    if @user and @user.persisted?
      flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => "Twitter"
      sign_in_and_redirect @user, :event => :authentication
      puts "sign_in_and_redirect @user, :event => :authentication"
    else
      session["devise.twitter_data"] = request.env["omniauth.auth"]
      redirect_to new_user_registration_url
    end

  end
  def facebook
    # You need to implement the method below in your model
    @user = User.find_for_facebook_oauth(request.env["omniauth.auth"], current_user)
    if @user and @user.persisted?
      flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => "Facebook"
      sign_in_and_redirect @user, :event => :authentication
      puts "sign_in_and_redirect @user, :event => :authentication"
    else
      session["devise.facebook_data"] = request.env["omniauth.auth"]
      redirect_to new_user_registration_url
    end
  end
  def google_oauth2
    @user = User.find_for_google_oauth(request.env["omniauth.auth"], current_user)
    if @user and @user.persisted?
      flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => "Google"
      sign_in_and_redirect @user, :event => :authentication
    else
      session["devise.google_data"] = request.env["omniauth.auth"]
      redirect_to new_user_registration_url
    end
  end
  def yahoo
    # You need to implement the method below in your model
    @user = User.find_for_yahoo_oauth(request.env["omniauth.auth"], current_user)
    #    auth =  request.env["omniauth.auth"]
    #    render :text => auth.to_json
    if @user and @user.persisted?
      flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => "Yahoo"
      sign_in_and_redirect @user, :event => :authentication
      puts "sign_in_and_redirect @user, :event => :authentication"
    else
      session["devise.yahoo_data"] = request.env["omniauth.auth"]
      redirect_to new_user_registration_url
    end
  end
end
