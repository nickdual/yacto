class OfferInsertionsController < ApplicationController
  load_and_authorize_resource

  # GET /offer_insertions
  # GET /offer_insertions.json
  def index
    @offer_insertions = OfferInsertion.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @offer_insertions }
    end
  end

  # GET /offer_insertions/1
  # GET /offer_insertions/1.json
  def show
    @offer_insertion = OfferInsertion.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @offer_insertion }
    end
  end

  # GET /offer_insertions/new
  # GET /offer_insertions/new.json
  def new
    @offer_insertion = OfferInsertion.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @offer_insertion }
    end
  end

  # GET /offer_insertions/1/edit
  def edit
    @offer_insertion = OfferInsertion.find(params[:id])
  end

  # POST /offer_insertions
  # POST /offer_insertions.json
  def create
    @offer_insertion = OfferInsertion.new(params[:offer_insertion])

    respond_to do |format|
      if @offer_insertion.save
        format.html { redirect_to @offer_insertion, notice: t('offerinsertion.create') }
        format.json { render json: @offer_insertion, status: :created, location: @offer_insertion }
      else
        format.html { render action: "new" }
        format.json { render json: @offer_insertion.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /offer_insertions/1
  # PUT /offer_insertions/1.json
  def update
    @offer_insertion = OfferInsertion.find(params[:id])

    respond_to do |format|
      if @offer_insertion.update_attributes(params[:offer_insertion])
        format.html { redirect_to @offer_insertion, notice: t('offerinsertion.update') }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @offer_insertion.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /offer_insertions/1
  # DELETE /offer_insertions/1.json
  def destroy
    @offer_insertion = OfferInsertion.find(params[:id])
    @offer_insertion.destroy

    respond_to do |format|
      format.html { redirect_to offer_insertions_url }
      format.json { head :no_content }
    end
  end
end
