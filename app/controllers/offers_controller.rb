require 'json'
require "crack"
require 'tzinfo'
class OffersController < ApplicationController
  #load_and_authorize_resource
  include Geokit::Geocoders
  body_class "dashboard"
  helper_method :offers
  # GET /offers
  # GET /offers.json
  #load_and_authorize_resource
  def index
    cookies[:offset] = 0
    session[:current_location] = current_location(params[:lat].to_f, params[:lon].to_f, cookies) if session[:current_location] == nil
    set_country_and_timezone(session[:current_location]) if session[:timezone] == nil
    set_current_city if session[:city_id] == nil
    @res = session[:current_location]
    #params[:merchant_type_id] = nil  if params[:favorite] == 'true'
    session[:timezone] = "Asia/Ho_Chi_Minh"
    tz = TZInfo::Timezone.get(session[:timezone])
    @now_time = tz.utc_to_local(Time.current)

    @active_tab = :list
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: offers }
      format.mobile { render :index, :layout => "new_application"}
      format.xml { render xml: offers }
      format.js #{ render xml: @offers }
    end
  end

  def offers
    return @offers if @offers.present?

    # puts "@res: #{@res} <==================================="
    # puts "distance_or_default: #{distance_or_default} <==================================="
    @offers = Offer.geo_scope(:origin => @res, :within => distance_or_default)
    @offers = @offers.where(query_string, conditions) if query_string != ''
    if params[:merchant_type_id].present? 
      @offers = @offers.joins(:merchant)
      @offers = @offers.where("merchants.merchant_type_id = ?", params[:merchant_type_id])
    end            
    if params[:favorite] == 'true'
      @offers = @offers.joins({:merchant => {:offers => :favorites}})
      @offers = @offers.where({:favorites => {:user_id => current_user.id}})
    end

    @offers = @offers.active.limit(15).order('distance asc').with_merchant
    @offers = @offers.includes(:merchant => [:merchant_type])
    @offers = @offers.offset(params[:offset]) if params[:offset]

    # if @offers and @offers.last
    #   @offer_last_id = @offers.last.id
    # end

    # puts "@offers.to_sql: #{@offers.to_sql} <==================================="
    @offers = cleanup_offers(@offers)
  end

  def query_string
    params[:time] ? Offer.compare_time_with_labels : ""
  end

  def conditions
    conditions = {}
    if params[:time]
      if params[:time] == '0'
        next_hours = Time.current.end_of_day
      else
        next_hours = @now_time + params[:time].to_i.hour
      end
      conditions[:start_time] = @now_time.strftime("%H-%M-%S")
      conditions[:end_time] = next_hours.strftime("%H-%M-%S")
    end
    conditions
  end

  def distance_or_default
    params[:distance] ? params[:distance].to_f : 1
  end

  def filter
    @merchant_types = MerchantType.find(:all,:conditions => ["sub_type = ?"," "], :order => 'merchant_type')
    cookies[:time] = params[:time].to_i
    cookies[:distance] = params[:distance].to_f
  end
  def ajax_time
    cookies[:time] = params[:id].to_i
    render :text => 'ok'
  end
  def ajax_distance
    cookies[:distance] = params[:id].to_f
    render :text => 'ok'
  end
  def nearby
    session[:current_location] = current_location(params[:lat].to_f, params[:lon].to_f, cookies) if session[:current_location] == nil
    set_country_and_timezone(session[:current_location]) if session[:timezone] == nil
    set_current_city if session[:city_id] == nil
    @res = session[:current_location]
    if @res
      @my_location = "#{@res.lat.to_s},#{@res.lng.to_s}"
    end
    tz = TZInfo::Timezone.get(session[:timezone])
    @now_time = tz.utc_to_local(Time.current)
        
    @active_tab = :nearby
    respond_to do |format|
      format.html # show.html.erb
      format.mobile {render :nearby, :layout => "new_application" }
      format.json 
    end
    
  end
  def ajax_offer_group
    @res = session[:current_location]

    query_string = ""
    conditions = {}
    tz = TZInfo::Timezone.get(session[:timezone])
    @now_time = tz.utc_to_local(Time.current)
    if params[:time]
      query_string = Offer.compare_time
      if params[:time] == '0'
        next_hours = Time.current.end_of_day()
      else
        next_hours = @now_time + params[:time].to_i.hour
      end
      conditions[:start_time] = @now_time.strftime("%H-%M-%S")
      conditions[:end_time] = next_hours.strftime("%H-%M-%S")
    end
    if params[:merchant_type_id].present?
      @offers = Offer.geo_scope( :origin => @res,:within => distance_or_default)
      @offers = @offers.find(:all,:order => 'distance asc' ,:joins => :merchant, :conditions => ["merchants.merchant_type_id = ? AND merchants.id = ? AND offers.active = ? AND offers.city_id = ? AND " + query_string, params[:merchant_type_id].to_i, params[:id].to_i, 1,session[:city_id], conditions[:start_time],conditions[:start_time],conditions[:end_time],conditions[:end_time],conditions[:end_time],conditions[:start_time]])
    end
    if params[:favorite] == 'true'
      if query_string != ''
        @offers = Offer.geo_scope( :origin => @res,:within => distance_or_default).find(:all, :order => 'distance asc' , :joins => {:merchant => {:offers => :favorites}} ,:conditions =>["favorites.user_id = ? AND offers.active = ? AND merchants.id = ? AND offers.city_id = ? AND " + query_string, current_user.id, 1, params[:id].to_i, session[:city_id],conditions[:start_time],conditions[:start_time],conditions[:end_time],conditions[:end_time],conditions[:end_time],conditions[:start_time]])
      else
        @offers = Offer.geo_scope( :origin => @res,:within =>  1)
        @offers = @offers.find(:all, :order => 'distance asc' , :joins => {:merchant => {:offers => :favorites}} ,:conditions => {:favorites => {:user_id => current_user.id}, :active => 1, :city_id => session[:city_id], :merchants => {:id => params[:id].to_i}})
      end
    else
      if params[:merchant_type_id] == nil or params[:merchant_type_id] == ''
        if query_string != ''
          @offers = Offer.geo_scope( :origin => @res,:within => distance_or_default)
          @offers = @offers.find(:all, :order => 'distance asc' , :joins => :merchant, :conditions => ["offers.active = ? AND merchants.id = ? AND offers.city_id = ? AND " + query_string, 1, params[:id].to_i, session[:city_id],  conditions[:start_time],conditions[:start_time],conditions[:end_time],conditions[:end_time],conditions[:end_time],conditions[:start_time]])
        else
          @offers = Offer.geo_scope( :origin => @res,:within => 1)
          @offers = @offers.find(:all, :order => 'distance asc', :joins => :merchant, :conditions => ["offers.active = ? AND merchants.id = ? AND offers.city_id =?", 1, params[:id].to_i, session[:city_id]])
        end
      end
    end
    @merchant = Merchant.find_by_id(params[:id].to_i);
    #    @offers = Offer.where(:merchant_id => 1)
    render :layout => false

  end
  
  def list_detail
    session[:current_location] = current_location(params[:lat].to_f, params[:lon].to_f, cookies) if session[:current_location] == nil
    set_country_and_timezone(session[:current_location]) if session[:timezone] == nil
    set_current_city if session[:city_id] == nil
    @res = session[:current_location]
    query_string = ""
    conditions = {}
    tz = TZInfo::Timezone.get(session[:timezone])
    @now_time = tz.utc_to_local(Time.current)

    if params[:time]
     
      query_string = Offer.compare_time

      if params[:time] == '0'
        next_hours = Time.current.end_of_day()
      else
        next_hours = @now_time + params[:time].to_i.hour
      end
      conditions[:start_time] = @now_time.strftime("%H-%M-%S")
      conditions[:end_time] = next_hours.strftime("%H-%M-%S")
    end
    if params[:merchant_type_id].present?
      @offers = Offer.geo_scope( :origin => @res,:within => distance_or_default)
      @offers = @offers.find(:all ,:order => 'distance asc',:joins => :merchant, :conditions => ["merchants.merchant_type_id = ? AND offers.active = ? AND offers.city_id = ? AND " + query_string,params[:merchant_type_id].to_i, 1,session[:city_id], conditions[:start_time],conditions[:start_time],conditions[:end_time],conditions[:end_time],conditions[:end_time],conditions[:start_time]])
    end            
    if params[:favorite] == 'true'
      if query_string != ''
        @offers = Offer.geo_scope( :origin => @res,:within => distance_or_default)
        @offers = @offers.find(:all ,:order => 'distance asc', :joins => {:merchant => {:offers => :favorites}}  ,:conditions => ["favorites.user_id = ? AND offers.active = ? AND offers.city_id = ? AND " + query_string ,current_user.id, 1,session[:city_id], conditions[:start_time],conditions[:start_time],conditions[:end_time],conditions[:end_time],conditions[:end_time],conditions[:start_time]])
      else
        @offers = Offer.geo_scope( :origin => @res,:within => 1)
        @offers = @offers.find(:all ,:order => 'distance asc', :joins => {:merchant => {:offers => :favorites}}  ,:conditions => {:favorites => {:user_id => current_user.id}, :active => 1, :city_id => session[:city_id]})
      end
    else
      if params[:merchant_type_id] == nil or params[:merchant_type_id] == ''
        if query_string != ''
          @offers = Offer.geo_scope( :origin => @res,:within => distance_or_default)
          @offers = @offers.find(:all , :order => 'distance asc', :conditions => ["offers.active = ? AND offers.city_id = ? AND " + query_string, 1,session[:city_id], conditions[:start_time],conditions[:start_time],conditions[:end_time],conditions[:end_time],conditions[:end_time],conditions[:start_time]])
        else
          @offers = Offer.geo_scope( :origin => @res,:within => 1).find(:all , :order => 'distance asc', :conditions => ["offers.active = ? AND offers.city_id = ? ", 1, session[:city_id]])
        end
      end
    end
    if params[:id]
      @offer = Offer.find(params[:id].to_i)
    else
      @offer = @offers.first
    end
    offer_index = @offers.index(@offer)
    if offer_index
      if offer_index > 0
        @offer_before = @offers.at(offer_index - 1)
      end
      @offer_next = @offers.at(offer_index + 1)
    end
    if @offer and @offer.lat
      @map_center = "#{@offer.lat},#{@offer.lon}"
    else 
      @map_center = "37.789046,-122.4340045"
    end
    
    respond_to do |format|
      format.html # show.html.erb
      format.mobile # show.html.erb
      format.json { render json: @offer }
    end
  end
  # GET /offers/1
  # GET /offers/1.json
  def show
    @res = session[:current_location]
    @offer = Offer.find(params[:id])
    if @offer and @offer.lat
      @map_center = "#{@offer.lat},#{@offer.lon}"
    else 
      @map_center = "37.789046,-122.4340045"
    end
    #    @offers = Offer.find(:all)
    @check = false
    @offers.each do |t|
      @check =true
      #      @map_points = "var point = new google.maps.LatLng(#{(t.lat ? (t.lat.to_s + "," + t.lon.to_s) : "37.789046,-122.4340045")} );
      #                      var image = '/assets/markers/#{(t.merchant ? t.merchant.merchant_type.marker_image : 'look.png')}'; 
      #                      var info = 'Offer : name#{t.name} link: <a href=\"#{url_for(t)}\"> #{url_for(t)}</a>';
      #                    var marker = createMarker(point,info,image);
      #" + @map_points.to_s
    end
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @offer }
    end
  end

  # GET /offers/new
  # GET /offers/new.json
  def new
    @map_center = ''
    if current_city
      @map_center = "#{current_city.latitude},#{current_city.longitude}"
    end
    if params["offer_id"].blank?
      @offer = Offer.new
    else
      offer = Offer.find params["offer_id"]
      @offer = offer.copy
    end
    if params[:merchant_id]
      params[:city_name] = (Merchant.find(params[:merchant_id].to_i).city ? Merchant.find(params[:merchant_id].to_i).city.name : "")
      params[:primary_address] = (Merchant.find(params[:merchant_id].to_i) ? Merchant.find(params[:merchant_id].to_i).primary_address : "")
      params[:offer_city_id ] = (Merchant.find(params[:merchant_id].to_i).city ? Merchant.find(params[:merchant_id].to_i).city.id : "")
    else
      params[:city_name] = (current_user.merchant.city ? current_user.merchant.city.name : "")
      params[:primary_address] = (current_user.merchant ? current_user.merchant.primary_address : "")
      params[:offer_city_id ] = (current_user.merchant.city ? current_user.merchant.city.id : "")
    end
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @offer }
    end
  end

  # GET /offers/1/edit
  def edit
    @offer = Offer.find(params[:id])
    if @offer.lat
      @map_center = "#{@offer.lat},#{@offer.lon}"
    else 
      @map_center = "37.789046,-122.4340045"
    end
    if @offer.merchant
      params[:city_name] = (@offer.merchant.city ? @offer.merchant.city.name : "")
      params[:primary_address] = (@offer.merchant ? @offer.merchant.primary_address : "")
      params[:offer_city_id ] = (@offer.merchant.city ? @offer.merchant.city.id : "")
    end
  end

  # POST /offers
  # POST /offers.json
  def create
    unless params[:offers].blank?
      offers = Crack::XML.parse(params[:offers])
      return render xml: Offer.create_batch(offers["offers"])
    end
    unless params[:deactivate].blank?
      deactivate = Crack::XML.parse(params[:deactivate])
      return render xml: Offer.deactivate_batch(deactivate["offers"])
    end
    params[:city_name] = (current_user.merchant.city ? current_user.merchant.city.name : "")
    params[:primary_address] = (current_user.merchant ? current_user.merchant.primary_address : "")
    params[:offer_city_id ] = (current_user.merchant.city ? current_user.merchant.city.id : "")
    params[:offer][:lat] = (current_user.merchant ? current_user.merchant.lat : "")
    params[:offer][:lon] = (current_user.merchant ? current_user.merchant.lon : "")
    @offer = Offer.new(params[:offer])
    if current_user
      @offer.user_id = current_user.id
      @offer.merchant = current_user.merchant
      @offer.city_id = current_user.merchant.city_id
    end
    if params[:merchant_id]
      @offer.merchant = Merchant.find(params[:merchant_id].to_i)
    end
    @map_center = "#{@offer.lat},#{@offer.lon}"
    respond_to do |format|
      if @offer.save
        if current_user
          current_user.offer_description( @offer.description)
        end
        
        format.html { redirect_to @offer, notice: t('offers.create') }
        format.mobile { redirect_to :action => 'list_detail',:id => @offer.id , notice: t('offers.create') }
        format.json { render json: @offer, status: :created, location: @offer }
        format.xml  { render xml:  @offer, status: :created, location: @offer }
      else
        format.html { render action: "new" }
        format.mobile { render action: 'new' }
        format.json { render json: @offer.errors, status: :unprocessable_entity }
        format.xml { render xml: @offer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /offers/1
  # PUT /offers/1.json
  def update
    @offer = Offer.find(params[:id])
    if params[:offer][:primary_address]
      res=MultiGeocoder.geocode(params[:offer][:primary_address])

      lat = res.ll.split(",")[0]
      lon = res.ll.split(",")[1]
      res = GoogleGeocoder.reverse_geocode([lat.to_f, lon.to_f])
      params[:offer][:primary_address] = res.full_address
      params[:offer][:lat] = lat
      params[:offer][:lon] = lon
    end

    puts 'aaa'
    respond_to do |format|
      if @offer.update_attributes(params[:offer])

        @offer["format_start_time"] = @offer.start_time.strftime("%H:%M")
        @offer["format_end_time"] = @offer.end_time.strftime("%H:%M")
        format.html { redirect_to @offer, notice: I18n.t('offers.update') }
        format.mobile { redirect_to url_for(:controller =>'offers', :action => 'index'), notice: I18n.t('offers.update') }
        format.json { render json: @offer, location: @offer }
      else
        format.html { render action: "edit" }
        format.json { render json: @offer.errors, status: :unprocessable_entity }
      end
    end



  end

  # DELETE /offers/1
  # DELETE /offers/1.json
  def destroy
    @offer = Offer.find(params[:id])
    @offer.destroy

    respond_to do |format|
      format.html { redirect_to offers_url }
      format.json { head :no_content }
    end
  end

  def ajax_unfavorite
    @offer = Offer.find(params[:id].to_i)
    Favorite.where(:user_id => current_user.id, :offer_id => @offer.id).delete_all
    render :text => '"ok"'
  end

  def ajax_favorite
    @offer = Offer.find(params[:id].to_i)
    @favorite = Favorite.where(:user_id => current_user.id, :offer_id => @offer.id)
    if @favorite.first == nil
      @favorite = Favorite.new
      @favorite.user = current_user if current_user
      @favorite.offer = @offer
      @favorite.post_to_twitter = cookies[:twitter_on]
      @favorite.post_to_facebook = cookies[:facebook_on]
      @favorite.save
    end
    render :text => '"ok"'
  end
  def require_sign_in
  end
  def get_more_product

    cookies[:offset] = cookies[:offset].to_i + 15
    @res = session[:current_location]
    query_string = ""
    conditions = {}
    tz = TZInfo::Timezone.get(session[:timezone])
    @now_time = tz.utc_to_local(Time.current)
    if params[:time]
      query_string = Offer.compare_time
      if params[:time] == '0'
        next_hours = Time.current.end_of_day()
      else
        next_hours = @now_time + params[:time].to_i.hour
      end
      conditions[:start_time] = @now_time.strftime("%H-%M-%S")
      conditions[:end_time] = next_hours.strftime("%H-%M-%S")
    end
    if params[:merchant_type_id].present?
      @offers = Offer.geo_scope( :origin => @res,:within => distance_or_default)
      @offers = @offers.find(:all ,:order => 'distance asc',:limit => 15, :offset => cookies[:offset].to_i,:joins => :merchant, :conditions => ["merchants.merchant_type_id = ? AND offers.active = ? AND offers.city_id = ? AND " + query_string, params[:merchant_type_id].to_i,1, session[:city_id], conditions[:start_time],conditions[:start_time],conditions[:end_time],conditions[:end_time],conditions[:end_time],conditions[:start_time]])
    end            
    if params[:favorite] == 'true'
      if query_string != ''
        @offers = Offer.geo_scope( :origin => @res,:within => distance_or_default)
        @offers = @offers.find(:all ,:order => 'distance asc',:limit => 15,:offset => cookies[:offset].to_i, :joins => {:merchant => {:offers => :favorites}}  ,:conditions => ["favorites.user_id = ? AND offers.active = ? AND offers.city_id = ? AND " + query_string, current_user.id, 1, session[:city_id], conditions[:start_time],conditions[:start_time],conditions[:end_time],conditions[:end_time],conditions[:end_time],conditions[:start_time]])
      else
        @offers = Offer.geo_scope( :origin => @res,:within =>  1)
        @offers = @offers.find(:all ,:order => 'distance asc',:limit => 15, :joins => {:merchant => {:offers => :favorites}} ,:offset => cookies[:offset].to_i ,:conditions => {:favorites => {:user_id => current_user.id}, :active => 1, :city_id => session[:city_id]})
      end
    else
      if params[:merchant_type_id] == nil or params[:merchant_type_id] == ''
        if query_string != ''
          @offers = Offer.geo_scope( :origin => @res,:within => distance_or_default)
          @offers = @offers.find(:all , :order => 'distance asc',:limit => 15, :offset => cookies[:offset].to_i, :conditions => ["offers.active = ? AND offers.city_id = ? AND " + query_string, 1,session[:city_id],conditions[:start_time],conditions[:start_time],conditions[:end_time],conditions[:end_time],conditions[:end_time],conditions[:start_time]])
        else
          @offers = Offer.geo_scope( :origin => @res,:within => 1)
          @offers = @offers.find(:all , :order => 'distance asc',:limit => 15, :offset => cookies[:offset].to_i, :conditions => ["offers.active = ? AND offers.city_id = ?", 1, session[:city_id]])
        end
      end
    end

    @html_offers = render_to_string :partial => "page_content",:formats => [:html]
    render :json => @html_offers.to_json
  end
  def ajax_lonlat
    if params[:lon] != ''
      cookies[:lon] = params[:lon]
      cookies[:lat] = params[:lat]
      cookies[:first_time] = 'true'
    end
    render :text => 'ok'
  end
 
  def set_location
    if params[:address]
      res = ","
      begin
        loc=GoogleGeocoder.geocode(params[:address])
        if loc.success
          res = {:lat => loc.lat, :lon => loc.lng}
          cookies[:lon] = loc.lng
          cookies[:lat] = loc.lat
          session[:current_location]  = loc
          set_country_and_timezone(loc)
          if session[:city_id] == nil
            set_current_city
          end
          cookies[:first_time] = 'true'
        end
      rescue Exception => e
        log_error(e)
      end
      if res == ","
        render :text => "not found", :status => 500
      else
        render :json => res
      end
    end
  end
  def filter_to_offers
    redirect_to(:action => "index", :lat => params[:lat], :lon => params[:lon], :time => cookies[:time],:distance => cookies[:distance],:merchant_type_id => params[:merchant_type_id])
  end
  def redirect_to_offers
    redirect_to(:action => "index", :lat => cookies[:lat], :lon => cookies[:lon], :time => params[:time],:distance => params[:distance])
  end
  def dashboard
    render :layout => false
  end
  def unique_email
    user = User.find_by_email(params[:email])
    if user
      render :text => 'false'
    else
      render :text => 'true'
    end
  end
  def create_offer
    chars = 'ABCDEFGHJKLMNOPQRSTUVWXYZ23456789'
    password = ''
    6.times { |i| password << chars[rand(chars.length)] }

    user = User.new( :mobile_number => Time.now.to_s, :user_admin => true,
      :role_id => Role.where(:name => "Merchant admin").first.id.to_i, :email => params[:email], :password => password,
      :username => Time.now.to_s, :first_name => "Update me", :last_name => "Update me")
    #user.skip_confirmation!
    user.save
    
    cookies[:create_offer_user_id] = user.id
    merchant =      Merchant.create! :merchant_type_id => MerchantType.find_by_merchant_type("Default").id,
      :name => 'Default',:postal_code => "12344"
    user.update_attribute("merchant_id", merchant.id)

    if params[:offer_text].split(',').size == 2
      message = params[:offer_text].split(',')
      time = message.first.split("-")
      description = message.last
      name = "offer name"

      start_time = time.first
      end_time = time.last
      if start_time.length == 2
        start_time = start_time + '00'
      end
      if end_time.length == 2
        end_time = end_time + '00'
      end
      if time.size == 2 and start_time.size == 4 and end_time.size == 4 and start_time.to_i < end_time.to_i
        time = message.first
        merchant.offers << user.create_offer(name, time, description, '',nil ,nil, merchant.id, nil)

      end
    end
    render :text => user.id.to_s
  end
  def get_google_places
    merchants = Merchant.get_google_places(params[:content], session[:city_id])
    #puts merchants.to_json
    render :json => merchants.to_json
  end
  def get_business_detail
    if cookies[:create_offer_user_id]
      user = User.find(cookies[:create_offer_user_id])
      if user
        if params[:lat].present?
        else
          begin
            loc=GoogleGeocoder.geocode(params[:primary_address])
            if loc.success
              res= loc.lat.to_s + "," + loc.lng.to_s
              session[:current_location] = current_location(loc.lat.to_f, loc.lng.to_f, cookies) if session[:current_location] == nil
              set_country_and_timezone(session[:current_location]) if session[:timezone] == nil
              set_current_city if session[:city_id] == nil
              params[:city_id] = session[:city_id]
              params[:lat] = loc.lat
              params[:lng] = loc.lng
            end
          rescue
          end
        end
        user.merchant.update_attributes(:name => params[:name], :primary_address => params[:primary_address], :city_id => params[:city_id], :postal_code => params[:postal_code],:lat => params[:lat], :lon => params[:lng],:mon => false,:mon_start_time => Offer.time(params[:mon_fri_open]),:mon_end_time => Offer.time(params[:mon_fri_close]),:tue => false,:tue_start_time => Offer.time(params[:mon_fri_open]),:tue_end_time => Offer.time(params[:mon_fri_close]),:wed => false,:wed_start_time => Offer.time(params[:mon_fri_open]),:wed_end_time => Offer.time(params[:mon_fri_close]),:thu => false,:thu_start_time => Offer.time(params[:mon_fri_open]),:thu_end_time => Offer.time(params[:mon_fri_close]),:fri => false,:fri_start_time => Offer.time(params[:mon_fri_open]),:fri_end_time => Offer.time(params[:mon_fri_close]),:sat => false,:sat_start_time => Offer.time(params[:sat_open]),:sat_end_time => Offer.time(params[:sat_close]),:sun => false,:sun_start_time => Offer.time(params[:sun_open]),:sun_end_time => Offer.time(params[:sun_close]), :merchant_type_id => params[:merchant_type_id].to_i, :country => params[:country_id], :state_province_region => params[:state_province_region])
        offer = Offer.find(:first,:conditions => {:merchant_id => user.merchant.id}) if user.merchant
        offer.update_attributes(:city_id => params[:city_id],:active => 0,:primary_address => params[:primary_address], :lat => params[:lat], :lon => params[:lng]   ) if offer

      end
    end
    render :text => 'true'
  end
  def post_social_network_offer
    if cookies[:create_offer_user_id]
      user = User.find(cookies[:create_offer_user_id])
      if user and (params[:facebook] == '1' or params[:google] == '1' or params[:twitter] == '1' or params[:yelp] == '1' or params[:yahoo] == '1')
        recipient = Yacto::Application::MAIL_FROM

        subject = user.email + " :Social media Links"
        message = "My offer is ready to go live. please add social media links: "
        message = message + "Facebook " if params[:facebook] == '1'
        message = message + "Google " if params[:google] == '1'
        message = message + "Twitter " if params[:twitter] == '1'
        message = message + "Yelp " if params[:yelp] == '1'
        message = message + "Yahoo " if params[:yahoo] == '1'
        UserMailer.confirmation_post_offer(recipient,subject,message).deliver
        user.merchant.update_attributes(:yelp_id => params[:yelp_id], :google_id => params[:google_place_id],:twitter_id => params[:twitter_id], :facebook_page_id => params[:facebook_id])
      end
    end
    render :text => 'true'
  end
  def post_persional_detail
    if cookies[:create_offer_user_id]
      user = User.find(cookies[:create_offer_user_id])
      user.update_attributes(:first_name => params[:first_name],:last_name => params[:last_name],:mobile_number => Offer.format_phone(params[:mobile]),:email => params[:email],:password => params[:password])
      user.merchant.update_attributes(:mobile_number => params[:mobile], :phone => params[:mobile])
      user.send_confirmation_instructions
    end
    #render :text => 'true'
    redirect_to(:controller => "users",:action => "show", :id => cookies[:create_offer_user_id])
  end
  def update_repeat_day
    @offer = Offer.find(params[:id])
    case params[:offer][:value]
      when 'mon_check'
        @offer.update_attributes(:mon => "true")
      when 'mon_uncheck'
        @offer.update_attributes(:mon => "false")
      when 'tue_check'
        @offer.update_attributes(:tue => "true")
      when 'tue_uncheck'
        @offer.update_attributes(:tue => "false")
      when 'wed_check'
        @offer.update_attributes(:wed => "true")
      when 'wed_uncheck'
        @offer.update_attributes(:wed => "false")
      when 'thu_check'
        @offer.update_attributes(:thu => "true")
      when 'thu_uncheck'
        @offer.update_attributes(:thu => "false")
      when 'fri_check'
        @offer.update_attributes(:fri => "true")
      when 'fri_uncheck'
        @offer.update_attributes(:fri => "false")
      when 'sat_check'
        @offer.update_attributes(:sat => "true")
      when 'sat_uncheck'
        @offer.update_attributes(:sat => "false")
      when 'sun_check'
        @offer.update_attributes(:sun => "true")
      when 'sun_uncheck'
        @offer.update_attributes(:sun => "false")
        
    end
    render :text => 'true'
  end
  def update_local_end_day
    @offer = Offer.find(params[:id])
    @offer.update_attributes(:local_end_day => params[:offer][:value])

    render :text => 'true'
  end

  def update_merchant_type
    @offer = Offer.find(params[:id])
    @offer.update_attributes(:merchant_type_id => params[:offer][:value])

    render :text => 'true'
  end


end
