class MerchantPhotosController < ApplicationController
  def destroy
    @merchant_photo = MerchantPhoto.find(params[:id])
    @merchant_photo.destroy
    render :text => "ok"
    
  end
end
