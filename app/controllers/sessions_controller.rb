class SessionsController < Devise::SessionsController
  def destroy
    redirect_path = root_path
    signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))
    set_flash_message :notice, :signed_out if signed_out

    # We actually need to hardcode this as Rails default responder doesn't
    # support returning empty response on GET request
    respond_to do |format|
      format.html { redirect_to redirect_path }
      format.mobile { redirect_to redirect_path }
    end
  end
  
 def create
    resource = warden.authenticate!(auth_options)
    set_flash_message(:notice, :signed_in) if is_navigational_format?
    sign_in(resource_name, resource)
    respond_with resource, :location => after_sign_in_path_for(resource)
    cookies[:user_id] = current_user.id
 end
end



