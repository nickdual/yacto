class HomeController < ApplicationController
  require 'geokit'
  include Geokit::Geocoders
  def index
    if request.format == :mobile 
      redirect_to bb_url(:anchor=>"filters")
    end
  end
  
  def ajax_twitter_on
    cookies[:twitter_on] = true
    render :text => 'on'
  end
  
  def ajax_twitter_off
    cookies[:twitter_on] = false
    render :text => 'off'
  end
  def ajax_gplus_on
    cookies[:gplus_on] = true
    render :text => 'on'
  end

  def ajax_gplus_off
    cookies[:gplus_on] = false
    render :text => 'off'
  end
  def ajax_facebook_on
    cookies[:facebook_on] = true
    render :text => 'on'
  end
  
  def ajax_facebook_off
    cookies[:facebook_on] = false
    render :text => 'off'
  end
      
  def lookup_address
    res = ","
    begin
      loc=GoogleGeocoder.geocode(params[:address])
      if loc.success
        res= loc.lat.to_s + "," + loc.lng.to_s
      end
    rescue
    end
    if res == ","
      render :text => "not found", :status => 500
    else
      render :text => "#{(res.split(",")).to_json}"
    end
  end
  def autocomplete
    
    @cities = City.all(:joins => :country, :conditions => ["lower(cast(cities.name || cast(' ' AS varchar) || countries.name AS varchar)) LIKE ?", "#{params[:term].downcase}%"],:order => 'cities.name, countries.name asc', :limit => 10)
    @cities.collect! {|item| {:label => item.name + " " + item.country.name, :id => item.id,:country => item.country.name}}
    render :json => @cities
  end
  def autocomplete_address
    @address = Merchant.find(:all, :conditions => ['lower(primary_address) LIKE ?', "%#{params[:term].downcase}%"],:order => 'primary_address asc', :limit => 5)
    @address.collect! {|item| {:label => item.primary_address, :id => item.id}}
    render :json => @address
  end

end
