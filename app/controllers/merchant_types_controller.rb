class MerchantTypesController < ApplicationController
  load_and_authorize_resource

  # GET /merchant_types
  # GET /merchant_types.json
  #load_and_authorize_resource
  def index
    @merchant_types = MerchantType.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @merchant_types }
    end
  end

  # GET /merchant_types/1
  # GET /merchant_types/1.json
  def show
    @merchant_type = MerchantType.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @merchant_type }
    end
  end

  # GET /merchant_types/new
  # GET /merchant_types/new.json
  def new
    @merchant_type = MerchantType.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @merchant_type }
    end
  end

  # GET /merchant_types/1/edit
  def edit
    @merchant_type = MerchantType.find(params[:id])
  end

  # POST /merchant_types
  # POST /merchant_types.json
  def create
    @merchant_type = MerchantType.new(params[:merchant_type])

    respond_to do |format|
      if @merchant_type.save
        format.html { redirect_to @merchant_type, notice: t('merchanttype.create') }
        format.json { render json: @merchant_type, status: :created, location: @merchant_type }
      else
        format.html { render action: "new" }
        format.json { render json: @merchant_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /merchant_types/1
  # PUT /merchant_types/1.json
  def update
    @merchant_type = MerchantType.find(params[:id])

    respond_to do |format|
      if @merchant_type.update_attributes(params[:merchant_type])
        format.html { redirect_to @merchant_type, notice: t('merchanttype.update') }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @merchant_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /merchant_types/1
  # DELETE /merchant_types/1.json
  def destroy
    @merchant_type = MerchantType.find(params[:id])
    @merchant_type.destroy

    respond_to do |format|
      format.html { redirect_to merchant_types_url }
      format.json { head :no_content }
    end
  end
  def top_lvl
    render :json => MerchantType.top_lvl.select("id,merchant_type")
  end
end
