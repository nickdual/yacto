class LocationsController < ApplicationController
  respond_to :json, :only => :show
  def show
    begin
      loc = GoogleGeocoder.geocode(params[:address])
      if loc.success
         respond_with({
           lat: loc.lat,
           lon: loc.lng
         })
      end
    rescue Exception => e
      log_error(e)
      render status: 500
    end
  end
end