class DashboardsController < ApplicationController
  body_class "dashboard"
  def show
    redirect_to bb_url
  end
#TODO - remove all session usage here ! makes it difficult to spot bugs
  def offers
    set_location 

    params[:merchant_type] = nil  if params[:favorite] == 'true'
    puts "**************"
    puts params[:merchant_type]
    @offers = Offer.geo_scope(:origin => @res, :within => distance_or_default)
    if params[:merchant_type].present? #is it OK that we ignore query_string here ?
      @offers = @offers.joins(:merchant)
      types = MerchantType.merchant_type(params[:merchant_type]).select(:id).map(&:id)

      @offers = @offers.where("merchants.merchant_type_id IN (?)", types)
    end
    puts "******************"
    @offers = @offers.where(query_string, conditions) if query_string != ''
    @offers = @offers.active.city_id(session[:city_id]).limit(20).order('distance asc, offers.id').with_merchant
    @offers = @offers.includes(:merchant => [:merchant_type, {:city => :country}])
    @offers = @offers.offset(params[:offset]) if params[:offset]

    @offers = cleanup_offers(@offers)

  end

  def query_string
    params[:time] ? Offer.compare_time_with_labels : ""
  end

  def conditions
    conditions = {}
    if params[:time]
      tz = TZInfo::Timezone.get(session[:timezone])
      now_time = tz.utc_to_local(Time.current)
      if params[:time] == '0'
        next_hours = Time.current.end_of_day
      else
        next_hours = now_time + params[:time].to_i.hour
      end
      conditions[:start_time] = now_time.strftime("%H-%M-%S")
      conditions[:end_time] = next_hours.strftime("%H-%M-%S")
    end
    conditions
  end

  def distance_or_default
    params[:distance] ? params[:distance].to_f : 1
  end

  def set_location
    if params[:address]
      #disabling caching for now because it causes problems
#      @res = find_local_city  
      
      fetch_city_data if @res.nil?
    end
  end

  def find_local_city
    City.cityname(params[:address]).first
     #TODO - maybe try to split the address, use first part, or like
  end

  def fetch_city_data
    begin
      loc=GoogleGeocoder.geocode(params[:address])
      if loc.success
        @res = loc
        set_country_and_timezone(loc)
        #TODO - maybe save city data if not present
      end
    rescue Exception => e
      log_error(e)
    end
  end
end
