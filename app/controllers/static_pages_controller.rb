class StaticPagesController < ApplicationController
  def about_us
  end

  def blog
  end

  def contact_us
  end

  def our_partners
  end

  def terms_and_conditions
  end

  def how_it_works_merchants
	end
	def how_it_works_consumers
	end


	def privacy
	end

	def business_owners
	end
end
