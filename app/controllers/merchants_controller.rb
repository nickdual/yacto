# encoding: utf-8
require 'phony'
require 'open-uri'
require 'net/http'
class MerchantsController < ApplicationController
  include Geokit::Geocoders
  # load_and_authorize_resource
  # GET /merchants
  # GET /merchants.json
  def index
    query_string = ""
    conditions = {}
    if params[:merchant]
      if params[:merchant][:merchant_id] != nil and params[:merchant][:merchant_id] != ''
        query_string = query_string + " AND UPPER(merchants.merchant_id) LIKE :merchant_id"
        conditions[:merchant_id] = "%" + params[:merchant][:merchant_id].strip.upcase + "%"
      end
      if params[:merchant][:name] != nil and params[:merchant][:name] != ''
        query_string = query_string + " AND UPPER(merchants.name) LIKE :name"
        conditions[:name] = "%" + params[:merchant][:name].strip.upcase + "%"
      end
      if params[:merchant][:mobile_number] != nil and params[:merchant][:mobile_number] != ''
        query_string = query_string + " AND UPPER(merchants.mobile_number) LIKE :mobile_number"
        conditions[:mobile_number] = "%" + params[:merchant][:mobile_number].strip.upcase + "%"
      end
      if params[:merchant][:primary_address] != nil and params[:merchant][:primary_address] != ''
        query_string = query_string + " AND UPPER(merchants.primary_address) LIKE :primary_address"
        conditions[:primary_address] = "%" + params[:merchant][:primary_address].strip.upcase + "%"
      end
    end
    query_string = query_string + " AND countries.id = :country"
    params[:country] = Country.find_by_name("United States").id if params[:country].nil?
    puts params
    conditions[:country] = params[:country].to_i
    @merchants = Merchant.joins({:city => :country}).where("created_at is not NULL" + query_string,  conditions).paginate(:page => params[:page],:per_page =>  15).order("created_at DESC")
   #@merchants = Merchant.find(:all,:joins => {:city => :country}, :conditions =>(:contries))
    respond_to do |format|
      format.html # index.html.erb
      format.mobile
      format.json { render :json => @merchants }
    end
  end

  # GET /merchants/1
  # GET /merchants/1.json
  def show
    if session[:current_location] == nil
      session[:current_location] = current_location(nil,nil, cookies)
    end
    if session[:timezone] == nil
      set_country_and_timezone(session[:current_location])
    end
    if session[:timezone]
      tz = TZInfo::Timezone.get(session[:timezone])
      @now_time = tz.utc_to_local(Time.current)
    end
    @res = session[:current_location]
    if @res
      @current_location = @res.full_address
    end
    puts params
    @merchant = Merchant.find(params[:id])

    @offers_mon = @merchant.offers.active.monday_offers.order('created_at asc').to_a 
    @offers_tue = @merchant.offers.active.tuesday_offers.order('created_at asc').to_a
    @offers_wed = @merchant.offers.active.wednesday_offers.order('created_at asc').to_a
    @offers_thu = @merchant.offers.active.thursday_offers.order('created_at asc').to_a
    @offers_fri = @merchant.offers.active.friday_offers.order('created_at asc').to_a
    @offers_sat = @merchant.offers.active.saturday_offers.order('created_at asc').to_a
    @offers_sun = @merchant.offers.active.sunday_offers.order('created_at asc').to_a

    @favorite = current_user.favorites.where(:merchant_id => @merchant.id).first if current_user

    @phone_number = @merchant.phone_format()
    @map_center = "#{@merchant.lat},#{@merchant.lon}"
    @offers = @merchant.offers
    respond_to do |format|
      format.html # show.html.erb
      format.json #{ render json: @merchant }
    end
  end

  # GET /merchants/new
  # GET /merchants/new.json
  def new
    @map_center = ''
    if current_city
      @map_center = "#{current_city.latitude},#{current_city.longitude}"
    end
    @merchant = Merchant.new
    object = @merchant
    respond_to do |format|
      format.html # new.html.erb
      format.mobile
      format.json { render json: object }
    end
  end

  # GET /merchants/1/edit
  def edit
    @merchant = Merchant.find(params[:id])
    object = @merchant
    params[:city_name] = @merchant.city ? @merchant.city.name : ''
    @map_center = "#{@merchant.lat},#{@merchant.lon}"
  end

  # POST /merchants
  # POST /merchants.json
  def create
    begin
      loc=GoogleGeocoder.geocode(params[:merchant][:primary_address],:bias => current_country.code)
      params[:merchant][:primary_address] = loc.full_address
      params[:merchant][:lat] = loc.lat
      params[:merchant][:lon] = loc.lng
      loc.all.each { |e|
        if e.hash[:city]

          city = City.where(:name => e.hash[:city].strip).first
          if city

            params[:merchant][:city_id] = city.id
            #          session[:twilio_number] = city.country.twilio_number if city.country
            break
          end
        end
      }
    rescue
      res = GoogleGeocoder.reverse_geocode([params[:merchant][:lat].to_f, params[:merchant][:lon].to_f])
      params[:merchant][:primary_address] = res.full_address
      params[:merchant][:lat] = params[:merchant][:lat].to_f
      params[:merchant][:lon] = params[:merchant][:lon].to_f
      res.all.each { |e|
        if e.hash[:city]

          city = City.where(:name => e.hash[:city].strip).first
          if city

            params[:merchant][:city_id] = city.id
            #          session[:twilio_number] = city.country.twilio_number if city.country
            break
          end
        end
      }
    end

    @merchant = Merchant.new(params[:merchant])
    if params[:merchant][:phone]
      params[:merchant][:phone] = params[:merchant][:phone].strip.scan(/\d/).map { |n| n.to_i }.join()
    end
    if params[:merchant][:mon_start_time] != ""
      params[:merchant][:mon_start_time] = Time.parse(params[:merchant][:mon_start_time])
    end
    if params[:merchant][:tue_start_time] != ""
      params[:merchant][:tue_start_time] = Time.parse(params[:merchant][:tue_start_time])
    end
    if params[:merchant][:wed_start_time] != ""
      params[:merchant][:wed_start_time] = Time.parse(params[:merchant][:wed_start_time])
    end
    if params[:merchant][:thu_start_time] != ""
      params[:merchant][:thu_start_time] = Time.parse(params[:merchant][:thu_start_time])
    end
    if params[:merchant][:fri_start_time] != ""
      params[:merchant][:fri_start_time] = Time.parse(params[:merchant][:fri_start_time])
    end
    if params[:merchant][:sat_start_time] != ""
      params[:merchant][:sat_start_time] = Time.parse(params[:merchant][:sat_start_time])
    end
    if params[:merchant][:sun_start_time] != ""
      params[:merchant][:sun_start_time] = Time.parse(params[:merchant][:sun_start_time])
    end
    params[:merchant][:mon] = params[:merchant][:mon] == 'true' ? true : false
    params[:merchant][:tue] = params[:merchant][:tue] == 'true' ? true : false
    params[:merchant][:wed] = params[:merchant][:wed] == 'true' ? true : false
    params[:merchant][:fri] = params[:merchant][:fri] == 'true' ? true : false
    params[:merchant][:thu] = params[:merchant][:thu] == 'true' ? true : false
    params[:merchant][:sat] = params[:merchant][:sat] == 'true' ? true : false
    params[:merchant][:sun] = params[:merchant][:sun] == 'true' ? true : false
    @merchant.third_party_code = 'yacto'
    respond_to do |format|
      if @merchant.save
        if current_user.admin?
          current_user.update_attribute("merchant_id" ,@merchant.id)
        else
          current_user.update_attributes(:merchant_id => @merchant.id, :role_id => Role.where(:name => 'Merchant admin').first.id)
        end
        if params[:merchant][:merchant_type_id ] == 1 and params[:type_name ] != 'Default'
          UserMailer.confirm_merchant_type("New merchant type: " + params[:type_name ] + ' from google. link: ' + url_for(@merchant)).deliver
        end
        format.html { redirect_to '/', notice: t("merchant.create") }
        format.mobile { redirect_to '/', notice: t("merchant.create")}
        #        format.json { render json: @merchant, status: :created, location: @merchant }
      else
        format.html { render action: "new" }
        format.mobile { render action: "new" }
        format.json { render json: @merchant.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /merchants/1
  # PUT /merchants/1.json
  def update
    address = params[:merchant][:primary_address] if  params[:merchant][:primary_address].present?
    address += ' , ' + params[:city_name] if params[:city_name].present?
    address += ' , ' + params[:merchant][:state_province_region] if params[:merchant][:state_province_region].present?
    address += ' ' + params[:merchant][:postal_code] if params[:merchant][:postal_code].present?
    address += ' , '  + params[:merchant][:country] if params[:merchant][:country].present?

    @merchant = Merchant.find(params[:id])
    puts(address)
    begin
      loc=GoogleGeocoder.geocode(address)
      params[:merchant][:primary_address] = loc.street_address
      params[:merchant][:lat] = loc.lat
      params[:merchant][:lon] = loc.lng
      params[:merchant][:postal_code] = loc.zip
      params[:merchant][:state_province_region] = loc.state
      puts loc.to_json
      loc.all.each { |e|
        if e.hash[:city]
  
          city = City.where(:name => e.hash[:city].strip,:country_iso_code_two_letters => loc.country_code).first
          if city
  
            params[:merchant][:city_id] = city.id
            #          session[:twilio_number] = city.country.twilio_number if city.country
            break
          end
        end
      }
    rescue
      res = GoogleGeocoder.reverse_geocode([params[:merchant][:lat].to_f, params[:merchant][:lon].to_f])
      params[:merchant][:primary_address] = res.full_address
      params[:merchant][:lat] = params[:merchant][:lat].to_f
      params[:merchant][:lon] = params[:merchant][:lon].to_f
      res.all.each { |e|
        if e.hash[:city]
  
          city = City.where(:name => e.hash[:city].strip).first
          if city
  
            params[:merchant][:city_id] = city.id
            #          session[:twilio_number] = city.country.twilio_number if city.country
            break
          end
        end
      }
    end
    #params[:merchant][:mon] = params[:merchant][:mon] == 'true' ? true : false
    #params[:merchant][:tue] = params[:merchant][:tue] == 'true' ? true : false
    #params[:merchant][:wed] = params[:merchant][:wed] == 'true' ? true : false
    #params[:merchant][:fri] = params[:merchant][:fri] == 'true' ? true : false
    #params[:merchant][:thu] = params[:merchant][:thu] == 'true' ? true : false
    #params[:merchant][:sat] = params[:merchant][:sat] == 'true' ? true : false
    #params[:merchant][:sun] = params[:merchant][:sun] == 'true' ? true : false
    params[:merchant][:country] = City.find(params[:merchant][:city_id].to_i).country.name if params[:merchant][:city_id].present?
    params[:merchant][:third_party_code]  = 'yacto' if @merchant.third_party_code == 'yacto'
    respond_to do |format|
      if @merchant.update_attributes(params[:merchant])
        if params[:merchant][:merchant_type_id ] == '1' and params[:merchant][:type_name ] != 'Default' and  params[:type_name ] != nil and  params[:type_name ] != ''
          UserMailer.confirm_merchant_type("New merchant type: " + params[:type_name ] + ' from google. Link: ' + url_for(@merchant)).deliver
        end
        format.html { redirect_to @merchant, notice: t("merchant.update") }
        format.mobile { redirect_to @merchant, notice: t("merchant.update") }
        format.json { head :no_content }
      else
        puts @merchant.errors.full_messages
        format.html { render action: "edit" }
        format.mobile { render action: "edit" }
        format.json { render json: @merchant.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /merchants/1
  # DELETE /merchants/1.json
  def destroy
    @merchant = Merchant.find(params[:id])
    @merchant.destroy

    respond_to do |format|
      format.html { redirect_to merchants_url }
      format.json { head :no_content }
    end
  end
  def call
    @merchant = Merchant.find(params[:id].to_i)
    @client = Twilio::REST::Client.new ACCOUNT_SID, AUTH_TOKEN
    begin
      @call = @client.account.calls.create(
        :from => current_user.mobile_number,
        :to => @merchant.phone,
        :url => url_for(:controller => 'merchants', :action => "call")
      )
  
      render :text => 'ok'
    rescue
      render :text => 'error'
    end
  end

  def ajax_unfavorite
    @merchant = Merchant.find(params[:id].to_i)
    Favorite.where(:user_id => current_user.id, :merchant_id => @merchant.id).delete_all
    render :text => '"ok"'
  end

  def ajax_favorite
    @merchant = Merchant.find(params[:id].to_i)
    @favorite = Favorite.where(:user_id => current_user.id, :merchant_id => @merchant.id).first
    if @favorite.nil?
      @favorite = Favorite.new()
      @favorite.user = current_user if current_user
      @favorite.merchant = @merchant
      @favorite.save
    end
    merchant_admin =  @merchant.users.find(:all,:joins => :role,:conditions => {:roles => {:name => "Merchant admin"}} ).first
    if merchant_admin
      current_user.merchant_facebook(360977223958342)
      current_user.merchant_twitter(merchant_admin.twitter_name)
    end
    if @merchant
      if cookies[:twitter_on] == true and current_user and current_user.twitter_token
        tweet = I18n.t("message.i_like") + @merchant.name + I18n.t('message.check_it_out') + @merchant.merchant_id.to_s
        current_user.client_twitter("#{tweet}")
      end
      if cookies[:facebook_on] == true and current_user and current_user.facebook_token
        tweet = I18n.t("message.i_like") + @merchant.name + I18n.t('message.check_it_out') + @merchant.merchant_id.to_s
        current_user.client_facebook("#{tweet}")
      end
    end
    render :text => '"ok"'
  end
  def sms_request
    unless params[:Body].blank?
      if params[:From].strip == '' and current_user and (current_user.admin? or current_user.yacto_employee?) and  params[:Body].split(DELIMITER).size == 5
        parameters = params[:Body].split(DELIMITER)
        from_phone = parameters[4].scan(/\d/).map { |n| n.to_i }.join()

      else
        from_phone = params[:From].strip.scan(/\d/).map { |n| n.to_i }.join()
      end

      @user = User.where(:mobile_number => from_phone).first
      @message = ""
      @response = ""
      if (@user.blank? or @user.merchant == nil) and (( params[:From].strip == '' and current_user and (current_user.admin? or current_user.yacto_employee?)) or  params[:From].strip != '' )
        if params[:Body].split(DELIMITER).size != 5

          ErrorMessage.create!(:error => I18n.t("merchant.smsrequest.fourcommaformat"), :message => params[:Body])
          case params[:Body].downcase
          when I18n.t("merchant.smsrequest.action.help")
            @message = I18n.t("merchant.smsrequest.help.intro")
            @response = {:success => false, :description => @message}
          else
            @message = I18n.t("merchant.smsrequest.fourcommaformat")
            @response = {:success => false, :description => @message}
          end

        else
          parameters = params[:Body].split(DELIMITER)
          time = parameters[0].split("-")
          start_time = time.first
          end_time = time.last
          if start_time.length == 2
            start_time = start_time + '00'
          end
          if end_time.length == 2
            end_time = end_time + '00'
          end

          begin
            start_time = Time.strptime(start_time, "%H%M").to_time
            end_time = Time.strptime(end_time, "%H%M").to_time
            chars = 'abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ23456789'
            password = ''
            loc=GoogleGeocoder.geocode(parameters[3])
            if loc.success
              6.times { |i| password << chars[rand(chars.length)] }
              if @user.blank?
                code = ''
                4.times { |i| code << chars[rand(chars.length)] }
                @user = User.new( :mobile_number => from_phone, :user_admin => true,
                  :role_id => Role.where(:name => "Merchant admin").first.id.to_i, :email => "usman+#{Time.now.to_f}@devsinc.com", :password => password,
                  :username => from_phone, :first_name => "Update me", :last_name => "Update me", :code => code)
                @user.skip_confirmation!
                @user.save
              end
              @response = @user.new_merchant_sms_response(params[:Body].split(DELIMITER), session, loc)
              @message = @response[:description]
            else
              @response = {:success => false, :description => I18n.t("merchant.smsrequest.badaddress")}
              @message = @response[:description]
            end
          rescue
            @message = I18n.t("merchant.smsrequest.badtime")
            @response = {:success => false, :description => @message}
          end
        end
      else
        if params[:From].strip != ''
          @response = @user.old_merchant_sms_response(params[:Body], session)
          @message = @response[:description]
        else
          if !(current_user and (current_user.admin? or current_user.yacto_employee?))
            @response = {:success => false, :description =>  t("merchant.smsrequest.exist_phone_number")}
            @message =  @response[:description]
          end
        end
      end
      begin
        if session[:twilio_number] and from_phone != nil and from_phone != ''
          Merchant.send_twilio(from_phone,session[:twilio_number], @message)
          if @user and @user.merchant.merchant_type.merchant_type == 'Default'
            @merchant_type =  MerchantType.all.map(&:merchant_type).uniq
            @merchant_type.delete("Default")
            @second_message = t("merchant.smsrequest.require_merchant_type", :merchant_types => @merchant_type.join(','), :merchant_id => @user.merchant.merchant_id)
            @client.account.sms.messages.create(:from => session[:twilio_number], :to => from_phone,
              :body =>  @second_message)
          end
        else
          Merchant.send_twilio(from_phone,TWILIO_DEFAULT_PHONE, @message)
          if current_user == nil or (current_user and current_user.admin? == false and current_user.yacto_employee? == false) and params[:From].strip == ''
            @response = {:success => false, :description =>  t("merchant.smsrequest.exist_phone_number")}
          else
            if @message == nil
              @response = {:success => false, :description =>  t("merchant.smsrequest.twilio_number")}
            end
          end
        end
      rescue StandardError => error
        @response[:description] = "Error #{ error }"
      end
      if @second_message
        @response = {:success => true, :description => @message  + @second_message}
      end
      return render :json => @response
    end
  end
  def search
    @merchants = Merchant.search(params)
    merchant_response = ""
    @merchants.each do |merchant|
      merchant_row = ""
      if merchant.users.first.blank?
        merchant_row = "<tr><td><a href='#{user_path(User.all.first)}'>#{merchant.merchant_id}</a></td>"
      else
        merchant_row = "<tr><td><a href='#{user_path(merchant.users.first)}'>#{merchant.merchant_id}</a></td>"
      end
      merchant_row = merchant_row + "<td>#{merchant.name}</td><td>#{merchant.administrative_contact}</td><td>#{merchant.mobile_number}</td><td>#{merchant.administrative_email}</td><td></td></tr>"
      merchant_response = merchant_response + merchant_row
    end
    return render :json => {:success => true, :merchant_rows_html => merchant_response.html_safe}
  end
  def search_google_place
    if params[:search] != ''
      docs = URI.parse(URI.encode('https://maps.googleapis.com/maps/api/place/autocomplete/json?input=' + params[:search] + '&types=establishment&sensor=true&key=AIzaSyAzjO9pC_cyHdNYW74s_rc-5owbmrABsG8')).read
      docs = JSON.parse(docs)
      if docs["status"] == 'OK'
        docs = URI.parse('https://maps.googleapis.com/maps/api/place/details/json?reference=' + docs['predictions'][0]["reference"] + '&sensor=true&key=AIzaSyAzjO9pC_cyHdNYW74s_rc-5owbmrABsG8').read
        docs = JSON.parse(docs)
        if docs["status"] == 'OK'
          result = Hash.new
          docs["result"]["address_components"].each do |type|
            if type["types"][0] == 'locality'
              result["city_name"] = type["long_name"]
            end        
            if type["types"][0] == 'country'
              country = Country.find(:first, :conditions => ["iso_code_two_letter = ?", type["long_name"]])
              if country
                result["country_id"] = country.iso_code_two_letter
                if result["city_name"]
                  city = City.find(:first, :conditions => ["UPPER(name) LIKE ? and country_id = ?", "%" + result["city_name"].strip.upcase + "%", country.id])
                  if city
                    result["city_name"] = city.name
                    result["city_id"] = city.id
                  else
                    result["city_name"]  = nil
                  end
                end
              end
            end
            if type["types"][0] == 'postal_code'
              result["postal_code"] = type["long_name"]
            end
          end
          result["name"] = docs["result"]["name"]
          result["merchant_type_name"] = Merchant.merchant_type(docs["result"]["types"][0])
          if result["merchant_type_name"]
            merchant_type = MerchantType.find(:first, :conditions => ["UPPER(merchant_type) LIKE ?", result["merchant_type_name"].strip.upcase])
            if merchant_type
              result["merchant_type_id"] = merchant_type.id
            else
              result["merchant_type_id"] = 1

            end
          else
            result["merchant_type_id"] = 1
          end
          result["formatted_phone_number"] = docs["result"]["formatted_phone_number"]
          result["formatted_address"] = docs["result"]["formatted_address"]
          result["lat"] = docs["result"]["geometry"]["location"]["lat"]
          result["lng"] = docs["result"]["geometry"]["location"]["lng"]
          doc = Nokogiri::HTML(open(docs["result"]["url"].gsub("http","https")))
          (doc.xpath('//div[@class="hRb AIcYTe"]//div//span[@class="iRb"]') + doc.xpath('//div[@class="hRb AIcYTe"]//div//span[@class="iRb I5a"]')).each do |day|
            if day.at('span[@class="H5a"]')
              case day.at('span[@class="H5a"]').text.strip

              when 'Monday'
                result["mon"] = true
                if day.at('span[@class="oTa"]') and day.at('span[@class="oTa"]').text.strip == 'Open 24 hours'
                  result["mon_start_time"] = "00:00"
                  result["mon_end_time"] = "23:59 pm"
                else
       
                  time = day.at('span[@class="oTa"]').text.strip.split("â\u0080\u0093")
                  start_time = time[0]
                  end_time = time[1]
                  if end_time == "12:00 am"
                    end_time = "23:59 pm"
                  end
                  if start_time.split().length > 1
                    result["mon_start_time"] = start_time
                    result["mon_end_time"] = end_time
                  else
                    result["mon_start_time"] = start_time + " " + end_time.split()[1]
                    result["mon_end_time"] = end_time
                  end
                end
              when "Tuesday"
                result["tue"] = true
                if day.at('span[@class="oTa"]') and day.at('span[@class="oTa"]').text.strip == 'Open 24 hours'
                  result["tue_start_time"] = "00:00"
                  result["tue_end_time"] = "23:59 pm"
                else
                  time = day.at('span[@class="oTa"]').text.strip.split("â\u0080\u0093")
                  start_time = time[0]
                  end_time = time[1]
                  if end_time == "12:00 am"
                    end_time = "23:59 pm"
                  end
                  if start_time.split().length > 1
                    result["tue_start_time"] = start_time
                    result["tue_end_time"] = end_time
                  else
                    result["tue_start_time"] = start_time + " " + end_time.split()[1]
                    result["tue_end_time"] = end_time
                  end
                end
              when 'Wednesday'
                result["wed"] = true
                if day.at('span[@class="oTa"]') and day.at('span[@class="oTa"]').text.strip == 'Open 24 hours'
                  result["wed_start_time"] = "00:00"
                  result["wed_end_time"] = "23:59 pm"
                else
                  time = day.at('span[@class="oTa"]').text.strip.split("â\u0080\u0093")
                  start_time = time[0]
                  end_time = time[1]
                  if end_time == "12:00 am"
                    end_time = "23:59 pm"
                  end
                  if start_time.split().length > 1
                    result["wed_start_time"] = start_time
                    result["wed_end_time"] = end_time
                  else
                    result["wed_start_time"] = start_time + " " + end_time.split()[1]
                    result["wed_end_time"] = end_time
                  end
                end
              when "Thursday"
                result["thu"] = true
                if day.at('span[@class="oTa"]') and day.at('span[@class="oTa"]').text.strip == 'Open 24 hours'
                  result["thu_start_time"] = "00:00"
                  result["thu_end_time"] = "23:59:59"
                else
                  time = day.at('span[@class="oTa"]').text.strip.split("â\u0080\u0093")
                  start_time = time[0]
                  end_time = time[1]
                  if end_time == "12:00 am"
                    end_time = "23:59 pm"
                  end
                  if start_time.split().length > 1
                    result["thu_start_time"] = start_time
                    result["thu_end_time"] = end_time
                  else
                    result["thu_start_time"] = start_time + " " + end_time.split()[1]
                    result["thu_end_time"] = end_time
                  end
                end
              when "Friday"
                result["fri"] = true
                if day.at('span[@class="oTa"]') and day.at('span[@class="oTa"]').text.strip == 'Open 24 hours'
                  result["fri_start_time"] = "00:00"
                  result["fri_end_time"] = "23:59 pm"
                else
                  time = day.at('span[@class="oTa"]').text.strip.split("â\u0080\u0093")
                  start_time = time[0]
                  end_time = time[1]
                  if end_time == "12:00 am"
                    end_time = "23:59 pm"
                  end
                  if start_time.split().length > 1
                    result["fri_start_time"] = start_time
                    result["fri_end_time"] = end_time
                  else
                    result["fri_start_time"] = start_time + " " + end_time.split()[1]
                    result["fri_end_time"] = end_time
                  end
                end
              when "Saturday"
                result["sat"] = true
                if day.at('span[@class="oTa"]') and day.at('span[@class="oTa"]').text.strip == 'Open 24 hours'
                  result["sat_start_time"] = "00:00"
                  result["sat_end_time"] = "23:59 pm"
                else
                  time = day.at('span[@class="oTa"]').text.strip.split("â\u0080\u0093")
                  start_time = time[0]
                  end_time = time[1]
                  if end_time == "12:00 am"
                    end_time = "23:59 pm"
                  end
                  if start_time.split().length > 1
                    result["sat_start_time"] = start_time
                    result["sat_end_time"] = end_time
                  else
                    result["sat_start_time"] = start_time + " " + end_time.split()[1]
                    result["sat_end_time"] = end_time
                  end
                end
              when "Sunday"
                result["sun"] = true
                if day.at('span[@class="oTa"]') and day.at('span[@class="oTa"]').text.strip == 'Open 24 hours'
                  result["sun_start_time"] = "00:00 pm"
                  result["sun_end_time"] = "23:59 pm"
                else
                  time = day.at('span[@class="oTa"]').text.strip.split("â\u0080\u0093")
                  start_time = time[0]
                  end_time = time[1]
                  if end_time == "12:00 am"
                    end_time = "23:59 pm"
                  end
                  if start_time.split().length > 1
                    result["sun_start_time"] = start_time
                    result["sun_end_time"] = end_time
                  else
                    result["sun_start_time"] = start_time + " " + end_time.split()[1]
                    result["sun_end_time"] = end_time
                  end
                end
              else
              end
              puts day.at('span[@class="oTa"]').text
            end
          end
          puts result
          render :json => result
          return
        end
      end
    end
    render :text => 'false'
  end
  def survey

    #@merchants = Merchant.select("countries.name as country_name, cities.id, cities.name, count(distinct merchants.id) as merchant_count ,count(offers.id) as offer_count").paginate(:page => params[:page],:per_page =>  100).find(:all, :joins => [ "left join offers on offers.merchant_id = merchants.id",{:city => :country}], :group => "countries.name , cities.name, cities.id", :order => 'countries.name , cities.name')

    if params[:city]
      @merchants = Merchant.select("countries.name as country_name, cities.id, cities.name, count(distinct merchants.id) as merchant_count ,count(offers.id) as offer_count").paginate(:page => params[:page],:per_page =>  100).find(:all, :joins => ["left join offers on offers.merchant_id = merchants.id",{:city => :country}], :group => "countries.name , cities.name, cities.id", :order => 'countries.name , cities.name',:conditions =>["lower(cast(cities.name || cast(' ' AS varchar) || countries.name AS varchar)) LIKE ?","#{params[:city].downcase}%"])
    else
      @merchants = Merchant.select("countries.name as country_name, cities.id, cities.name, count(distinct merchants.id) as merchant_count ,count(offers.id) as offer_count").paginate(:page => params[:page],:per_page =>  100).find(:all, :joins => ["left join offers on offers.merchant_id = merchants.id",{:city => :country}], :group => "countries.name , cities.name, cities.id", :order => 'countries.name , cities.name')
    end

    respond_to do |format|
      format.html
      format.json { render :json => @merchants }
    end
  end
  def survey_city
    @type_group_merchants = Merchant.select("countries.name as country_name, merchants.third_party_code, merchant_types.merchant_type as merchant_type_name, cities.id, cities.name, count(distinct merchants.id) as merchant_count ,count(offers.id) as offer_count").find(:all, :conditions => ["merchant_types.sub_type= ? and cities.id = ?", " ", params[:id].to_i],:joins => ["left join offers on offers.merchant_id = merchants.id", :merchant_type ,{:city => :country}], :group => "countries.name , cities.name, cities.id, merchants.third_party_code, merchant_types.merchant_type", :order => 'countries.name , cities.name, merchants.third_party_code, merchant_types.merchant_type')
    @source_group_merchants = Merchant.select("countries.name as country_name, merchants.third_party_code, cities.id, cities.name, count(distinct merchants.id) as merchant_count ,count(offers.id) as offer_count").find(:all, :conditions => ["cities.id = ?", params[:id].to_i],:joins => ["left join offers on offers.merchant_id = merchants.id" ,{:city => :country}], :group => "countries.name , cities.name, cities.id, merchants.third_party_code", :order => 'countries.name , cities.name, merchants.third_party_code')

    respond_to do |format|
      format.html
      format.json { render :json => @type_group_merchants }
    end
  end
  def survey_source
    @sources = Merchant.select("merchants.third_party_code, count(distinct merchants.id) as merchant_count ,count(offers.id) as offer_count").find(:all, :joins => "left join offers on offers.merchant_id = merchants.id", :group => "merchants.third_party_code", :order => 'merchants.third_party_code')
    respond_to do |format|
      format.html
      format.json { render :json => @sources }
    end
  end
  def admin
    @merchant = Merchant.find(params[:object_id].to_i)
    @images = []
    if params[:files]
      params[:files].each do |file| 
        @merchant_photo = MerchantPhoto.new(:photo => file)
        @merchant_photo.merchant_id = @merchant.id
        
        @merchant_photo.save
        @merchant_photo.url_medium = @merchant_photo.photo.url(:medium)
        @merchant_photo.url_thumb = @merchant_photo.photo.url(:thumb)
        @merchant_photo.save
        @images << @merchant_photo
      end
    else
      @images = @merchant.merchant_photos
    end
    respond_to do |format|
      format.html
      format.mobile 
      format.json {render json: @images}
    end
  end

  def update_repeat_merchant
    @merchant = Merchant.find(params[:id])
    case params[:merchant][:value]
      when 'mon_check'
        @merchant.update_attributes(:mon => "true")
      when 'mon_uncheck'
        @merchant.update_attributes(:mon => "false")
      when 'tue_check'
        @merchant.update_attributes(:tue => "true")
      when 'tue_uncheck'
        @merchant.update_attributes(:tue => "false")
      when 'wed_check'
        @merchant.update_attributes(:wed => "true")
      when 'wed_uncheck'
        @merchant.update_attributes(:wed => "false")
      when 'thu_check'
        @merchant.update_attributes(:thu => "true")
      when 'thu_uncheck'
        @merchant.update_attributes(:thu => "false")
      when 'fri_check'
        @merchant.update_attributes(:fri => "true")
      when 'fri_uncheck'
        @merchant.update_attributes(:fri => "false")
      when 'sat_check'
        @merchant.update_attributes(:sat => "true")
      when 'sat_uncheck'
        @merchant.update_attributes(:sat => "false")
      when 'sun_check'
        @merchant.update_attributes(:sun => "true")
      when 'sun_uncheck'
        @merchant.update_attributes(:sun => "false")

    end
    render :text => 'true'
  end




end
