module OffersHelper
  def offers_saved
    return false if cookies[:offers_key].blank? 
    current_key = [params[:lat], params[:lon], params[:merchant_type_id]]
    current_key == JSON.parse(cookies[:offers_key])
  end

  def offer_time(offer)
    if offer.all_day?
      t("offer.all_day")
    else
      "#{format_time(offer.start_time)} - #{format_time(offer.end_time, " ")}"
    end
  end

  def offer_icon_class(offer)
    offer.merchant.merchant_type.marker_image.split(".")[0]
  end

  def format_time(time, default = "")
    time ? time.strftime('%I:%M%P').gsub(/^0/,'') : default
  end

  def format_third_party_code(offer)
    offer.third_party_code.gsub("_", ' ').split.map(&:capitalize).join(' ')
  end

  def new_buy_via_label(offer)
    case offer.third_party_code
    when Offer.third_party_code_for(:factual), Offer.third_party_code_for(:eight_coupons),Offer.third_party_code_for(:yelp)
      "Buy Coupon at #{format_third_party_code(offer)}"
    when Offer.third_party_code_for(:vouchercodes)
      "Get Voucher at #{format_third_party_code(offer)}"
    when Offer.third_party_code_for(:gotime),Offer.third_party_code_for(:drinkowl), Offer.third_party_code_for(:yacto) 
      "Use now at #{format_third_party_code(offer)}!"
    when Offer.third_party_code_for(:toptable), Offer.third_party_code_for(:opentable), Offer.third_party_code_for(:booktable)
      "Reserve at #{format_third_party_code(offer)}"
    else 
      "Buy Coupon"
    end
  end

  def buy_via_link_label(offer)
    if offer.third_party_code and offer.link_present? and offer.description
      "Buy via " + format_third_party_code(offer)
    else
      if offer.link_present? and offer.merchant.try(:third_party_merchant_type) and offer.merchant.third_party_merchant_type.merchant_type_id and offer.description
        "Buy via 8coupons "
      else
        if offer.link_present? and offer.description 
          "Buy "
        end
      end
    end
  end
  
  def buy_message(offer, class_name)
    html = ""
    if  offer.third_party_code and offer.link and offer.link.strip != '' and offer.description
    
      if offer.third_party_code == "vouchercodes"
        html << offer.description[0,140 - (t('offer.get_voucher_through') + offer.third_party_code.gsub("_", ' ').split.each{|i| i.capitalize!}.join(' ')).length ]
        html << "#{link_to( t('offer.get_voucher_through') + offer.third_party_code.gsub("_", ' ').split.each{|i| i.capitalize!}.join(' '), offer.link ,:class => class_name ? class_name : nil)}"
      else
        html << offer.description[0,140 - (t('offer.buy_via') + offer.third_party_code.gsub("_", ' ').split.each{|i| i.capitalize!}.join(' ')).length ]
        html << "#{link_to( t('offer.buy_via')  + offer.third_party_code.gsub("_", ' ').split.each{|i| i.capitalize!}.join(' '), offer.link ,:class => class_name ? class_name : nil)}"
      end
    else
      if offer.link and offer.link.strip != '' and offer.merchant.third_party_merchant_type and offer.merchant.third_party_merchant_type.merchant_type_id and offer.description
        html << offer.description[0,140 - (t('offer.buy_via') + "8coupons ").length ] 
        html <<  "#{link_to( t('offer.buy_via') + "8coupons ", offer.link,:class => class_name ? class_name : nil )}"
      else
        if offer.link and offer.link.strip != '' and offer.link != 'http://dummy-link.com' and offer.description
          html << offer.description[0,140 - t('offer.buy').length ] 
          html <<  "#{link_to(  t('offer.buy'), offer.link, :class => class_name ? class_name : nil)}"
        else
          html <<  (offer.description != nil ? offer.description : "")
        end
      end
    end
  end

  def offer_sms_body(offer)
    'Check out this great limited-time special at ' << (offer.merchant.try(:name) ? offer.merchant.name : '')  << ' : www.yacto.com/' << offer.merchant.try(:merchant_id) << ' ' << (offer.description ? offer.description : '')
  end

  def offer_email_link(offer)
    "mailto:?subject=Yacto&body=Check out this great limited-time special at #{offer.merchant.name} : www.yacto.com/#{offer.merchant.merchant_id} #{offer.description}"
  end

  def offer_twitter_str(offer)
    return "" if offer.nil?
    twitter_str = "Check out this limited-time special I found on Yacto www.yacto.com/"+(offer.merchant.try(:merchant_id) ? offer.merchant.merchant_id : "")+" "+ (offer.description ? offer.description : "")
    if twitter_str.length > 137
      twitter_str = twitter_str[0,136] + "..."
    end
    twitter_str
  end
  
  def format_active(id)
    if id == 1
      return "Active"
    elsif id == 2
      return "Stop"
    end
  end

end
