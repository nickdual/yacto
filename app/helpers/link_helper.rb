module LinkHelper
  def new_filters_path_with_params
    url_for(:controller => "backbone", :action => 'show', :anchor => "filters", :lat => params[:lat], :lon => params[:lon], :merchant_type_id => params[:merchant_type_id], :favorite => params[:favorite], :time => params[:time], :distance => params[:distance])
  end

  def offers_path_with_params
    offers_path( :lat => params[:lat], :lon => params[:lon], :merchant_type_id => params[:merchant_type_id], :favorite => params[:favorite], :time => params[:time], :distance => params[:distance])
  end

  def nearby_path_with_params
    url_for(:controller => 'offers', :action => "nearby", :lat => params[:lat], :lon => params[:lon], :merchant_type_id => params[:merchant_type_id], :favorite => params[:favorite], :time => params[:time], :distance => params[:distance])
  end

  def selected_nearby_path_with_params(offer_id)
    url_for(:controller => 'offers', :action => "nearby", :lat => params[:lat], :lon => params[:lon], :merchant_type_id => params[:merchant_type_id], :favorite => params[:favorite], :time => params[:time], :distance => params[:distance], :offer_id => offer_id, :offset => params[:offset])
  end

  def favorites_path_with_params
    url_for(:controller => "offers", :action => 'index', :lat => params[:lat], :lon => params[:lon], :merchant_type_id => params[:merchant_type_id], :favorite => 'true', :time => params[:time], :distance => params[:distance])
  end

  def list_details_path_with_params(offer)
    url_for(:controller => "offers", :action => "list_detail",:id => offer.id, :lat=> params[:lat],:lon => params[:lon],:merchant_type_id => params[:merchant_type_id], :favorite => params[:favorite], :time=> params[:time],:distance=> params[:distance])
  end

  def backbone_merchant_path(merchant)
    bb_path(:anchor => "merchants/#{merchant.id}")
  end
end
