module MerchantsHelper
  def merchant_open_time(merchant)
    if merchant.start_open_time
      "#{format_time(merchant.start_open_time)} - #{format_time(merchant.end_open_time, " ")}"
    else
      t("merchant.not_available")
     end
  end

  def merchant_social_string(merchant)
    "Check out #{merchant.name}, www.yacto.com/#{merchant.merchant_id}"
  end
end
