module ApplicationHelper
def get_object_parent_id(object)
    class_name = object.class.name
    if class_name == 'Offer'
      return object.merchant.id
    end
  end

  def file_upload_path(class_name)
    if class_name == 'User'
      return user_profile_path
    elsif class_name == 'Merchant'
      return "/merchants/1"
    else
      return offer_get_image_path
    end
  end

  def name_object_delete_file(class_name)
    if class_name == 'User'
      return 'images'
    elsif class_name == 'Merchant'
      return 'merchant_images'
    else
      return 'offer_images'
    end
  end

# Get default url when img is null
  def get_image(object_img,img_default)
    puts '========================================='
    puts img_default
    if object_img.blank?
      return img_default
    else
      return object_img.url
    end
  end
  def merchant_path(merchant)

      return "/merchants/" + merchant.id.to_s + "?payment=1"

  end

  def update_status(param)
    if param == true
     return  "checked='checked'"
    else
     return ''
    end
  end

  def select_expire_day

    my_date_time = Date.current
    select_date(my_date_time)

  end

  def check_authorized(param)
    if param == 'true'
      return  true
    else
      return false
    end
  end

  def with_format(format, &block)
    old_formats = formats
    self.formats = [format]
    block.call
    self.formats = old_formats
    nil
  end

  
end
