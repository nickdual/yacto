class UserMailer < ActionMailer::Base
  default from: Yacto::Application::MAIL_FROM

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.confirm_merchant_type.subject
  #
  def confirm_merchant_type(message)
    @message = message

    mail to: Yacto::Application::MAIL_FROM
  end
  def confirmation_post_offer(email,subject,message)
    @message = message
    mail to: email,subject: subject 
	end

	def third_party_integration_check(email,message)
		@message = message
		mail to: email, subject: "#{Date.yesterday} Integration Status"
  end
  def create_new_country(iso_code_two_letter)
    @message =  "Please update the info of country: " + iso_code_two_letter
    mail to: Yacto::Application::MAIL_FROM
  end
  def create_new_city(message)
    @message =  "Please check the info of city: " + message
    mail to: Yacto::Application::MAIL_FROM
  end
end
