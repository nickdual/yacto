class MerchantType < ActiveRecord::Base

#  validates :merchant_type, :inclusion => { :in => %w(Eat Drink Fun Look Services Shoping Travel Other),
#    :message => "%{value} is not a valid type" },

#    :length => { :maximum => 20 }
##  validates :sub_type, :inclusion => { :in => %w(Chinese Sushi Other),
##    :message => "%{value} is not a valid sub type" },
##    :length => { :maximum => 20 }
#  validates :language, :inclusion => { :in => %w(USEnglish UKEnglish German Swedish),
#    :message => "%{value} is not a valid language" },
#    :length => { :maximum => 30 }
  has_many :offers
  #belongs_to :user
  has_many :merchants
  attr_accessible :merchant_type, :sub_type, :language, :lookup_item_id,:color,
    :image, :marker_image, :map_icon

  scope :merchant_type, lambda {|type| where(:merchant_type => type) }
  scope :top_lvl, where(:sub_type => " ")
  #  validate :type_included, :sub_type_included, :language_included
  #  def type_included
  #    unless ["Eat", "Drink", "Fun", "Do", "Other"].include? type
  #      errors.add(:type, "type is not valid")
  #    end
  #  end
  #  def sub_type_included
  #    unless ["Chinese", "Sushi"].include? sub_type
  #      errors.add(:type, "sub_type is not valid")
  #    end
  #  end
  #  def language_included
  #    unless ["USEnglish", "UKEnglish", "German", "Swedish"].include? language
  #      errors.add(:type, "language is not valid")
  #    end
  #  end
end

# == Schema Information
#
# Table name: merchant_types
#  t.string   "language"
#  t.string   "lookup_item_id"
#  t.string   "merchant_type"
#  t.string   "sub_type"
#  t.datetime "created_at",     :null => false
#  t.datetime "updated_at",     :null => false
