class City < ActiveRecord::Base
    include Geokit::Geocoders
 acts_as_mappable  :default_units => :miles, 
                   :default_formula => :sphere, 
                   :distance_field_name => :distance,
                   :lat_column_name => :latitude,
                   :lng_column_name => :longitude
  ## Validations
  validates_uniqueness_of :name,:scope => [:country_id,:geonames_id]
#  validates_uniqueness_of :slug
  validates :name, :presence => true
#  validates :slug, :presence => true
  validates :latitude, :presence => true
  validates :longitude, :presence => true

  scope :cityname, lambda { |name| where(name: name.strip) }
  scope :country_code, lambda { |code| where(country_iso_code_two_letters: code.strip) }
  
  ## Relationship
  belongs_to :country
  has_many :offers
  has_many :merchants
  has_many :users
end
