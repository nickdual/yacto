class MerchantPhoto < ActiveRecord::Base
  attr_accessible :photo ,:url_medium, :merchant_id
  has_attached_file :photo, :styles => { :medium => "300x300>", :thumb => "68x41>" },:s3_credentials => "#{Rails.root}/config/s3.yml",
:bucket => "streetmavens.test",
:storage => :s3,
:path => "/:style/:id/:filename"
   
belongs_to :merchant
end
