require 'link_validator'
require 'military_time_validator'
require 'json'

class Offer < ActiveRecord::Base
  include FlagShihTzu
  include Geokit::Geocoders

  THIRD_PARTY_CODES = {
    :gotime => "gotime",
    :factual => "factual",
    :vouchercodes => "vouchercodes",
    :eight_coupons => "8coupons",
    :groupon => "groupon_now",
    :yelp => "Yelp",
    :grub_hub => "GrubHub",
    :sign_post => "Signpost",
    :yacto => "yacto",
    :toptable => "toptable",
    :opentable => "opentable",
    :booktable => "booktable",
    :drinkowl => "drinkowl"
    # Groupon - which is groupon :?
  }
#  has_flags 1 => :mon,
#    2 => :tue,
#    3 => :web,
#    4 => :thu,
#    5 => :fri,
#    6 => :sat,
#    7 => :sun, :named_scopes => true,
#    :column => 'day_in_week'
  acts_as_mappable  :default_units => :miles,
    :default_formula => :sphere,
    :distance_field_name => :distance,
    :lat_column_name => :lat,
    :lng_column_name => :lon
  validates :price, :numericality => { :greater_than_or_equal_to => 0.0 }, :allow_blank => true
  validates :value, :numericality => { :greater_than_or_equal_to => 0.0 }, :allow_blank => true
  validates :name, :length => { :maximum => 140 },:presence => true
  validates :description, :length => { :maximum => 140 }
  validates :third_party_code, :length => { :maximum => 30 }
  validates :link, :link => true, :allow_blank => true
  validates :image, :link => true, :allow_nil  => true, :allow_blank => true
  validates :image_thumbnail, :link => true, :allow_blank => true
  validates :image_thumbnail_retina, :link => true, :allow_blank => true
  #  validates :start_time, :military_time => true
  validate :discount_accurate_to_two_decimal_places
  attr_accessible :name, :description, :third_party_code, :value, :price, :link,
    :image, :image_thumbnail, :image_thumbnail_retina, :start_time, :discount,
    :active, :primary_address, :detail_views, :third_party_id, :offer_duration,
    :recurrence, :merchant_id, :city, :country, :state_province_region,
    :postal_code, :city_id , :lat ,:lon,:end_time, :merchant_type_id, :offer_guid,
    :source,:mon, :tue, :wed, :thu, :fri, :sun, :sat,:user_id, :day_in_week, :expire_day, :local_end_day,
    :utc_end_day, :local_start_day, :utc_start_day, :updated_at

  after_create :offer_yacto_id, :default_merchant_type
  ## Relationship
  belongs_to :merchant
  belongs_to :merchant_type
  has_many :favorites
  belongs_to :city
  belongs_to :user

  scope :active, where(:active => 1)
  scope :city_id, lambda { |city_id| where(:city_id => city_id) }
  scope :with_merchant, where(arel_table[:merchant_id].not_eq(nil))

  #TODO maybe we could refactor how to get :day_in_week and the rest is the same, but it's ok for now
  scope :monday_offers, where(:day_in_week => [1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35,37,39,41,43,45,47,49,51,53,55,57,59,61,63,65,67,69,71,73,75,77,79,81,83,85,87,89,91,93,95,97,99,101,103,105,107,109,111,113,115,117,119,121,123,125,127,nil])
  scope :tuesday_offers, where(:day_in_week => [2,3,6,7,10,11,14,15,18,19,22,23,26,27,30,31,34,35,38,39,42,43,46,47,50,51,54,55,58,59,62,63,66,67,70,71,74,75,78,79,82,83,86,87,90,91,94,95,98,99,102,103,106,107,110,111,114,115,118,119,122,123,126,127,nil])
  scope :wednesday_offers, where(:day_in_week => [4,5,6,7,12,13,14,15,20,21,22,23,28,29,30,31,36,37,38,39,44,45,46,47,52,53,54,55,60,61,62,63,68,69,70,71,76,77,78,79,84,85,86,87,92,93,94,95,100,101,102,103,108,109,110,111,116,117,118,119,124,125,126,127,nil])
  scope :thursday_offers, where(:day_in_week => [8,9,10,11,12,13,14,15,24,25,26,27,28,29,30,31,40,41,42,43,44,45,46,47,56,57,58,59,60,61,62,63,72,73,74,75,76,77,78,79,88,89,90,91,92,93,94,95,104,105,106,107,108,109,110,111,120,121,122,123,124,125,126,127,nil])
  scope :friday_offers, where(:day_in_week => [16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,nil])
  scope :saturday_offers, where(:day_in_week => [32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,nil])    
  scope :sunday_offers, where(:day_in_week => [64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,nil])    

  def self.third_party_code_for(code_symbol)
    THIRD_PARTY_CODES[code_symbol]
  end

  def offer_yacto_id
    self.update_column(:offer_id, (self.id + 10000000).to_s(36))
  end
  def default_merchant_type
    if self.merchant and self.merchant.merchant_type_id
      self.update_attribute("merchant_type_id", self.merchant.merchant_type_id)
    end
  end

  def discount_accurate_to_two_decimal_places
    # TODO-----------------move error message to language file
    errors.add(:discount, "must be accurate to two decimal places") if self.discount and self.discount.to_s.split('.').last.length > 2
  end

  def period(now_time)
    if end_time.hour - now_time.hour == 1
      if end_time - now_time < 1.minute
        return (60 - now_time.min + end_time.min).to_s
      else
        return nil  #btw usually it's not a good idea to return nil :|
      end
    end
    if end_time.hour - now_time.hour == 0 and end_time.min - now_time.min >= 0
      return end_time.min - now_time.min
    end
    return nil #btw usually it's not a good idea to return nil :|
  end
  def self.time(time)
    return Time.strptime(time, "%H:%M").strftime("%d-%m-%Y %H:%M:%S")
  end
  def self.format_phone(phone)
    return phone.strip.scan(/\d/).map { |n| n.to_i }.join()
  end
  def set_repeat_days(mon, tue, wed, thu, fri, sat, sun)
    self.mon = mon
    self.tue = tue
    self.wed = wed
    self.thu = thu
    self.fri = fri
    self.sat = sat
    self.sun = sun
    self.save!
  end
  def copy
    #here I would use self.attributes
    #sth like Offer.build(attributes), or some basic filtering, like for the name
    offer = Offer.new
    offer.active = self.active
    offer.city_id = self.city_id
    offer.currency = self.currency
    offer.description = self.description
    offer.discount = self.discount
    offer.end_time = self.end_time
    offer.image = self.image
    offer.image_thumbnail = self.image_thumbnail
    offer.image_thumbnail_retina = self.image_thumbnail_retina
    offer.lat = self.lat
    offer.link = self.link
    offer.lon = self.lon
    offer.merchant_id = self.merchant_id
    offer.merchant_type_id = self.merchant_type_id
    offer.offer_duration = self.offer_duration
    offer.name = "Copy of #{self.name}"
    return offer
  end

  #lots of params - code smell, maybe some hidden object :?
  #I've got a better idea - refactor into separate object, builder pattern, but it's a bigger thing
  def new_offer_8coupon(name, description, post_date, time_zone, expiration_date, link, image, merchant) 
    preinit_offer(name, description, link, image, merchant,127,Time.current.beginning_of_day , Time.current.end_of_day )

    self.local_start_day = parse_date(post_date)
    self.utc_start_day = local_to_utc(local_start_day, time_zone) if local_start_day.present?
    
    self.local_end_day = parse_date(expiration_date).try(:end_of_day)
    self.utc_end_day = local_to_utc(local_end_day, time_zone) if local_end_day.present?
    self.set_repeat_days(true, true, true, true, true, true, true)
    self.third_party_code = '8coupons'
    self.save
  end

  def new_offer_groupon(name, value, price, description, start_time, end_time, time_zone, link, image, third_party_code, third_party_id, merchant)
    preinit_offer(name, description, link, image, merchant,127,Time.current.beginning_of_day , Time.current.end_of_day )
    self.value = value.to_f if value.present?
    self.price = price.to_f if price.present?

    self.utc_start_day = parse_date(post_date)
    self.local_start_day = utc_to_local(utc_start_day, time_zone) if utc_start_day.present?

    self.utc_end_day = parse_date(end_time)
    self.local_end_day = utc_to_local(utc_end_day,time_zone)

    self.third_party_code = third_party_code if third_party_code.present?
    self.third_party_id = third_party_id if third_party_id.present?

    self.save
  end

  def new_offer_factual (name,description,merchant,options = {})
		description = description[0,139] if description
    preinit_offer(name,description,options[:link],nil,merchant,127,Time.current.beginning_of_day,\
      Time.current.end_of_day)

    # override price with original price information
    self.price = options[:price].to_i if options[:price].present?

    # set local/utc end date
    self.utc_end_day = options[:expires]
    self.local_end_day = options[:expires_local]

    # set third party information
    self.third_party_code = options[:source_provider]
    self.third_party_id = options[:source_id]
		self.source = "factual"

    # set currency
    self.currency = options[:currency]
    # set geo location
    self.lon = options[:lon]
    self.lat = options[:lat]
    self.city_name = options[:city_name]
    self.set_repeat_days(true, true, true, true, true, true, true)
    # save offer
    if self.save
    else
      puts self.errors.full_messages
    end
	end
	# link start_time end_time repeat_days source_provider source_id lon lat city_name
	def new_offer_drinkowl(name,merchant,options = {})
		preinit_offer(name,nil,options[:link],nil,merchant,127,options[:start_time],options[:end_time])

		repeat_days = [false,false,false,false,false,false,false]
		if options[:repeat_days]
			arr = options[:repeat_days]
			for x in 0..(arr.length-1)
				repeat_days[arr[x]] = true
			end
		end
		self.set_repeat_days(repeat_days[0],repeat_days[1],repeat_days[2],repeat_days[3],repeat_days[4],repeat_days[5],
												 repeat_days[6])

		# set third party information
		self.third_party_code = options[:source_provider]
		self.third_party_id = options[:source_id]
		self.source = "drinkowl"

		# set geo location
		self.lon = options[:lon]
		self.lat = options[:lat]
		self.city_name = options[:city_name]

		if self.save
		else
			p self.errors.full_messages
		end

	end
  def new_offer_vouchercodes(name, description, start_time, end_time, time_zone, link, image, third_party_code, vouchercodes_merchant_id, vouchercodes_offer_id, merchant)
    preinit_offer(name, description, link, image, merchant,127,Time.current.beginning_of_day , Time.current.end_of_day )

    self.local_start_day =  start_time
    self.utc_start_day = local_to_utc(local_start_day, time_zone) if local_start_day.present?

    self.local_end_day = end_time
    self.utc_end_day = local_to_utc(local_end_day, time_zone) if local_end_day.present?
    self.set_repeat_days(true, true, true, true, true, true, true)
    self.vouchercodes_merchant_id = vouchercodes_merchant_id if vouchercodes_merchant_id
    self.vouchercodes_offer_id = vouchercodes_offer_id if vouchercodes_offer_id
    self.third_party_code = third_party_code if third_party_code != ''

    self.save
  end
  def new_offer_gotime(name, description, link, time, time_zone, third_party_code, merchant)
    if !time.include? 'and'

      if time.include? "Every Day"
        self.open_hours(time.split(/[[:space:]]/)[0] + " " + time.split(/[[:space:]]/)[1])
        local_start_time = Time.parse(time.split(/[[:space:]]/)[2].split("-")[0])
        local_end_time = Time.parse(time.split(/[[:space:]]/)[2].split("-")[1])
        preinit_offer(name, description, link, nil, merchant,nil,local_start_time.strftime("%d-%m-%Y %H:%M:%S"), local_end_time.strftime("%d-%m-%Y %H:%M:%S"))
      else
        self.open_hours(time.split(/[[:space:]]/)[0])
        local_start_time = Time.parse(time.split(/[[:space:]]/)[1].split("-")[0])
        local_end_time = Time.parse(time.split(/[[:space:]]/)[1].split("-")[1])
        preinit_offer(name, description, link, nil, merchant,nil,local_start_time.strftime("%d-%m-%Y %H:%M:%S"), local_end_time.strftime("%d-%m-%Y %H:%M:%S") )
      end
      self.third_party_code = third_party_code if third_party_code != ''

      self.save
    end
  end
  def self.days
    return {"Mon" => 0, "Tue" => 1, "Wed" => 2, "Thu" => 3, "Fri" => 4, "Sat" => 5, "Sun" => 6, "Every Day" => 7}
  end
  def day_week(number)
    case number
    when 0
      self.mon = true
    when 1
      self.tue = true
    when 2
      self.wed = true
    when 3
      self.thu = true
    when 4
      self.fri = true
    when 5
      self.sat = true
    when 6
      self.sun = true
    else
      self.mon = true
      self.tue = true
      self.wed = true
      self.thu = true
      self.fri = true
      self.sat = true
      self.sun = true
    end
  end
  def open_hours(days)
    if days
      day_array = days.split("-")
      if day_array[1]
        from = Offer.days[day_array[0]]
        to = Offer.days[day_array[1]]
        while from <= to
          self.day_week(from)
          from +=1
        end
      else
        self.day_week(Offer.days[days])
      end
   
    end
  end
  def preinit_offer(name, description, link, image, merchant, day_in_week, start_time, end_time)
    self.start_time = start_time
    self.end_time = end_time
    self.day_in_week = day_in_week if day_in_week != nil

    self.name = name[0,139] if name.present?
    self.description = description
    self.link = link
    self.image = image

    set_merchant(merchant)
  end

  def set_merchant(merchant)
    self.merchant_id = merchant.id
    self.city_id = merchant.city_id
    self.lat = merchant.lat
    self.lon = merchant.lon
    self.primary_address = merchant.primary_address
  end

  def parse_date(date)
    begin
      return Time.parse(date) if date.present?
    rescue 
      return nil
    end
  end

  def local_to_utc(date, time_zone)
    begin
      unless time_zone.nil?
        tz = TZInfo::Timezone.get(time_zone)
        return tz.local_to_utc(date)
      end
    rescue 
      return nil
    end
  end
  #should create static function self
  def utc_to_local(date, time_zone)
    begin
      unless time_zone.nil?
        tz = TZInfo::Timezone.get(time_zone)
        return tz.local_to_utc(date)
      end
    rescue 
      return nil
    end
  end

  def self.utc_to_local(date, time_zone)
    begin
      unless time_zone.nil?
        tz = TZInfo::Timezone.get(time_zone)
        return tz.local_to_utc(date)
      end
    rescue
      return nil
    end
  end
  
  def self.create_batch params
    offers = []
    params.each do |offer|
      merchants = Merchant.where(:third_party_id => offer["merchant"]["third_party_id"].to_s)
      m = ""
      if merchants.blank?
        m = Merchant.create!(offer["merchant"])
      else
        m = merchants.first
      end
      address = offer["merchant"]["primary_address"] + "," + offer["merchant"]["city_name"] + ',' + offer["merchant"]["state_province_region"] + ',' + offer["merchant"]["postal_code"] + ',' + offer["merchant"]["country"]

      loc=GoogleGeocoder.geocode(address)

      address = loc.full_address
      m.update_attributes(:merchant_type_id => MerchantType.where(:merchant_type => "Default",:language => 'en', :sub_type => ' ').first.id, :lat => loc.lat, :lon => loc.lng, :primary_address => address)
      if m != ""
        chars = 'abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ23456789'
        password = ''
        6.times { |i| password << chars[rand(chars.length)] }
        user = User.new( :mobile_number => Time.now.to_f, :user_admin => true, :role_id => Role.find_by_name("Merchant admin").id,
          :email => "usman+#{Time.now.to_f}@devsinc.com", :password => password,
          :username => "usman+#{(Time.now.to_f)}@devsinc.com", :first_name => "Update me", :last_name => "Update me")
        user.skip_confirmation!
        user.save

        user.update_attribute("merchant_id", m.id)
      end

      offer.delete("merchant")
      offer["third_party_id"] = m.third_party_id
      new_offer = Offer.create!(offer)
      if offer["start_time"]
        if offer["start_time"].length == 2
          offer["start_time"] = offer["start_time"] + '00'
        end
        start_time = Time.strptime(offer["start_time"], "%H%M").strftime("%d-%m-%Y %H:%M:%S")
      end
      if offer['end_time']
        if offer["end_time"].length == 2
          offer["end_time"] = offer["end_time"] + '00'
        end
        end_time = Time.strptime(offer['end_time'], "%H%M").strftime("%d-%m-%Y %H:%M:%S")
      end
      new_offer.update_attributes(:merchant_id => m.id.to_i, :lat => loc.lat, :lon => loc.lng, :primary_address => address, :user_id => user.id,:day_in_week => 127,:start_time => start_time, :end_time => end_time)
      offers << new_offer
    end
    return offers
  end
  def self.compare_time
    return "(( to_char(offers.start_time, 'HH24-MI-ss') <= ?
    AND to_char(offers.end_time, 'HH24-MI-ss') >= ?) 
    OR (to_char(offers.end_time, 'HH24-MI-ss') >= ?
    AND to_char(offers.start_time, 'HH24-MI-ss') <= ?) 
    OR (to_char(offers.end_time, 'HH24-MI-ss') <= ?
    AND to_char(offers.start_time, 'HH24-MI-ss') >= ?))"
  end

  def self.compare_time_with_labels
    return "(( to_char(offers.start_time, 'HH24-MI-ss') <= :start_time
    AND to_char(offers.end_time, 'HH24-MI-ss') >= :start_time) 
    OR (to_char(offers.end_time, 'HH24-MI-ss') >= :end_time
    AND to_char(offers.start_time, 'HH24-MI-ss') <= :end_time) 
    OR (to_char(offers.end_time, 'HH24-MI-ss') <= :end_time
    AND to_char(offers.start_time, 'HH24-MI-ss') >= :start_time))"
  end

  def self.deactivate_batch offer_guids
    offers = []

    offer_guids.each do |guid|

      offer = Offer.where(:offer_guid => guid["offer_guid"].to_i).first
      if offer
        offer.update_attribute("active", guid["active"].to_i)
        offers << offer
      end

    end
    return offers
  end

  def all_day?
    start_time and start_time.strftime('%H:%M') == '00:00' and end_time and end_time.strftime('%H:%M') == '23:59'
  end

  def link_present?
    link and link.strip != '' and link != 'http://dummy-link.com'
  end
end
# == Schema Information
#
# Table name: offers
#  t.integer  "merchant_id"
#  t.string   "offer_id"
#  t.string   "name"
#  t.integer  "active"
#  t.string   "description"
#  t.string   "primary_address"
#  t.time     "start_time"
#  t.integer  "detail_views"
#  t.string   "third_party_code"
#  t.string   "third_party_id"
#  t.float    "price"
#  t.float    "value"
#  t.float    "discount"
#  t.string   "link"
#  t.string   "image"
#  t.string   "image_thumbnail"
#  t.string   "image_thumbnail_retina"
#  t.string   "offer_duration"
#  t.string   "recurrence"
#  t.datetime "created_at",             :null => false
#  t.datetime "updated_at",             :null => false
#  t.string   "city"
#  t.string   "state_province_region"
#  t.string   "postal_code"
#  t.string   "country"
#  t.float    "lon"
#  t.float    "lat"
