class OfferInsertion < ActiveRecord::Base
  validates :name, :length => { :maximum => 140 }
  attr_accessible :name
end
# == Schema Information
#
# Table name: offer_insertions
#  t.string   "name"
#  t.datetime "created_at", :null => false
#  t.datetime "updated_at", :null => false