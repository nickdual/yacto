require 'curl'
require 'open-uri'
require 'net/http'
require 'openssl'
require 'active_support/core_ext'
require 'geonames'
require 'geokit'
class Merchant < ActiveRecord::Base
  include Geokit::Geocoders
  include Rails.application.routes.url_helpers
  include ActionView::Helpers::SanitizeHelper

  acts_as_mappable :default_units => :miles,
    :default_formula => :sphere,
    :distance_field_name => :distance,
    :lat_column_name => :lat,
    :lng_column_name => :lon
  #  validates :merchant_type, :inclusion => { :in => %w(Eat Drink Fun Do Other),
  #    :message => "%{value} is not a valid type" }, :length => { :maximum => 20 }
  #  validates :sub_type, :inclusion => { :in => %w(Chinese Sushi),
  #    :message => "%{value} is not a valid sub type" }, :length => { :maximum => 20 }

  validates :name, :length => { :maximum => 140 }, :presence => true
  #validates :postal_code, :presence => true
  #validates :merchant_type_id, :presence => true
  validates :mobile_description, :length => { :maximum => 500 }
  validates :third_party_code, :length => { :maximum => 30 }
  validates :why_visit, :length => { :maximum => 200 }
  validates :best_thing, :length => { :maximum => 200 }
  validates :unique, :length => { :maximum => 200 }
  validates :featured_review, :length => { :maximum => 200 }
  attr_accessible :merchant_type_name, :sub_type, :name, :mobile_description,
    :third_party_code, :address_description, :mobile_number, :third_party_id,
    :merchant_id, :another_address, :country, :state_province_region,
    :postal_code, :primary_address, :user_rating, :lat, :lon,:city_id,
    :special_instructions,:merchant_type_id, :start_open_time, :end_open_time,
    :phone, :city_name, :google_id, :twitter_name, :facebook_page_id,
    :mon, :mon_start_time, :mon_end_time, :tue, :tue_start_time, :tue_end_time, :wed, :wed_start_time, :wed_end_time,
    :fri, :fri_start_time, :fri_end_time, :thu, :thu_start_time, :thu_end_time, :sat, :sat_start_time, :sat_end_time,
    :sun, :sun_start_time, :sun_end_time, :desktop_description,:why_visit,:best_thing,:unique,:featured_review,
    :yelp_id, :twitter_id,:flickr_id,:instagram_id, :another_address,:active,:factual_id, :merchant_type_id, :time_zone, :third_party_merchant_type_id ,:website_url

  after_create :merchant_yacto_id

  ## Relationship
  has_many :users, :dependent => :destroy
  has_many :offers ,:dependent => :destroy
  belongs_to :merchant_type
  belongs_to :third_party_merchant_type
  belongs_to :city
  has_many :favorites,:dependent => :destroy
  has_many :merchant_photos

  def to_uri
    merchant_path(self)
  end
  def merchant_yacto_id
    self.update_column(:merchant_id, (self.id + 10000000).to_s(36))
  end
  # should change static function
  def get_time_zone
    time_zone = nil
    if self.city and self.city.timezone
      time_zone == self.city.timezone
    else
      begin
        timezone = Geonames::WebService.timezone self.lat,self.lon
        if timezone.timezone_id
          time_zone = timezone.timezone_id
        end
      rescue
      end
    end
    return time_zone
  end
  def self.get_timezone(lat,lon)
    begin
      timezone = Geonames::WebService.timezone lat,lon
      if timezone.timezone_id
        time_zone = timezone.timezone_id
        return time_zone
      end
      return nil
    rescue
      return nil
    end
  end
  # Pass city, state, postal code, country code
  # we query for geocode and timezone data, create a new city record and return this recor
  def self.create_new_city(city, state, postal_code,country_code)
      name = ''
      name = name + ", " + city if city.present?
      name = name + ", " + state if state.present?
      name = name + ", " + postal_code if postal_code.present?
      name = name + ", " + country_code if country_code.present?
      loc=GoogleGeocoder.geocode(name)  if name.present?
      message = ''
      if loc and loc.success and loc.city.present?
        country = Country.find(:first ,:conditions =>{:iso_code_two_letter => loc.country_code})
        if country == nil and loc.country_code.present? and loc.length == 2
           country = Country.new(:iso_code_two_letter => loc.country_code, :iso_code_three_letter => '',:iso_number => 0,:name => '')
           if country.save
             UserMailer.create_new_country(loc.country_code).deliver
             city = City.find(:first, :conditions => ["UPPER(name) = ? and country_id = ?", loc.city.strip.upcase, country.id])
             if city == nil
              city = City.new(:country_id => country.id, :name => loc.city, :latitude => loc.lat, :longitude => loc.lng, :country_iso_code_two_letters => country.iso_code_three_letter, :timezone => Merchant.get_timezone(loc.lat, loc.lng))
              if city.save
                message = "name: " + city.name if city.name.present?
                message = message + ",lat: " + city.latitude.to_s if city.latitude.present?
                message = message + ",lng: " + city.longitude.to_s if city.longitude.present?
                message = message + "country_iso_code_two_letters: " + city.country_iso_code_two_letters if city.country_iso_code_two_letters.present?
                message = message + ",timezone: " + city.timezone  if city.timezone.present?
                puts message
                return city
              else
                return nil
              end
             end
             return city
           end
        else
          if country
          city = City.find(:first, :conditions => ["UPPER(name) = ? and country_id = ?", loc.city.strip.upcase, country.id])
          if city == nil
            city = City.new(:country_id => country.id, :name => loc.city, :latitude => loc.lat, :longitude => loc.lng, :country_iso_code_two_letters => country.iso_code_three_letter, :timezone => Merchant.get_timezone(loc.lat, loc.lng))
            if city.save
              message = "name: " + city.name if city.name.present?
              message = message + ",lat: " + city.latitude.to_s if city.latitude.present?
              message = message + ",lng: " + city.longitude.to_s if city.longitude.present?
              message = message + "country_iso_code_two_letters: " + city.country_iso_code_two_letters if city.country_iso_code_two_letters.present?
              message = message + ",timezone: " + city.timezone  if city.timezone.present?
              puts message
              return city
            else
              return nil
            end
          end

          return city
          end
        end
      end
      return nil

  end
  def administrative_email
    return "administrative.contact.email"
  end
  def set_open_hours(start_time ,end_time)
    self.mon_start_time = start_time
    self.mon_end_time = end_time

    self.tue_start_time = start_time
    self.tue_end_time = end_time

    self.wed_start_time = start_time
    self.wed_end_time = end_time

    self.thu_start_time = start_time
    self.thu_end_time = end_time

    self.fri_start_time = start_time
    self.fri_end_time = end_time

    self.sat_start_time = start_time
    self.sat_end_time = end_time

    self.sun_start_time = start_time
    self.sun_end_time = end_time
    self.save!
  end
  def administrative_contact
    return "administrative.contact.first"
  end
  def self.days
    return {"Mon" => 0, "Tue" => 1, "Wed" => 2, "Thu" => 3, "Fri" => 4, "Sat" => 5, "Sun" => 6, "Every Day" => 7}
  end
  def self.send_twilio(from_phone, twilio_phone_number, message)
    @client = Twilio::REST::Client.new ACCOUNT_SID, AUTH_TOKEN
    case message.length
    when 0..160
     @client.account.sms.messages.create(:from => twilio_phone_number, :to => from_phone,
                                        :body => message)
      when 161..320
        @client.account.sms.messages.create(:from => twilio_phone_number, :to => from_phone,
                                            :body => message[0,160])
        @client.account.sms.messages.create(:from => twilio_phone_number, :to => from_phone,
                                            :body => message[160,message.length - 160])
    end

  end
  def cancel
    offer = self.offers.last
    offer.update_attribute("active", 2)
  end
  def cancel_all
    offers = self.offers
    offers.each do |offer|
      offer.update_attribute("active", 2)
    end
  end


  def repeat(frequency)
    last_offer = self.offers.last
    if last_offer
      if frequency.casecmp(I18n.t("merchant.smsrequest.repeat.daily")) == 0
        last_offer.set_repeat_days(true, true, true, true, true, true, true)
       return
      end
      if frequency.casecmp(I18n.t("merchant.smsrequest.repeat.weekday")) == 0
        last_offer.set_repeat_days(true, true, true, true, true, false, false)
        return
      end
      if frequency.casecmp(I18n.t("merchant.smsrequest.repeat.weekend")) == 0
        last_offer.set_repeat_days(false, false, false, false, false, true, true)
        return
      end
      if frequency.casecmp(I18n.t("merchant.smsrequest.repeat.weekly")) == 0
        if self.city
          tz = TZInfo::Timezone.get(self.city.timezone)
          now_time = tz.utc_to_local(Time.current)
          case now_time.strftime("%u").to_i
          when 1
            last_offer.set_repeat_days(true, false, false, false, false, false, false)

          when 2
            last_offer.set_repeat_days(false, true, false, false, false, false, false)
          when 3
            last_offer.set_repeat_days(false, false, true, false, false, false, false)
          when 4
            last_offer.set_repeat_days(false, false, false, true, false, false, false)
          when 5
            last_offer.set_repeat_days(false, false, false, false, true, false, false)
          when 6
            last_offer.set_repeat_days(false, false, false, false, false, true, false)
          when 7
            last_offer.set_repeat_days(false, false, false, false, false, false, true)

          end

        end
        return
      end
    end
  end
  def create_dummy_user()
    chars = 'abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ23456789'
    password = ''
    6.times { |i| password << chars[rand(chars.length)] }
    begin
      user = User.new( :mobile_number => Time.now.to_f, :user_admin => true, :role_id => Role.find_by_name("Merchant admin").id,
                       :email => "usman+#{Time.now.to_f}@devsinc.com", :password => password,
                       :username => "usman+#{(Time.now.to_f)}@devsinc.com", :first_name => "Update me", :last_name => "Update me")
      user.skip_confirmation!
      user.merchant_id = self.id
      user.save
    rescue
    end
  end
  def day_in_week(number, start_time, end_time)
    case number
    when 0
      self.mon = true
      self.mon_start_time = start_time
      self.mon_end_time = end_time
    when 1
      self.tue = true
      self.tue_start_time = start_time
      self.tue_end_time = end_time
    when 2
      self.wed = true
      self.wed_start_time = start_time
      self.wed_end_time = end_time
    when 3
      self.thu = true
      self.thu_start_time = start_time
      self.thu_end_time = end_time
    when 4
      self.fri = true
      self.fri_start_time = start_time
      self.fri_end_time = end_time
    when 5
      self.sat = true
      self.sat_start_time = start_time
      self.sat_end_time = end_time
    when 6
      self.sun = true
      self.sun_start_time = start_time
      self.sun_end_time = end_time
    else
      self.mon = true
      self.mon_start_time = start_time
      self.mon_end_time = end_time
      
      self.tue = true
      self.tue_start_time = start_time
      self.tue_end_time = end_time
      
      self.wed = true
      self.wed_start_time = start_time
      self.wed_end_time = end_time
      
      self.thu = true
      self.thu_start_time = start_time
      self.thu_end_time = end_time
      
      self.fri = true
      self.fri_start_time = start_time
      self.fri_end_time = end_time
      
      self.sat = true
      self.sat_start_time = start_time
      self.sat_end_time = end_time
      
      self.sun = true
      self.sun_start_time = start_time
      self.sun_end_time = end_time
    end
  end

  def merchant_open_hours(days, time)
    day_array = days.split("-")
    begin
      if day_array[1]
        from = Merchant.days[day_array[0]]
        to = Merchant.days[day_array[1]]
        while from <= to
          self.day_in_week(from, Time.parse(time.split("-")[0]), Time.parse(time.split("-")[1]))
          from +=1
        end
      else
        self.day_in_week(Merchant.days[days], Time.parse(time.split("-")[0]), Time.parse(time.split("-")[1]))
      end
    rescue
    end
  end

  def self.search(params)
    params.delete("action")
    params.delete("controller")
    params.each_key do |key|
      if params[key].blank?
        params.delete(key)
      end
    end
    return where(params)
  end
  def self.api_vouchercodes(content,zoom)
    if content != nil
      results =JSON.parse(content)
      merchants =  results["places"]
      offers = results["offers"]
      merchants.each do |merchant|
        if merchant[1]["city"].present? and merchant[1]["phone"].present?
          city = City.find(:first ,:conditions => ["UPPER(name) = ? and country_iso_code_two_letters IN (?)", merchant[1]["city"].strip.upcase, ["IE","GB"]])
          city = Merchant.create_new_city(merchant[1]["city"] ,nil,merchant[1]["postcode"], nil) if city == nil and merchant[1]["city"].present? and merchant[1]["postcode"].present?
          if city
            old_merchant = Merchant.find(:first, :conditions => {:vouchercodes_merchant_id => merchant[1]["merchantid"].to_i, :vouchercodes_place_id => merchant[1]["placeid"].to_i, :phone => merchant[1]["phone"].strip.scan(/\d/).map { |n| n.to_i }.join()})
            if old_merchant
              offers[merchant[1]["merchantid"]].each do |offer|
                if offer["offertitle"].present?
                  old_offer = Offer.find(:first, :conditions => {:vouchercodes_offer_id => offer["offerid"].to_i, :vouchercodes_merchant_id => offer["merchantid"].to_i, :name => offer["offertitle"][0,139], :merchant_id => old_merchant.id })
                  if old_offer == nil
                    new_offer = Offer.new
                    new_offer.new_offer_vouchercodes(offer["offertitle"][0,139],offer["offertitle"][0,139],offer["formatted_starts"] ,offer["formatted_expires"], old_merchant.city.timezone,"http://www.vouchercodes.co.uk" + offer["link"].tr("\\", ''),"http://static.vouchercodes.co.uk/images/merchants/logo/" + merchant[1]["merchantid"] + "png","vouchercodes", offer["merchantid"].to_i, offer["offerid"].to_i, old_merchant)
                  end
                end
                #local_end_day = Time.parse(offer["formatted_expires"]) if offer["formatted_expires"] != ''
                #tz = TZInfo::Timezone.get(city.timezone)
                #utc_end_day  = tz.local_to_utc(local_end_day)
                #if utc_end_day and offer["offertitle"] and offer["offertitle"].strip != '' and old_offer != nil  and offer["formatted_expires"] != '' and utc_end_day > Time.current and old_offer.link == nil
                  #local_start_day =  Time.parse(offer["formatted_starts"]) if offer["formatted_starts"] != ''
                  #tz = TZInfo::Timezone.get(city.timezone)
                  #utc_start_day  = tz.local_to_utc(local_start_day)
                  #old_offer.update_attributes(:name => offer["offertitle"][0,139], :description => offer["offertitle"][0,139],
                  #    :active => 1, :day_in_week => 127,:local_start_day => local_start_day, :utc_start_day => utc_start_day, :local_end_day => local_end_day, :utc_end_day => utc_end_day,:link => "http://www.vouchercodes.co.uk" + offer["link"].tr("\\", ''), :image => "http://static.vouchercodes.co.uk/images/merchants/logo/" + merchant[1]["merchantid"] + "png", :third_party_code => "vouchercodes")
                  #end
              end
            else
              new_merchant = Merchant.new_merchant_vouchercodes(merchant[1]["merchantname"], merchant[1]["phone"], merchant[1]["geo_lat"], merchant[1]["geo_long"], city, merchant[1]["postcode"], "vouchercodes", merchant[1]["address_1"], merchant[1]["merchantid"].to_i, merchant[1]["placeid"].to_i )
              offers[merchant[1]["merchantid"]].each do |offer|
                if new_merchant.merchant_type == nil
                  merchant_type = Merchant.get_merchant_type_id(offer["subtype"],city,"groupon")
                  new_merchant.update_attributes(:merchant_type_id => merchant_type[0],:third_party_merchant_type_id => merchant_type[1])
                  if offer["offertitle"].present?
                    new_offer = Offer.new
                    new_offer.new_offer_vouchercodes(offer["offertitle"][0,139],offer["offertitle"][0,139],offer["formatted_starts"] ,offer["formatted_expires"], city.timezone,"http://www.vouchercodes.co.uk" + offer["link"].tr("\\", ''),"http://static.vouchercodes.co.uk/images/merchants/logo/" + merchant[1]["merchantid"] + "png","vouchercodes", offer["merchantid"].to_i, offer["offerid"].to_i, new_merchant)
                  end
                end
              end
            end

          end
        end
      end
      clusters = results["clusters"]
      if zoom <= 13
        zoom = zoom + 1
        clusters.each do |cluster|
          lat = cluster[1]["location"]["lat"]
          lng = cluster[1]["location"]["lng"]
          height = (0.2*(0.001676*(2**(20-zoom))))
          width = (0.2*(0.003219*(2**(20-zoom))))
          x = (lng/width).floor
          y = (lat/height).floor
          cell_ne_lat = (y + 1) * height
          cell_ne_lng =  (x + 1) * width
          cell_sw_lat =  y * height
          cell_sw_lng = x * width
          ne_lat = cell_ne_lat + height
          ne_lng = cell_ne_lng + width
          sw_lat = cell_sw_lat - height
          sw_lng = cell_sw_lng - width
          puts zoom
          begin
            status = Timeout::timeout(100) {
              curl = CURL.new
              page = curl.get('http://www.vouchercodes.co.uk/local/?xhr=1&z=' + zoom.to_s + '&ne-lat=' + ne_lat.to_s + '&ne-lng=' + ne_lng.to_s + '&sw-lat=' + sw_lat.to_s + '&sw-lng=' + sw_lng.to_s)

              Merchant.api_vouchercodes(page, zoom)
            }
          rescue  Exception => exc
            puts exc.message
          end
        end
      end
          
    end
  end
  def self.new_merchant_factual(name, description, options = {})
    open_days = "Mon-Sun"
		new_merchant = Merchant.new
    new_merchant.name = name if name
    new_merchant.phone = options[:phone].strip.scan(/\d/).map { |n| n.to_i }.join() if options[:phone].present?
		new_merchant.mobile_number =  options[:phone] ?  options[:phone].strip.scan(/\d/).map { |n| n.to_i }.join() :
				'mobile phone' if options[:phone].present?
    new_merchant.mobile_description = description[0,499] if description
    new_merchant.desktop_description = description
    new_merchant.third_party_code = 'factual'
    new_merchant.factual_id = options[:factual_id]
		new_merchant.merchant_open_hours("Mon-Sun","00:00-23:59")
    #only get the first category

    merchant_type_name = Merchant.merchant_type(options[:category].split(/[\&\>]/)[0].strip) if options[:category]
    if merchant_type_name.present?
      like_merchant_type_name = "%" + merchant_type_name.strip.upcase + "%"
      merchant_type = MerchantType.where("UPPER(merchant_type) LIKE ? and sub_type = ?", like_merchant_type_name, " ").first
      if merchant_type
        new_merchant.merchant_type_id = merchant_type.id

      else
        third_party_merchant_type =  ThirdPartyMerchantType.where("UPPER(name) LIKE ?", like_merchant_type_name).first
        if third_party_merchant_type
          new_merchant.third_party_merchant_type_id = third_party_merchant_type.id
        else
          new_third_party_merchant_type = ThirdPartyMerchantType.new
          new_third_party_merchant_type.name = merchant_type_name.strip
          new_third_party_merchant_type.save
          new_merchant.third_party_merchant_type_id = new_third_party_merchant_type.id
        end
        new_merchant.merchant_type_id = MerchantType.find(:all, :conditions => {:merchant_type => "Default"}).first.id
      end
    else
      new_merchant.merchant_type_id = MerchantType.find(:all, :conditions => {:merchant_type => "Default"}).first.id
    end
    new_merchant.primary_address = options[:address]
    new_merchant.lon = options[:lon]
    new_merchant.lat = options[:lat]
    new_merchant.city_name = options[:city].name if options[:city]
    new_merchant.postal_code = options[:postal_code]
    new_merchant.country = options[:country]
    new_merchant.state_province_region = options[:region] if  options[:region]
    new_merchant.set_repeat_days(false, false, false, false, false, false, false)
    city = options[:city]
    if city
      new_merchant.city_id = city.id
      if city.country and city.country.language and new_merchant.third_party_merchant_type_id
        third_party_merchant_type = ThirdPartyMerchantType.find(new_merchant.third_party_merchant_type_id)
        third_party_merchant_type.update_attribute("language",city.country.language)
      end
    end
    time_zone = nil
    if new_merchant.city and new_merchant.city.timezone
      time_zone == new_merchant.city.timezone
    else

      begin
        timezone = Geonames::WebService.timezone new_merchant.lat,new_merchant.lon
        if timezone.timezone_id
          time_zone = timezone.timezone_id
        end
      rescue
      end
    end
    new_merchant.time_zone = time_zone
    if new_merchant.save
    else
      puts new_merchant.errors.full_messages
    end

    chars = 'abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ23456789'
    password = ''
    begin
      6.times { |i| password << chars[rand(chars.length)] }
      user = User.new( :mobile_number => (options[:phone] != nil and options[:phone].strip != '') ? options[:phone].strip.scan(/\d/).map { |n| n.to_i }.join() : 'mobile phone', :user_admin => true, :role_id => Role.find_by_name("Merchant admin").id,
                       :email => "usman+#{Time.now.to_f}@devsinc.com", :password => password,
                       :username => "usman+#{(Time.now.to_f)}@devsinc.com", :first_name => "Update me", :last_name => "Update me")
      user.skip_confirmation!
      user.merchant_id = new_merchant.id
      user.save
      if user.save  == false
        user.mobile_number = Time.current.to_f
        user.save
      end
    rescue
    end
    return new_merchant

  end
  def set_repeat_days(mon, tue, wed, thu, fri, sat, sun)
    self.mon = mon
    self.tue = tue
    self.wed = wed
    self.thu = thu
    self.fri = fri
    self.sat = sat
    self.sun = sun
  end
  def self.new_merchant_8coupon(name,phone, description,type_id, address, lon, lat, city, state, postal_code)
    new_merchant = Merchant.new
    new_merchant.name = name[0,30].strip if name
    new_merchant.phone = phone.strip.scan(/\d/).map { |n| n.to_i }.join() if phone
    new_merchant.mobile_number =  phone ?  phone.strip.scan(/\d/).map { |n| n.to_i }.join() : 'mobile phone'
    new_merchant.mobile_description = description[0,499] if description
    new_merchant.desktop_description = description
    new_merchant.third_party_code = '8coupons'
    merchant_type_id = type_id if type_id
    if merchant_type_id != ''
      third_party_merchant_type =  ThirdPartyMerchantType.where("merchant_type_id = ?", merchant_type_id).first
      if third_party_merchant_type
        new_merchant.third_party_merchant_type_id = third_party_merchant_type.id
        merchant_type = MerchantType.find(:first, :conditions => ["merchant_type = ? and sub_type = ?", Merchant.merchant_type(third_party_merchant_type.name), ' '])
        new_merchant.merchant_type_id = merchant_type.id
      end
    end
    if new_merchant.merchant_type_id == nil
      new_merchant.merchant_type_id = MerchantType.find(:all, :conditions => {:merchant_type => "Default"}).first.id
    end
    new_merchant.primary_address = address
    new_merchant.lon = lon
    new_merchant.lat = lat
    new_merchant.city_name = city.name if city
    new_merchant.state_province_region = state if  state
    new_merchant.postal_code =  postal_code
    new_merchant.set_repeat_days(false, false, false, false, false, false, false)
    if city
      new_merchant.city_id = city.id
      if city.country and city.country.language and new_merchant.third_party_merchant_type_id
        third_party_merchant_type = ThirdPartyMerchantType.find(new_merchant.third_party_merchant_type_id)
        third_party_merchant_type.update_attribute("language", city.country.language)
      end
    end

    time_zone = nil
    if new_merchant.city and new_merchant.city.timezone
      time_zone == new_merchant.city.timezone
    else

      begin
        timezone = Geonames::WebService.timezone new_merchant.lat,new_merchant.lon
        if timezone.timezone_id
          time_zone = timezone.timezone_id
        end
      rescue
        puts 'time zone'
      end
    end
    new_merchant.time_zone = time_zone
    if new_merchant.save
    else
      puts new_merchant.errors.full_messages
    end

    chars = 'abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ23456789'
    password = ''
    begin
      6.times { |i| password << chars[rand(chars.length)] }
      user = User.new( :mobile_number => (phone != nil and phone.strip != '') ? phone.strip.scan(/\d/).map { |n| n.to_i }.join() : 'mobile phone', :user_admin => true, :role_id => Role.find_by_name("Merchant admin").id,
        :email => "usman+#{Time.now.to_f}@devsinc.com", :password => password,
        :username => "usman+#{(Time.now.to_f)}@devsinc.com", :first_name => "Update me", :last_name => "Update me")
      user.skip_confirmation!
      user.merchant_id = new_merchant.id
      user.save
      if user.save  == false
        user.mobile_number = Time.current.to_f
        user.save
      end
    rescue
      puts 'user error'
    end
    return new_merchant
	end

	def self.new_merchant_drinkowl(options = {})    # name phone full_address lon lat city state postal_code country
		new_merchant = Merchant.new
		new_merchant.name = options[:name]
		new_merchant.phone = options[:phone].strip.scan(/\d/).map { |n| n.to_i }.join() if options[:phone].present?
		new_merchant.mobile_number =  options[:phone] ?  options[:phone].strip.scan(/\d/).map { |n| n.to_i }.join() :
				'mobile phone' if options[:phone].present?
		new_merchant.primary_address = options[:full_address]
		new_merchant.lon = options[:lon]
		new_merchant.lat = options[:lat]
		new_merchant.city_name = options[:city]
		new_merchant.state_province_region = options[:state]
		new_merchant.postal_code =  options[:postal_code]
		new_merchant.country = options[:country]
		new_merchant.city_id = options[:city].id if options[:city]
		new_merchant.time_zone = options[:city].timezone if options[:city]
		new_merchant.third_party_code = "drinkowl"
		merchant_type = MerchantType.where("UPPER(merchant_type) LIKE ?", "DRINK").first
		new_merchant.merchant_type_id = merchant_type.id if merchant_type

		if new_merchant.save
		else
			puts new_merchant.errors.full_messages
		end
		chars = 'abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ23456789'
		password = ''
		6.times { |i| password << chars[rand(chars.length)] }
		begin
			user = User.new( :mobile_number => (phone != nil and phone.text.strip != '') ? phone.text.strip.scan(/\d/).map { |n| n.to_i }.join() : 'mobile phone', :user_admin => true, :role_id => Role.find_by_name("Merchant admin").id,
											 :email => "usman+#{Time.now.to_f}@devsinc.com", :password => password,
											 :username => "usman+#{(Time.now.to_f)}@devsinc.com", :first_name => "Update me", :last_name => "Update me")
			user.skip_confirmation!
			user.merchant_id = new_merchant.id
			user.save
      if user.save  == false
        user.mobile_number = Time.current.to_f
        user.save
      end
    rescue
		end
		return new_merchant
	end

  def self.new_merchant_groupon(name, phone, description, type, location, city, state_province_region, postal_code,country, third_party_code)
    new_merchant = Merchant.new
    new_merchant.name = name.text[0,30] if name
    new_merchant.phone = phone.text.strip.scan(/\d/).map { |n| n.to_i }.join() if phone
    new_merchant.mobile_number = phone ? phone.text.strip.scan(/\d/).map { |n| n.to_i }.join() : 'mobile phone'
    new_merchant.mobile_description = strip_tags(description.text)[0,499] if description
    new_merchant.desktop_description = description.text
    new_merchant.third_party_code = third_party_code if third_party_code != ''
    merchant_type_name = Merchant.merchant_type(type.text) if type
    if merchant_type_name != ''

      like_merchant_type_name = "%" + merchant_type_name.strip.upcase + "%"
      merchant_type = MerchantType.where("UPPER(merchant_type) LIKE ?", like_merchant_type_name).first
      if merchant_type
        new_merchant.merchant_type_id = merchant_type.id
      else
        third_party_merchant_type =  ThirdPartyMerchantType.where("UPPER(name) LIKE ?", like_merchant_type_name).first
        if third_party_merchant_type
          new_merchant.third_party_merchant_type_id = third_party_merchant_type.id
        else
          new_third_party_merchant_type = ThirdPartyMerchantType.new
          new_third_party_merchant_type.name = merchant_type_name.strip
          new_third_party_merchant_type.save
          new_merchant.third_party_merchant_type_id = new_third_party_merchant_type.id

        end
        new_merchant.merchant_type_id = MerchantType.find(:all, :conditions => {:merchant_type => "Default"}).first.id
      end
    end

    if location.success
      new_merchant.primary_address = location.full_address
      new_merchant.lon = location.lng
      new_merchant.lat = location.lat
    end
    new_merchant.city_name = city.name if city
    new_merchant.state_province_region = state_province_region.text if state_province_region
    new_merchant.postal_code =  postal_code ? postal_code.text : 'postal code'
    new_merchant.country = country.text if country

    if city
      new_merchant.city_id = city.id
      if city.country and city.country.language and new_merchant.third_party_merchant_type_id
        third_party_merchant_type = ThirdPartyMerchantType.find(new_merchant.third_party_merchant_type_id)
        third_party_merchant_type.update_attribute("language", city.country.language)
      end
    end

    time_zone = nil
    if new_merchant.city and new_merchant.city.timezone
      time_zone == new_merchant.city.timezone
    else

      begin
        timezone = Geonames::WebService.timezone new_merchant.lat,new_merchant.lon
        if timezone.timezone_id
          time_zone = timezone.timezone_id
        end
      rescue
      end
    end
    new_merchant.time_zone = time_zone
    if new_merchant.save
      if new_third_party_merchant_type
        UserMailer.confirm_merchant_type("New merchant type: " + merchant_type_name.strip + ' from groupon, link :' + new_merchant.to_uri).deliver
      end
    else
      puts new_merchant.errors.full_messages
    end
    chars = 'abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ23456789'
    password = ''
    6.times { |i| password << chars[rand(chars.length)] }
    begin
      user = User.new( :mobile_number => (phone != nil and phone.text.strip != '') ? phone.text.strip.scan(/\d/).map { |n| n.to_i }.join() : 'mobile phone', :user_admin => true, :role_id => Role.find_by_name("Merchant admin").id,
        :email => "usman+#{Time.now.to_f}@devsinc.com", :password => password,
        :username => "usman+#{(Time.now.to_f)}@devsinc.com", :first_name => "Update me", :last_name => "Update me")
      user.skip_confirmation!
      user.merchant_id = new_merchant.id
      if user.save  == false
        user.mobile_number = Time.current.to_f
        user.save
      end

    rescue
    end
    return new_merchant

  end
  def self.new_merchant_vouchercodes(name, phone, lat,lon, city, postal_code, third_party_code, address,vouchercodes_merchant_id, vouchercodes_place_id)
    new_merchant = Merchant.new
    new_merchant.name = name[0,30].strip if name
    new_merchant.phone = phone.strip.scan(/\d/).map { |n| n.to_i }.join() if phone
    new_merchant.mobile_number = phone ? phone.strip.scan(/\d/).map { |n| n.to_i }.join() : 'mobile phone'
    new_merchant.third_party_code = third_party_code if third_party_code != ''
    new_merchant.primary_address = address
    new_merchant.lon = lon
    new_merchant.lat = lat
    new_merchant.vouchercodes_place_id = vouchercodes_place_id if vouchercodes_place_id
    new_merchant.vouchercodes_merchant_id = vouchercodes_merchant_id if vouchercodes_merchant_id
    new_merchant.city_name = city.name if city
    new_merchant.postal_code =  postal_code ? postal_code : 'postal code'
    new_merchant.country = city.country_iso_code_two_letters if city
    new_merchant.city_id = city.id
    new_merchant.set_repeat_days(false, false, false, false, false, false, false)
    new_merchant.merchant_type_id=  MerchantType.find(:all, :conditions => {:merchant_type => "Default"}).first.id
    time_zone = nil
    if new_merchant.city and new_merchant.city.timezone
      time_zone == new_merchant.city.timezone
    else
      begin
        timezone = Geonames::WebService.timezone new_merchant.lat,new_merchant.lon
        if timezone.timezone_id
          time_zone = timezone.timezone_id
        end
      rescue
      end
    end
    new_merchant.time_zone = time_zone
    if new_merchant.save
    else
      puts new_merchant.errors.full_messages
    end
    chars = 'abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ23456789'
    password = ''
    6.times { |i| password << chars[rand(chars.length)] }
    begin
      user = User.new( :mobile_number => (phone != nil and phone.strip != '') ? phone.strip.scan(/\d/).map { |n| n.to_i }.join() : 'mobile phone', :user_admin => true, :role_id => Role.find_by_name("Merchant admin").id,
        :email => "usman+#{Time.now.to_f}@devsinc.com", :password => password,
        :username => "usman+#{(Time.now.to_f)}@devsinc.com", :first_name => "Update me", :last_name => "Update me")
      user.skip_confirmation!
      user.merchant_id = new_merchant.id
      user.save
      if user.save  == false
        user.mobile_number = Time.current.to_f
        user.save
      end
    rescue
      puts user.errors.messages
    end
    return new_merchant
  end
  def self.new_merchant_gotime(name, phone, lat,lon, city, postal_code, third_party_code, address, featured_review, image, open_hours, state)
    new_merchant = Merchant.new
    new_merchant.name = name[0,30].strip if name
    new_merchant.phone = phone.strip.scan(/\d/).map { |n| n.to_i }.join() if phone
    new_merchant.mobile_number = phone ? phone.strip.scan(/\d/).map { |n| n.to_i }.join() : 'mobile phone'
    new_merchant.third_party_code = third_party_code if third_party_code != ''
    new_merchant.primary_address = address
    new_merchant.featured_review = featured_review
    new_merchant.state_province_region = state
    new_merchant.lon = lon
    new_merchant.lat = lat
    merchant_photo = MerchantPhoto.new
    merchant_photo.url_medium = image
    merchant_photo.save
    new_merchant.merchant_photos << merchant_photo
    open_hours.each do |time|
      if time.text.include? 'Every Day'
        new_merchant.merchant_open_hours(time.text.split(/[[:space:]]/)[0] + " " + time.text.split(/[[:space:]]/)[1] , time.text.split(/[[:space:]]/)[2])
      else
        new_merchant.merchant_open_hours(time.text.split(/[[:space:]]/)[0], time.text.split(/[[:space:]]/)[1])
      end
    end   
    new_merchant.city_name = city.name if city
    new_merchant.postal_code =  postal_code ? postal_code : 'postal code'
    new_merchant.country = city.country_iso_code_two_letters if city
    new_merchant.city_id = city.id
    time_zone = nil
    if new_merchant.city and new_merchant.city.timezone
      time_zone == new_merchant.city.timezone
    else
      begin
        timezone = Geonames::WebService.timezone new_merchant.lat,new_merchant.lon
        if timezone.timezone_id
          time_zone = timezone.timezone_id
        end
      rescue
      end
    end
    new_merchant.time_zone = time_zone
    if new_merchant.save
    else
      puts new_merchant.errors.full_messages
    end

    chars = 'abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ23456789'
    password = ''
    6.times { |i| password << chars[rand(chars.length)] }
    begin
      user = User.new( :mobile_number => (phone != nil and phone.strip != '') ? phone.strip.scan(/\d/).map { |n| n.to_i }.join() : 'mobile phone', :user_admin => true, :role_id => Role.find_by_name("Merchant admin").id,
        :email => "usman+#{Time.now.to_f}@devsinc.com", :password => password,
        :username => "usman+#{(Time.now.to_f)}@devsinc.com", :first_name => "Update me", :last_name => "Update me")
      user.skip_confirmation!
      user.merchant_id = new_merchant.id
      user.save
      if user.save  == false
        user.mobile_number = Time.current.to_f
        user.save
      end
    rescue
      puts '..............................................................................'
      puts user.errors.full_messages
      puts '..............................................................................................'
    end
    return new_merchant
  end
  def self.gotime_list(doc_list, city)
    doc_list.xpath('//li[@class="place_item list_item"]').each do |merchant|
      old_merchant = Merchant.find(:first, :conditions => ["city_id = ? and UPPER(name) = ?", city.id, merchant.css('h3').at('a')['title'][0,30].strip.upcase ])
      if old_merchant == nil
        doc_detail = Nokogiri::HTML(open('http://www.gotime.com' +  merchant.css('h3').at('a')['href']))
        if doc_detail.xpath('//img[@class="profileImage"]').first and doc_detail.xpath('//meta[@property="og:latitude"]').first and doc_detail.xpath('//meta[@property="og:street-address"]').first and  doc_detail.xpath('//meta[@property="og:longitude"]').first  and doc_detail.xpath('//meta[@property="og:type"]').first    and doc_detail.xpath('//meta[@property="og:url"]').first  and doc_detail.xpath('//meta[@property="og:postal-code"]').first  and  doc_detail.xpath('//meta[@property="og:region"]').first
        new_merchant = Merchant.new_merchant_gotime(merchant.css('h3').at('a')['title'].strip, doc_detail.xpath('//div[@class="miniSection basicInfo"]').xpath('//div[@class="content"]')[1].css('div').text, doc_detail.xpath('//meta[@property="og:latitude"]').first["content"], doc_detail.xpath('//meta[@property="og:longitude"]').first["content"], city,doc_detail.xpath('//meta[@property="og:postal-code"]').first["content"] , "gotime", doc_detail.xpath('//meta[@property="og:street-address"]').first["content"], doc_detail.xpath('//meta[@property="og:url"]').first["content"] ,doc_detail.xpath('//img[@class="profileImage"]').first["src"] , doc_detail.xpath('//div[@class="content open_hours"]/div'), doc_detail.xpath('//meta[@property="og:region"]').first["content"])
        if new_merchant.merchant_type == nil
          merchant_type_name = Merchant.merchant_type(doc_detail.xpath('//meta[@property="og:type"]').first["content"] ) if doc_detail.xpath('//meta[@property="og:type"]').first["content"] != nil
          if merchant_type_name != ''
            like_merchant_type_name = "%" + merchant_type_name.strip.upcase + "%"
            merchant_type = MerchantType.where("UPPER(merchant_type) LIKE ? and sub_type = ?", like_merchant_type_name, " ").first
            if merchant_type
              new_merchant.update_attribute("merchant_type_id",merchant_type.id)
            else
              third_party_merchant_type =  ThirdPartyMerchantType.where("UPPER(name) LIKE ?", like_merchant_type_name).first
              if third_party_merchant_type
                new_merchant.update_attribute("third_party_merchant_type_id", third_party_merchant_type.id)
              else
                new_third_party_merchant_type = ThirdPartyMerchantType.new
                new_third_party_merchant_type.name = merchant_type_name.strip
                new_third_party_merchant_type.save
                new_merchant.update_attribute("third_party_merchant_type_id", new_third_party_merchant_type.id)
              end
              new_merchant.update_attribute("merchant_type_id", MerchantType.find(:all, :conditions => {:merchant_type => "Default"}).first.id)
            end
          end
          if city.country and city.country.language and new_merchant.third_party_merchant_type_id
            third_party_merchant_type = ThirdPartyMerchantType.find(new_merchant.third_party_merchant_type_id)
            third_party_merchant_type.update_attribute("language", city.country.language)
          end
          if new_third_party_merchant_type and new_merchant
            UserMailer.confirm_merchant_type("New merchant type: " + merchant_type_name.strip + ' from groupon, link :' +   new_merchant.to_uri).deliver
          end
        end
        end
        if new_merchant
          doc_detail.css('td[class=happy_hour]').each do |offer|
            if offer and  offer.css('p').text.present?
              offer.css('span[class=time]').each do |time|
                if time.text.present?
                new_offer = Offer.new
                new_offer.new_offer_gotime(offer.css('p').text[0,139],offer.css('p').text[0,139], doc_detail.xpath('//meta[@property="og:url"]').first["content"] , time.text, city.timezone,"gotime", new_merchant)
                end
              end
            end
          end
        end
      end
    end
    doc_list.xpath('//a[@class="listNavPage"]').each do |next_page|
      if next_page.content.include? 'Next'
        begin
          status = Timeout::timeout(500) {
            doc_next_list = Nokogiri::HTML(open('http://www.gotime.com' + next_page["href"]))
            Merchant.gotime_list(doc_next_list, city)
          }
        rescue  Exception => exc
          puts 'gotime'
          puts exc.message
        end
      end
    end
  end
  def self.gotime_task(city_list)
    city_list.css('a').each do |link|
      city = City.find(:first, :conditions => ["country_iso_code_two_letters = ? and upper(name) = ? ", 'US', link["title"].split("/")[0].strip.upcase])
      city = Merchant.create_new_city(link["title"].split("/")[0], nil, nil,"US") if city == nil and link["title"].split("/")[0].present?
      if city
        begin
          status = Timeout::timeout(500) {
            link_doc_list = 'http://www.gotime.com' + link['href'] + "/find/places/All-Places-in-" + link['title'].gsub(" ", "+")
            doc_list = Nokogiri::HTML(open(link_doc_list))
            Merchant.gotime_list(doc_list, city)
            doc_list.xpath('//a[@class="listNavPage"]').each do |next_page|
              if next_page.content.include? 'Next'
                puts '....................................'
                puts 'http://www.gotime.com' + next_page["href"]
                doc_next_list = Nokogiri::HTML(open('http://www.gotime.com' + next_page["href"]))
                Merchant.gotime_list(doc_next_list, city)
              end
            end
          }
        rescue  Exception => exc
          puts exc.message
          puts 'gotime'
        end
      end
    end
  end
  def self.gotime_task_index(number)
    LogMessage.integration_log("begin integrate","gotime task #{number}")

    doc = Nokogiri::HTML(open('http://www.gotime.com'))
    index = 1
    doc.xpath('//ul[@class="cityList"]').each do |city_list|
      if(index == number )
        Merchant.gotime_task(city_list)
      end
      index = index + 1
    end
    LogMessage.integration_log("end integrate","gotime task #{number}")

  end
  def self.vouchercodes_task_index(from,to)

    require 'curl'
    curl = CURL.new
    page = curl.get("http://www.vouchercodes.co.uk/local/")
    doc = Nokogiri::XML(page[286,page.length])
    doc.css('script').each do |script|
      if script.content.include? "var json = "
        content =     script.content[(script.content.index("var json = ").to_i + 11),(script.content.size - (script.content.size - script.content.index("};").to_i - 1 ) - script.content.index("var json = ").to_i - 11)]
        zoom = 6
        if content != nil
          results =JSON.parse(content)
          merchants =  results["places"]
          offers = results["offers"]
          merchants.each do |merchant|
            if merchant[1]["city"].present? and merchant[1]["phone"].present?
              city = City.find(:first ,:conditions => ["UPPER(name) = ? and country_iso_code_two_letters IN (?)", merchant[1]["city"].strip.upcase, ["IE","GB"]])
              city = Merchant.create_new_city(merchant[1]["city"] ,nil,merchant[1]["postcode"], nil) if city == nil and merchant[1]["city"].present? and merchant[1]["postcode"].present?
              if city
                old_merchant = Merchant.find(:first, :conditions => {:vouchercodes_merchant_id => merchant[1]["merchantid"].to_i, :vouchercodes_place_id => merchant[1]["placeid"].to_i, :phone => merchant[1]["phone"].strip.scan(/\d/).map { |n| n.to_i }.join()})
                if old_merchant
                  offers[merchant[1]["merchantid"]].each do |offer|
                    if offer["offertitle"].present?
                      old_offer = Offer.find(:first, :conditions => {:vouchercodes_offer_id => offer["offerid"].to_i, :vouchercodes_merchant_id => offer["merchantid"].to_i, :name => offer["offertitle"][0,139], :merchant_id => old_merchant.id })
                      if old_offer == nil
                        new_offer = Offer.new
                        new_offer.new_offer_vouchercodes(offer["offertitle"][0,139],offer["offertitle"][0,139],offer["formatted_starts"] ,offer["formatted_expires"], old_merchant.city.timezone,"http://www.vouchercodes.co.uk" + offer["link"].tr("\\", ''),"http://static.vouchercodes.co.uk/images/merchants/logo/" + merchant[1]["merchantid"] + "png","vouchercodes", offer["merchantid"].to_i, offer["offerid"].to_i, old_merchant)
                      end
                    end
                    #local_end_day = Time.parse(offer["formatted_expires"]) if offer["formatted_expires"] != ''
                    #tz = TZInfo::Timezone.get(city.timezone)
                    #utc_end_day  = tz.local_to_utc(local_end_day)
                    #if utc_end_day and offer["offertitle"] and offer["offertitle"].strip != '' and old_offer != nil  and offer["formatted_expires"] != '' and utc_end_day > Time.current and old_offer.link == nil
                    #local_start_day =  Time.parse(offer["formatted_starts"]) if offer["formatted_starts"] != ''
                    #tz = TZInfo::Timezone.get(city.timezone)
                    #utc_start_day  = tz.local_to_utc(local_start_day)
                    #old_offer.update_attributes(:name => offer["offertitle"][0,139], :description => offer["offertitle"][0,139],
                    #    :active => 1, :day_in_week => 127,:local_start_day => local_start_day, :utc_start_day => utc_start_day, :local_end_day => local_end_day, :utc_end_day => utc_end_day,:link => "http://www.vouchercodes.co.uk" + offer["link"].tr("\\", ''), :image => "http://static.vouchercodes.co.uk/images/merchants/logo/" + merchant[1]["merchantid"] + "png", :third_party_code => "vouchercodes")
                    #end
                  end
                else
                  new_merchant = Merchant.new_merchant_vouchercodes(merchant[1]["merchantname"], merchant[1]["phone"], merchant[1]["geo_lat"], merchant[1]["geo_long"], city, merchant[1]["postcode"], "vouchercodes", merchant[1]["address_1"], merchant[1]["merchantid"].to_i, merchant[1]["placeid"].to_i )
                  offers[merchant[1]["merchantid"]].each do |offer|
                    if new_merchant.merchant_type == nil
                      merchant_type = Merchant.get_merchant_type_id(offer["subtype"],city,"groupon")
                      new_merchant.update_attributes(:merchant_type_id => merchant_type[0],:third_party_merchant_type_id => merchant_type[1])
                      if offer["offertitle"].present?
                        new_offer = Offer.new
                        new_offer.new_offer_vouchercodes(offer["offertitle"][0,139],offer["offertitle"][0,139],offer["formatted_starts"] ,offer["formatted_expires"], city.timezone,"http://www.vouchercodes.co.uk" + offer["link"].tr("\\", ''),"http://static.vouchercodes.co.uk/images/merchants/logo/" + merchant[1]["merchantid"] + "png","vouchercodes", offer["merchantid"].to_i, offer["offerid"].to_i, new_merchant)
                      end
                    end
                  end
                end

              end
            end
          end
          clusters = results["clusters"].to_a
          if zoom <= 13
            zoom = zoom + 1
            to = clusters.size - from  if to == nil
            clusters[from,to].each do |cluster|
              lat = cluster[1]["location"]["lat"]
              lng = cluster[1]["location"]["lng"]
              height = (0.2*(0.001676*(2**(20-zoom))))
              width = (0.2*(0.003219*(2**(20-zoom))))
              x = (lng/width).floor
              y = (lat/height).floor
              cell_ne_lat = (y + 1) * height
              cell_ne_lng =  (x + 1) * width
              cell_sw_lat =  y * height
              cell_sw_lng = x * width
              ne_lat = cell_ne_lat + height
              ne_lng = cell_ne_lng + width
              sw_lat = cell_sw_lat - height
              sw_lng = cell_sw_lng - width
              puts zoom
              begin
                status = Timeout::timeout(100) {
                  curl = CURL.new
                  page = curl.get('http://www.vouchercodes.co.uk/local/?xhr=1&z=' + zoom.to_s + '&ne-lat=' + ne_lat.to_s + '&ne-lng=' + ne_lng.to_s + '&sw-lat=' + sw_lat.to_s + '&sw-lng=' + sw_lng.to_s)

                  Merchant.api_vouchercodes(page, zoom)
                }
              rescue  Exception => exc
                puts exc.message
              end
            end
          end

        end
      end
    end
    puts '...................:api_vouchercodes_data_second...................'
  end


  ## Get google places autocomplete
  # params: content = business name & address
  def self.get_google_places(content,city_id)
    places = Hash.new
    if content != ''  and city_id.present?
      city = City.find(city_id)
      if city
      test = URI.encode('https://maps.googleapis.com/maps/api/place/autocomplete/json?input=' + content + '&location=' + city.latitude.to_s + ',' +  city.longitude.to_s + '&rankby=distance&sensor=false&key=AIzaSyAzjO9pC_cyHdNYW74s_rc-5owbmrABsG8')
      puts 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=' + content + '&location=' + city.latitude.to_s + ',' +  city.longitude.to_s + '&rankby=distance&sensor=false&key=AIzaSyAzjO9pC_cyHdNYW74s_rc-5owbmrABsG8'
      docs = URI.parse(test).read
      docs = JSON.parse(docs)
      i = 0
      puts test
      if docs["status"] == 'OK'
        docs['predictions'].each do |prediction|
          places[i] = {}
          places[i]["description"]  = prediction["description"]
          results = URI.parse('https://maps.googleapis.com/maps/api/place/details/json?reference=' + prediction["reference"] + '&sensor=true&key=AIzaSyAzjO9pC_cyHdNYW74s_rc-5owbmrABsG8').read
          results = JSON.parse(results)
          if results["status"] == 'OK'
            results["result"]["address_components"].each do |type|
              if type["types"][0] == 'locality'
                places[i]["city_name"] = type["long_name"]
              end

              if type["types"][0] == 'country'
                country = Country.find(:first, :conditions => ["iso_code_two_letter = ?", type["long_name"]])
                if country
                  places[i]["country_id"] = country.iso_code_two_letter
                  if places[i]["city_name"]
                    city = City.find(:first, :conditions => ["UPPER(name) = ? and country_id = ?", places[i]["city_name"].strip.upcase , country.id])
                    if city
                      places[i]["city_name"] = city.name
                      places[i]["city_id"] = city.id
                    else
                      places[i]["city_name"]  = nil
                    end
                  end
                end
              end
              if type["types"][0] == 'postal_code'
                places[i]["postal_code"] = type["long_name"]
              end
              if type["types"][0] == 'administrative_area_level_1'
                places[i]["state_province_region"] = type["long_name"]
              end
            end

            places[i]["name"] = prediction["terms"][0]["value"]
            places[i]["merchant_type_name"] = Merchant.merchant_type(results["result"]["types"][0])
            if places[i]["merchant_type_name"]
              merchant_type = MerchantType.find(:first, :conditions => ["UPPER(merchant_type) LIKE ?", places[i]["merchant_type_name"].strip.upcase])
              if merchant_type
                places[i]["merchant_type_id"] = merchant_type.id
              else
                places[i]["merchant_type_id"] = 1

              end
            else
              places[i]["merchant_type_id"] = 1
            end
            places[i]["formatted_phone_number"] = results["result"]["formatted_phone_number"]
            places[i]["formatted_address"] = results["result"]["formatted_address"]
            places[i]["lat"] = results["result"]["geometry"]["location"]["lat"]
            places[i]["lng"] = results["result"]["geometry"]["location"]["lng"]
          end
          i = i + 1
        end
      end
    end
    end
    return places
  end
  def self.merchant_type(merchant_type)
    if  ['ARTS AND ENTERTAINMENT','ENTERTAINMENT', 'BOWLING', 'COMEDY CLUBS','CONCERTS', 'DANCE CLASSES', 'DINING & NIGHTLIFE', 'GAY', 'GOLF', 'GYM', 'KIDS','OUTDOOR ADVENTURES', 'SKIING','SKYDIVING','SPORTING EVENTS', 'THEATER'].include? merchant_type.strip.upcase
      return 'Fun'
    end
    if ['AUTOMOTIVE','EDUCATION', 'HOME SERVICES','PETS','PROFESSIONAL SERVICES','SERVICES','BABY','JEWISH','KOSHER'].include? merchant_type.strip.upcase
      return 'Services'
    end
    if ['BEAUTY & SPA', 'BEAUTY & SPAS', 'HEALTH & FITNESS', 'BOOT CAMP','FACIAL', 'FITNESS', 'FITNESS CLASSES','HAIR SALON','HEALTH & BEAUTY','MAKEUP','MANICURE & PEDICURE','MARTIAL ARTS','MASSAGE','PERSONAL TRAINING','PILATES','SPA', 'TANNING','YOGA'].include? merchant_type.strip.upcase
      return 'Look & Feel'
    end
    if ['FOOD & DRINK', 'RESTAURANTS', 'RESTAURANT', 'BAKERY','TREATS'].include? merchant_type.strip.upcase
      return 'Eat'
    end
    if ['GROUPON'].include? merchant_type.strip.upcase
      return 'Default'
    end
    if ['NIGHTLIFE', 'BAR', 'BARS & CLUBS', 'WINE TASTING'].include? merchant_type.strip.upcase
      return 'Drink'
    end
    if ['SHOPPING','FOOD & GROCERY',"MEN'S CLOTHING",'PETS','RETAIL & SERVICES',"WOMEN'S CLOTHING" ].include? merchant_type.strip.upcase
      return 'Shop'
    end
    if ['TRAVEL','CITY TOURS', 'MUSEUMS', 'TRAVEL'].include? merchant_type.strip.upcase
      return 'Tourism'
    end
    return merchant_type
  end
	def self.get_merchant_type(merchant_type)
		if  (%w(ARTS ENTERTAINMENT) & merchant_type.strip.upcase.split(" ")).size > 0
			return 'Fun'
		end
		if (%w(BEAUTY SPA SPAS HEALTH FITNESS SPORT) & merchant_type.strip.upcase.split(" ")).size > 0
			return 'Look & Feel'
		end
		if (%w(FOOD DRINK RESTAURANTS RESTAURANT BAKERY) & merchant_type.strip.upcase.split(" ")).size > 0
			return 'Eat'
		end
		if (%w(COMMUNITY) & merchant_type.strip.upcase.split(" ")).size > 0
			return 'Default'
		end
		if (%w(NIGHTLIFE BAR BARS) & merchant_type.strip.upcase.split(" ")).size > 0
			return 'Drink'
		end
		if (%w(SHOPPING) & merchant_type.strip.upcase.split(" ")).size > 0
			return 'Shop'
		end
		if (%w(TRAVEL) & merchant_type.strip.upcase.split(" ")).size > 0
			return 'Tourism'
		end
		if (%w(AUTOMOTIVE EDUCATION HOME PETS PROFESSIONAL SERVICES FINANCIAL) & merchant_type.strip.upcase.split(" "))
		.size > 0
			return 'Services'
		end
		return merchant_type
  end
  def self.get_merchant_type_id(name,city, third_party_code)
    merchant_type_name = Merchant.merchant_type(name) if name.present?
    if merchant_type_name.present?
      like_merchant_type_name = "%" + merchant_type_name.strip.upcase + "%"
      merchant_type = MerchantType.where("UPPER(merchant_type) LIKE ? and sub_type = ?", like_merchant_type_name, " ").first
      if merchant_type
        return [merchant_type.id, nil]
      else
        third_party_merchant_type =  ThirdPartyMerchantType.where("UPPER(name) LIKE ?", like_merchant_type_name).first
        if third_party_merchant_type == nil
          third_party_merchant_type = ThirdPartyMerchantType.new
          third_party_merchant_type.name = merchant_type_name.strip
          third_party_merchant_type.language = city.country.language
          third_party_merchant_type.save
          UserMailer.confirm_merchant_type("New merchant type: " + merchant_type_name.strip + ' from ' + third_party_code ).deliver
        end
        return [ MerchantType.find(:first, :conditions => {:merchant_type => "Default"}).id, third_party_merchant_type.id]
      end
    end
    return [nil, nil]
  end
  def phone_format()
    iso_code_two_letter = ''
    if self.phone != nil and self.city and self.city.country
      iso_code_two_letter = self.city.country.iso_code_two_letter
    else
      country = Geonames::WebService.country_code self.lat,self.lon
      if country[0] and country[0].country_code
        iso_code_two_letter = country[0].country_code
      end
    end
    merchant_phone = self.phone ? self.phone.strip.scan(/\d+/).map { |n| n.to_s }.join() : ''
    if iso_code_two_letter != ''
      country =  ISO3166::Country.find_country_by_alpha2(iso_code_two_letter)
      if country and  self.phone  and country.country_code == self.phone[0,country.country_code.length]
        begin
          result =( Phony.plausible?(merchant_phone) and Phony.formatted(Phony.normalize(merchant_phone.to_s),:format => :national).to_s != '' ? Phony.formatted(Phony.normalize(merchant_phone.to_s),:format => :national).to_s : merchant_phone.to_s)
          return  result
        rescue
          return merchant_phone
        end
      else
        if country

          if self.phone and country.national_prefix != self.phone[0,country.national_prefix.length]
            begin
              merchant_phone = country.country_code + merchant_phone
              result =( Phony.plausible?(merchant_phone) and Phony.formatted(Phony.normalize(merchant_phone.to_s),:format => :national).to_s != '' ? Phony.formatted(Phony.normalize(merchant_phone.to_s),:format => :national).to_s : merchant_phone.to_s)
              return  result
            rescue
              return merchant_phone
            end
          end
        end
      end
    end
    return merchant_phone
  end
end
# == Schema Information
#
# Table name: merchants
#  t.string   "merchant_id"
#  t.string   "name"
#  t.text     "description"
#  t.string   "primary_address"
#  t.integer  "user_rating"
#  t.string   "merchant_type"
#  t.string   "sub_type"
#  t.string   "latest_offer_id"
#  t.string   "third_party_code"
#  t.string   "third_party_id"
#  t.text     "address_description"
#  t.string   "special_instructions"
#  t.string   "another_address"
#  t.string   "city"
#  t.string   "state_province_region"
#  t.string   "postal_code"
#  t.string   "country"
#  t.string   "mobile_number"
#  t.datetime "created_at",            :null => false
#  t.datetime "updated_at",            :null => false
#  t.float    "lon"
#  t.float    "lat"
