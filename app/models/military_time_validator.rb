class MilitaryTimeValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if value.min > 24 || value.hour > 23
      record.errors[attribute] << (options[:message] || "is not a military time")
    end
  end
end
# == Schema Information
#
# Table name: military_time_validator