class Favorite < ActiveRecord::Base
  ## Relationship
  belongs_to :user
  belongs_to :offer
  belongs_to :merchant

  attr_accessor :twitter_on, :facebook_on

  scope :for_offers, where("offer_id IS NOT NULL")

  #TODO - this would be better done with polimorphic association, but for now
  validate :validate_correct_connection 

  after_create :post_to_twitter, :post_to_facebook

  def validate_correct_connection 
    if offer.nil? && merchant.nil?
      errors[:base] << I18n.t("activerecord.errors.models.favorite.no_connection")
    else
      if offer.present? && merchant.present?
        errors[:base] << I18n.t("activerecord.errors.models.favorite.to_many_connections")
      end
    end
  end

  def as_json(options)
    super(options.reverse_merge({only: [:id, :user_id, :offer_id, :merchant_id]}))
  end


  def post_to_twitter
    if offer_id and twitter_on and user and user.twitter_token
      tweet = I18n.t("message.i_like") + offer.merchant.name + I18n.t('message.check_it_out') + offer.merchant.merchant_id.to_s
      user.client_twitter("#{tweet}")
    end
  end

  def post_to_facebook
    if offer_id && facebook_on and user and user.facebook_token
      post = I18n.t("message.i_like") + offer.merchant.name + I18n.t('message.check_it_out') + offer.merchant.merchant_id.to_s
      user.client_facebook("#{post}")
    end
  end
end
# == Schema Information
#
# Table name: favorite
#  t.datetime "created_at", :null => false
#  t.datetime "updated_at", :null => false
#  t.string   "name"
