class IpGroupCity < ActiveRecord::Base

  include Geokit::Geocoders


  def self.locate_ip(ip)
    debugger
    return nil if ip.to_s.blank?
   # This is a temporary fix for now. Return the first city. We willh have to replace this with a geokit call anyway.
   # in that case, the above code will be irrelevant. 

    m = MultiGeocoder.geocode(ip);

    city = City.find( :all, :origin => [m.lat, m.lng], :within  => 70).first

    # And even if geocoding falis, default to NewYork
    city = City.find 1 if city.nil?

    return city 
  end

  def location
    "#{self.latitude},#{self.longitude}"
  end

end
