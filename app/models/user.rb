require "email_validator"
require 'twitter'
class User < ActiveRecord::Base
  include Geokit::Geocoders
  devise :omniauthable
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
    :recoverable, :rememberable, :trackable, :validatable,:confirmable,:authentication_keys => [:login]


  # Setup accessible (or protected) attributes for your model

  attr_accessible :username
  attr_accessible :email, :password, :password_confirmation, :remember_me
  validates :username, :length => { :maximum => 50,:message => I18n.t("users.validate.user_name_length") }, :presence => {:message => I18n.t("users.validate.user_name")},
    :uniqueness => {:message => I18n.t("users.validate.user_name_unique")}
  #validates :mobile_number, :presence => {:message => I18n.t("users.validate.mobile_number")},:uniqueness => { :message => I18n.t("users.validate.mobile_number_unique")}
  validates :mobile_number,:uniqueness => { :message => I18n.t("users.validate.mobile_number_unique")}, :allow_nil => true
  validates :first_name, :presence => {:message => I18n.t("users.validate.first_name")}
  validates :first_name,:length => { :maximum => 20,:message => I18n.t("users.validate.first_name_length") }
  validates :last_name, :length => { :maximum => 20,:message => I18n.t("users.validate.last_name_length") },:presence => {:message => I18n.t("users.validate.last_name")}
  validates :mobile_emei, :length => { :maximum => 30 }
  validates :mobile_authorized, :length => { :maximum => 10 }
  validates :mobile_handset_brand, :length => { :maximum => 20 }
  validates :password, :length => { :maximum => 20 }
  validates :authorization_level, :length => { :maximum => 20 }
  validates :email, :presence => false, :allow_blank => true, :allow_nil  => true
#  validates :email,:presence => {:message => I18n.t("users.validate.email")}, :email => {:message => I18n.t("users.validate.email_valid")}
  attr_accessor :login
  attr_accessible :login
  ## Relationship
  belongs_to :role
  belongs_to :merchant
  belongs_to :city
  has_many :favorites
  has_many :favorite_offers, :through => :favorites, :source => :offer
  has_many :favorite_merchants, :through => :favorites, :source => :merchant


  attr_accessible :first_name, :last_name, :username, :mobile_emei, :email, :password, :password_confirmation, :remember_me,
    :mobile_authorized, :mobile_handset_brand, :authorization_level,
    :mobile_number, :user_admin, :twitter_name, :google_name, :twitter_token, :twitter_secret,:facebook_name ,:facebook_token,
    :role_id, :merchant_id,:yahoo_name,:yahoo_token,:code ,:address_line1,:address_line2,:city_name,:country,:state_province_region,:postal_code,:gender,:city_id

  #  attr_writer :merchant_id
  def send_confirmation_instructions
    generate_confirmation_token! if self.confirmation_token.nil?
    Devise::Mailer.confirmation_instructions(self).deliver
  end


  def new_merchant_sms_response parameters, session, loc
    time = parameters[0]
    description = parameters[1]
    name = parameters[2]
    mobile_number = parameters[4].scan(/\d/).map { |n| n.to_i }.join()
    address = loc.full_address
    primary_address = loc.street_address
    postal_code = loc.zip
    state_province_region = loc.state
    country_code = loc.country_code
    location = GoogleGeocoder.reverse_geocode([loc.lat ,loc.lng])
    city_id = nil
    location.all.each { |e| 
      if e.hash[:city]
        
        city = City.where("name = ? and country_iso_code_two_letters = ?",e.hash[:city].strip, country_code.strip.upcase).first
        if city 
          session[:twilio_number] = city.country.twilio_number if city.country
          city_id = city.id
          break
        end
      end
    }
    if session[:twilio_number] == nil
      country = Geonames::WebService.country_code location.lat,location.lng
      if country[0] and country[0].country_code 
        if Country.where(:iso_code_two_letter => country[0].country_code).first
          session[:twilio_number] =  Country.where(:iso_code_two_letter => country[0].country_code).first.twilio_number
        end
      end
    end
    puts 'cccc'
    puts city_id
    if session[:twilio_number] 
      merchant = create_merchant name, address, primary_address, postal_code, state_province_region, country_code, nil, mobile_number, loc.lat ,loc.lng, time,city_id
      case merchant
      when "time error"
        return {:success => false, :description => I18n.t("merchant.smsrequest.badtime")}
      else
        self.update_attribute("merchant_id", merchant.id)

        name = "offer name"
        self.merchant.offers << create_offer(name, time, description, address,loc.lat, loc.lng,merchant.id, city_id)
        return {:success => true, :description => I18n.t("merchant.smsrequest.code.confirm", :code => self.code )}
      end
    else
      return {:success => true, :description => I18n.t("merchant.smsrequest.twilio_number")}
    end
  end

  def old_merchant_sms_response parameters,session
    self.update_attribute("mobile_authorized", 'true')  if self.mobile_authorized == 'stop' and parameters.casecmp(I18n.t("merchant.smsrequest.action.stop")) != 0
    merchant_types =  MerchantType.find(:all, :conditions =>{:sub_type => " "}).map(&:merchant_type).uniq
    merchant_types.delete("Default")
    lat = self.merchant.lat
    lon = self.merchant.lon
    location = GoogleGeocoder.reverse_geocode([lat.to_f ,lon.to_f])
    location.all.each { |e| 
      if e.hash[:city]
        
        city = City.where(:name => e.hash[:city].strip).first
        if city 
          session[:twilio_number] = city.country.twilio_number if city.country
          break
        end
      end
    }
    if session[:twilio_number] == nil
      country = Geonames::WebService.country_code location.lat,location.lng
      if country[0] and country[0].country_code 
        if Country.where(:iso_code_two_letter => country[0].country_code).first
          session[:twilio_number] =  Country.where(:iso_code_two_letter => country[0].country_code).first.twilio_number
        end
      end
    end
    if parameters.casecmp(I18n.t("merchant.smsrequest.action.help")) == 0
      return {:success => true, :description => I18n.t("merchant.smsrequest.help.basic")}
    elsif parameters.casecmp(I18n.t("merchant.smsrequest.action.help_system")) == 0
      return {:success => true, :description => I18n.t("merchant.smsrequest.help.system")}
    elsif parameters.casecmp(I18n.t("merchant.smsrequest.action.help_account")) == 0
      return {:success => true, :description => I18n.t("merchant.smsrequest.help.account")}
    elsif parameters.casecmp(I18n.t("merchant.smsrequest.action.help_offers1")) == 0
      return {:success => true, :description => I18n.t("merchant.smsrequest.help.offers1")}
    elsif parameters.casecmp(I18n.t("merchant.smsrequest.action.help_offers2")) == 0
      return {:success => true, :description => I18n.t("merchant.smsrequest.help.offers2")}
    elsif parameters.casecmp(I18n.t("merchant.smsrequest.action.help_stop")) == 0
      return {:success => true, :description => I18n.t("merchant.smsrequest.help.stop")}
    elsif parameters.casecmp(I18n.t("merchant.smsrequest.action.cancel")) == 0
      self.merchant.cancel
      return {:success => true, :description => I18n.t("merchant.smsrequest.offer.cancel")}
    elsif parameters.casecmp(I18n.t("merchant.smsrequest.action.cancel_all")) == 0
      self.merchant.cancel_all
      return {:success => true, :description => I18n.t("merchant.smsrequest.offer.cancel_all")}
    elsif parameters.split(DELIMITER).size == 2
      message = parameters.split(DELIMITER)
      name = self.merchant.name
      time = message.first.split("-")
      description = message.last
      address = self.merchant.address_description

      name = "offer name"

      start_time = time.first
      end_time = time.last
      if start_time.length == 2
        start_time = start_time + '00'
      end
      if end_time.length == 2
        end_time = end_time + '00'
      end
      if time.size != 2 || start_time.size != 4 || end_time.size != 4 || start_time.to_i >= end_time.to_i
        return {:success => false, :description => I18n.t("merchant.smsrequest.timeformat") }
      else

        if session[:twilio_number]
          time = message.first
          self.merchant.offers << create_offer(name, time, description, address,lat ,lon,self.merchant.id, self.merchant.city_id)
          return {:success => true, :description => I18n.t("merchant.smsrequest.newoffer")}
        else
          return {:success => false, :description =>  I18n.t("merchant.smsrequest.twilio_number")}
        end
      end
    elsif parameters.casecmp(I18n.t("merchant.smsrequest.action.stop")) == 0
      self.update_attribute("mobile_authorized", 'stop')
      return {:success => true, :description => I18n.t("merchant.smsrequest.stop")}
    elsif parameters.casecmp("stop all") == 0 #.match(/stop all/im)
      self.merchant.offers.each do |offer|
        offer.active = 0
        offer.save!
      end
      return {:success => true, :description => I18n.t("merchant.smsrequest.disableoffer")}
    elsif parameters.casecmp("start all") == 0 #.match(/start all/im)
      self.merchant.offers.each do |offer|
        offer.active = 1
        offer.save!
      end
      return {:success => true, :description => I18n.t("merchant.smsrequest.enableoffer")}
    elsif parameters.split(DELIMITER).size > 2
      return {:success => false, :description => I18n.t("merchant.smsrequest.merchantexist")}
    elsif ["Daily", "Weekly", "Weekday", "Weekend"].include?(parameters)
      unless self.merchant.offers.last.blank?
        self.merchant.offers.last.recurrence = parameters
        self.merchant.offers.last.save!
        return {:success => true, :description => I18n.t("merchant.smsrequest.offerrecurred", :parameters => parameters)}
      end
      return {:success => false, :description => I18n.t("merchant.smsrequest.offerabsent")}
    elsif parameters.split(" ")[0].casecmp(I18n.t("merchant.smsrequest.action.type")) == 0
      if parameters.split(" ")[1] and MerchantType.find(:first, :conditions => ["UPPER(merchant_type) LIKE ? and sub_type = ?", parameters.split(" ")[1].strip.upcase,' '])
        self.merchant.update_attribute("merchant_type_id", MerchantType.find(:first, :conditions => ["UPPER(merchant_type) LIKE ? and sub_type = ?", parameters.split(" ")[1].strip.upcase,' ']).id)
        return {:success => true, :description => I18n.t("merchant.smsrequest.changetype.confirm", :merchant_type => parameters.split(" ")[1])}
      else
        return {:success => false, :description => I18n.t("merchant.smsrequest.changetype.ambiguous", :merchant_type_list => merchant_types.join(',') )}
      end
    elsif parameters.split(" ")[0].casecmp(I18n.t("merchant.smsrequest.action.signup")) == 0
      if parameters.split(" ")[1] and self.code = parameters.split(" ")[1].strip
        return {:success => true, :description => I18n.t("merchant.smsrequest.signup.confirm", :merchant_id => self.merchant.merchant_id , :mobile_phone => self.mobile_number) + I18n.t("merchant.smsrequest.help.offers1")}
      else
        return {:success => false, :description => I18n.t("merchant.smsrequest.code.error")}
      end
    elsif (parameters.split(" ")[0]).casecmp(I18n.t("merchant.smsrequest.action.desc")) == 0
      self.merchant.update_attributes(:mobile_description => parameters[5, parameters.length - 5], :desktop_description => parameters[5, parameters.length - 5])
      return {:success => true, :description => I18n.t("merchant.smsrequest.desc.confirm", :merchant_id => self.merchant.merchant_id)}
    elsif parameters.split(" ")[0].casecmp(I18n.t("merchant.smsrequest.action.repeat")) == 0
      if parameters.split(" ")[1].casecmp(I18n.t("merchant.smsrequest.repeat.daily")) == 0 or parameters.split(" ")[1].casecmp(I18n.t("merchant.smsrequest.repeat.weekly")) == 0 or parameters.split(" ")[1].casecmp(I18n.t("merchant.smsrequest.repeat.weekday")) == 0 or parameters.split(" ")[1].casecmp(I18n.t("merchant.smsrequest.repeat.weekend"))  == 0
        self.merchant.repeat(parameters.split(" ")[1])
        return {:success => true, :description => I18n.t("merchant.smsrequest.changefrequency.confirm", :offer_id => self.merchant.offers.last.id, :repeat => parameters.split(" ")[1])}
      else
        return {:success => false, :description => I18n.t("merchant.smsrequest.changefrequency.ambiguous", :daily => I18n.t("merchant.smsrequest.repeat.daily"), :weekly => I18n.t("merchant.smsrequest.repeat.weekly"), :weekday => I18n.t("merchant.smsrequest.repeat.weekday"), :weekend => I18n.t("merchant.smsrequest.repeat.weekend")) }
      end
    elsif parameters.split(" ")[0].casecmp(I18n.t("merchant.smsrequest.action.hours")) == 0
      time = parameters.split(" ")[1]
      begin
        start_time = time.split('-').first.scan(/\d/).map { |n| n.to_i }.join()
        end_time = time.split('-').last.scan(/\d/).map { |n| n.to_i }.join()
        puts start_time
        if start_time.length == 2
          start_time = start_time + '00'
        end
        if end_time.length == 2
          end_time = end_time + '00'
        end
        start_time = Time.strptime(start_time, "%H%M").to_time
        end_time = Time.strptime(end_time, "%H%M").to_time
        start_time = start_time.strftime("%d-%m-%Y %H:%M:%S")
        end_time = end_time.strftime("%d-%m-%Y %H:%M:%S")
        self.merchant.set_open_hours(start_time, end_time)
        return {:success => true, :description => I18n.t("merchant.smsrequest.changehours.confirm", :open_hours => time)}
      rescue
        return {:success => false, :description => I18n.t("merchant.smsrequest.changehours.ambiguous")}
      end
    else
      ErrorMessage.create!(:error => I18n.t("merchant.smsrequest.fourcommaformat"), :message => parameters )
      return {:success => false, :description => I18n.t("merchant.smsrequest.fourcommaformat")}
    end
  end

  def create_offer name, time, description, address,lat ,lon, id, city_id
    start_time = time.split('-').first
    end_time = time.split('-').last
    if start_time.length == 2
      start_time = start_time + '00'
    end
    if end_time.length == 2
      end_time = end_time + '00'
    end
    start_time = Time.strptime(start_time, "%H%M").to_time
    end_time = Time.strptime(end_time, "%H%M").to_time
    offer_duration = (end_time - start_time)/60/60
    start_time = start_time.strftime("%d-%m-%Y %H:%M:%S")
    end_time = end_time.strftime("%d-%m-%Y %H:%M:%S")
    self.offer_description( description[0,139].strip)
    return Offer.create! :name => name.strip, :description => description[0,139].strip, :merchant_id => id,
      :price => 44, :value => 3.3, :link => "http://dummy-link.com",
      :image => "http://dummy-link.com", :image_thumbnail => "http://dummy-link.com",
      :image_thumbnail_retina => "http://dummy-link.com",:day_in_week => 127,
      :start_time =>  start_time, :primary_address => address.strip, :active => 1,:end_time => end_time,
      :offer_duration => offer_duration, :user_id => self.id, :lat => lat, :lon => lon, :city_id => city_id
  end

  def create_merchant name, address, primary_address, postal_code, state_province_region, country_code, description, phone, lat, lon, time, city_id
    start_time = time.split('-').first
    end_time = time.split('-').last
    if start_time.length == 2
      start_time = start_time + '00'
    end
    if end_time.length == 2
      end_time = end_time + '00'
    end
    start_time = Time.strptime(start_time, "%H%M").to_time
    end_time = Time.strptime(end_time, "%H%M").to_time
    start_time = start_time.strftime("%d-%m-%Y %H:%M:%S")
    end_time = end_time.strftime("%d-%m-%Y %H:%M:%S")
    name = name.strip
    if name.length > 30
      name = name[0,29]
    end
    return Merchant.create! :merchant_type_id => MerchantType.find_by_merchant_type("Default").id,
      :name => name.strip, :address_description => address.strip, :lat => lat, :lon => lon,
      :mobile_description => description ? description.strip : nil, :desktop_description => description ? description.strip : nil, :mobile_number => phone.strip, :phone => phone.strip,
      :postal_code => postal_code,:primary_address => primary_address, :start_open_time => start_time, :end_open_time => end_time, :city_id => city_id, :country => country_code, :state_province_region => state_province_region
  end

  def self.find_for_twitter_oauth(access_token, signed_in_resource=nil)
    data = access_token.extra.raw_info
    if signed_in_resource == nil
      if user = User.where(:username => data.screen_name).first
        user
      else # Create a user with a stub password. 
        user = User.new(:username => data.screen_name, "password" => Devise.friendly_token[0,20],:email => data.screen_name + '@twitter.com', :twitter_name => data.screen_name, :twitter_token => access_token.credentials.token, :twitter_secret => access_token.credentials.secret, :mobile_number => 'mobile number ', :last_name => 'last name', :first_name => 'first name') 
        user.skip_confirmation!
        user.save
        user
      end
    else
      signed_in_resource.update_attributes(:twitter_name => data.screen_name, :twitter_token => access_token.credentials.token, :twitter_secret => access_token.credentials.secret)
      nil
    end
  end
  
  def self.find_for_facebook_oauth(access_token, signed_in_resource=nil)
    data = access_token.extra.raw_info
    if signed_in_resource == nil
      if user = User.where(:facebook_name => data.email).first
        user
      else # Create a user with a stub password. 
        user = User.new(:username => data.email, "password" => Devise.friendly_token[0,20],:email => data.email, :facebook_name => data.email, :facebook_token => access_token.credentials.token,:mobile_number => 'mobile number ', :last_name => 'last name', :first_name => 'first name') 
        user.skip_confirmation!
        user.save
        user
      end
    else
      signed_in_resource.update_attributes(:facebook_name => data.email, :facebook_token => access_token.credentials.token)
      nil
    end
  end
  def self.find_for_yahoo_oauth(access_token, signed_in_resource=nil)
    data = access_token
    if signed_in_resource == nil
      if user = User.where(:yahoo_name => data["uid"].to_s).first
        return user
      else # Create a user with a stub password. 
        user = User.new(:username => data["info"]["email"], "password" => Devise.friendly_token[0,20],:email => data["info"]["email"], :yahoo_name => data["uid"].to_s, :yahoo_token => access_token.credentials.token,:mobile_number => 'mobile number ', :last_name => 'last name', :first_name => 'first name') 
        user.skip_confirmation!
        user.save
        return user
      end
    else
      signed_in_resource.update_attributes(:yahoo_name => data["uid"].to_s, :yahoo_token => access_token.credentials.token)
      nil
    end
  end
  def self.find_for_google_oauth(access_token, signed_in_resource=nil)
    data = access_token.info
    if signed_in_resource == nil
      if user = User.where(:google_name =>  data["email"].to_s).first
        user
      else # Create a user with a stub password. 
        user = User.new(:username =>  data["email"], "password" => Devise.friendly_token[0,20],:email =>  data["email"], :google_name =>  data["email"].to_s,:mobile_number => 'mobile number ', :last_name => 'last name', :first_name => 'first name') 
        user.skip_confirmation!
        user.save
        user
      end
    else
      signed_in_resource.update_attribute("google_name", data["email"].to_s)
      nil
    end
  end

  def email_required?
    false 
  end  
  def client_twitter(message)
    if twitter_token
      @client ||= begin
   
        Twitter.configure do |config|
          config.consumer_key = ENV['TWITTER_CONSUMER_KEY']
          config.consumer_secret = ENV['TWITTER_CONSUMER_SECRET']
          config.oauth_token = twitter_token
          config.oauth_token_secret = twitter_secret 
        end
  
        Twitter.update(message)
      rescue
      end
    end
  end
  def offer_description(message)
    if twitter_token
      begin
      
        Twitter.configure do |config|
          config.consumer_key = ENV['TWITTER_CONSUMER_KEY']
          config.consumer_secret = ENV['TWITTER_CONSUMER_SECRET']
          config.oauth_token = twitter_token
          config.oauth_token_secret = twitter_secret 
        end
   
        Twitter.update(message)
      rescue
      end
    end
  end
  def merchant_twitter(merchant_twitter_name)
    begin
      Twitter.configure do |config|
        config.consumer_key = ENV['TWITTER_CONSUMER_KEY']
        config.consumer_secret = ENV['TWITTER_CONSUMER_SECRET']
        config.oauth_token = twitter_token
        config.oauth_token_secret = twitter_secret 
      end
      Twitter.follow(merchant_twitter_name)
   
    rescue
    end
  end
  def merchant_facebook(facebook_page_id)
    if facebook_token
      me = FbGraph::User.me(facebook_token)
      page = FbGraph::Page.new(facebook_page_id,:access_token => facebook_token)
      note = page.note!(
        :access_token => facebook_token,
        :subject => 'testing',
        :message => 'Hey, I\'m testing you!'
      )     

    end
  end
  def client_facebook(message)
    if facebook_token
      begin
        @client ||= begin
          me = FbGraph::User.me(facebook_token)
          me.feed!(
            :message => message
          )
        end
      rescue
      end
    end
  end

  def name
    return (self.first_name or "") + " " + (self.last_name or "")
  end

  def admin?
    return self.role.name == 'Super User' unless self.role.blank?
    return false
  end

  def yacto_employee?
    return self.role.name == 'Yacto employee' unless self.role.blank?
    return false
  end

  def merchant_admin?
    return self.role.name == 'Merchant admin' unless self.role.blank?
    return true
  end

	def merchant_employee?
    return self.role.name == 'Merchant employee' unless self.role.blank?
    return true
  end

  def consumer?
    return self.role.name == 'Consumer' unless self.role.blank?
    return true
  end

  def yacto_system?
    return self.role.name == 'Yacto system' unless self.role.blank?
    return true
	end

	def role?
		self.role.name
	end

  protected
  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    login = conditions.delete(:login)
    where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.strip.downcase }]).first
  end
 

  # Attempt to find a user by it's email. If a record is found, send new
  # password instructions to it. If not user is found, returns a new user
  # with an email not found error.
  def self.send_reset_password_instructions(attributes={})
    recoverable = find_recoverable_or_initialize_with_errors(reset_password_keys, attributes, :not_found)
    recoverable.send_reset_password_instructions if recoverable.persisted?
    recoverable
  end 

  def self.find_recoverable_or_initialize_with_errors(required_attributes, attributes, error=:invalid)
    (case_insensitive_keys || []).each { |k| attributes[k].try(:downcase!) }

    attributes = attributes.slice(*required_attributes)
    attributes.delete_if { |key, value| value.blank? }

    if attributes.size == required_attributes.size
      if attributes.has_key?(:login)
        login = attributes[:login]
        record = find_record(login)
      else  
        record = where(attributes).first
      end  
    end  

    unless record
      record = new

      required_attributes.each do |key|
        value = attributes[key]
        record.send("#{key}=", value)
        record.errors.add(key, value.present? ? error : :blank)
      end  
    end  
    record
  end
  def self.find_record(login)
    where(["username = :value OR email = :value", { :value => login }]).first
  end
  def self.user_time_zone(lat, lon)
    client = AskGeo.new(:account_id => '940006', :api_key => 'p4fiqq3gubaj080ig3ilpfj0ij')
    resp = client.lookup(lat.to_s + ","+ lon.to_s)
    return resp["timeZone"]
  end
end
# == Schema Information
#
# Table name: users
#  t.integer  "merchant_id"
#  t.string   "first_name"
#  t.string   "last_name"
#  t.string   "mobile_emei"
#  t.string   "mobile_number"
#  t.string   "username"
#  t.string   "mobile_authorized"
#  t.string   "mobile_handset_brand"
#  t.string   "password"
#  t.string   "authorization_level"
#  t.string   "fb_auth_data"
#  t.string   "twitter_auth_data"
#  t.string   "twitter_name"
#  t.boolean  "user_admin"
#  t.integer  "role_id"
#  t.datetime "created_at",                             :null => false
#  t.datetime "updated_at",                             :null => false
#  t.string   "email",                  :default => "", :null => false
#  t.string   "encrypted_password",     :default => "", :null => false
#  t.string   "reset_password_token"
#  t.datetime "reset_password_sent_at"
#  t.datetime "remember_created_at"
#  t.integer  "sign_in_count",          :default => 0
#  t.datetime "current_sign_in_at"
#  t.datetime "last_sign_in_at"
#  t.string   "current_sign_in_ip"
#  t.string   "last_sign_in_ip"
#  t.string   "confirmation_token"
#  t.datetime "confirmed_at"
#  t.datetime "confirmation_sent_at"
#  t.string   "unconfirmed_email"
#  t.string   "facebook_name"
#  t.string   "google_name"
#  t.string   "twitter_token"
#  t.string   "twitter_secret"
