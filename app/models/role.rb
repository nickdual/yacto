class Role < ActiveRecord::Base
  validates :name,:presence => true
  validates_uniqueness_of :name
  ## Relationship
  has_many :users
end
# == Schema Information
#
# Table name: roles
#  t.string   "name"
#  t.datetime "created_at", :null => false
#  t.datetime "updated_at", :null => false
