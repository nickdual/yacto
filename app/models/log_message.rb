class LogMessage < ActiveRecord::Base
	attr_accessible :process, :event, :third_party_provider, :description

	## attributes:
	## process => integration
	## event => begin integrate, pre-delete, post-delete
	## task => third party type e.g. Factual
	## description => Total Merchants: Total Offers: etc
	def self.integration_log(action,third_party_name)
		message = LogMessage.new
		message.update_attributes(:process => "integration", :third_party_provider => third_party_name,:event => action,
															:description => ["Total Merchants:", Merchant.count, "Total Offers:",
																							 Offer.count,
																							 "#{third_party_name} Merchants:", Merchant.where(:third_party_code =>	third_party_name).count,
																							 "#{third_party_name} Offers:", Offer.where(:source => third_party_name)
																	.count].join(" "))
		message.save
	end

	def self.delete_offers_log(action)
		message = LogMessage.new
		message.update_attributes(:process => "integration", :event => action,
															:description => ["Total Merchant:",Merchant.count,"Total Offers:",Offer.count].join(" "))
		message.save
	end

end
