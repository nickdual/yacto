class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user permission to do.
    # If you pass :manage it will apply to every action. Other common actions here are
    # :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. If you pass
    # :all it will apply to every resource. Otherwise pass a Ruby class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details: https://github.com/ryanb/cancan/wiki/Defining-Abilities

    if user.blank? ######################################### for temporary basis
      can :manage, :all
    elsif user.admin?
      can :manage, :all
    elsif user.yacto_employee?
      can :manage, Merchant
      can :manage, Offer
      can :manage, User
      can :manage, MerchantType
    elsif user.merchant_admin?
      can :manage, Merchant do |merchant|
        merchant.id == user.merchant.id
      end
      can :manage, User do |u|
        if u.merchant
        u.merchant.id == user.merchant.id
        end
      end
      can :manage, Offer do |offer|
       offer.user_id == user.id or ( user.admin?)
      end
#      can :manage, BillingInformation do |bi| ######### continue ##############
#        bi.merchant.id == user.merchant.id
#      end
    elsif user.merchant_employee?
      can :manage, User, :id => user.id
      can :manage, Offer do |offer|
        offer.merchant.id == user.merchant.id
      end
    elsif user.consumer?
      can :manage, User, :id => user.id
    elsif user.yacto_system?
      # reminder that API and SMS processor both will CRUD merchants / offer
      can :manage, :all
    else
      can :read, :all
    end
    
#    if user and user.admin?
#      can :manage, :all
#    else
#      can :read,:all
#    end
#    if user
#    can :manage, Offer do |offer|
#      offer.user_id == user.id or ( user.admin?)
#    end
#    end
#    can [:new,:create], Offer if user and user.merchant?
#    can :read, Offer
#
#    can :show, Merchant
#    if user
#     if user.merchant_id == nil
#    can [:new, :show,:create,:read],Merchant
#     else
#    can [:edit, :destroy, :update,:read], Merchant, :id => user.merchant_id
#     end
#    end

#
#    if user.blank? ######################################### for temporary basis
#      can :manage, :all 
#    elsif user.admin?
#      can :manage, :all
#    elsif user.merchant_id == nil
#      can [:new, :show,:create,:read], Merchant
#    else
#      can [:edit, :destroy, :update,:read], Merchant, :id => user.merchant_id
#    end

################### place the following abilities in any of the above roles ####
#    can :manage, Offer do |offer|
#      offer.user_id == user.id or user.admin?
#    end
#    can [:new,:create], Offer if user.merchant?
#    can :read, Offer
#    can :show, Merchant
################################################################################

  end
end
