class ThirdPartyMerchantType < ActiveRecord::Base
  has_many :merchants
end
